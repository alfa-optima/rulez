<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::IncludeModule("sale");
use \Bitrix\Sale\Order;
use Bitrix\Sale;

/**
 * Скрипт для перевода статусов на сайте с bitrix24
 */

$statusesLeads = [
    "Новый" => "A",
//    "NEW" => "A",
    "В обработке" => "OB"
//    "1" => "OB"
];
$statusesDeals = [
    "PREPARATION" => "N",
    "1" => "B",
//    "3" => "O",
    "2" => "C",
    "3" => "E",
    "7" => "G",
    "5" => "H",
    "WON" => "J",

    "C2:PREPARATION" => "O",
    "C2:PREPAYMENT_INVOICE" => "N",
    "C2:EXECUTING" => "B",
    "C2:FINAL_INVOICE" => "C",
    "C2:3" => "E",
    "C2:5" => "H",
    "C2:7" => "G",
    "C2:WON" => "J"
];

if(isset($_GET['type'])){
    if(isset($statusesDeals[$_GET['status']])){
        $status = $statusesDeals[$_GET['status']];
    }
} else{
    if(isset($statusesLeads[$_GET['status']])){
        $status = $statusesLeads[$_GET['status']];
    }
}
//file_put_contents("/home/bitrix/www/script/log.log", print_r($status, true));
$data = CSaleOrder::StatusOrder($_GET['id'], $status);
//var_dump($data);
?>