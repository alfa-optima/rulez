<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('iblock');

$arURI = tools\funcs::arURI( tools\funcs::pureURL() );

// SEO-страница
$seo_page = project\SeopageTable::getByCode($arURI[1]);

if( intval($seo_page['ID']) > 0 ){
    $section = project\catalog::getSectionByXMLID( $seo_page['SECTION_XML_ID'] );
}

if(
    intval($seo_page['ID']) > 0
    &&
    intval($section['ID']) > 0
){
    
    $smart_filter_path = '';
    if( substr_count( tools\funcs::pureURL(), '/filter/' ) ){
        if(preg_match("/\/filter\/(.+)\/apply\//", tools\funcs::pureURL(),$matches)){
            $smart_filter_path = $matches[1];
        }
    }

    $APPLICATION->SetPageProperty("description", $seo_page["DESCRIPTION"]?$seo_page["DESCRIPTION"]:$seo_page["NAME"]);
    $APPLICATION->SetPageProperty("keywords", $seo_page["KEYWORDS"]?$seo_page["KEYWORDS"]:$seo_page["NAME"]);
    $APPLICATION->SetPageProperty("title", $seo_page["TITLE"]?$seo_page["TITLE"]:$seo_page["NAME"]);

    $APPLICATION->AddChainItem($section["NAME"], $section["SECTION_PAGE_URL"]);
    $APPLICATION->AddChainItem($seo_page["NAME"], '/'.$seo_page["CODE"].'/'); ?>


    <div class="m-container p-filters-js is___seo_page">

        <div class="m-aside shutter--filters-js stop-remove-class">

            <div class="m-aside__header">
                Фильтр
                <span class="p-filters-activated p-filters-activated-js hide">0</span>
                <a href="#" class="btn-filters-close js-btn-shutter-close" title="Закрыть фильтр">
                    <svg class="svg-ico-close" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 57.2 57.2">
                        <path d="M34.3 28.6L56 6.9c1.6-1.6 1.6-4.1 0-5.7 -1.6-1.6-4.1-1.6-5.7 0L28.6 22.9 6.9 1.3c-1.6-1.6-4.1-1.6-5.7 0 -1.6 1.6-1.6 4.1 0 5.7l21.7 21.6L1.3 50.3c-1.6 1.5-1.6 4.1 0 5.6 0.8 0.8 1.8 1.2 2.8 1.2s2-0.4 2.8-1.2l21.7-21.6L50.3 56c0.8 0.8 1.8 1.2 2.8 1.2s2-0.4 2.8-1.2c1.6-1.6 1.6-4.1 0-5.7L34.3 28.6z"></path>
                    </svg>
                </a>
            </div>

            <div class="m-aside__align">
                <div class="m-aside__holder">

                    <? $arFilter = [];
                    $filter_json = $seo_page['FILTER_JSON'];
                    if( strlen($filter_json) > 0 ){
                        $arFilter = tools\funcs::json_to_array($filter_json);
                    }
                    if( isset($arFilter['PRICE_FROM']) ){
                        $GLOBALS[project\catalog::FILTER_NAME]['>=CATALOG_PRICE_'.project\catalog::BASE_PRICE_ID] = $arFilter['PRICE_FROM'];
                    }
                    if( isset($arFilter['PRICE_TO']) ){
                        $GLOBALS[project\catalog::FILTER_NAME]['<=CATALOG_PRICE_'.project\catalog::BASE_PRICE_ID] = $arFilter['PRICE_TO'];
                    }
                    if( is_array($arFilter['PROP_VALUES']) ){
                        foreach ( $arFilter['PROP_VALUES'] as $prop_code => $values ){
                            $prop = tools\prop::getByCode( $section['IBLOCK_ID'], $prop_code );
                            if( $prop['PROPERTY_TYPE'] == 'L' ){
                                $GLOBALS[project\catalog::FILTER_NAME]['=PROPERTY_'.$prop['ID']] = $values;
                            } else if( $prop['PROPERTY_TYPE'] == 'N' ){
                                if( isset($values['MIN']) ){
                                    $GLOBALS[project\catalog::FILTER_NAME]['>=PROPERTY_'.$prop['ID']] = $values['MIN'];
                                    $GLOBALS[project\catalog::FILTER_NAME]['<=PROPERTY_'.$prop['ID']] = $values['MAX'];
                                }
                            } else {
                                if( is_array($values) ){
                                    $GLOBALS[project\catalog::FILTER_NAME]['PROPERTY_'.$prop['ID']] = $values;
                                } else if( strlen($values) > 0 ){
                                    $GLOBALS[project\catalog::FILTER_NAME]['PROPERTY_'.$prop['ID']][] = $values;
                                }
                            }
                        }
                    }
                    
                    // Фильтр
                    $APPLICATION->IncludeComponent(
                        "bitrix:catalog.smart.filter", "catalog_filter",
                        array(
                            "arFilter" => $arFilter,
                            "SECTION_FILTER_PROPS" => project\catalog::getFilterProps( $section['XML_ID'] ),
                            "IBLOCK_TYPE" => project\catalog::IBLOCK_TYPE,
                            "IBLOCK_ID" => $section['IBLOCK_ID'],
                            "SECTION_ID" => $section['ID'],
                            "FILTER_NAME" => project\catalog::FILTER_NAME,
                            "PRICE_CODE" => [ project\catalog::BASE_PRICE_CODE ],
                            "CACHE_TYPE" => project\catalog::CACHE_TYPE,
                            "CACHE_TIME" => project\catalog::CACHE_TIME,
                            "CACHE_GROUPS" => 'Y',
                            "SAVE_IN_SESSION" => "N",
                            "FILTER_VIEW_MODE" => 'VERTICAL',
                            "XML_EXPORT" => "N",
                            "SECTION_TITLE" => "NAME",
                            "SECTION_DESCRIPTION" => "DESCRIPTION",
                            'HIDE_NOT_AVAILABLE' => 'N',
                            "TEMPLATE_THEME" => "",
                            'CONVERT_CURRENCY' => 'N',
                            'CURRENCY_ID' => '',
                            "SEF_MODE" => 'Y',
                            "SEF_RULE" => '/'.$seo_page['CODE'].'/filter/#SMART_FILTER_PATH#/apply/',
                            "SMART_FILTER_PATH" => $smart_filter_path,
                            "PAGER_PARAMS_NAME" => '',
                            "INSTANT_RELOAD" => '',
                        ),
                        $component, array('HIDE_ICONS' => 'Y')
                    ); ?>


                    <div class="advise">

                        <? // manager
                        $APPLICATION->IncludeComponent(
                            "aoptima:manager", "", ['section_id' => $section['ID']]
                        ); ?>

                        <? // callback_form
                        $APPLICATION->IncludeComponent(
                            "aoptima:callback_form", "left",
                            array()
                        ); ?>


                    </div>

                </div>
            </div>

        </div>


        <div class="m-content">

            <div class="m-heading">

                <h1><?=$seo_page['NAME']?></h1>

                <div class="view-switcher m-heading__switcher view-switcher-products-js" data-toggle-view-for="products">
                    <a href="#" class="active" data-mod="grid-view" title="Продукты плиткой">
                        <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 28 28">
                            <path d="M6 7.5C6 6.67157 6.67157 6 7.5 6C8.32843 6 9 6.67157 9 7.5C9 8.32843 8.32843 9 7.5 9C6.67157 9 6 8.32843 6 7.5Z"></path>
                            <path d="M6 13.5C6 12.6716 6.67157 12 7.5 12C8.32843 12 9 12.6716 9 13.5C9 14.3284 8.32843 15 7.5 15C6.67157 15 6 14.3284 6 13.5Z"></path>
                            <path d="M6 20.5C6 19.6716 6.67157 19 7.5 19C8.32843 19 9 19.6716 9 20.5C9 21.3284 8.32843 22 7.5 22C6.67157 22 6 21.3284 6 20.5Z"></path>
                            <path d="M13 7.5C13 6.67157 13.6716 6 14.5 6C15.3284 6 16 6.67157 16 7.5C16 8.32843 15.3284 9 14.5 9C13.6716 9 13 8.32843 13 7.5Z"></path>
                            <path d="M13 13.5C13 12.6716 13.6716 12 14.5 12C15.3284 12 16 12.6716 16 13.5C16 14.3284 15.3284 15 14.5 15C13.6716 15 13 14.3284 13 13.5Z"></path>
                            <path d="M13 20.5C13 19.6716 13.6716 19 14.5 19C15.3284 19 16 19.6716 16 20.5C16 21.3284 15.3284 22 14.5 22C13.6716 22 13 21.3284 13 20.5Z"></path>
                            <path d="M20 7.5C20 6.67157 20.6716 6 21.5 6C22.3284 6 23 6.67157 23 7.5C23 8.32843 22.3284 9 21.5 9C20.6716 9 20 8.32843 20 7.5Z"></path>
                            <path d="M20 13.5C20 12.6716 20.6716 12 21.5 12C22.3284 12 23 12.6716 23 13.5C23 14.3284 22.3284 15 21.5 15C20.6716 15 20 14.3284 20 13.5Z"></path>
                            <path d="M20 20.5C20 19.6716 20.6716 19 21.5 19C22.3284 19 23 19.6716 23 20.5C23 21.3284 22.3284 22 21.5 22C20.6716 22 20 21.3284 20 20.5Z"></path>
                        </svg>
                        <span>Плиткой</span>
                    </a>
                    <a href="#" data-mod="list-view" title="Продукты списком">
                        <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 28 28">
                            <path d="M3 7C3 6.44772 3.44772 6 4 6H24C24.5523 6 25 6.44772 25 7C25 7.55228 24.5523 8 24 8H4C3.44772 8 3 7.55228 3 7Z"></path>
                            <path d="M3 14C3 13.4477 3.44772 13 4 13H24C24.5523 13 25 13.4477 25 14C25 14.5523 24.5523 15 24 15H4C3.44772 15 3 14.5523 3 14Z"></path>
                            <path d="M3 21C3 20.4477 3.44772 20 4 20H24C24.5523 20 25 20.4477 25 21C25 21.5523 24.5523 22 24 22H4C3.44772 22 3 21.5523 3 21Z"></path>
                        </svg>
                        <span>Списком</span>
                    </a>
                </div>

            </div>

            <div class="m-tags">
                <div class="p-filters-tags p-filters-tags-js">
                    <!--filters tags-->
                </div>
            </div>

            <div class="m-options">

                <a href="#" class="filters-switcher btn-filters-js" title="Показать фильтры">
                    <div class="p-filters-activated p-filters-activated-js hide">0</div>
                    <span>Фильтр</span>
                    <em><svg class="svg-ico-filters" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 79.2 89.6">
                        <path d="M3.8 18.7h6.6C12 25.1 18 30 24.9 30c6.9 0 12.8-4.8 14.5-11.3h36.1c2.1 0 3.7-1.6 3.7-3.7 0-2.1-1.6-3.7-3.7-3.7H39.3C37.7 4.9 31.7 0 24.8 0 17.9 0 12 4.7 10.4 11.3H3.8c-2.1 0-3.7 1.6-3.7 3.7C0.1 17.1 1.8 18.7 3.8 18.7zM24.8 6.1c4.9 0 8.9 4 8.9 8.9 0 4.9-4 8.9-8.9 8.9 -4.9 0-8.9-4-8.9-8.9C15.9 10.1 19.9 6.1 24.8 6.1zM75.4 41.1h-6.3c-1.6-6.4-7.6-11.3-14.5-11.3s-12.8 4.8-14.5 11.3H3.8c-2.1 0-3.7 1.6-3.7 3.7s1.6 3.7 3.7 3.7h36.4c1.6 6.4 7.6 11.3 14.4 11.3S67.4 55 69.1 48.5h6.3c2.1 0 3.7-1.6 3.7-3.7S77.4 41.1 75.4 41.1zM54.6 53.7c-4.9 0-8.9-4-8.9-8.9s4-8.9 8.9-8.9 8.9 4 8.9 8.9S59.6 53.7 54.6 53.7zM75.4 70.9H39.3c-1.6-6.4-7.6-11.3-14.5-11.3 -6.9 0-12.8 4.8-14.5 11.3H3.7c-2.1 0-3.7 1.6-3.7 3.7s1.6 3.7 3.7 3.7h6.6c1.6 6.4 7.6 11.3 14.5 11.3 6.9 0 12.8-4.8 14.5-11.3h36.1c2.1 0 3.7-1.6 3.7-3.7S77.4 70.9 75.4 70.9zM24.8 83.5c-4.9 0-8.9-4-8.9-8.9s4-8.9 8.9-8.9c4.9 0 8.9 4 8.9 8.9S29.8 83.5 24.8 83.5z"></path>
                    </svg></em>
                </a>
                <!--.cs - control-sort // Sorting products-->

                <? // catalogSortBlock
                $APPLICATION->IncludeComponent(
                    "aoptima:catalogSortBlock", ""
                ); ?>

                <!--<label class="check-label">
                    <input type="checkbox" name="stock">
                    <span>Товар в наличии</span>
                </label>-->

            </div>

            <? $sortField = project\catalog::getSortField();
            $sortOrder = project\catalog::getSortOrder();

            $GLOBALS[project\catalog::FILTER_NAME]['SECTION_ID'] = $section['ID'];
            $GLOBALS[project\catalog::FILTER_NAME]['INCLUDE_SUBSECTIONS'] = 'Y';
            $GLOBALS[project\catalog::FILTER_NAME]['PROPERTY_STATUS_TOVARA'] = '_%';

            
            $APPLICATION->IncludeComponent(
                "bitrix:catalog.section", "catalog_goods",
                Array(
                    "IBLOCK_TYPE" => project\catalog::IBLOCK_TYPE,
                    "IBLOCK_ID" => $section['IBLOCK_ID'],
                    "SECTION_USER_FIELDS" => array(),
                    "ELEMENT_SORT_FIELD" => 'PROPERTY_STATUS_TOVARA',
                    "ELEMENT_SORT_ORDER" => 'ASC',
                    "ELEMENT_SORT_FIELD2" => project\catalog::getCatSortFieldCode($sortField),
                    "ELEMENT_SORT_ORDER2" => $sortOrder,
                    "FILTER_NAME" => project\catalog::FILTER_NAME,
                    "HIDE_NOT_AVAILABLE" => "N",
                    "PAGE_ELEMENT_COUNT" => project\catalog::GOODS_CNT + 1,
                    "LINE_ELEMENT_COUNT" => "3",
                    "PROPERTY_CODE" => ['STATUS_TOVARA'],
                    "OFFERS_LIMIT" => 0,
                    "TEMPLATE_THEME" => "",
                    "PRODUCT_SUBSCRIPTION" => "N",
                    "SHOW_DISCOUNT_PERCENT" => "N",
                    "SHOW_OLD_PRICE" => "N",
                    "MESS_BTN_BUY" => "Купить",
                    "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                    "MESS_BTN_SUBSCRIBE" => "Подписаться",
                    "MESS_BTN_DETAIL" => "Подробнее",
                    "MESS_NOT_AVAILABLE" => "Нет в наличии",
                    "SECTION_URL" => "",
                    "DETAIL_URL" => "",
                    "SECTION_ID_VARIABLE" => "SECTION_ID",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "CACHE_TYPE" => project\catalog::CACHE_TYPE,
                    "CACHE_TIME" => project\catalog::CACHE_TIME,
                    "CACHE_GROUPS" => "Y",
                    "SET_META_KEYWORDS" => "N",
                    "META_KEYWORDS" => "",
                    "SET_META_DESCRIPTION" => "N",
                    "META_DESCRIPTION" => "",
                    "BROWSER_TITLE" => "-",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "DISPLAY_COMPARE" => "N",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "CACHE_FILTER" => "Y",
                    "PRICE_CODE" => array(
                        project\catalog::RASSROCHKA_PRICE_CODE,
                        project\catalog::BASE_PRICE_CODE
                    ),
                    "USE_PRICE_COUNT" => "N",
                    "SHOW_PRICE_COUNT" => "1",
                    "PRICE_VAT_INCLUDE" => "Y",
                    "CONVERT_CURRENCY" => "N",
                    "BASKET_URL" => "/personal/basket.php",
                    "ACTION_VARIABLE" => "action",
                    "PRODUCT_ID_VARIABLE" => "id",
                    "USE_PRODUCT_QUANTITY" => "N",
                    "ADD_PROPERTIES_TO_BASKET" => "Y",
                    "PRODUCT_PROPS_VARIABLE" => "prop",
                    "PARTIAL_PRODUCT_PROPERTIES" => "N",
                    "PRODUCT_PROPERTIES" => "",
                    "PAGER_TEMPLATE" => "",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Товары",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "Y",
                    "ADD_PICT_PROP" => "-",
                    "LABEL_PROP" => "-",
                    'SHOW_ALL_WO_SECTION' => "Y"
                )
            ); ?>

            <? // Описание
            if( strlen($seo_page['TEXT']) > 0){
                if( !substr_count($seo_page['TEXT'], '<p') ){
                    $seo_page['TEXT'] = str_replace("\n", "<br>", $seo_page['TEXT']);
                } ?>
                <div class="m-description"><?=$seo_page['TEXT']?></div>
            <? } ?>

        </div>

        <div class="loader-cover">&nbsp;</div>
        <div class="filters-shutter-overlay">&nbsp;</div>

    </div>

    <input type="hidden" name="section_id" value="<?=$section['ID']?>">
    <input type="hidden" name="seo_page_url" value="/<?=$seo_page['CODE']?>/">



<? } else {

    include($_SERVER["DOCUMENT_ROOT"]."/local/templates/main/include/404include.php");
}



require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>