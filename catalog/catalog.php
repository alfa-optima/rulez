<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

Bitrix\Main\Loader::includeModule('iblock');

$fullURL = $_SERVER['REQUEST_URI'];
$pureURL = tools\funcs::pureURL();
$arURI = tools\funcs::arURI( $pureURL );

$smart_filter_path = '';
if( substr_count( $pureURL, '/filter/' ) ){
    if( preg_match( "/\/filter\/(.+)\/apply\//", $pureURL,$matches ) ){
        $smart_filter_path = $matches[1];
    }
    $filterKey = array_search( 'filter', $arURI );
    foreach ( $arURI as $key => $urlItem ){
        if( $key >= $filterKey ){   unset($arURI[$key]);   }
    }
    $arURI[] = false;
}
$sectCode = $arURI[ count($arURI) - 2 ];
if( strlen($smart_filter_path) > 0 ){
    $pureCatalogURL = str_replace( [ '/filter/'.$smart_filter_path.'/apply/' ], "/", $pureURL );
} else {
    $pureCatalogURL = $pureURL;
}


// Каталожные инфоблоки
$catalogIblocks = project\catalog::iblocks();

$section = false;   $iblock = false;
foreach ( $catalogIblocks as $iblock_id => $ib ){
    if( !$section ){
        $section = tools\section::info_by_code($sectCode, $iblock_id);
        if( intval($section['ID']) > 0 ){   $iblock = $ib;   }
    }
}

if(
    intval($section['ID']) > 0
    &&
    $section['SECTION_PAGE_URL'] == $pureCatalogURL
//    &&
//    $section['IBLOCK_ID'] != project\complect::IBLOCK_ID
    &&
    !substr_count($pureURL, '/filter/clear/apply/')
){

    $ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues( $section["IBLOCK_ID"], $section["ID"] );
    $meta_tags = $ipropValues->getValues();

    $APPLICATION->SetPageProperty("title", $meta_tags["SECTION_META_TITLE"]?$meta_tags["SECTION_META_TITLE"]:$el["NAME"]);
    $APPLICATION->SetPageProperty("description", $meta_tags["SECTION_META_DESCRIPTION"]?$meta_tags["SECTION_META_DESCRIPTION"]:$el["NAME"]);
    $APPLICATION->SetPageProperty("keywords", $meta_tags["SECTION_META_KEYWORDS"]?$meta_tags["SECTION_META_KEYWORDS"]:$el["NAME"]);

    $sect_chain = tools\section::chain($section['ID']);
    foreach ( $sect_chain as $key => $sect ){
        $APPLICATION->AddChainItem($sect["NAME"], $sect["SECTION_PAGE_URL"]);
    }

    // Подразделы раздела
    $sub_sects = tools\section::sub_sects( $section['ID'] );

    // Если есть подразделы
    if( count($sub_sects) > 0 ){



        // Подразделы
        $APPLICATION->IncludeComponent(
            "aoptima:catalogSubsections", "",
            array( 'section' => $section, 'sub_sects' => $sub_sects )
        );



    } else {



        // Товары раздела
        $template = $section['IBLOCK_ID']==project\complect::IBLOCK_ID?'complects':'';
        $APPLICATION->IncludeComponent(
            "aoptima:catalogProductsList", $template,
            array(
                'section' => $section,
                'iblock' => $iblock,
                'component' => $component,
                'smart_filter_path' => $smart_filter_path,
            )
        );



    }



    $APPLICATION->IncludeComponent(
        "aoptima:section_seo_pages_block", "",
        array('section' => $section)
    );



    $APPLICATION->IncludeComponent(
        "bitrix:news.list", "catalog_services",
        array(
            "COMPONENT_TEMPLATE" => "catalog_services",
            "IBLOCK_TYPE" => "content",
            "IBLOCK_ID" => "22",
            "NEWS_COUNT" => "20",
            "SORT_BY1" => "SORT",
            "SORT_ORDER1" => "ASC",
            "SORT_BY2" => "NAME",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "",
            "FIELD_CODE" => array(),
            "PROPERTY_CODE" => array('CLASS'),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "Y",
            "PREVIEW_TRUNCATE_LEN" => "",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "SET_TITLE" => "N",
            "SET_BROWSER_TITLE" => "N",
            "SET_META_KEYWORDS" => "N",
            "SET_META_DESCRIPTION" => "N",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "INCLUDE_SUBSECTIONS" => "Y",
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "PAGER_TEMPLATE" => ".default",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "PAGER_TITLE" => "Новости",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "AJAX_OPTION_ADDITIONAL" => "undefined",
            "SET_LAST_MODIFIED" => "N",
            "PAGER_BASE_LINK_ENABLE" => "N",
            "SHOW_404" => "N",
            "MESSAGE_404" => ""
        ),
        false
    ); ?>


    <input type="hidden" name="section_id" value="<?=$section['ID']?>">



<? } else {

    include($_SERVER["DOCUMENT_ROOT"]."/local/templates/main/include/404include.php");
}



require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>