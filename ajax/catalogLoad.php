<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $section_id = strip_tags($_POST['section_id']);
    $section = tools\section::info($section_id);
    
    if( is_array($_POST['stop_ids']) && count($_POST['stop_ids']) > 0 ){
        $GLOBALS[project\catalog::FILTER_NAME]['!ID'] = $stop_ids;
    }

    if( $_POST['isComplects'] != 'Y' ){

        $smart_filter_path = '';
        $url = strip_tags(trim(rawurldecode($_POST['url'])));
        if( substr_count( $url, '/filter/' ) ){
            if(preg_match("/\/filter\/(.+)\/apply\//", $url,$matches)){
                $smart_filter_path = $matches[1];
            }
        }
        ob_start();
        $APPLICATION->IncludeComponent(
            "bitrix:catalog.smart.filter", "json",
            array(
                "IBLOCK_TYPE" => project\catalog::IBLOCK_TYPE,
                "IBLOCK_ID" => $section['IBLOCK_ID'],
                "SECTION_ID" => $section['ID'],
                "FILTER_NAME" => project\catalog::FILTER_NAME,
                "PRICE_CODE" => [ project\catalog::BASE_PRICE_CODE ],
                "CACHE_TYPE" => project\catalog::CACHE_TYPE,
                "CACHE_TIME" => project\catalog::CACHE_TIME,
                "CACHE_GROUPS" => 'Y',
                "SAVE_IN_SESSION" => "N",
                "FILTER_VIEW_MODE" => 'VERTICAL',
                "XML_EXPORT" => "N",
                "SECTION_TITLE" => "NAME",
                "SECTION_DESCRIPTION" => "DESCRIPTION",
                'HIDE_NOT_AVAILABLE' => 'N',
                "TEMPLATE_THEME" => "",
                'CONVERT_CURRENCY' => 'N',
                'CURRENCY_ID' => '',
                "SEF_MODE" => 'Y',
                "SEF_RULE" => $section['SECTION_PAGE_URL'].'filter/#SMART_FILTER_PATH#/apply/',
                "SMART_FILTER_PATH" => $smart_filter_path,
                "PAGER_PARAMS_NAME" => '',
                "INSTANT_RELOAD" => '',
            ),
            $component, array('HIDE_ICONS' => 'Y')
        );
        $filterJSON = ob_get_contents();
        ob_end_clean();

    }

    $sortField = project\catalog::getSortField();
    $sortOrder = project\catalog::getSortOrder();
    $GLOBALS[project\catalog::FILTER_NAME]['SECTION_ID'] = $section['ID'];
    $GLOBALS[project\catalog::FILTER_NAME]['INCLUDE_SUBSECTIONS'] = 'Y';
    $GLOBALS[project\catalog::FILTER_NAME]['PROPERTY_STATUS_TOVARA'] = '_%';

    ob_start();

        $template = $_POST['isComplects']=='Y'?"catalog_complects":"catalog_goods";
        $list_cnt = $_POST['isComplects']=='Y'?project\catalog::COMPLECTS_CNT:project\catalog::GOODS_CNT;
        $cacheTime = $_POST['isComplects']=='Y'?project\catalog::COMPLECTS_CACHE_TIME:project\catalog::CACHE_TIME;
        $cacheType = $_POST['isComplects']=='Y'?project\catalog::COMPLECTS_CACHE_TYPE:project\catalog::CACHE_TYPE;

        if( intval($_POST['pageNumber']) > 1 ){
            $pageNumber = intval($_POST['pageNumber']);
            ${"PAGEN_1"} = $pageNumber;
        }

        $APPLICATION->IncludeComponent(
            "bitrix:catalog.section", $template,
            Array(
                "CUR_URL" => $url,
                "PAGE_NUMBER" => $pageNumber,
                "IS_ADMIN" => $USER->IsAdmin()?"Y":"N",
                "IS_LOAD" => is_array($_POST['stop_ids'])?"Y":"N",
                "IS_RELOAD" => $_POST['is_reload']=='Y'?"Y":"N",
                "IS_AJAX" => "Y",
                "IBLOCK_TYPE" => project\catalog::IBLOCK_TYPE,
                "IBLOCK_ID" => $section['IBLOCK_ID'],
                "SECTION_USER_FIELDS" => array(),
                "ELEMENT_SORT_FIELD" =>
                'PROPERTY_STATUS_TOVARA',
                "ELEMENT_SORT_ORDER" => 'ASC',
                "ELEMENT_SORT_FIELD2" => project\catalog::getCatSortFieldCode($sortField),
                "ELEMENT_SORT_ORDER2" => strtoupper($sortOrder),
                "FILTER_NAME" => project\catalog::FILTER_NAME,
                "HIDE_NOT_AVAILABLE" => "N",
                "PAGE_ELEMENT_COUNT" => $list_cnt,
                "LINE_ELEMENT_COUNT" => "3",
                "PROPERTY_CODE" => array('STATUS_TOVARA'),
                "OFFERS_LIMIT" => 0,
                "TEMPLATE_THEME" => "",
                "PRODUCT_SUBSCRIPTION" => "N",
                "SHOW_DISCOUNT_PERCENT" => "N",
                "SHOW_OLD_PRICE" => "N",
                "MESS_BTN_BUY" => "Купить",
                "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                "MESS_BTN_SUBSCRIBE" => "Подписаться",
                "MESS_BTN_DETAIL" => "Подробнее",
                "MESS_NOT_AVAILABLE" => "Нет в наличии",
                "SECTION_URL" => "",
                "DETAIL_URL" => "",
                "SECTION_ID_VARIABLE" => "SECTION_ID",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => $cacheType,
                "CACHE_TIME" => $cacheTime,
                "CACHE_GROUPS" => "Y",
                "SET_META_KEYWORDS" => "N",
                "META_KEYWORDS" => "",
                "SET_META_DESCRIPTION" => "N",
                "META_DESCRIPTION" => "",
                "BROWSER_TITLE" => "-",
                "ADD_SECTIONS_CHAIN" => "N",
                "DISPLAY_COMPARE" => "N",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "CACHE_FILTER" => "Y",
                "PRICE_CODE" => array(
                    project\catalog::RASSROCHKA_PRICE_CODE,
                    project\catalog::BASE_PRICE_CODE
                ),
                "USE_PRICE_COUNT" => "N",
                "SHOW_PRICE_COUNT" => "1",
                "PRICE_VAT_INCLUDE" => "Y",
                "CONVERT_CURRENCY" => "N",
                "BASKET_URL" => "/personal/basket.php",
                "ACTION_VARIABLE" => "action",
                "PRODUCT_ID_VARIABLE" => "id",
                "USE_PRODUCT_QUANTITY" => "N",
                "ADD_PROPERTIES_TO_BASKET" => "Y",
                "PRODUCT_PROPS_VARIABLE" => "prop",
                "PARTIAL_PRODUCT_PROPERTIES" => "N",
                "PRODUCT_PROPERTIES" => "",
                "PAGER_TEMPLATE" => "",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Товары",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "Y",
                "ADD_PICT_PROP" => "-",
                "LABEL_PROP" => "-",
                'INCLUDE_SUBSECTIONS' => "Y",
                'SHOW_ALL_WO_SECTION' => "Y"
            )
        );
    	$html = ob_get_contents();
    ob_end_clean();


    ob_start();
        $params['IS_SEARCH'] = $_POST['isSearch'];
        $APPLICATION->IncludeComponent(
            "aoptima:catalogSortBlock", "", $params
        );
    	$sort_html = ob_get_contents();
    ob_end_clean();


	// Ответ
	echo json_encode([
	    "status" => "ok",
        "html" => $html,
        "filterJSON" => $filterJSON,
        "sort_html" => $sort_html,
        "hasNext" => project\catalog::hasNextProducts(
            $GLOBALS[project\catalog::FILTER_NAME],
            $list_cnt
        )
    ]);
	return;

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;