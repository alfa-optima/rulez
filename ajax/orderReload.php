<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\CModule::IncludeModule("sale");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    if(
        $_POST['action'] == 'unset_item'
        &&
        intval($_POST['item_id']) > 0
    ){
        $res = \CSaleBasket::Delete($_POST['item_id']);
    }

    ob_start();
        $APPLICATION->IncludeComponent(
            "aoptima:order", "", ['IS_AJAX' => 'Y']
        );
        $html = ob_get_contents();
    ob_end_clean();

    $arResult = Array(
        "status" => "ok",
        "html" => $html
    );

    // Ответ
    echo json_encode($arResult);
    return;
}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;