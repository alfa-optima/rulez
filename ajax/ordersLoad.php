<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    ob_start();
        // personal_orders
        $APPLICATION->IncludeComponent(
            "aoptima:personal_orders", "",
            array( 'IS_AJAX' => 'Y' )
        );
        $html = ob_get_contents();
    ob_end_clean();


    // Ответ
    echo json_encode(Array("status" => "ok", "html" => $html));
    return;

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;