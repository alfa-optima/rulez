<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main,
    Bitrix\Main\Localization\Loc as Loc,
    Bitrix\Main\Loader,
    Bitrix\Main\Config\Option,
    Bitrix\Sale\Delivery,
    Bitrix\Sale\PaySystem,
    Bitrix\Sale,
    Bitrix\Sale\Order,
    Bitrix\Sale\DiscountCouponsManager,
    Bitrix\Main\Context;
CModule::IncludeModule("catalog");   CModule::IncludeModule("sale");

if( intval($_POST['order_id']) > 0 ){
    $order_id = intval($_POST['order_id']);
} else if( intval($_GET['order_id']) > 0 ){
    $order_id = intval($_GET['order_id']);
}

if( intval($order_id) > 0 ){

    $order = Sale\Order::load($order_id);

    $paymentIds = $order->getPaymentSystemId();

    $_REQUEST["ORDER_ID"] = $order_id;
    $_REQUEST["PAYMENT_ID"] = $paymentIds[0];

    $APPLICATION->IncludeComponent( "aoptima:sale.order.payment", "", Array(), false );

}




