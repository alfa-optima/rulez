<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');  use AOptima\Tools as tools;
\Bitrix\Main\Loader::includeModule('aoptima.project');  use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('sale');
use Bitrix\Sale;


$results = array();


if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $term = strip_tags($_GET['term']);

    $ob = new project\delivery_price_courier();
    $res = $ob->searchCities( $term );
    if( is_array($res['data']) ){
        foreach ( $res['data'] as $key => $item ){
            $results[$item['id']] = array(
                'label' => $item['name'].' ('.$item['regionName'].' обл., '.$item['districtName'].' р-н)',
                "value" => $item['name'].' ('.$item['regionName'].' обл., '.$item['districtName'].' р-н) ['.$item['id'].']'
            );
        }
    }



}



echo json_encode($results);