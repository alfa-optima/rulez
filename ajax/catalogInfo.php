<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $ratings = [];

    $ids = $_POST['ids'];
    if( is_array($ids) && count($ids) > 0 ){
        $ids = array_unique($ids);
        foreach ( $ids as $id ){
            $rating = (new project\product_rating($id))->get();
            $ratings[$id] = $rating;
        }
    }

	// Ответ
	echo json_encode([
	    "status" => "ok",
	    "ratings" => $ratings,
    ]);
	return;

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;