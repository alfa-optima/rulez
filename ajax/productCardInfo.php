<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('iblock');

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $product_id = strip_tags($_POST['product_id']);

    if( intval($product_id) > 0 ){

        $el = tools\el::info($product_id);

        // каталожные инфоблоки
        $catalogIblocks = project\catalog::iblocks();

        ob_start();
            $APPLICATION->IncludeComponent(
                "aoptima:product_card_dop_block", "", [ 'el' => $el ]
            );
            $productCardDopBlock = ob_get_contents();
        ob_end_clean();


        $productCardPhotosBlock = project\catalog::product_photo_block($el);

        $recommendProductHTML = '';
        $recommendProducts = [];
        if(
            is_array($el['PROPERTY_'.project\catalog::REC_PRODUCT_PROP_CODE.'_VALUE'])
            &&
            count($el['PROPERTY_'.project\catalog::REC_PRODUCT_PROP_CODE.'_VALUE']) > 0
        ){
            foreach ( $el['PROPERTY_'.project\catalog::REC_PRODUCT_PROP_CODE.'_VALUE'] as $xml_id ){
                $product = false;
                foreach ( $catalogIblocks as $key => $iblock ){
                    if( !$product ){
                        $product = tools\el::info_by_xml_id($xml_id, $iblock['ID']);
                    }
                }
                if( intval($product['ID']) > 0 ){
                    $recommendProducts[] = $product;
                }
            }
        }

        if( count($recommendProducts) > 0 ){
            $recommendProduct = $recommendProducts[0];
            $recommendProductHTML = '<div class="p-card-services__item"><a href="'.$recommendProduct['DETAIL_PAGE_URL'].'"><strong class="text-large"><i class="icon-rulik_purple">&nbsp;</i><span>Рулеззз дня!</span></strong><div class="recommended-prod"><div class="recommended-prod__figure"><img src="'.tools\funcs::rIMGG($recommendProduct['PREVIEW_PICTURE'], 4, 48, 27).'"></div><div class="recommended-prod__title"><span class="recommended-prod__title-inner">'.$recommendProduct['NAME'].'</span></div></div></a></div>';
        }

        $arResult = [
            "status" => "ok",
            "productCardDopBlock" => $productCardDopBlock,
            "productCardPhotosBlock" => $productCardPhotosBlock,
            "recommendProductHTML" => $recommendProductHTML,
        ];

        // Ответ
        echo json_encode($arResult);
        return;

    }

}


// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;