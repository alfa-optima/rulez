<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){
    
    if( $USER->IsAuthorized() ){

        $user_id = $USER->GetID();
        $review_id = $_POST['item_id'];
        $vote = $_POST['vote']=='plus'?1:0;


        // Ищем голосования за этот же отзыв этого же пользователя
        $product_review_vote = new project\product_review_vote();
        $votes = $product_review_vote->getList( $review_id, $user_id );

        // Голос уже есть
        if( count($votes) > 0 ){

            $old_vote = $votes[0];

            // Голос такой же
            if( $old_vote['UF_VOTE'] == $vote ){

                // Удаляем прежний голос
                $product_review_vote->delete($old_vote['ID']);

                // Ответ
                echo json_encode(Array("status" => "ok", "action" => 'minus'));
                return;

            // Голос другой
            } else {

                // Удаляем прежний голос
                $product_review_vote->delete($old_vote['ID']);

                // Создаём новую запись
                $res = $product_review_vote->add( $review_id, $user_id, $vote );
                if( $res ){
                    // Ответ
                    echo json_encode(Array("status" => "ok", "action" => 'plus_minus'));
                    return;
                }

            }


        // Голоса ещё нет
        } else {

            // Создаём новую запись
            $res = $product_review_vote->add( $review_id, $user_id, $vote );
            if( $res ){
                // Ответ
                echo json_encode(Array("status" => "ok", "action" => 'plus'));
                return;
            }


        }

    } else {

        // Ответ
        echo json_encode(Array("status" => "error", "text" => "Для голосования необходимо авторизоваться"));
        return;

    }

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;