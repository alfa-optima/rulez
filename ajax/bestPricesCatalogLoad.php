<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    ob_start();
    	$APPLICATION->IncludeComponent(
    	    "aoptima:bestPricesDetail", "",
            array( 'IS_AJAX' => 'Y' )
        );
    	$html = ob_get_contents();
    ob_end_clean();

    ob_start();
        $APPLICATION->IncludeComponent(
            "aoptima:manager", "", [ 'section_id' => $_POST['section_id'] ]
        );
        $manager_html = ob_get_contents();
    ob_end_clean();

    ob_start();
        $params['IS_BEST_PRICES'] = 'Y';
        $APPLICATION->IncludeComponent(
            "aoptima:catalogSortBlock", "", $params
        );
        $sort_html = ob_get_contents();
    ob_end_clean();


	// Ответ
	echo json_encode(Array(
	    "status" => "ok",
        "html" => $html,
        "manager_html" => $manager_html,
        "sort_html" => $sort_html,
        "hasNext" => project\catalog::hasNextProducts(
            $GLOBALS[project\catalog::FILTER_NAME],
            project\catalog::GOODS_CNT
        )
    ));
	return;

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;