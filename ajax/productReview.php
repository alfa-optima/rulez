<? require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('iblock');


$response = json_encode([
    'status' => 'error',
    'text' => 'Ошибка создания отзыва'
]);

if( count($_POST) > 0 ){

    if( $USER->IsAuthorized() ){

        $product_id = strip_tags($_POST['product_id']);
        $name = strip_tags($_POST['name']);
        $email = strip_tags($_POST['email']);
        $pluses = strip_tags($_POST['pluses']);
        $minuses = strip_tags($_POST['minuses']);
        $comment = strip_tags($_POST['comment']);
        $vote = strip_tags($_POST['vote']);
        $recaptchaResponse = strip_tags($_POST['g-recaptcha-response']);
        $errors = [];

        if( intval($product_id) > 0 ){} else {
            $errors[] = 'Ошибка данных';
        }
        if( strlen($name) == 0 ){
            $errors[] = 'Укажите, пожалуйста, Ваше имя!';
        }
        if( strlen($email) == 0 ){
            $errors[] = 'Укажите, пожалуйста, Ваш Email!';
        } else if ( !filter_var($email, FILTER_VALIDATE_EMAIL) ){
            $errors[] = 'Email содержит ошибки';
        }
        if( strlen($pluses) == 0 ){
            $errors[] = 'Укажите, пожалуйста, достоинства товара!';
        }
        if( strlen($minuses) == 0 ){
            $errors[] = 'Укажите, пожалуйста, недостатки товара!';
        }
        if( strlen($comment) == 0 ){
            $errors[] = 'Введите, пожалуйста, Ваш комментарий!';
        }

        // Проверка файла
        if (
            $_FILES['file']['error'] == 0
            &&
            $_FILES['file']['size'] > 0
        ){
            $file = new project\product_review_file($_FILES);
            $check_status = $file->check();
            if( $check_status != 'ok' ){
                $errors[] = $check_status;
            }
        }

        if( strlen($recaptchaResponse) == 0 ){
            $errors[] = 'Подтвердите, пожалуйста, что вы не робот';
        }

        if ( count($errors) == 0 ){

            $client = new Client(array('timeout' => 5));

            try {

                $response = $client->request('POST', 'https://www.google.com/recaptcha/api/siteverify', array('form_params' => array('secret' => \Bitrix\Main\Config\Option::get('aoptima.project', 'GOOGLE_RECAPTCHA_SECRET_KEY'), 'response' => $recaptchaResponse, 'remoteip' => $_SERVER['REMOTE_ADDR'])));

                if ($response->getStatusCode() == 200){

                    $body = $response->getBody();
                    $json = $body->getContents();
                    $res = tools\funcs::json_to_array($json);

                    if ($res['success'] == true ){

                        $element = new \CIBlockElement;
                        $PROP['PRODUCT'] = $product_id;
                        if( $USER->IsAuthorized() ){
                            $PROP['USER'] = $USER->GetID();
                        }
                        if( $file && file_exists($file->getFilePath()) ){
                            $arFile = \CFile::MakeFileArray($file->getFilePath());
                            $PROP['FILE'] = $arFile;
                        }
                        if( strlen($pluses) > 0 ){    $PROP['PLUSES'] = $pluses;     }
                        if( strlen($minuses) > 0 ){    $PROP['MINUSES'] = $minuses;     }
                        if( strlen($name) > 0 ){    $PROP['NAME'] = $name;     }
                        if( strlen($email) > 0 ){    $PROP['EMAIL'] = $email;     }
                        if( intval($vote) > 0 ){    $PROP['VOTE'] = $vote;     }
                        $PROP['RECCOMEND'] = $_POST['recommend']=='Y';

                        $fields = [
                            "IBLOCK_ID"				=> project\product_review::IBLOCK_ID,
                            "NAME"					=> 'Новый отзыв',
                            "ACTIVE"				=> "N",
                            "PROPERTY_VALUES"       => $PROP,
                            "PREVIEW_TEXT" => $comment,
                        ];

                        $id = $element->add($fields);
                        if( intval($id) > 0 ){

                            $product_vote = new project\product_vote();
                            // Удаляем предыдущие оценки пользователя по данному товару
                            $votes = $product_vote->getList( $product_id, $USER->GetID() );
                            foreach ( $votes as $voteItem ){
                                $product_vote->delete($voteItem['ID']);
                            }
                            // Новый голос за товар
                            $product_vote->add( $product_id, $USER->GetID(), $vote );

                            BXClearCache(true, "/product_rating/".$product_id."/");

                            $el = tools\el::info($product_id);
                            ob_start();
                                $APPLICATION->IncludeComponent(
                                    "aoptima:productReviewFormBlock", "",
                                    array( 'PRODUCT' => $el )
                                );
                                $html = ob_get_contents();
                            ob_end_clean();


                            $response = json_encode([
                                'status' => 'ok',
                                'html' => htmlspecialchars($html)
                            ]);

                        }

                    } else {
                        $response = json_encode([
                            'status' => 'error',
                            'text' => 'Ошибка проверки пользователя!'
                        ]);
                    }

                } else {
                    $response = json_encode([
                        'status' => 'error',
                        'text' => 'Ошибка проверки пользователя!'
                    ]);
                }

            } catch (RequestException $ex) {
                $response = json_encode([
                    'status' => 'error',
                    'text' => 'Ошибка проверки пользователя!'
                ]);
            }

        } else {
            $response = json_encode([
                'status' => 'error',
                'text' => implode('<br>', $errors)
            ]);
        }

    } else {
        $response = json_encode([
            'status' => 'error',
            'text' => 'Для написание отзыва необходимо авторизоваться'
        ]);
    }

}


echo '<script type="text/javascript">
window.parent.productReviewResponse('.$response.');
</script>';
