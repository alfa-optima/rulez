<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('sale');

use Bitrix\Main,
Bitrix\Main\Localization\Loc as Loc,
Bitrix\Main\Loader,
Bitrix\Main\Config\Option,
Bitrix\Main\Application,
Bitrix\Sale\Delivery,
Bitrix\Sale\PaySystem,
Bitrix\Sale,
Bitrix\Sale\Basket,
Bitrix\Sale\Order,
Bitrix\Sale\DiscountCouponsManager,
Bitrix\Main\Context;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;



if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){
	



	// Авторизация
	if ( $_POST['action'] == "auth" ){
        tools\user::auth();
	}




	// Регистрация
	if ($_POST['action'] == "register"){
        global $USER;
        if( !$USER->IsAuthorized() ){
            // данные в массив
            $arFields = Array();
            parse_str($_POST["form_data"], $arFields);
            // проверяем EMAIL на логин
            $filter = Array( "LOGIN_EQUAL" => strip_tags(trim($arFields["EMAIL"])) );
            $rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter);
            while ($user = $rsUsers->GetNext()){
                // Если Email уже есть
                echo json_encode( Array( "status" => "error", "text" => "Данный логин уже зарегистрирован на&nbsp;сайте!" ) );  return;
            }
            // проверяем EMAIL на наличие у других пользователей
            $filter = Array( "EMAIL" => strip_tags($arFields["EMAIL"]) );
            $rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter);
            while ($user = $rsUsers->GetNext()){
                // Если Email уже есть
                echo json_encode( Array( "status" => "error", "text" => "Данный Email уже зарегистрирован на&nbsp;сайте!" ) );  return;
            }
            // Регистрируем пользователя

            $regCode = md5(time().randString(10, array("abcdefghijklmnopqrstuvwxyz")));

            $user = new \CUser;
            $arUserFields = Array(
                "ACTIVE" => "Y",
                "LOGIN" => strip_tags(trim($arFields["EMAIL"])),
                "EMAIL" => strip_tags(trim($arFields["EMAIL"])),
                "PASSWORD" => strip_tags($arFields["PASSWORD"]),
                "CONFIRM_PASSWORD" => strip_tags($arFields["CONFIRM_PASSWORD"]),
                "GROUP_ID" => Array(6),
                //"UF_REG_CODE" => $regCode,
                "UF_CONFIRMED" => true
            );
            $userID = $user->Add($arUserFields);
            if ($userID){
                tools\user::sendRegInfo(
                    $userID, false /*, $regCode*/
                );
                $_SESSION['success_register'] = 'Y';
                $USER->Authorize($userID);
                // Ответ
                echo json_encode([
                    "status" => 'ok',
                    'text' => 'Вы успешно зарегистрированы!',
                    //'text' => 'Вы успешно зарегистрированы!<br><br>На указанный Email выслана <u>ссылка для подтверждения регистрации</u>!'
                ]); return;
            } else {
                // Ответ
                echo json_encode( array("status" => 'error', "text" => $user->LAST_ERROR) ); return;
            }
        } else {
            // Ответ
            echo json_encode( Array( "status" => "auth", "text" => "Вы уже авторизованы" ) ); return;
        }
	}





	// Формирование контрольной строки для восстановления пароля
	if ($_POST['action'] == "password_recovery"){
		// данные в массив
		$arFields = Array();
		parse_str($_POST["form_data"], $arFields);
		$user_id = false;
		// Если какие-то данные определены
		if ( strlen($arFields['email']) > 0 ){
			// проверяем EMAIL на наличие у других пользователей
			$filter = Array(
			    "=EMAIL" => $arFields["email"],
                '!EXTERNAL_AUTH_ID' => 'socservices'
            );
			$users = \CUser::GetList(($by="id"), ($order="desc"), $filter); // выбираем пользователей
			if ($user = $users->GetNext()){
                $user_id = $user['ID'];
				$kod = md5(time().rand(1, 10000));
				// Заносим код в профиль пользователя
				$obUser = new \CUser;
				$res = $obUser->Update($user['ID'], array( 'UF_RECOVERY_CODE' => $kod ));
				if ($res){
					
                    $title = 'Ссылка для восстановления пароля (сайт "'.$_SERVER['SERVER_NAME'].'")';

                    $html = '<p>Ссылка для восстановления пароля (сайта "'.\Bitrix\Main\Config\Option::get('main', 'server_name').'"):</p>';
                    $html .= '<p><a href="'.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].'/password_recovery/?code='.$kod.'">'.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].'/password_recovery/?code='.$kod.'</a></p>';

                    // Отправка на почту
                    tools\feedback::sendMainEvent (
                        $title,
                        $html,
                        $arFields["email"]
                    );

					// Ответ
					echo json_encode( Array( "status" => "ok" ) );  return;
				}
			}
			if ( !$user_id ){
				// Ответ
				echo json_encode( Array( "status" => "error", "text" => "Такой Email на сайте не зарегистрирован" ) );  
				return;
			}
		}
	}



    // Восстановление пароля (сохранение)
    if ($action == "password_recovery_save"){
        // данные в массив
        $arFields = Array();
        parse_str(tools\funcs::param_post("form_data"), $arFields);
        if( strlen($arFields['code']) > 0 ){
            $user_id = false;
            // Ищем пользователя с таким кодом
            $filter = Array( "=UF_RECOVERY_CODE" => strip_tags($arFields['code']) );
            $rsUsers = \CUser::GetList(($by = "id"), ($order = "desc"), $filter, array('FIELDS' => array('ID'), 'SELECT' => array('UF_*')));
            if ( $arUser = $rsUsers->GetNext() ){
                $user_id = $arUser['ID'];
                // Заносим в профиль пользователя
                $fields['UF_RECOVERY_CODE'] = '';
                $fields['PASSWORD'] = strip_tags($arFields['PASSWORD']);
                $fields['CONFIRM_PASSWORD'] = strip_tags($arFields['CONFIRM_PASSWORD']);
                $user = new \CUser;
                $res = $user->Update($user_id, $fields);
                if ($res){
                    $USER->Logout();
                    // Ответ
                    echo json_encode( Array("status" => "ok") );
                } else {
                    // Ответ
                    echo json_encode(array("status" => 'error', "text" => $user->LAST_ERROR));
                }
            } else {
                // Ответ
                echo json_encode(Array("status" => "error", "text" => "Ссылка ошибочная либо устарела"));
                return;
            }
        } else {
            // Ответ
            echo json_encode(Array("status" => "error", "text" => "Ошибочная ссылка"));
            return;
        }
    }



	// ЛК - Редактирование личных данных
    if ($_POST['action'] == "personal_data"){
        if( $USER->IsAuthorized() ){
            $arFields = Array();
            parse_str($_POST["form_data"], $arFields);
            $arUser = tools\user::info($USER->GetID());
            if(
                strlen($arFields['EMAIL']) > 0
                &&
                $arUser['EXTERNAL_AUTH_ID'] != 'socservices'
            ){
                if ( tools\user::getByLogin( $arFields['EMAIL'], $USER->GetID() ) ){
                    // Ответ
                    echo json_encode( Array( "status" => "error", "text" => "Данный Email уже зарегистрирован на сайте другим пользователем" ) );
                    return;
                }
                if ( tools\user::getByEmail( $arFields['EMAIL'], $USER->GetID() ) ){
                    // Ответ
                    echo json_encode( Array( "status" => "error", "text" => "Данный Email уже зарегистрирован на сайте другим пользователем" ) );
                    return;
                }
            }
            // Сохранение
            $user = new \CUser;
            $fields = [
                'NAME' => trim(strip_tags($arFields['NAME'])),
                'LAST_NAME' => trim(strip_tags($arFields['LAST_NAME'])),
                'SECOND_NAME' => trim(strip_tags($arFields['SECOND_NAME'])),
                'PERSONAL_PHONE' => trim(strip_tags($arFields['PERSONAL_PHONE'])),
                'UF_DOP_PHONE' => trim(strip_tags($arFields['UF_DOP_PHONE'])),
                'PERSONAL_BIRTHDATE' => trim(strip_tags($arFields['PERSONAL_BIRTHDATE'])),
            ];
            if( strlen(trim(strip_tags($arFields['EMAIL']))) > 0 ){
                $fields['EMAIL'] = trim(strip_tags($arFields['EMAIL']));
            }
            $res = $user->Update( $USER->GetID(), $fields );
            if( $res ){
                // Ответ
                echo json_encode( Array( "status" => "ok" ) );
                return;
            } else {
                // Ответ
                echo json_encode( Array( "status" => "error", "text" => $user->LAST_ERROR ) );
                return;
            }
        } else {
            // Ответ
            echo json_encode( Array( "status" => "error", "text" => "Ошибка авторизации" ) );
            return;
        }
    }



    // ЛК - Редактирование пароля
    if ($_POST['action'] == "personal_password_edit"){
        if( $USER->IsAuthorized() ){
            $arFields = Array();
            parse_str($_POST["form_data"], $arFields);
            // Сохранение
            $user = new \CUser;
            $fields = [
                'PASSWORD' => strip_tags($arFields['PASSWORD']),
                'CONFIRM_PASSWORD' => strip_tags($arFields['CONFIRM_PASSWORD']),
            ];
            $res = $user->Update( $USER->GetID(), $fields );
            if( $res ){
                // Ответ
                echo json_encode( Array( "status" => "ok" ) );
                return;
            } else {
                // Ответ
                echo json_encode( Array( "status" => "error", "text" => $user->LAST_ERROR ) );
                return;
            }
        } else {
            // Ответ
            echo json_encode( Array( "status" => "error", "text" => "Ошибка авторизации" ) );
            return;
        }
    }




	// Изменение пароля
	if ($_POST['action'] == "personal_password"){
		// данные в массив
		$arFields = Array();
		parse_str(tools\funcs::param_post("form_data"), $arFields);
		// Ищем пользователя с таким кодом
		$filter = Array( "=UF_RECOVERY_CODE" => $arFields['code'] );
		$rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter,
			array(
				'FIELDS' => array( 'ID' ),
				'SELECT' => array('UF_*')
			)
		);
		while ($arUser = $rsUsers->GetNext()){
			$user_id = $arUser['ID'];
		}
		// Если какие-то данные определены
		if ( intval($user_id) > 0 ){
			// Заносим в профиль пользователя
			$fields['UF_RECOVERY_CODE'] = '';
			$fields['PASSWORD'] = $arFields['PASSWORD'];
			$fields['CONFIRM_PASSWORD'] = $arFields['CONFIRM_PASSWORD'];
			$user = new \CUser;
			$res = $user->Update($user_id, $fields);
			if ($res){
				$_SESSION['password_ok'] = 1;
				$USER->Logout();
				// Ответ
				echo json_encode( Array( "status" => "ok", 'html' => $html ) );	
			} else {
				// Ответ
				echo json_encode( array("status" => 'error', "text" => $user->LAST_ERROR) );
			}
		} else {
			// Ответ
			echo json_encode( array("status" => 'error', "text" => 'Ошибочный код') );
		}
	}





	// Добавляем товар в корзину
	if ($_POST['action'] == "add_to_basket"){
		// Инфо о товаре
		$tov_id = tools\funcs::param_post("item_id");
		$quantity = 1;
		$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
		if ($item = $basket->getExistsItem('catalog', $tov_id)){
			$item->setField('QUANTITY', $item->getQuantity() + $quantity);
		} else {
			$item = $basket->createItem('catalog', $tov_id);
			$item->setFields(array(
				'QUANTITY' => $quantity,
				'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
				'LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
				'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
				/* 'PRICE' => 123, // произвольная цена
				'CUSTOM_PRICE' => 'Y', // с его помощью мы как бы говорим системе поле PRICE находится под нашим контролем */
			));
		}
		$basket->save();

		ob_start();
			$APPLICATION->IncludeComponent("aoptima:basket_small", "");
			$top_basket_html = ob_get_contents();
		ob_end_clean();

        $arResult = Array(
            "status" => "ok",
            "top_basket_html" =>  $top_basket_html
        );

		if( $_POST['isInBasket'] == 'Y' ){
            ob_start();
                $APPLICATION->IncludeComponent("aoptima:basket", "");
                $arResult['basket_html'] = ob_get_contents();
            ob_end_clean();
		}

		// Ответ
		echo json_encode($arResult);	
	}







    // Быстрый заказ
	if( $_POST['action'] == "quick_order" ){

        \Bitrix\Main\Loader::includeModule('sale');

        $tov_id = strip_tags($_POST["item_id"]);
        $quantity = strip_tags($_POST["quantity"]);
        $phone = strip_tags($_POST["phone"]);

	    // Инфо по текущей корзине
        $basket_info = project\user_basket::info();
        $cur_basket = [];
        foreach ( $basket_info['basket'] as $key => $basketItem ){
            $cur_basket[$basketItem->getProductId()] = $basketItem->getQuantity();
        }

        // Очистим текущую корзину
        \CSaleBasket::DeleteAll(\CSaleBasket::GetBasketUserID());

        // Добавим товар в корзину
        $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
        $item = $basket->createItem('catalog', $tov_id);
        $item->setFields(array(
            'QUANTITY' => $quantity,
            'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
            'LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
            'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider'
        ));
        $basket->save();

        // ID пользователя
        $userID = project\user::SERVICE_USER_ID;
        if ($USER->IsAuthorized()){    $userID = $USER->GetID();    }

        $rounding = project\catalog::PRICE_ROUND;

        // Инфо по новой корзине
        $basketInfo = project\user_basket::info();
        $basket_cnt = $basketInfo['basket_cnt'];
        $arBasket = $basketInfo['arBasket'];
        $basket_disc_sum = $basketInfo['basket_disc_sum'];
        $discount = $basketInfo['discount'];
        $pay_sum = $basket_disc_sum;

        // ID варианта оплаты
        $ps_id = 2;
        // ID варианта доставки
        $ds_id = 2;

        // СОЗДАЁМ ЗАКАЗ
        $order = Sale\Order::create('s1', $userID);

        // Передаём корзину в заказ
        $basket = Basket::create('s1');
        foreach( $arBasket as $basketItem ){
            $item = $basket->createItem('catalog', $basketItem->getField('PRODUCT_ID'));
            $el = tools\el::info($basketItem->getField('PRODUCT_ID'));
            $obIblocks = \CIBlock::GetByID($el['IBLOCK_ID']);
            if($iblock = $obIblocks->GetNext()){     $iblock_xml_id = $iblock['XML_ID'];     }
            $item->setFields(array(
                'QUANTITY' => $basketItem->getField('QUANTITY'),
                'CURRENCY' => 'BYN',
                'LID' => 's1',
                'PRODUCT_XML_ID' => $el['XML_ID'],
                'CATALOG_XML_ID' => $iblock_xml_id,
                'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
            ));
        }
        $order->setBasket($basket);

        // Тип плательщика
        $order->setPersonTypeId(1);

        // Оплата
        $paymentCollection = $order->getPaymentCollection();
        $payment = $paymentCollection->createItem(Sale\PaySystem\Manager::getObjectById( $ps_id ));
        $payment->setField('SUM', $pay_sum); // Сумма к оплате
        $payment->setField('CURRENCY', 'BYN');

        // Доставка
        $shipmentCollection = $order->getShipmentCollection();
        $shipment = $shipmentCollection->createItem(Sale\Delivery\Services\Manager::getObjectById( $ds_id ));
        $shipmentItemCollection = $shipment->getShipmentItemCollection();
        foreach ($basket as $basketItem){
            $item = $shipmentItemCollection->createItem($basketItem);
            $item->setQuantity($basketItem->getQuantity());
        }

        // Заполняем свойства заказа
        $propertyCollection = $order->getPropertyCollection();
        $prop = $propertyCollection->getItemByOrderPropertyId(11);  $prop->setValue('заказ в 1 клик');
        $prop = $propertyCollection->getItemByOrderPropertyId(1);  $prop->setValue($phone);

        // Сохраняем новый заказ
        $order->doFinalAction(true);
        $result = $order->save();

        // Определяем ID нового заказа
        $order_id = $order->GetId();

        if (intval($order_id) > 0){

            // Очищаем корзину
            \CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());

            $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
            foreach ( $cur_basket as $id => $q ){
                $item = $basket->createItem('catalog', $id);
                $item->setFields(array(
                    'QUANTITY' => $q,
                    'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
                    'LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
                    'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider'
                ));
                $basket->save();
            }

            $mail_html = '<p>Новый заказ в 1 клик на сайте '.\Bitrix\Main\Config\Option::get('main', 'server_name').'</p>';
            $mail_html .= '<p>Телефон: '.$phone.';</p>';

            $mail_html .= '<table width="100%" border="1" cellpadding="4" cellspacing="4" style="border-collapse:collapse; font-size: 13px;">';
            $mail_html .= '<tr>
                <td><b>Товар</b></td>
                <td><b>Цена за ед., руб.</b></td>
                <td><b>Количество</b></td>
                <td><b>Сумма, руб.</b></td>
            </tr>';
            foreach ($arBasket as $basketItem){
                // Инфо о товаре
                $product = $basketItem->product;
                $price = $basketItem->price;
                $discPrice = $basketItem->discPrice;
                $quantity = $basketItem->getField('QUANTITY');
                $mail_html .= '<tr>
                    <td>'.$product['NAME'].'</td>
                    <td>'.number_format($discPrice, $rounding, ",", " ").'</td>
                    <td>'.round($quantity, 0).'</td>
                    <td>'.number_format($discPrice * $quantity, $rounding, ",", " ").'</td>
                </tr>';
            }
            $mail_html .= '</table>';

            $disc_sum = number_format($basket_disc_sum, $rounding, ",", " ");
            $delivery_price = number_format(0, $rounding, ",", " ");
            $pay_sum = number_format($pay_sum, $rounding, ",", " ");

            $mail_title = 'Новый заказ в 1 клик (сайт '.\Bitrix\Main\Config\Option::get('main', 'server_name').')';
            $email_to = \Bitrix\Main\Config\Option::get('aoptima.project', 'QUICK_ORDER_EMAIL');
            if( !$email_to ){
                $email_to = \Bitrix\Main\Config\Option::get('main', 'email_from');
            }
            tools\feedback::sendMainEvent( $mail_title, $mail_html, $email_to );

            // Ответ
            echo json_encode([
                "status" => "ok",
                "order_id" => $order_id
            ]);
        }
	}
	
	
	



	// *** ОФОРМЛЕНИЕ ЗАКАЗА***
	if ($_POST['action'] == "order"){

        \Bitrix\Main\Loader::includeModule('sale');

        $arFields = Array();
        parse_str($_POST["form_data"], $arFields);

        // ID варианта оплаты
        $ps_id = $arFields['ps_id'];
        $ps = project\ps::getByID( $ps_id );

        // ID варианта доставки
        $ds_xml_id = $arFields['ds_xml_id'];
        $ds = project\ds::getByXMLID($ds_xml_id);

        // ID пользователя
        $userID = project\user::SERVICE_USER_ID;
        if ($USER->IsAuthorized()){    $userID = $USER->GetID();    }

        $rounding = project\catalog::PRICE_ROUND;

        // Инфо по новой корзине
        $_SESSION['ACTIVE_PS_XML_ID'] = $ps['XML_ID'];
        $basketInfo = project\user_basket::info( false,  $ds['ID'], $ps['ID']);
        $basket_cnt = $basketInfo['basket_cnt'];
        $arBasket = $basketInfo['arBasket'];
        $basket = $basketInfo['basket'];
        $basket_disc_sum = $basketInfo['basket_disc_sum'];
        $discount = $basketInfo['discount'];
        //$delivery_price = project\catalog::getDeliveryPrice($ds, $basketInfo);
        $delivery_price = $basketInfo['calcInfo']['DELIVERY_PRICE'];

        $pay_sum = $basket_disc_sum + $delivery_price;

        if( $ps['XML_ID'] == 'rassrochka' ){

            $no_rassrochka_products = [];
            foreach( $arBasket as $basketItem ){
                if(
                    $basketItem->el['CATALOG_PRICE_'.project\catalog::RASSROCHKA_PRICE_ID] > 0
                    &&
                    $basketItem->el['PROPERTY_'.project\catalog::RASSROCHKA_PROP_CODE.'_VALUE'] == 'Да'
                ){} else {
                    $no_rassrochka_products[$basketItem->el['ID']] = [
                        'el' => $basketItem->el,
                        'quantity' => $basketItem->getQuantity(),
                    ];
                }
            }
            if( count( $no_rassrochka_products ) > 0 ){
                if( count( $no_rassrochka_products ) == count($arBasket) ){
                    echo json_encode([
                        "status" => "error",
                        "text" => "В данный момент в&nbsp;корзине нет&nbsp;товаров, доступных для&nbsp;оформления в&nbsp;рассрочку"
                    ]);
                    return;
                } else {
                    if( $arFields['rassrochka_accept'] == 'Y' ){
                        foreach( $arBasket as $key => $basketItem ){
                            foreach ( $no_rassrochka_products as $no_rassrochka_product ){
                                if( $no_rassrochka_product['el']['ID'] == $basketItem->el['ID'] ){
                                    $res = \CSaleBasket::Delete($basket[$key]->getID());
                                }
                            }
                        }
                        // Инфо по новой корзине
                        $_SESSION['ACTIVE_PS_XML_ID'] = $ps['XML_ID'];
                        $basketInfo = project\user_basket::info();
                        $basket_cnt = $basketInfo['basket_cnt'];
                        $arBasket = $basketInfo['arBasket'];
                        $basket_disc_sum = $basketInfo['basket_disc_sum'];
                        $discount = $basketInfo['discount'];
                        //$delivery_price = project\catalog::getDeliveryPrice($ds, $arResult['bInfo']);
                        $delivery_price = $basketInfo['calcInfo']['DELIVERY_PRICE'];
                        $pay_sum = $basket_disc_sum + $delivery_price;

                    } else {
                        echo json_encode([
                            "status" => "no_rassrochka",
                            "products" => $no_rassrochka_products
                        ]);
                        return;
                    }
                }
            }

        } else if( $ps['XML_ID'] == 'credit' ){

            $no_credit_products = [];
            foreach( $arBasket as $basketItem ){
                if(
                    $basketItem->el['CATALOG_PRICE_'.project\catalog::CREDIT_PRICE_ID] > 0
                    &&
                    $basketItem->el['PROPERTY_'.project\catalog::RASSROCHKA_PROP_CODE.'_VALUE'] == 'Да'
                ){} else {
                    $no_credit_products[$basketItem->el['ID']] = [
                        'el' => $basketItem->el,
                        'quantity' => $basketItem->getQuantity(),
                    ];
                }
            }
            if( count( $no_credit_products ) > 0 ){
                if( count( $no_credit_products ) == count($arBasket) ){
                    echo json_encode([
                        "status" => "error",
                        "text" => "В данный момент в&nbsp;корзине нет&nbsp;товаров, доступных для&nbsp;оформления в&nbsp;кредит"
                    ]);
                    return;
                } else {
                    if( $arFields['credit_accept'] == 'Y' ){
                        foreach( $arBasket as $key => $basketItem ){
                            foreach ( $no_credit_products as $no_credit_product ){
                                if( $no_credit_product['el']['ID'] == $basketItem->el['ID'] ){
                                    $res = \CSaleBasket::Delete($basket[$key]->getID());
                                }
                            }
                        }
                        // Инфо по новой корзине
                        $_SESSION['ACTIVE_PS_XML_ID'] = $ps['XML_ID'];
                        $basketInfo = project\user_basket::info();
                        $basket_cnt = $basketInfo['basket_cnt'];
                        $arBasket = $basketInfo['arBasket'];
                        $basket_disc_sum = $basketInfo['basket_disc_sum'];
                        $discount = $basketInfo['discount'];
                        //$delivery_price = project\catalog::getDeliveryPrice($ds, $arResult['bInfo']);
                        $delivery_price = $basketInfo['calcInfo']['DELIVERY_PRICE'];
                        $pay_sum = $basket_disc_sum + $delivery_price;

                    } else {
                        echo json_encode([
                            "status" => "no_credit",
                            "products" => $no_credit_products
                        ]);
                        return;
                    }
                }
            }

        } else if( $ps['XML_ID'] == 'yur' ){

            $no_yur_products = [];
            foreach( $arBasket as $basketItem ){
                if(
                    $basketItem->el['CATALOG_PRICE_'.project\catalog::RASSROCHKA_PRICE_ID] > 0
                    &&
                    $basketItem->el['PROPERTY_'.project\catalog::RASSROCHKA_PROP_CODE.'_VALUE'] == 'Да'
                ){} else {
                    $no_yur_products[$basketItem->el['ID']] = [
                        'el' => $basketItem->el,
                        'quantity' => $basketItem->getQuantity(),
                    ];
                }
            }
            if( count( $no_yur_products ) > 0 ){
                if( count( $no_credit_products ) == count($arBasket) ){
                    echo json_encode([
                        "status" => "error",
                        "text" => "В данный момент в&nbsp;корзине нет&nbsp;товаров, доступных для&nbsp;оформления юр.&nbsp;лицом"
                    ]);
                    return;
                } else {
                    if( $arFields['yur_accept'] == 'Y' ){
                        foreach( $arBasket as $key => $basketItem ){
                            foreach ( $no_yur_products as $no_yur_product ){
                                if( $no_yur_product['el']['ID'] == $basketItem->el['ID'] ){
                                    $res = \CSaleBasket::Delete($basket[$key]->getID());
                                }
                            }
                        }
                        // Инфо по новой корзине
                        $_SESSION['ACTIVE_PS_XML_ID'] = $ps['XML_ID'];
                        $basketInfo = project\user_basket::info();
                        $basket_cnt = $basketInfo['basket_cnt'];
                        $arBasket = $basketInfo['arBasket'];
                        $basket_disc_sum = $basketInfo['basket_disc_sum'];
                        $discount = $basketInfo['discount'];
                        //$delivery_price = project\catalog::getDeliveryPrice($ds, $arResult['bInfo']);
                        $delivery_price = $basketInfo['calcInfo']['DELIVERY_PRICE'];
                        $pay_sum = $basket_disc_sum + $delivery_price;

                    } else {
                        echo json_encode([
                            "status" => "no_yur",
                            "products" => $no_credit_products
                        ]);
                        return;
                    }
                }
            }
        }

        $_SESSION['ACTIVE_PS_XML_ID'] = $ps['XML_ID'];

        // СОЗДАЁМ ЗАКАЗ
        $order = Sale\Order::create('s1', $userID);

        // Передаём корзину в заказ
        $basket = Basket::create('s1');
        foreach( $arBasket as $basketItem ){
            $item = $basket->createItem('catalog', $basketItem->getField('PRODUCT_ID'));
            $el = tools\el::info($basketItem->getField('PRODUCT_ID'));
            $obIblocks = \CIBlock::GetByID($el['IBLOCK_ID']);
            if($iblock = $obIblocks->GetNext()){     $iblock_xml_id = $iblock['XML_ID'];     }
            $item->setFields(array(
                'QUANTITY' => $basketItem->getField('QUANTITY'),
                'CURRENCY' => 'BYN',
                'LID' => 's1',
                'PRODUCT_XML_ID' => $el['XML_ID'],
                'CATALOG_XML_ID' => $iblock_xml_id,
                'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
            ));
        }
        $order->setBasket($basket);

        // Тип плательщика
        $order->setPersonTypeId(1);

        // Оплата
        $paymentCollection = $order->getPaymentCollection();
        $payment = $paymentCollection->createItem(Sale\PaySystem\Manager::getObjectById( $ps_id ));
        $payment->setField('SUM', $pay_sum); // Сумма к оплате
        $payment->setField('CURRENCY', 'BYN');

        // Доставка
        $shipmentCollection = $order->getShipmentCollection();
        $shipment = $shipmentCollection->createItem(Sale\Delivery\Services\Manager::getObjectById( $ds['ID'] ));
        // ПРОИЗВОЛЬНАЯ СТОИМОСТЬ ДОСТАВКИ (ПРИ НЕОБХОДИМОСТИ)
        if ( !is_null($delivery_price) ){
            $shipment->setField('CUSTOM_PRICE_DELIVERY', 'Y');
            $shipment->setField('PRICE_DELIVERY', $delivery_price);
        }
        $shipmentItemCollection = $shipment->getShipmentItemCollection();
        foreach ($basket as $basketItem){
            $item = $shipmentItemCollection->createItem($basketItem);
            $item->setQuantity($basketItem->getQuantity());
        }

        // Заполняем свойства заказа
        $propertyCollection = $order->getPropertyCollection();
        $prop = $propertyCollection->getItemByOrderPropertyId(11);  $prop->setValue('обычный');

        $prop = $propertyCollection->getItemByOrderPropertyId(1);  $prop->setValue($arFields['phone']);
        $prop = $propertyCollection->getItemByOrderPropertyId(2);  $prop->setValue($arFields['name']);
        $prop = $propertyCollection->getItemByOrderPropertyId(3);  $prop->setValue($arFields['email']);

        if(
            $arFields['ds_xml_id'] != project\order::SAMOVYVOZ_DS_XMLID
            &&
            strlen($arFields['np_name']) > 0
        ){
            $prop = $propertyCollection->getItemByOrderPropertyId(9);
            $prop->setValue(strip_tags($arFields['np_name']));
        }
        if(
            $arFields['ds_xml_id'] != project\order::SAMOVYVOZ_DS_XMLID
            &&
            strlen($arFields['np_id']) > 0
        ){
            $prop = $propertyCollection->getItemByOrderPropertyId(10);
            $prop->setValue(strip_tags($arFields['np_id']));
        }
        if(
            $arFields['ds_xml_id'] == project\order::POCHTA_DS_XMLID
            &&
            strlen($arFields['index']) > 0
        ){
            $prop = $propertyCollection->getItemByOrderPropertyId(8);
            $prop->setValue(strip_tags($arFields['index']));
        }
        if(
            $arFields['ds_xml_id'] != project\order::SAMOVYVOZ_DS_XMLID
            &&
            strlen($arFields['address']) > 0
        ){
            $prop = $propertyCollection->getItemByOrderPropertyId(4);
            $prop->setValue(strip_tags($arFields['address']));
        }
        if(
            $arFields['ds_xml_id'] != project\order::SAMOVYVOZ_DS_XMLID
            &&
            strlen($arFields['kv']) > 0
        ){
            $prop = $propertyCollection->getItemByOrderPropertyId(5);
            $prop->setValue(strip_tags($arFields['kv']));
        }
        if(
            $arFields['ds_xml_id'] != project\order::SAMOVYVOZ_DS_XMLID
            &&
            strlen($arFields['floor']) > 0
        ){
            $prop = $propertyCollection->getItemByOrderPropertyId(6);
            $prop->setValue(strip_tags($arFields['floor']));
        }

        if( strlen($arFields['comment']) > 0 ){
            $prop = $propertyCollection->getItemByOrderPropertyId(7);
            $prop->setValue(strip_tags($arFields['comment']));
        }

        // Сохраняем новый заказ
        $order->doFinalAction(true);
        //$order->setField('STATUS_ID', 'N');
        $result = $order->save();

        // Определяем ID нового заказа
        $order_id = $order->GetId();

        if (intval($order_id) > 0){

            $obOrder = Sale\Order::load($order_id);
            $orderNumber = $obOrder->getField('ACCOUNT_NUMBER');
            $orderDate = ConvertDateTime($obOrder->getDateInsert(), "DD.MM.YYYY", "ru");

            unset($_SESSION['ACTIVE_PS_XML_ID']);

            // Очищаем корзину
            \CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());

            $mail_html = '<p>Новый заказ на сайте '.\Bitrix\Main\Config\Option::get('main', 'server_name').'</p>';
            $mail_html .= '<p>ID заказа: '.$order_id.';</p>';
            //$mail_html .= '<p>Номер заказа: '.$orderNumber.';</p>';
            $mail_html .= '<p>Дата заказа: '.$orderDate.';</p>';
            $mail_html .= '<p>Имя: '.strip_tags($arFields['name']).';</p>';
            $mail_html .= '<p>Email: '.strip_tags($arFields['email']).';</p>';
            $mail_html .= '<p>Телефон: '.strip_tags($arFields['phone']).';</p>';

            $mail_html .= '<p>Вариант оплаты: '.$ps['PSA_NAME'].';</p>';
            $mail_html .= '<p>Вариант доставки: '.$ds['NAME'].';</p>';

            if(
                $arFields['ds_xml_id'] != project\order::SAMOVYVOZ_DS_XMLID
                &&
                strlen($arFields['np_name']) > 0
            ){
                $mail_html .= '<p>Населённый пункт: '.strip_tags($arFields['np_name']).';</p>';
            }
            if(
                $arFields['ds_xml_id'] != project\order::SAMOVYVOZ_DS_XMLID
                &&
                strlen($arFields['np_id']) > 0
            ){
                $prop = $propertyCollection->getItemByOrderPropertyId(10);
                $prop->setValue(strip_tags($arFields['np_id']));
            }
            if(
                $arFields['ds_xml_id'] == project\order::POCHTA_DS_XMLID
                &&
                strlen($arFields['index']) > 0
            ){
                $mail_html .= '<p>Почтовый индекс: '.strip_tags($arFields['index']).';</p>';
            }
            if(
                $arFields['ds_xml_id'] != project\order::SAMOVYVOZ_DS_XMLID
                &&
                strlen($arFields['address']) > 0
            ){
                $mail_html .= '<p>Адрес: '.strip_tags($arFields['address']).';</p>';
            }
            if(
                $arFields['ds_xml_id'] != project\order::SAMOVYVOZ_DS_XMLID
                &&
                strlen($arFields['kv']) > 0
            ){
                $mail_html .= '<p>Квартира: '.strip_tags($arFields['kv']).';</p>';
            }
            if(
                $arFields['ds_xml_id'] != project\order::SAMOVYVOZ_DS_XMLID
                &&
                strlen($arFields['floor']) > 0
            ){
                $mail_html .= '<p>Этаж: '.strip_tags($arFields['floor']).';</p>';
            }

            if( strlen($arFields['comment']) > 0 ){
                $mail_html .= '<p>Комментарий к заказу: '.strip_tags($arFields['comment']).';</p>';
            }

            $mail_html .= '<table width="100%" border="1" cellpadding="4" cellspacing="4" style="border-collapse:collapse; font-size: 13px;">';
            $mail_html .= '<tr>
                <td><b>Товар</b></td>
                <td><b>Цена за ед., руб.</b></td>
                <td><b>Количество</b></td>
                <td><b>Сумма, руб.</b></td>
            </tr>';
            foreach ($arBasket as $basketItem){

                // Инфо о товаре
                $product = $basketItem->product;
                $price = $basketItem->price;
                $discPrice = $basketItem->discPrice;
                $quantity = $basketItem->getField('QUANTITY');

                $complect_items_html = '';
                if( $basketItem->el['IBLOCK_ID'] == project\complect::IBLOCK_ID ){
                    $complect_items = project\complect::getProducts($basketItem->el['ID']);
                    if( count($complect_items) > 0 ){
                        $complect_items_html .= '<br><br>Состав комплекта:';
                        foreach ( $complect_items as $complect_item_id ){
                            $el = tools\el::info($complect_item_id);
                            if( intval($el['ID']) > 0 ){
                                $complect_items_html .= '<br>- '.$el['NAME'].';';
                            }
                        }
                    }
                }

                $mail_html .= '<tr>
                    <td>'.$product['NAME'].$complect_items_html.($discPrice==0?' &nbsp; <span style="color:green; font-weight:bold;">подарок!</span>':'').'</td>
                    <td>'.number_format($discPrice, $rounding, ",", " ").'</td>
                    <td>'.round($quantity, 0).'</td>
                    <td>'.number_format($discPrice * $quantity, $rounding, ",", " ").'</td>
                </tr>';
            }
            $mail_html .= '</table>';

            $disc_sum = number_format($basket_disc_sum, $rounding, ",", " ");
            $delivery_price = number_format($delivery_price, $rounding, ",", " ");
            $pay_sum = number_format($pay_sum, $rounding, ",", " ");

            $mail_html .= '<p>Стоимость: '.$disc_sum.' руб.</p>';
            $mail_html .= '<p>Стоимость доставки: '.$delivery_price.' руб.</p>';
            $mail_html .= '<p>Итого к оплате: '.$pay_sum.' руб.</p>';

            $mail_title = 'Новый заказ ID '.$order_id.' (сайт '.\Bitrix\Main\Config\Option::get('main', 'server_name').')';

            // Письмо админу
            $email_to = \Bitrix\Main\Config\Option::get('aoptima.project', 'ORDER_EMAIL');
            if( !$email_to ){
                $email_to = \Bitrix\Main\Config\Option::get('main', 'email_from');
            }
            tools\feedback::sendMainEvent( $mail_title, $mail_html, $email_to );

            // Письмо покупателю
            tools\feedback::sendMainEvent( $mail_title, $mail_html, strip_tags($arFields['email']) );

            if( count( $no_rassrochka_products ) > 0 ){
                $new_basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
                foreach ( $no_rassrochka_products as $no_rassrochka_item ){
                    $item = $new_basket->createItem( 'catalog', $no_rassrochka_item['el']['ID'] );
                    $item->setFields(array(
                        'QUANTITY' => $no_rassrochka_item['quantity'],
                        'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
                        'LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
                        'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
                    ));
                    $new_basket->save();
                }
            }

            // Ответ
            echo json_encode( Array(
                "status" => "ok",
                "order_id" => $order_id
            ) );

        }
	}





	// Подписка на рассылку
	if ($_POST['action'] == "subscribe"){
		$email = strip_tags($_POST['email']);
		if( strlen($email) > 0 ){
		    $subscribe = new project\subscribe();
		    $res = $subscribe->save($email, [1]);
		    if( $res ){
                // Ответ
                echo json_encode( [ "status" => "ok" ] );
		    } else {
                // Ответ
                echo json_encode( [ "status" => "error", 'text' => 'Ошибка сохранения подписки' ] );
		    }
        } else {
            // Ответ
            echo json_encode( [ "status" => "error", 'text' => 'Ошибка данных' ] );
        }
	}




	// Обратная связь
    if ($_POST['action'] == "feedback"){
        /////////////////////////////////
        $iblock_code = 'feedback';
        $iblock_name = 'Форма обратной связи';
        ////////////////////////////////
        $settings = array(
            'name' => 'Имя',
            'phone' => 'Телефон',
            'email' => 'Email',
            'message' => 'Текст сообщения',
        );
        /////////////////////////////////
        $email_to = \Bitrix\Main\Config\Option::get('aoptima.project', 'CALLBACK_EMAIL_TO');
        $mail_title = 'Новое сообщение из формы "Обратная связь" (сайт "'.$_SERVER['SERVER_NAME'].'")';
        $mail_html = '<p>Новое сообщение из формы "Обратная связь" на сайте "'.$_SERVER['SERVER_NAME'].'"</p><hr>';
        /////////////////////////////////
        // Отправка
        $res = tools\feedback::send(
            array(
                'email_to' => $email_to,
                'iblock_code' => $iblock_code,
                'iblock_name' => $iblock_name,
                'settings' => $settings,
                'mail_title' => $mail_title,
                'mail_html' => $mail_html,
            )
        );
        if( $res ){
            echo json_encode( array("status" => 'ok') ); return;
        } else {
            echo json_encode( array("status" => 'error', 'text' => 'Ошибка отправки') ); return;
        }
    }



    // Запросы обратного звонка
    if ($_POST['action'] == "callback"){
        /////////////////////////////////
        $iblock_code = 'callback';
        $iblock_name = 'Запросы обратного звонка';
        ////////////////////////////////
        $settings = array(
            'name' => 'Имя',
            'phone' => 'Телефон'
        );
        /////////////////////////////////
        $email_to = \Bitrix\Main\Config\Option::get('aoptima.project', 'CALLBACK_EMAIL_TO');
        $mail_title = 'Новый запрос обратного звонка (сайт "'.$_SERVER['SERVER_NAME'].'")';
        $mail_html = '<p>Новый запрос обратного звонка на сайте "'.$_SERVER['SERVER_NAME'].'"</p><hr>';
        /////////////////////////////////
        // Отправка
        $res = tools\feedback::send(
            array(
                'email_to' => $email_to,
                'iblock_code' => $iblock_code,
                'iblock_name' => $iblock_name,
                'settings' => $settings,
                'mail_title' => $mail_title,
                'mail_html' => $mail_html,
            )
        );


        $params = explode('&', $_POST['form_data']);
        foreach ($params as $key => $param){
            $tmp = explode('=', $param);
            $params[$tmp[0]] = $tmp[1];
            unset($params[$key]);
        }

        $queryData = [
            'fields' => [
                "TITLE" => "Обратный звонок rulezzz",
                "TYPE_ID" => "SALE",
                "STAGE_ID" => "NEW",
                "NAME" => $params['name'],
                "CATEGORY_ID" => 0,
                "IS_NEW" => "Y",
                "OPENED" => "Y",
                "SOURCE_ID" => 15, //источник сайт Rulezzz
                "PHONE" => [
                    [ "VALUE" => $params['phone'], "VALUE_TYPE" => "WORK"]
                ],
            ],
            'params' => []
        ];

        $curl = curl_init();
        $queryUrl = 'https://ctm.bitrix24.by/rest/70/bloltlt2cppcpei2/crm.lead.add';

        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $queryUrl,
            CURLOPT_POSTFIELDS => http_build_query($queryData),
        ));

        curl_exec($curl);

        if( $res ){
            echo json_encode( array("status" => 'ok') ); return;
        } else {
            echo json_encode( array("status" => 'error', 'text' => 'Ошибка отправки') ); return;
        }
    }



}