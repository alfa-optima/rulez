<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $stop_ids = $_POST['stop_ids'];
    $product_id = strip_tags($_POST['product_id']);
    $el = tools\el::info($product_id);

    ob_start();
        $arResult = $APPLICATION->IncludeComponent(
            "aoptima:productReviews", "",
            array('PRODUCT' => $el, 'IS_LOAD' => 'Y')
        );
        $reviews_html = ob_get_contents();
    ob_end_clean();

    if( count($arResult['SUB_ITEMS']) > 0 ){
        foreach ( $arResult['SUB_ITEMS'] as $key => $sub_review ){
            ob_start();
                // productSubReviewItem
                $APPLICATION->IncludeComponent(
                    "aoptima:productSubReviewItem", "",
                    array('review' => $sub_review)
                );
            	$html = ob_get_contents();
            ob_end_clean();
            $arResult['SUB_ITEMS'][$key]['HTML'] = $html;
        }
    }
    

	// Ответ
	echo json_encode(Array(
	    "status" => "ok",
        'arResult' => $arResult,
        'reviews_html' => $reviews_html
    ));
	return;

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;