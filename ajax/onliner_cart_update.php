<?php

define('B24_ONLINER_ID', 'UF_CRM_1579593318');
define('B24_ONLINER_VIPCOMP_CART_SOURCE_ID', '4');
define('B24_ONLINER_RULEZ_CART_SOURCE_ID', '3');

define('CLIENT_ID_cart_rulez', '6ec320b97b87b6c86cbc');
define('CLIENT_SECRET_cart_rulez', '92fb6bed3a1b2aef451ea9e2b12dea6786334fd7');

define('CLIENT_ID_cart_vipcomp', 'f605c93074b2a372fe76');
define('CLIENT_SECRET_cart_vipcomp', '43961141961cef4da6df5dd25666ceae58156d97');

//b24 key => onliner key
$cancelReasons = [
    '12' => 8,       //Заказ продублирован
    '5' => 3,      //Покупателя не устроил срок доставки
    '4' => 2,      //Покупателя не устроила стоимость товара
    '7' => 5,      //Покупатель отказался от заказа
    '10' => 6,      //Не удалось связаться с покупателем
    'JUNK' => 7,      //Иное
];

if ($_POST['document_id'][1] === 'CCrmDocumentLead') {

    $curl = curl_init();

    $queryData = [
        'filter' => [
            "ID" => substr($_POST['document_id'][2], 5)
        ],
        'select' => [
            'ID', 'STATUS_ID', 'SOURCE_ID', 'UF_CRM_1579593318'
        ]
    ];
    $queryUrl = 'https://ctm.bitrix24.by/rest/70/bloltlt2cppcpei2/crm.lead.list';

    curl_setopt_array($curl, array(
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $queryUrl,
        CURLOPT_POSTFIELDS => http_build_query($queryData),
    ));

    $result = json_decode(curl_exec($curl), 1);
    curl_close($curl);
    $lead = $result['result'][0];
    $shop = '';
    $status = $cancelReasons[$lead['STATUS_ID']];

    if ($lead['SOURCE_ID'] == B24_ONLINER_VIPCOMP_CART_SOURCE_ID) {
        $shop = 'vipcomp';
        $id = CLIENT_ID_cart_vipcomp;
        $secret = CLIENT_SECRET_cart_vipcomp;
    } elseif ($lead['SOURCE_ID'] == B24_ONLINER_RULEZ_CART_SOURCE_ID) {
        $shop = 'rulez';
        $id = CLIENT_ID_cart_rulez;
        $secret = CLIENT_SECRET_cart_rulez;
    }


    if (!empty($lead[B24_ONLINER_ID]) && !empty($shop) && !empty($status)) {

        $process = curl_init("https://b2bapi.onliner.by/oauth/token");
        curl_setopt($process, CURLOPT_HTTPHEADER, array('Accept: application/json'));
        curl_setopt($process, CURLOPT_USERPWD, $id . ":" . $secret);
        curl_setopt($process, CURLOPT_POST, 1);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($process, CURLOPT_POSTFIELDS, array('grant_type' => 'client_credentials'));
        $auth = curl_exec($process);

        $params = http_build_query(['status' => 'shop_canceled', 'reason' => ['id' => $status]]);
        $process = curl_init("https://cart.api.onliner.by/orders/" . $lead[B24_ONLINER_ID] . "?" . $params);
        curl_setopt($process, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . json_decode($auth)->access_token, 'Accept: application/json', 'Content-Type: application/json'));
        curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($process, CURLOPT_CUSTOMREQUEST, 'PATCH');
        $test = curl_exec($process);
        curl_close($process);
    }
}
//elseif ($_POST['document_id'][1] === 'CCrmDocumentDeal'){
//    $curl = curl_init();
//
//    $queryData = [
//        'filter' => [
//            "ID" => substr($_POST['document_id'][2], 5)
//        ],
//        'select' => [
//            'ID', 'STATUS_ID', 'SOURCE_ID', 'UF_CRM_5E28584F526CB'
//        ]
//    ];
//    $queryUrl = 'https://ctm.bitrix24.by/rest/70/bloltlt2cppcpei2/crm.deal.list';
//
//    curl_setopt_array($curl, array(
//        CURLOPT_SSL_VERIFYPEER => 0,
//        CURLOPT_POST => 1,
//        CURLOPT_HEADER => 0,
//        CURLOPT_RETURNTRANSFER => 1,
//        CURLOPT_URL => $queryUrl,
//        CURLOPT_POSTFIELDS => http_build_query($queryData),
//    ));
//    $result = json_decode(curl_exec($curl), 1);
//    $deal = $result['result'][0];
//    curl_close($curl);
//
//    if ($deal['SOURCE_ID'] == B24_ONLINER_VIPCOMP_CART_SOURCE_ID) {
//        $shop = 'vipcomp';
//        $id = CLIENT_ID_cart_vipcomp;
//        $secret = CLIENT_SECRET_cart_vipcomp;
//    } elseif ($deal['SOURCE_ID'] == B24_ONLINER_RULEZ_CART_SOURCE_ID) {
//        $shop = 'rulez';
//        $id = CLIENT_ID_cart_rulez;
//        $secret = CLIENT_SECRET_cart_rulez;
//    }
//
//    if (!empty($deal[B24_ONLINER_ID]) && !empty($shop)) {
//
//        $process = curl_init("https://b2bapi.onliner.by/oauth/token");
//        curl_setopt($process, CURLOPT_HTTPHEADER, array('Accept: application/json'));
//        curl_setopt($process, CURLOPT_USERPWD, $id . ":" . $secret);
//        curl_setopt($process, CURLOPT_POST, 1);
//        curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
//        curl_setopt($process, CURLOPT_POSTFIELDS, array('grant_type' => 'client_credentials'));
//        $auth = curl_exec($process);
//
//        $process = curl_init("https://cart.api.onliner.by/orders/".$deal[B24_ONLINER_ID]."?status=confirmed");
//        curl_setopt($process, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . json_decode($auth)->access_token, 'Accept: application/json', 'Content-Type: application/json'));
//        curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
//        curl_setopt($process, CURLOPT_CUSTOMREQUEST, 'PATCH');
//        curl_exec($process);
//        curl_close($process);
//    }

//    file_put_contents("/home/bitrix/www/ajax/log.log", print_r($deal, true));
//}

