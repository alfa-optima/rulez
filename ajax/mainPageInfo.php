<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    ob_start();
        $APPLICATION->IncludeComponent(
            "aoptima:mainPageBestPrices", "default"
        );
    	$mainPageBestPrices = ob_get_contents();
    ob_end_clean();

    ob_start();
        $APPLICATION->IncludeComponent(
            "aoptima:mainPageGiftIdeas", ""
        );
        $mainPageGiftIdeas = ob_get_contents();
    ob_end_clean();

    // Ответ
    echo json_encode([
        "status" => "ok",
        "mainPageBestPrices" => $mainPageBestPrices,
        "mainPageGiftIdeas" => $mainPageGiftIdeas,
    ]);
    return;

}


// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;