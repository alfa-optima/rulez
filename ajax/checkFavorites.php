<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    $cur_favs_str = strip_tags($_POST['cur_favs']);
    $item_id = strip_tags($_POST['item_id']);
    $item_section_id = tools\el::sections($item_id)[0]['ID'];

    $cur_favs = [];   $new_favs = [];

    if( strlen($cur_favs_str) > 0 ){
        $cur_favs = explode('|', $cur_favs_str);
    }

    if( count($cur_favs) > 0 ){
        foreach ( $cur_favs as $key => $tov_id ){
            $el = tools\el::info($tov_id);
            if( intval($el['ID']) > 0 ){
                $new_favs[] = $el['ID'];
            }
        }
    }

    if( !in_array($item_id, $new_favs) ){
        if( count($new_favs) >= project\favorites::MAX_CNT ){
            // Ответ
            echo json_encode(Array(
                "status" => "error",
                "text" => "Максимальное количество товаров<br>в&nbsp;избранном - ".project\favorites::MAX_CNT.'шт.'
            ));
            return;
        }
        $new_favs[] = $item_id;
        $type = 'plus';
    } else {
        foreach ( $new_favs as $key => $id ){
            if( $id == $item_id ){    unset($new_favs[$key]);    }
        }
        $type = 'minus';
    }

    $first_level_sections = [];
    $first_level_sections_html = '';
    foreach ( $new_favs as $el_id ){
        $iblock_id = tools\el::getIblock($el_id);
        $firstLevelCategory = project\catalog::firstLevelCategory($iblock_id);
        $first_level_sections[$firstLevelCategory['ID']] = $firstLevelCategory;
    }

    if( count($first_level_sections) > 0 ){
        $first_level_sections_html .= '<div class="p-filters-list favorites_left_sections_block">';
            $first_level_sections_html .= '<div class="p-filters-item p-filters-item-js p-filters-is-open">';
                $first_level_sections_html .= '<div class="p-filters-select p-filters-select-js">';
                    $first_level_sections_html .= '<div class="p-filters-title"><span>Разделы</span></div>';
                    $first_level_sections_html .= '<div class="p-filters-angle">';
                        $first_level_sections_html .= '<svg height="6" width="10" fill="none">';
                            $first_level_sections_html .= '<use xlink:href="#icon-arr-filter"></use>';
                            $first_level_sections_html .= '</svg>';
                        $first_level_sections_html .= '</div>';
                    $first_level_sections_html .= '</div>';
                $first_level_sections_html .= '<div class="p-filters-drop p-filters-drop-js">';
                $first_level_sections_html .= '<ul class="categories-menu search___categories">';
                        foreach( $first_level_sections as $sect_1 ){
                        $first_level_sections_html .= '<li '.($item_section_id == $sect_1['ID']?'class="current"':'').'>';
                            if( $item_section_id == $sect_1['ID'] ){
                                $first_level_sections_html .= '<a onclick="favoritesChangeCategory($(this));" style="cursor: pointer" class="search_category_button to___process" section_id="'.$sect_1['ID'].'" iblock_id="'.$sect_1['IBLOCK_ID'].'">';
                                $first_level_sections_html .= '<span>'.$sect_1['NAME'].'</span>';
                                $first_level_sections_html .= '</a>';
                            } else {
                                $first_level_sections_html .= '<a onclick="favoritesChangeCategory($(this));" style="cursor: pointer" class="search_category_button to___process" section_id="'.$sect_1['ID'].'" iblock_id="'.$sect_1['IBLOCK_ID'].'">'.$sect_1['NAME'].'</a>';
                            }
                            $first_level_sections_html .= '</li>';
                        }
                        $first_level_sections_html .= '</ul>';
                    $first_level_sections_html .= '</div>';
                $first_level_sections_html .= '</div>';
        $first_level_sections_html .= '</div>';
    }

	// Ответ
	echo json_encode(Array(
	    "status" => "ok",
        "new_favs" => $new_favs,
        "first_level_sections_html" => $first_level_sections_html,
        "type" => $type,
    ));
	return;

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;