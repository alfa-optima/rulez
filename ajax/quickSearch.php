<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(
    !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    &&
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
){

    ob_start();
        $APPLICATION->IncludeComponent( "aoptima:quickSearchResults_new", "" );
    	$html = ob_get_contents();
    ob_end_clean();

	// Ответ
	echo json_encode([
	    "status" => "ok",
        "session" => strip_tags(trim($_POST['session'])),
        "html" => $html
    ]);
	return;

}

// Ответ
echo json_encode(Array("status" => "error", "text" => "Ошибка запроса"));
return;