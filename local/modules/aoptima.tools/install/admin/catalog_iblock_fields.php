<? use Bitrix\Main,  Bitrix\Main\Loader;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

$CURRENCY_RIGHT = $APPLICATION->GetGroupRight("aoptima.tools");

if ($CURRENCY_RIGHT=="D") $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('iblock');

$APPLICATION->SetTitle('Aoptima.tools - поля для инфоблоков (КАТАЛОГ)');

$module_path = tools\module::getPath('aoptima.tools');

if( $module_path ){

    $file_path = $module_path.'install/module_settings/catalog_iblock_fields.json';

    // Если файла нет
    if( !file_exists($file_path) ){
        tools\el::createCatalogSettingsFile($file_path);
    }

    if( file_exists($file_path) ){

        $json = file_get_contents($file_path);
        $fields = tools\funcs::json_to_array($json);

        if( count($_POST) > 0 ){

            $fields = $_POST;

            // сохраним файл настроек
            $f = fopen($file_path, "w");
            $result = fwrite( $f,  json_encode($fields) );
            fclose($f);

            BXClearCache(true, "/".tools\el::EL_CACHE_NAME."/");
            BXClearCache(true, "/".tools\el::EL_CACHE_NAME."_by_code/");
            BXClearCache(true, "/".tools\section::SECTION_CACHE_NAME."/");
            BXClearCache(true, "/".tools\section::SECTION_CACHE_NAME."_by_code/");

        } ?>


        <style>
            .admin_iblock_fields_table, .inner_table {
                border-collapse: collapse;
                margin: 0 0 15px 0;
                width: 100%;
            }
            .admin_iblock_fields_table th, .admin_iblock_fields_table td{
                padding: 12px 12px;
                border: 1px solid #bdb8b8;
                vertical-align:top;
            }
            .first_td {
                text-align:left;
            }
            .buttons_td {
                text-align:right;
            }
            .admin_iblock_fields_plus {
                margin-top:15px
            }
            .iblock_type_tr td {
                text-align: left;
                font-size: 18px;
                color:#3b9c0b;
                padding:20px;
            }
        </style>


        <form method="post">

            <table class="admin_iblock_fields_table">

                <tr>
                    <th>Поля (элементы)</th>
                    <th>Свойства (элементы)</th>
                    <th>Поля (разделы)</th>
                </tr>

                <tr class="iblock_tr">
                    <td>
                        <div class="inputs_block">
                            <? if(
                                $fields['EL_FIELDS']
                                &&
                                is_array($fields['EL_FIELDS'])
                                &&
                                count($fields['EL_FIELDS']) > 0
                            ){
                                foreach ( $fields['EL_FIELDS'] as $item ){ ?>
                                    <input type="text" name="EL_FIELDS[]" value="<?=$item?>">
                                    <br>
                                <? }
                            } ?>
                            <input  type="text" name="EL_FIELDS[]">
                        </div>
                        <div class="clear___both" style="clear:both"></div>
                        <input class="admin_iblock_fields_plus" item_name="EL_FIELDS[]" type="button" value="+">
                    </td>
                    <td>
                        <div class="inputs_block">
                            <? if(
                                $fields['EL_PROPS']
                                &&
                                is_array($fields['EL_PROPS'])
                                &&
                                count($fields['EL_PROPS']) > 0
                            ){
                                foreach ( $fields['EL_PROPS'] as $item ){ ?>
                                    <input type="text" name="EL_PROPS[]" value="<?=$item?>">
                                    <br>
                                <? }
                            } ?>
                            <input  type="text" name="EL_PROPS[]">
                        </div>
                        <div class="clear___both" style="clear:both"></div>
                        <input class="admin_iblock_fields_plus" item_name="EL_PROPS[]" type="button" value="+">
                    </td>
                    <td>
                        <div class="inputs_block">
                            <? if(
                                $fields['SECT_FIELDS']
                                &&
                                is_array($fields['SECT_FIELDS'])
                                &&
                                count($fields['SECT_FIELDS']) > 0
                            ){
                                foreach ( $fields['SECT_FIELDS'] as $item ){ ?>
                                    <input type="text" name="SECT_FIELDS[]" value="<?=$item?>">
                                    <br>
                                <? }
                            } ?>
                            <input  type="text" name=SECT_FIELDS[]">
                        </div>
                        <div class="clear___both" style="clear:both"></div>
                        <input class="admin_iblock_fields_plus" item_name="SECT_FIELDS[]" type="button" value="+">
                    </td>
                </tr>

                <tr>
                    <td colspan="3" class="buttons_td">
                        <input class="admin_iblock_fields_save adm-btn-save" type="submit" value="Сохранить">
                    </td>
                </tr>

            </table>

        </form>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <script type="text/javascript">
        $(document).ready(function(){

            $(document).on('click', '.admin_iblock_fields_plus', function(){

                var item_name = $(this).attr('item_name');
                $(this).parent('td').find('.inputs_block').append('<br><input type="text" name="'+item_name+'">');

            })

        });
        </script>



    <? }

}





require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");