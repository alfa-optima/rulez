<? namespace AOptima\Tools;
use AOptima\Tools as tools;



class prop {
	

	
	// Код свойства по ID
	static function get_code( $prop_id ){
		\Bitrix\Main\Loader::includeModule('iblock');
		$prop_res = \CIBlockProperty::GetByID( $prop_id );
		if($el_prop = $prop_res->GetNext()){
			$prop_code = $el_prop['CODE'];
			return $prop_code;
		}
		return false;
	}



    static function getByCode( $iblock_id, $prop_code ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $properties = \CIBlockProperty::GetList(
            array("sort"=>"asc", "name"=>"asc"),
            array(
                "IBLOCK_ID" => $iblock_id,
                "CODE" => $prop_code
            )
        );
        while ($prop = $properties->GetNext()){
            return $prop;
        }
        return false;
    }



    static function getList( $iblock_id ){
	    $list = [];
        \Bitrix\Main\Loader::includeModule('iblock');
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 60*60;
        $cache_id = 'iblock_prop_list_'.$iblock_id;
        $cache_path = '/iblock_prop_list/'.$iblock_id.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $properties = \CIBlockProperty::GetList(
                [ "sort"=>"asc", "name"=>"asc" ],
                [ "IBLOCK_ID" => $iblock_id ]
            );
            while ($prop = $properties->GetNext()){
                $list[$prop['ID']] = $prop;
            }
        $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }



	
}