<? namespace AOptima\Tools;
use AOptima\Tools as tools;



class feedback {

    const IBLOCK_TYPE = 'aoptima_feedback';
    const MAIL_EVENT_TYPE = 'MAIN';




    // Создание типа инфоблока для обратной связи
    static function addIblockType(){
        \Bitrix\Main\Loader::includeModule('iblock');
        $types = \CIBlockType::GetList(array(), array('=ID' => static::IBLOCK_TYPE));
        if( $type = $types->GetNext() ){
            return true;
        } else {
            $fields = Array(
                'ID' => static::IBLOCK_TYPE,
                'SECTIONS' => 'N',
                'SORT' => 1000,
                'LANG' => Array(
                    'ru' => Array(
                        'NAME'=>'Обратная связь',
                        'ELEMENT_NAME' => 'Элементы'
                    ),
                    'en' => Array(
                        'NAME'=>'Feedback',
                        'ELEMENT_NAME' => 'Elements'
                    )
                )
            );
            $obBlocktype = new \CIBlockType;
            $res = $obBlocktype->Add($fields);
            if( $res ){
                return true;
            }
        }
        return false;
    }



    static function addIblock( $iblock_code, $iblock_name ){
        if( static::addIblockType() ){
            $iblocks = \CIBlock::GetList(
                Array(),  Array( "CODE" => $iblock_code ), true
            );
            if( $iblock = $iblocks->GetNext() ){
                return $iblock['ID'];
            } else {
                $ib = new \CIBlock;
                $fields = Array(
                    "ACTIVE" => 'Y',
                    "NAME" => $iblock_name,
                    "CODE" => $iblock_code,
                    "IBLOCK_TYPE_ID" => static::IBLOCK_TYPE,
                    "SITE_ID" => Array("s1"),
                    "SORT" => 100,
                    "WORKFLOW" => "N",
                    "GROUP_ID" => Array("2" => "R")
                );
                $ID = $ib->Add($fields);
                if( intval($ID) > 0 ){    return $ID;    }
            }
        }
        return false;
    }



    // Создание типа почтового шаблона
    static function addMailEventType(){
        $arFilter = array(
            "TYPE_ID" => static::MAIL_EVENT_TYPE
        );
        $types = \CEventType::GetList($arFilter);
        if ( $type = $types->GetNext() ){
            return true;
        } else {
            $et = new \CEventType;
            $id = $et->Add(array(
                "LID"           => 'ru',
                "EVENT_NAME"    => 'MAIN',
                "NAME"          => 'Основной шаблон'
            ));
            if( intval($id) > 0 ){
                return true;
            }
        }
        return false;
    }

    // Создание почтового шаблона
    static function addMailEvent(){
        if( static::addMailEventType() ){
            $arFilter = Array(
                "TYPE_ID" => static::MAIL_EVENT_TYPE,
                "SITE_ID" => "s1"
            );
            $items = \CEventMessage::GetList($by="site_id", $order="desc", $arFilter);
            if($item = $items->GetNext()){
                return true;
            } else {
                $arr["ACTIVE"] = "Y";
                $arr["EVENT_NAME"] = static::MAIL_EVENT_TYPE;
                $arr["LID"] = array("s1");
                $arr["EMAIL_FROM"] = "noreply@#SERVER_NAME#";
                $arr["EMAIL_TO"] = "#EMAIL_TO#";
                $arr["SUBJECT"] = "#TITLE#";
                $arr["BODY_TYPE"] = "html";
                $arr["MESSAGE"] = "#HTML#";

                $emess = new \CEventMessage;
                $id = $emess->Add($arr);

                if( intval($id) > 0 ){
                    return true;
                }
            }
        }
        return false;
    }




    // Отправка
    static function send( $params ){
        if(
            strlen($params['iblock_code']) > 0
            &&
            strlen($params['iblock_name'])
        ){
            // Проверка создания инфоблока
            $iblock_id = tools\feedback::addIblock(
                $params['iblock_code'],
                $params['iblock_name']
            );
            if( intval($iblock_id) > 0 ){
                // данные в массив
                $arFields = Array();
                parse_str($_POST["form_data"], $arFields);
                $cms_html = [];
                foreach ( $arFields as $input_name => $value ){
                    if( $params['settings'][$input_name] ){
                        $params['mail_html'] .= html_entity_decode(htmlspecialchars_decode('<p>'.$params['settings'][$input_name].': '.strip_tags($value).';</p>'));
                        $cms_html[] = html_entity_decode(htmlspecialchars_decode($params['settings'][$input_name].':&nbsp;&nbsp;&nbsp;'.strip_tags($value)));
                    }
                }
                $cms_html = implode("\n", $cms_html);
                $el = new \CIBlockElement;
                $fields = Array(
                    "IBLOCK_ID"      => $iblock_id,
                    "NAME"           => 'Новый элемент',
                    "PREVIEW_TEXT"   => $cms_html,
                    "ACTIVE"         => "Y"
                );
                if($ID = $el->Add($fields)){
                    if( strlen($params['email_to']) > 0 ){
                        $email_to = $params['email_to'];
                    } else {
                        $email_to = \Bitrix\Main\Config\Option::get('main', 'email_from');
                    }
                    if( strlen($email_to) > 0 ){
                        // отправка инфы на почту
                        static::sendMainEvent (
                            $params['mail_title'],
                            $params['mail_html'],
                            $email_to
                        );
                    }
                    return true;
                }
            }
        }
        return false;
    }




    // отправка основного почтового шаблона
    static function sendMainEvent ( $title, $html, $email_to ){
        static::addMailEvent();
        $arEventFields = Array(
            "TITLE" => $title,
            "HTML" => $html,
            "EMAIL_FROM" => static::getEmailFrom(),
            "EMAIL_TO" => $email_to,
        );
        \CEvent::Send(static::MAIL_EVENT_TYPE, "s1", $arEventFields);
    }





    static function getEmailFrom (){
        $email_from = \Bitrix\Main\Config\Option::get('aoptima.tools', 'FORMS_EMAIL_FROM');
        if( !$email_from ){
            $email_from = 'noreply@'.\Bitrix\Main\Config\Option::get('main', 'server_name');
        }
        return $email_from;
    }





}