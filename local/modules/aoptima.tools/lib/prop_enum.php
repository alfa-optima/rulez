<? namespace AOptima\Tools;
use AOptima\Tools as tools;



class prop_enum {



    static function list( $iblock_id, $prop_code = false ){
        $list = [];
        $filter = [ 'IBLOCK_ID' => $iblock_id ];
        if( strlen($prop_code) > 0 ){    $filter["CODE"] = $prop_code;    }
        $enums = \CIBlockPropertyEnum::GetList( ["SORT" => "ASC"], $filter );
        while($enum = $enums->GetNext()){    $list[] = $enum;    }
        return $list;
    }



    static function getByID( $enum_id = false ){
        $arEnum = false;
        if( intval($enum_id) > 0 ){
            $filter = [ 'ID' => $enum_id ];
            $enums = \CIBlockPropertyEnum::GetList( ["SORT" => "ASC"], $filter );
            if( $enum = $enums->GetNext() ){    $arEnum = $enum;    }
        }
        return $arEnum;
    }



}