<? namespace AOptima\Tools;
use AOptima\Tools as tools;



class user {
	

	
	const USER_CACHE_NAME = 'user';
	
	
	
	
	// Инфо о пользователе
	static function info($user_id){
		$arUser = false;
		if( intval($user_id) > 0 ){
			// Кешируем 
			$obCache = new \CPHPCache();
			$cache_time = 30*24*60*60;
			$cache_id = static::USER_CACHE_NAME.'_'.$user_id;
			$cache_path = '/'.static::USER_CACHE_NAME.'/'.$user_id.'/';
			if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
				$vars = $obCache->GetVars();   extract($vars);
			} elseif($obCache->StartDataCache()){
				// Инфо о пользователе
				$dbUser = \CUser::GetByID($user_id);   $arUser = $dbUser->GetNext();
				if ( intval($arUser['ID']) > 0 ){    
					$arUser = tools\funcs::clean_array($arUser);
				}
			$obCache->EndDataCache(array('arUser' => $arUser));
			}
		}
		return $arUser;
	}
	

	
	
	
	
	// Авторизация
	static function auth(){
		global $USER;
        $arFields = Array();
        parse_str($_POST["form_data"], $arFields);
        $result = $USER->Login(strip_tags($arFields['login']), strip_tags($arFields['password']), "Y");
        if (!is_array($result)){
            $USER->Authorize($USER->GetID());
            if( !$USER->IsAdmin() ){
                $user = tools\user::info( $USER->GetID() );
                if( !$user['UF_CONFIRMED'] ){
                    $USER->Logout();
                    // Ответ
                    echo json_encode( Array( "status" => "error", "text" => "Профиль ещё не подтверждён" ) ); return;
                }
            }
            // Ответ
            echo json_encode( Array( "status" => "ok" ) ); return;
        } else {
            // Ответ
            echo json_encode( Array( "status" => "error", "text" => "Неверный логин или пароль" ) ); return;
        }
	}
	
	

	
	
	// Регистрационная информация на почту
	static function sendRegInfo($user_id, $password = false, $regCode = false){
		if( intval($user_id) > 0 ){
			$user = static::info($user_id);
			if( intval($user['ID']) > 0 ){
			    //////////////////////
                if( strlen($regCode) > 0 ){
                    $title = 'Ссылка для подтверждения регистрации (сайт "'.$_SERVER['SERVER_NAME'].'")';
                } else {
                    $title = 'Регистрационная информация (сайт "'.$_SERVER['SERVER_NAME'].'")';
                }
			    /////////////////////
				$html .= '<p>Ваша регистрационная информация для сайта "'.\Bitrix\Main\Config\Option::get('main', 'server_name').'":</p>';
				$html .= '<p>ID пользователя: '.$user["ID"].'</p>';
				$html .= '<p>Статус профиля: '.($user['UF_CONFIRMED']?'подтверждён':'не подтверждён').'</p>';
				$html .= '<p>E-Mail: '.$user["EMAIL"].'</p>';
				if( $password ){
					$html .= '<p>Пароль: '.$password.'</p>';
				}
				if( strlen($regCode) > 0 ){
				    $link = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].'/register_confirm/?code='.$regCode;
                    $html .= '<br><p>Ссылка для подтверждения регистрации:<br><a href="'.$link.'">'.$link.'</a></p>';
				}
				/////////////////////////////
				// отправка инфы на почту
                tools\feedback::sendMainEvent (  $title,  $html,  $user["EMAIL"]  );
			}
		}
	}
	
	
	
	
	// Сверка пароля с пользовательским
	static function comparePassword($userId, $password){
		if( intval($userId) > 0 && strlen($password) > 0 ){
			$userData = static::info($userId);
			$salt = substr($userData['PASSWORD'], 0, (strlen($userData['PASSWORD']) - 32));
			$realPassword = substr($userData['PASSWORD'], -32);
			$password = md5($salt.$password);
			return ($password == $realPassword);
		}
		return false;
	}



    static function getByLogin ( $login, $not_id = false, $not_social = true ){
        $fields = Array( 'FIELDS' => array( 'ID', 'LOGIN', 'EXTERNAL_AUTH_ID' )  );
        $filter = Array( "LOGIN_EQUAL" => $login );
        if( $not_social ){    $filter["!EXTERNAL_AUTH_ID"] = "socservices";    }
        if( intval($not_id) > 0 ){    $filter["!ID"] = $not_id;    }
        $rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter, $fields);
        if ($user = $rsUsers->GetNext()){     return $user;     }
        return false;
    }



    static function getByEmail ( $email, $not_id = false, $not_social = true ){
        $fields = Array( 'FIELDS' => array( 'ID', 'EMAIL', 'EXTERNAL_AUTH_ID' )  );
        $filter = Array( "EMAIL" => $email );
        if( $not_social ){    $filter["!EXTERNAL_AUTH_ID"] = "socservices";    }
        if( intval($not_id) > 0 ){    $filter["!ID"] = $not_id;    }
        $rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter, $fields);
        if ($user = $rsUsers->GetNext()){     return $user;     }
        return false;
    }


	
}



