<?php

$aoptima_project_default_option = array(

	'COMPANY_NAME' => 'ООО "Компания"',

    'CALLBACK_EMAIL_TO' => '',
    'QUICK_ORDER_EMAIL' => '',
    'ORDER_EMAIL' => '',
    'TELEGRAM_CHAT' => '',
    'VIBER_CHAT' => '',
    'WATSAPP_PHONE' => '',



    'GOOGLE_RECAPTCHA_SITE_KEY' => '',
    'GOOGLE_RECAPTCHA_SECRET_KEY' => '',
    'GENERAL_EMAIL' => 'shop@rulez.by',

    'RASSROCHKA_PROCENT' => 0,
    'RASSROCHKA_MONTHS' => '',
//    'RASSROCHKA_MIN_MONTHS' => 2,
//    'RASSROCHKA_MAX_MONTHS' => 11,
    'RASSROCHKA_MIN_VZNOS' => 30,
    'RASSROCHKA_MAX_VZNOS' => 50,

    'DEFAULT_COURIER_DELIERY_PRICE' => 14.5,
    'COURIER_MINSK_DELIVERY_PRICE' => 5,
    'COURIER_MINSK_POROG_SUM' => 300,
    'DELIVERY_RB_PLUS_DAYS' => 1,

);