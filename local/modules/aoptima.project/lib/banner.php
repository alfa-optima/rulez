<?php

namespace AOptima\Project;
use AOptima\Project as project;


class banner {


    const IBLOCK_ID = 59;




    static function types(){
        $list = [];
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'all_banner_types';
        $cache_path = '/all_banner_types/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            \Bitrix\Main\Loader::includeModule('iblock');
            $filter = array(
            	"ACTIVE" => "Y",
            	"IBLOCK_ID" => static::IBLOCK_ID
            );
            $dbSections = \CIBlockSection::GetList(
            	array( "SORT" => "ASC" ),
            	$filter, false
            );
            while ($section = $dbSections->GetNext()){
                $list[$section['ID']] = $section;
            }
            $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }




    static function allList(){
        $list = [];
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'all_banners';
        $cache_path = '/all_banners/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            \Bitrix\Main\Loader::includeModule('iblock');
            $filter = [
                "IBLOCK_ID" => static::IBLOCK_ID,
                "ACTIVE" => "Y"
            ];
            $fields = [
                "ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PICTURE", "PROPERTY_LINK",
                "DATE_ACTIVE_FROM", "DATE_ACTIVE_TO", "PROPERTY_SECTION_XML_ID"
            ];
            $dbElements = \CIBlockElement::GetList(
                array( "SORT" => "ASC" ), $filter, false, false, $fields
            );
            while ($element = $dbElements->GetNext()){
                $list[$element['ID']] = $element;
            }
            $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }




}