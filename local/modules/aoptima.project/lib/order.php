<?php

namespace AOptima\Project;
use AOptima\Project as project;


class order {

    const SAMOVYVOZ_DS_XMLID = 'samovyvoz';
    const COURIER_DS_XMLID = 'courier';
    const COURIER_MINSK_DS_XMLID = 'courier_minsk';
    const POCHTA_DS_XMLID = 'pochta';

    const DEFAULT_PS_GROUP = 'online_payment';



    static $statuses = [
        'A' => [
            'NAME' => 'Принят',
            'CLASS' => 'oh-meta__status_pending',
        ],
        'B' => [
            'NAME' => 'Доставка',
            'CLASS' => 'oh-meta__status_pending',
        ],
        'C' => [
            'NAME' => 'Отменён',
            'CLASS' => 'oh-meta__status_rejected',
        ],
        'D' => [
            'NAME' => 'Доставка',
            'CLASS' => 'oh-meta__status_delivery',
        ],
        'P' => [
            'NAME' => 'Оплачен',
            'CLASS' => 'oh-meta__status_payed',
        ],
        'Z' => [
            'NAME' => 'Выполнен',
            'CLASS' => 'oh-meta__status_done',
        ],
    ];





    function __construct(){

        $this->formFields = array(
            'name' => array(
                'NAME' => 'Ваше имя',
                'CODE' => 'name',
                //'max_length' => 1000,
                //'CSS_CLASSES' => 'is___phone',
//                'check_reg' => '^[А-ЯЁа-яё -]{1,255}$',
//                'check_error' => 'допускаются русские буквы, тире и пробел, макс. 255 симв.'
            ),
            'phone' => array(
                'NAME' => 'Контактный телефон',
                'CODE' => 'phone',
                'CSS_CLASSES' => 'is___phone',
                'check_reg' => '^[0-9()+ -]{1,99}$',
                'check_error' => 'В номере телефона допускаются только цифры, а также символы "+", "-", пробел и круглые скобки!'
            ),
            'email' => array(
                'NAME' => 'E-mail',
                'CODE' => 'email',
                'check_reg' => '^.+@.+\..+$',
                'check_error' => 'содержит ошибки',
                //'type' => 'email',
            ),
            'np_name' => array(
                'NAME' => 'Населённый пункт',
                'CODE' => 'np_name'
            ),
            'index' => array(
                'NAME' => 'Почтовый индекс',
                'CODE' => 'index',
                'check_reg' => '^[0-9]+$',
                'check_error' => 'допускаются только цифры'
            ),
            'address' => array(
                'NAME' => 'Улица, дом, корпус',
                'CODE' => 'address'
            ),
            'kv' => array(
                'NAME' => 'Квартира',
                'CODE' => 'kv'
            ),
            'floor' => array(
                'NAME' => 'Этаж',
                'CODE' => 'floor'
            ),
        );


    }





    // Поля для формы
    public function getFormFields(){
        $formFields = $this->formFields;
        return $formFields;
    }




}