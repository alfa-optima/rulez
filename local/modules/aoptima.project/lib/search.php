<?php

namespace AOptima\Project;
use AOptima\Project as project;


class search {


    static function sortList(){
        return [
            'popular' => [
                'CODE' => 'popular',
                'FIELD' => 'RELEVANT',
                'NAME' => 'По популярности',
                'DEFAULT_ORDER' => 'asc'
            ],
            'price' => [
                'CODE' => 'price',
                'FIELD' => 'CATALOG_PRICE_'.project\catalog::BASE_PRICE_ID,
                'NAME' => 'По цене',
                'DEFAULT_ORDER' => 'asc'
            ],
        ];
    }


    static function getSort(){
        $sortFieldGET = trim(strip_tags( $_GET['sort_field'] ));
        $sortFieldPOST = trim(strip_tags( $_POST['sort_field'] ));
        $sort = false;
        // варианты сортировки
        $list = static::sortList();
        // поле сортировки
        if( strlen( $sortFieldGET ) > 0 ){
            $sortField = $sortFieldGET;
        } else if( strlen( $sortFieldPOST ) > 0 ){
            $sortField = $sortFieldPOST;
        } else {
            $sortField = $list['popular']['CODE'];
        }
        $sort = $list[$sortField];
        // порядок сортировки
        $sortOrderGET = trim(strip_tags($_GET['sort_order']));
        $sortOrderPOST = trim(strip_tags($_POST['sort_order']));
        if( strlen( $sortOrderGET ) > 0 ){
            $sortOrder = $sortOrderGET;
        } else if( strlen( $sortOrderPOST ) > 0 ){
            $sortOrder = $sortOrderPOST;
        } else {
            $sortOrder = $sort['DEFAULT_ORDER'];
        }
        $sort['ORDER'] = $sortOrder;
        return $sort;
    }



//    static function qHasAllWords(){
//
//    }



}