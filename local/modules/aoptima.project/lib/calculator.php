<? namespace AOptima\Project;
use AOptima\Project as project;



class calculator {



    static function rassrochkaMonths(){
        $months = [];
        $ms = \Bitrix\Main\Config\Option::get('aoptima.project', 'RASSROCHKA_MONTHS');
        // ожидаемый формат
        // 3-1.068|6-1.079|9-1.094|12-1.105|18-1.152|20-1.167|24-1.188|30-1.294|36-1.322
        if( strlen($ms) > 0 && substr_count($ms, '|') ){
            $rassrochkaMonths = explode('|', $ms);
            foreach ( $rassrochkaMonths as $key => $rassrochkaMonthItem ){
                if( substr_count($rassrochkaMonthItem, '-') ){
                    $ar = explode('-', $rassrochkaMonthItem);
                    $rassrochkaMonthsCNT = intval(trim($ar[0]));
                    $rassrochkaKoeff = trim($ar[1]);
                    $rassrochkaKoeff = str_replace(",", ".", $rassrochkaKoeff);
                    $rassrochkaKoeff = str_replace(" ", "", $rassrochkaKoeff);
                } else {
                    $rassrochkaMonthsCNT = intval(trim($rassrochkaMonthItem));
                    $rassrochkaKoeff = 1;
                }
                if( intval(trim($rassrochkaMonthsCNT)) > 0 ){
                    $months[] = [
                        'months_cnt' => $rassrochkaMonthsCNT,
                        'koeff' => $rassrochkaKoeff,
                    ];
                }
            }
        }
        return $months;
    }









}