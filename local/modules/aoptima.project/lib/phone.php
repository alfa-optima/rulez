<?php

namespace AOptima\Project;
use AOptima\Project as project;



class phone {

    const IBLOCK_ID = 131;



    static function categories(){
        $list = [];
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'phone_categories';
        $cache_path = '/phone_categories/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            \Bitrix\Main\Loader::includeModule('iblock');
            $filter = array(
                "ACTIVE" => "Y",
                "IBLOCK_ID" => static::IBLOCK_ID
            );
            $dbSections = \CIBlockSection::GetList(
                array("SORT"=>"ASC"),
                $filter, false,
                array("UF_CSS_CLASS", "UF_CSS_CLASS_2")
            );
            while ($section = $dbSections->GetNext()){
                $list[$section['ID']] = $section;
            }
        $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }





}