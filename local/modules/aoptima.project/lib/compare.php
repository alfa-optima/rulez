<?php

namespace AOptima\Project;
use AOptima\Project as project;



class compare {

    const COOKIE_NAME = 'compares';

    static $stop_props = [
        'CML2_BAR_CODE', 'CML2_ARTICLE', 'CML2_ATTRIBUTES', 'CML2_TRAITS', 'CML2_BASE_UNIT',
        'CML2_TAXES', 'MORE_PHOTO', 'FILES', 'RASSROCHKA', 'STATUS_TOVARA', 'NEW', 'BEZNAL',
        'DOSTAVKA', 'IMPORTER', 'OPISANIE_DLYA_SAYTA', 'POSLEDNIY_TOVAR', 'PREMIUM', 'SERVISNYY_TSENTR', 'SKIDKA', 'SROK_POSTAVKI', 'STRANA_PROISKHOZHDENIYA'
    ];



    static function checkCookieIDS(){
        global $APPLICATION;
        $cur_items_str = $APPLICATION->get_cookie(static::COOKIE_NAME);
        $cur_items = [];
        if( strlen($cur_items_str) > 0 ){
            $cur_items = explode('|', $cur_items_str);
        }
        $new_items = static::checkIDs( $cur_items );
        global $APPLICATION;
        $APPLICATION->set_cookie( static::COOKIE_NAME, implode('|', $new_items), time() + 2592000 );
        return $new_items;
    }



    static function checkIDs( $ids ){
        $items = [];
        if( count($ids) > 0 ){
            foreach ( $ids as $key => $id ){
                $el = \AOptima\Tools\el::info($id);
                if( intval($el['ID']) > 0 ){    $items[] = $el['ID'];    }
            }
            $items = array_unique($items);
        }
        return $items;
    }



}