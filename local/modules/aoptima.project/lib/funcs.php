<? namespace AOptima\Project;
use AOptima\Project as project; 

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;  



class funcs {



    static function redirects(){
        $oldURL = rawurldecode($_SERVER['REQUEST_URI']);
        $pureURL = tools\funcs::pureURL($oldURL);
        $arURI = tools\funcs::arURI( $pureURL );
        $newURL = false;
        $lastSymbol = substr($pureURL, strlen($pureURL)-1, 1);
        if(
            $lastSymbol != '/'
            &&
            !preg_match( "/\.[a-zA-Z]+$/", $newURL?$newURL:$pureURL,$matches )
        ){
            $newURL = $newURL?$newURL:$pureURL.'/';
        }
        if(
            (
                $arURI[count($arURI)-1] == 'index.php'
                &&
                file_exists($_SERVER['DOCUMENT_ROOT'].$pureURL)
            )
            ||
            (
                $arURI[count($arURI)-1] == 'index.html'
                &&
                file_exists($_SERVER['DOCUMENT_ROOT'].$pureURL)
            )
        ){
            $newURL = str_replace(
                ["index.php", "index.html"], "",
                $newURL?$newURL:$pureURL
            );
        }
        if( count($_GET) > 0 ){
            foreach ( $_GET as $key => $value ){
                $newURL = tools\funcs::addToRequestURI($newURL, $key, $value);
            }
        }
        if( substr_count( $newURL, "PAGEN_1=1" ) ){
            $newURL = tools\funcs::removeFromRequestURI($newURL, 'PAGEN_1');
        }
        if( strlen($newURL) > 0 && $oldURL != $newURL ){
            //LocalRedirect( $newURL , false, '301 Moved permanently' );
        }
    }



    static function searchUpdateName( $tov_name, $q ){
        if( substr_count(strtolower($tov_name), strtolower($q)) ){
            $pos_1 = strpos(strtolower($tov_name), strtolower($q));
            $pos_2 = $pos_1 + strlen($q);
            $str_start = substr($tov_name, 0, $pos_1);
            if( strlen($str_start) > 0 ){
                $str_start_new = str_replace($str_start, $str_start."<span style='background-color: #6e52dd; color: white;'>", $str_start);
            } else {
                $str_start_new = "<span style='background-color: #6e52dd; color: white;'>";
            }
            $str_medium = substr($tov_name, $pos_1, $pos_2-$pos_1);
            $str_end = substr($tov_name, $pos_2, strlen($tov_name)-$pos_2);
            if( strlen($str_end) > 0 ){
                $str_end_new = str_replace($str_end, "</span>".$str_end, $str_end);
            } else {
                $str_end_new = "</span>";
            }
            $tov_name = $str_start_new.$str_medium.$str_end_new;
        } else {
            $arLang = \CSearchLanguage::GuessLanguage($q);
            if( is_array($arLang) && $arLang["from"] != $arLang["to"] ){
                $lang_q = \CSearchLanguage::ConvertKeyboardLayout($q, $arLang["from"], $arLang["to"]);
                if( $lang_q != $q ){
                    $pos_1 = strpos(strtolower($tov_name), strtolower($lang_q));
                    $pos_2 = $pos_1 + strlen($lang_q);
                    $str_start = substr($tov_name, 0, $pos_1);
                    if( strlen($str_start) > 0 ){
                        $str_start_new = str_replace($str_start, $str_start."<span style='background-color: #6e52dd; color: white;'>", $str_start);
                    } else {
                        $str_start_new = "<span style='background-color: #6e52dd; color: white;'>";
                    }
                    $str_medium = substr($tov_name, $pos_1, $pos_2-$pos_1);
                    $str_end = substr($tov_name, $pos_2, strlen($tov_name)-$pos_2);
                    if( strlen($str_end) > 0 ){
                        $str_end_new = str_replace($str_end, "</span>".$str_end, $str_end);
                    } else {
                        $str_end_new = "</span>";
                    }
                    $tov_name = $str_start_new.$str_medium.$str_end_new;
                }
            }
        }
        return $tov_name;
    }


    static function variations($input) {
        $hash = md5(json_encode($input));
        if( $_SESSION['variations'][$hash] ){
            return $_SESSION['variations'][$hash];
        } else {
            if (count($input) <= 1) return array($input);
            $result = array();
            foreach ($input as $v) {
                $reduce = array_filter($input,
                    function ($e) use($v) { return $e !== $v; }
                );
                $result = array_merge( $result,
                    array_map(
                        function ($e) use ($v) { array_unshift( $e, $v); return $e; },
                        static::variations( $reduce )
                    )
                );
            }
            $_SESSION['variations'][$hash] = $result;
            return $result;
        }
    }
    
    
    static function searchLogic($q, $full_search = false){
        $q = str_replace('+', ' ', $q);
        
        $variants = [ $q."%" ];
        
        $searchLogic = [
            "LOGIC" => "OR",
            [ "=PROPERTY_CML2_ARTICLE" => $q ],
        ];
        
        $altQ = str_replace(['-', '+', ':'], ' ', $q);
        $arQ = explode(' ', $altQ);

        if( count($arQ) > 1 ){
            $variations = static::variations($arQ);
            foreach ( $variations as $vars ){
                $val = implode(' ', $vars);
                $variants[] = "%".$val."%";
                $variants[] = $val."%";
                $variants[] = "%".$val;
            }
        } else {
            $variants[] = "%".$q."%";
            $variants[] = $q."%";
            $variants[] = "%".$q;
        }
        
        if( $full_search ){
            if( is_array($arQ) && count($arQ) > 0 ){
                foreach ( $arQ as $value ){
                    $value = trim(strtolower($value));
                    $variants[] = $value."%";
                    $variants[] = " ".$value."%";
                }
            }
        }

        $variants = array_unique($variants);
        foreach ( $variants as $variant ){
            $searchLogic[] = [ 'NAME' => $variant ];
        }
        
        //echo "<pre>"; print_r( $searchLogic ); echo "</pre>";
        
        return $searchLogic;
    }


    
    static function isContentPage(){

        $arURI = tools\funcs::arURI( tools\funcs::pureURL() );
        if(
            in_array( $arURI[1], [
                'articles', 'product', 'basket', 'order', 'personal',
                'register_confirm', 'favorites', 'compare', 'search', 'search_new',
                'order_success', 'faq', 'pay_success', 'pay_error', 'pay_cancel', 'actions',
                'best_prices', 'gift_ideas', 'brand', 'password_recovery'
            ])
            ||
            ERROR_404 == 'Y'
        ){
            return false;
        } {

            $catalog_iblocks = project\catalog::iblocks();
            foreach ( $catalog_iblocks as $key => $catalog_iblock ){
                $first_level_sect = project\catalog::firstLevelCategory($catalog_iblock['ID']);
                if( $first_level_sect['CODE'] == $arURI[1] ){  return false;  }
            }

            $all_seo_pages = project\SeopageTable::allList();
            foreach ( $all_seo_pages as $key => $seo_page ){
                if( $seo_page['CODE'] == $arURI[1] ){  return false;  }
            }

        }
        return true;
    }




    static function clearOldCaches(){
        BXClearCache(false, "/");
        return '\AOptima\Project\funcs::clearOldCaches();';
    }



	
}