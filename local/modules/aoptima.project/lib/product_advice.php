<?php

namespace AOptima\Project;
use AOptima\Project as project;


class product_advice {

    const MAX_CNT = 2;



    static function getList( $product, $sect_id ){
        \Bitrix\Main\Loader::includeModule('iblock');
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        $list = [];
        // Категория "Полезные советы"
        $adv_category = \AOptima\Tools\section::info_by_code( project\article::ADVICES_CATEGORY_CODE, project\article::IBLOCK_ID );
        $fields = [ "ID", "DETAIL_PAGE_URL", "NAME", "PROPERTY_SECTION", "PROPERTY_EXPERT_PHOTO" ];
        // Поиск в привязке к категории, если задана
        if( intval($sect_id) > 0 ){
            $filter = Array(
                "IBLOCK_ID" => project\article::IBLOCK_ID,
                "ACTIVE" => "Y",
                "SECTION_ID" => $adv_category['ID'],
                "PROPERTY_SECTION" => $sect_id
            );
            $fields[] = "PROPERTY_SECTION";
            $dbElements = \CIBlockElement::GetList(
                array("SORT"=>"ASC"), $filter, false, array("nTopCount"=> static::MAX_CNT), $fields
            );
            while ($element = $dbElements->GetNext()){
                $list[$element['ID']] = $element;
            }
            // Попытка поиска по цепочке разделов
            // если ещё не набрано макс. количество
            if( count($list) < static::MAX_CNT ){
                $sect_chain = \AOptima\Tools\section::chain( $sect_id );
                $sect_chain = array_reverse($sect_chain);
                foreach ( $sect_chain as $key => $s ){
                    if(
                        $s['ID'] != $sect_id
                        &&
                        count($list) < static::MAX_CNT
                    ){
                        $filter = Array(
                            "IBLOCK_ID" => project\article::IBLOCK_ID,
                            "ACTIVE" => "Y",
                            "SECTION_ID" => $adv_category['ID'],
                            "PROPERTY_SECTION" => $s['ID'],
                            "!ID" => array_keys($list)
                        );
                        $dbElements = \CIBlockElement::GetList(
                            array( "SORT"=>"ASC" ), $filter, false,
                            array( "nTopCount" => (static::MAX_CNT - count($list)) ), $fields
                        );
                        while ($element = $dbElements->GetNext()){
                            $list[$element['ID']] = $element;
                        }
                    }
                }
            }
        }
        // Поиск привязки к товару
        // если ещё не набрано макс. количество
        if(
            intval($product['ID']) > 0
            &&
            count($list) < static::MAX_CNT
        ){
            $filter = Array(
                "IBLOCK_ID" => project\article::IBLOCK_ID,
                "ACTIVE" => "Y",
                "SECTION_ID" => $adv_category['ID'],
                "PROPERTY_TOVAR" => $product['ID'],
                "!ID" => array_keys($list)
            );
            $fields[] = "PROPERTY_TOVAR";
            $dbElements = \CIBlockElement::GetList(
                array("SORT" => "ASC"), $filter, false,
                array("nTopCount" => (static::MAX_CNT - count($list))), $fields
            );
            while ($element = $dbElements->GetNext()){
                $list[$element['ID']] = $element;
            }
        }
        return $list;
    }



}