<?php

namespace AOptima\Project;
use AOptima\Project as project;



class ds {


    static $stop_ids = [2];



    // Вариант доставки по ID
    static function getByID( $id ){
        \Bitrix\Main\Loader::includeModule('sale');
        $db_dtype = \CSaleDelivery::GetList(
            Array("SORT"=>"ASC", "NAME"=>"ASC"),
            Array(
                "ACTIVE"=>"Y",
                "LID" => SITE_ID,
                "ID" => $id
            ),
            false, false, array()
        );
        while ($ds = $db_dtype->GetNext()){
            return $ds;
        }
        return false;
    }



    // Вариант доставки по XML_ID
    static function getByXMLID( $xml_id ){
        \Bitrix\Main\Loader::includeModule('sale');
        $db_dtype = \CSaleDelivery::GetList(
            Array("SORT"=>"ASC", "NAME"=>"ASC"),
            Array(
                "ACTIVE"=>"Y",
                "LID" => SITE_ID,
                "XML_ID" => $xml_id
            ),
            false, false, array()
        );
        while ($ds = $db_dtype->GetNext()){
            return $ds;
        }
        return false;
    }



    // Список вариантов доставки
    static function getList(){
        \Bitrix\Main\Loader::includeModule('sale');
        $list = array();
        $db_dtype = \CSaleDelivery::GetList(
            Array("SORT"=>"ASC", "NAME"=>"ASC"),
            Array(
                "ACTIVE"=>"Y",
                "LID" => SITE_ID,
                "!ID" => static::$stop_ids
            ),
            false, false, array()
        );
        while ($ds = $db_dtype->GetNext()){
            $list[$ds['ID']] = $ds;
        }
        return  $list;
    }







}