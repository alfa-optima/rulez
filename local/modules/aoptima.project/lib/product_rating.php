<?php

namespace AOptima\Project;
use AOptima\Project as project;


class product_rating {


    public function __construct( $el_id ) {
        $this->el_id = $el_id;
        //\Bitrix\Main\Loader::includeModule('aoptima.tools');
        //$this->el = \AOptima\Tools\el::info($el_id);
    }


    public function get() {
        $rating = 0;
        if( $this->el_id ){
            // Кеширование
            $obCache = new \CPHPCache();
            $cache_time = 60*60;
            $cache_id = 'product_rating_'.$this->el_id;
            $cache_path = '/product_rating/'.$this->el_id.'/';
            if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            	$vars = $obCache->GetVars();   extract($vars);
            } elseif($obCache->StartDataCache()){

                // Получение перечня оценок
                $product_vote = new project\product_vote();
                $votes = $product_vote->getList( $this->el_id );

                if( count($votes) > 0 ){

                    $int_votes = [];
                    foreach ( $votes as $key => $vote ){   $int_votes[] = $vote['UF_VOTE'];   }
                    
                    // Сумма оценок
                    $sum = array_sum($int_votes);
                    // количество оценок
                    $cnt = count($int_votes);
                    
                    // рейтинг
                    $rating = $sum / $cnt;

                    // логика округления с онлайнера (по просьбе заказчика)
                    $celoe = floor($rating);
                    $drob = $rating - $celoe;
                    if( $drob < 0.3 ){
                        $rating = $celoe;
                    } else if( $drob >= 0.3 && $drob < 0.8 ){
                        $rating = $celoe + 0.5;
                    } else if( $drob >= 0.8 ){
                        $rating = ceil($rating);
                    }

                    // прежняя логика
//                    sort($int_votes);
//                    if( count($int_votes)%2 == 1 ){
//                        $key = ceil(count($int_votes)/2) - 1;
//                        $rating = $int_votes[$key];
//                    } else if( count($int_votes)%2 == 0 ){
//                        $key_1 = count($int_votes)/2 - 1;
//                        $key_2 = count($int_votes)/2;
//                        $rating = round(($int_votes[$key_1] + $int_votes[$key_2])/2, 0);
//                    }

                }

            $obCache->EndDataCache(array('rating' => $rating));
            }
        }
        return $rating;
    }


}