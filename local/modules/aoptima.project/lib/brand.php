<?php

namespace AOptima\Project;
use AOptima\Project as project;


class brand {


    const IBLOCK_ID = 68;




    static function allList(){
        $list = [];
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'all_brands';
        $cache_path = '/all_brands/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            \Bitrix\Main\Loader::includeModule('iblock');
            $filter = Array(
                "IBLOCK_ID" => static::IBLOCK_ID,
                "ACTIVE" => "Y"
            );
            $fields = Array(
                "ID", "NAME", "CODE", "PROPERTY_IBLOCK_XML_ID", "PROPERTY_PROP_CODE", "PROPERTY_PROP_VALUE"
            );
            $dbElements = \CIBlockElement::GetList(
                array( "SORT" => "ASC" ), $filter, false, false, $fields
            );
            while ($element = $dbElements->GetNext()){
                $list[$element['CODE']] = $element;
            }
            $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }




}