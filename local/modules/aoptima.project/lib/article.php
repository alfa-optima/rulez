<? namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;


class article {

    const IBLOCK_ID = 23;
    const ADVICES_CATEGORY_CODE = 'poleznye-sovety';



    // Категории статей
    static function sections(){
        $list = [];
        \Bitrix\Main\Loader::includeModule('iblock');
        $filter = array(
        	"ACTIVE" => "Y",
        	"IBLOCK_ID" => static::IBLOCK_ID
        );
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'article_sections';
        $cache_path = '/article_sections/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $dbSections = \CIBlockSection::GetList(
                array("SORT"=>"ASC"), $filter, false
            );
            while ($section = $dbSections->GetNext()){
                $list[$section['ID']] = $section;
            }
        $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }







}