<? namespace AOptima\Project;
use AOptima\Project as project;


class product_review {

    const IBLOCK_ID = 49;
    const LIST_CNT = 5;





    static function getList( $product_id ){
        $list = [];
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'productReviews_'.$product_id;
        $cache_path = '/productReviews/'.$product_id.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            \Bitrix\Main\Loader::includeModule('iblock');
            $filter = Array(
            	"IBLOCK_ID" => static::IBLOCK_ID,
            	"ACTIVE" => "Y",
            	"PROPERTY_PRODUCT" => $product_id
            );
            $fields = Array(
                "ID", "PROPERTY_PRODUCT", "PROPERTY_LINK"
            );
            $dbElements = \CIBlockElement::GetList(
            	array( "ID" => "DESC" ), $filter, false, false, $fields
            );
            while ($element = $dbElements->GetNext()){
                $list[$element['ID']] = $element;
            }
            $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }



    static function getOrderedList( $product_id, $stop_ids = [] ){
        $list = [];
        $maxCnt = static::LIST_CNT + 1;
        // Отзывы о товаре
        $allReviews = static::getList( $product_id );
        $reviewsTree = [];
        foreach ( $allReviews as $review_id => $review ){
            if( intval($review['PROPERTY_LINK_VALUE']) ){
                $reviewsTree[$review['PROPERTY_LINK_VALUE']][] = $review_id;
                $reviewsTree[$review['PROPERTY_LINK_VALUE']] = array_unique( $reviewsTree[$review['PROPERTY_LINK_VALUE']] );
                sort($reviewsTree[$review['PROPERTY_LINK_VALUE']]);
            } else if( !$reviewsTree[$review_id] ){
                $reviewsTree[$review_id] = [];
            }
        }
        foreach ( $reviewsTree as $parent_id => $sub_items ){
            if( count($list) < $maxCnt ){
                if( !in_array($parent_id, $stop_ids) ){
                    $el = \AOptima\Tools\el::info($parent_id);
                    if( intval($el['ID']) > 0 ){
                        if( intval($el['PROPERTY_USER_VALUE']) > 0 ){
                            $el['USER'] =  \AOptima\Tools\user::info($el['PROPERTY_USER_VALUE']);
                        }
                        if( strlen($el['PROPERTY_PLUSES_VALUE']['TEXT']) > 0 ){
                            $el['PROPERTY_PLUSES_VALUE']['TEXT'] = str_replace(array("\n"), "<br>", $el['PROPERTY_PLUSES_VALUE']['TEXT']);
                        }
                        if( strlen($el['PROPERTY_MINUSES_VALUE']['TEXT']) > 0 ){
                            $el['PROPERTY_MINUSES_VALUE']['TEXT'] = str_replace(array("\n"), "<br>", $el['PROPERTY_MINUSES_VALUE']['TEXT']);
                        }
                        $list[$el['ID']] = $el;
                    }
                }
                if( is_array( $sub_items ) ){
                    foreach ( $sub_items as $sub_item_id ){
                        if(
                            count($list) < $maxCnt
                            &&
                            !in_array($sub_item_id, $stop_ids)
                        ){
                            $el = \AOptima\Tools\el::info($sub_item_id);
                            if( intval($el['ID']) > 0 ){
                                if( strlen($el['PROPERTY_PLUSES_VALUE']['TEXT']) > 0 ){
                                    $el['PROPERTY_PLUSES_VALUE']['TEXT'] = str_replace(array("\n"), "<br>", $el['PROPERTY_PLUSES_VALUE']['TEXT']);
                                }
                                if( strlen($el['PROPERTY_MINUSES_VALUE']['TEXT']) > 0 ){
                                    $el['PROPERTY_MINUSES_VALUE']['TEXT'] = str_replace(array("\n"), "<br>", $el['PROPERTY_MINUSES_VALUE']['TEXT']);
                                }
                                if( intval($el['PROPERTY_USER_VALUE']) > 0 ){
                                    $el['USER'] =  \AOptima\Tools\user::info($el['PROPERTY_USER_VALUE']);
                                }
                                if( !in_array($el['ID'], $stop_ids) ){
                                    $list[$el['ID']] = $el;
                                }
                            }
                        }
                    }
                }
            }
        }
        return $list;
    }








}