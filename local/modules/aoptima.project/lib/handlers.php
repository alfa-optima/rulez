<? namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;



class handlers {



    static function OnAfterIBlockAdd( $arFields ){
        BXClearCache(true, "/catalog_iblocks/");
    }
    static function OnAfterIBlockUpdate( $arFields ){
        BXClearCache(true, "/catalog_iblocks/");
        BXClearCache(true, "/iblockFirstLevelCategory/".$arFields['ID']."/");
        BXClearCache(true, "/iblockFirstLevelCategory/".$arFields['ID']."/");
        BXClearCache(true, "/iblock_prop_list/".$arFields['ID']."/");
    }
    static function OnIBlockDelete( $ID ){
        BXClearCache(true, "/catalog_iblocks/");
        BXClearCache(true, "/iblockFirstLevelCategory/".$ID."/");
        BXClearCache(true, "/iblockFirstLevelCategory/".$ID."/");
        BXClearCache(true, "/iblock_prop_list/".$ID."/");
    }


    static function OnBeforeIBlockAdd( &$arFields ){
        if( $arFields['IBLOCK_TYPE_ID'] == '1c_catalog' ){
            $arFields['LIST_PAGE_URL'] = '#SITE_DIR#/';
            $arFields['SECTION_PAGE_URL'] = '#SITE_DIR#/#SECTION_CODE_PATH#/';
            $arFields['DETAIL_PAGE_URL'] = '#SITE_DIR#/product/#ELEMENT_CODE#/';
        }
    }
    static function OnBeforeIBlockUpdate( &$arFields ){
        if( $arFields['IBLOCK_TYPE_ID'] == '1c_catalog' ){
            $arFields['LIST_PAGE_URL'] = '#SITE_DIR#/';
            $arFields['SECTION_PAGE_URL'] = '#SITE_DIR#/#SECTION_CODE_PATH#/';
            $arFields['DETAIL_PAGE_URL'] = '#SITE_DIR#/product/#ELEMENT_CODE#/';
        }
    }





    static function OnAfterIBlockElementAdd( $arFields ){
        // Отзывы
        if( $arFields['IBLOCK_ID'] == project\product_review::IBLOCK_ID ){
            $review = tools\el::info($arFields['ID']);
            $product_id = $review['PROPERTY_PRODUCT_VALUE'];
            BXClearCache(true, "/productReviews/".$product_id.'/');
        }
        // Кредиты и рассрочки
        if( $arFields['IBLOCK_ID'] == project\credit_program::IBLOCK_ID ){
            BXClearCache(true, "/credit_programs/");
        }
        // Менеджеры
        if( $arFields['IBLOCK_ID'] == project\manager::IBLOCK_ID ){
            BXClearCache(true, "/managers/");
        }
        // Баннеры
        if( $arFields['IBLOCK_ID'] == project\banner::IBLOCK_ID ){
            BXClearCache(true, "/all_banners/");
        }
        // Бренды
        if( $arFields['IBLOCK_ID'] == project\brand::IBLOCK_ID ){
            BXClearCache(true, "/all_brands/");
        }
        // Стоп-свойства для карточки товара
        if( $arFields['IBLOCK_ID'] == project\catalog::DETAIL_STOP_PROPS_IBLOCK_ID ){
            BXClearCache(true, "/detailCardStopProps/");
        }
    }
    static function OnAfterIBlockElementUpdate( $arFields ){
        // Отзывы
        if( $arFields['IBLOCK_ID'] == project\product_review::IBLOCK_ID ){
            $review = tools\el::info($arFields['ID']);
            $product_id = $review['PROPERTY_PRODUCT_VALUE'];
            BXClearCache(true, "/productReviews/".$product_id.'/');
        }
        // Кредиты и рассрочки
        if( $arFields['IBLOCK_ID'] == project\credit_program::IBLOCK_ID ){
            BXClearCache(true, "/credit_programs/");
        }
        // Менеджеры
        if( $arFields['IBLOCK_ID'] == project\manager::IBLOCK_ID ){
            BXClearCache(true, "/managers/");
        }
        // Баннеры
        if( $arFields['IBLOCK_ID'] == project\banner::IBLOCK_ID ){
            BXClearCache(true, "/all_banners/");
        }
        // Бренды
        if( $arFields['IBLOCK_ID'] == project\brand::IBLOCK_ID ){
            BXClearCache(true, "/all_brands/");
        }
        // Стоп-свойства для карточки товара
        if( $arFields['IBLOCK_ID'] == project\catalog::DETAIL_STOP_PROPS_IBLOCK_ID ){
            BXClearCache(true, "/detailCardStopProps/");
        }
    }
    static function OnAfterIBlockElementDelete(){}

    static function OnBeforeIBlockElementAdd( &$arFields ){
        // Статьи
        if( $arFields['IBLOCK_ID'] == project\article::IBLOCK_ID ){
            if( intval($arFields['IBLOCK_SECTION'][0]) > 0 ){
                $sect_name = tools\section::info($arFields['IBLOCK_SECTION'][0])['NAME'];
                $sect_name_prop = tools\prop::getByCode(project\article::IBLOCK_ID, 'SECTION_NAME');
                $arFields['PROPERTY_VALUES'][$sect_name_prop['ID']][array_keys()[0]]['VALUE'] = $sect_name;
            }
        }
    }
    static function OnBeforeIBlockElementUpdate( &$arFields ){
        // Статьи
        if( $arFields['IBLOCK_ID'] == project\article::IBLOCK_ID ){
            if( intval($arFields['IBLOCK_SECTION'][0]) > 0 ){
                $sect_name = tools\section::info($arFields['IBLOCK_SECTION'][0])['NAME'];
                $sect_name_prop = tools\prop::getByCode(project\article::IBLOCK_ID, 'SECTION_NAME');
                $arFields['PROPERTY_VALUES'][$sect_name_prop['ID']][array_keys()[0]]['VALUE'] = $sect_name;
            }
        }
    }
    static function OnBeforeIBlockElementDelete( $ID ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $iblock_id = tools\el::getIblock($ID);
        $iblock = tools\iblock::getByID($iblock_id);
        // Каталог
        if( $iblock['IBLOCK_TYPE_ID'] == '1c_catalog' ){
            // Удаляем отзывы к товару при его удалении
            $filter = Array(
            	"IBLOCK_ID" => project\product_review::IBLOCK_ID,
            	"PROPERTY_PRODUCT" => $ID
            );
            $fields = Array( "ID", "PROPERTY_PRODUCT" );
            $reviews = \CIBlockElement::GetList(
            	array("SORT"=>"ASC"), $filter, false, false, $fields
            );
            while ($review = $reviews->GetNext()){
                \CIBlockElement::Delete( $review['ID'] );
            }
        }
        // Отзывы
        if( $iblock_id == project\product_review::IBLOCK_ID ){
            $el = tools\el::info($ID);
            if( intval( $el['PROPERTY_PRODUCT_VALUE'] ) > 0 ){
                BXClearCache(true, "/productReviews/".$el['PROPERTY_PRODUCT_VALUE'].'/');
            }
            // Удаляем ответы на данный отзыв
            $filter = Array(
                "IBLOCK_ID" => project\product_review::IBLOCK_ID,
                "PROPERTY_LINK" => $ID
            );
            $fields = Array( "ID", "PROPERTY_LINK" );
            $reviews = \CIBlockElement::GetList(
                array("SORT"=>"ASC"), $filter, false, false, $fields
            );
            while ($review = $reviews->GetNext()){
                \CIBlockElement::Delete( $review['ID'] );
            }
            // Удаляем оценки отзыва при его удалении
            $product_review_vote = new project\product_review_vote();
            $product_review_votes = $product_review_vote->getList($ID);
            foreach ( $product_review_votes as $vote ){
                $product_review_vote->delete($vote['ID']);
            }
        }
        // Кредиты и рассрочки
        if( $iblock_id == project\credit_program::IBLOCK_ID ){
            BXClearCache(true, "/credit_programs/");
        }
        // Менеджеры
        if( $iblock_id == project\manager::IBLOCK_ID ){
            BXClearCache(true, "/managers/");
        }
        // Баннеры
        if( $iblock_id == project\banner::IBLOCK_ID ){
            BXClearCache(true, "/all_banners/");
        }
        // Бренды
        if( $iblock_id == project\brand::IBLOCK_ID ){
            BXClearCache(true, "/all_brands/");
        }
        // Стоп-свойства для карточки товара
        if( $iblock_id == project\catalog::DETAIL_STOP_PROPS_IBLOCK_ID ){
            BXClearCache(true, "/detailCardStopProps/");
        }
    }






    static function OnAfterIBlockSectionAdd( $arFields ){
        BXClearCache(true, "/iblockFirstLevelCategory/".$arFields['IBLOCK_ID']."/");
        BXClearCache(true, '/allIblockCategories/'.$arFields['IBLOCK_ID'].'/');
        // Телефоны
        if( $arFields['IBLOCK_ID'] == project\phone::IBLOCK_ID ){
            BXClearCache(true, "/phone_categories/");
        }
        // Статьи
        if( $arFields['IBLOCK_ID'] == project\article::IBLOCK_ID ){
            BXClearCache(true, "/article_sections/");
        }
        // Акции
        if( $arFields['IBLOCK_ID'] == project\action::IBLOCK_ID ){
            BXClearCache(true, "/action_sections/");
        }
        // Кредиты и рассрочки
        if( $arFields['IBLOCK_ID'] == project\credit_program::IBLOCK_ID ){
            BXClearCache(true, "/credit_sections/");
        }
        // Баннеры
        if( $arFields['IBLOCK_ID'] == project\banner::IBLOCK_ID ){
            BXClearCache(true, "/all_banner_types/");
        }
    }
    static function OnAfterIBlockSectionUpdate( $arFields ){
        BXClearCache(true, "/iblockFirstLevelCategory/".$arFields['IBLOCK_ID']."/");
        BXClearCache(true, '/sectionByXMLID/');
        BXClearCache(true, '/allIblockCategories/'.$arFields['IBLOCK_ID'].'/');
        // Телефоны
        if( $arFields['IBLOCK_ID'] == project\phone::IBLOCK_ID ){
            BXClearCache(true, "/phone_categories/");
        }
        // Статьи
        if( $arFields['IBLOCK_ID'] == project\article::IBLOCK_ID ){
            BXClearCache(true, "/article_sections/");
        }
        // Акции
        if( $arFields['IBLOCK_ID'] == project\action::IBLOCK_ID ){
            BXClearCache(true, "/action_sections/");
        }
        // Кредиты и рассрочки
        if( $arFields['IBLOCK_ID'] == project\credit_program::IBLOCK_ID ){
            BXClearCache(true, "/credit_sections/");
        }
        // Баннеры
        if( $arFields['IBLOCK_ID'] == project\banner::IBLOCK_ID ){
            BXClearCache(true, "/all_banner_types/");
        }
    }
    static function OnAfterIBlockSectionDelete(  ){

    }


    static function OnBeforeIBlockSectionAdd( &$arFields ){

        // Телефоны
        if( $arFields['IBLOCK_ID'] == project\phone::IBLOCK_ID ){
            global $APPLICATION;
            $APPLICATION->throwException("Добавление новых категорий только через программиста");
            return false;
            /////////////
            BXClearCache(true, "/phone_categories/");
        }

    }
    static function OnBeforeIBlockSectionUpdate( &$arFields ){}
    static function OnBeforeIBlockSectionDelete( $ID ){
        $iblock_id = tools\section::getIblock($ID);
        BXClearCache(true, "/iblockFirstLevelCategory/".$iblock_id."/");
        BXClearCache(true, "/iblockFirstLevelCategory/".$iblock_id."/");
        BXClearCache(true, '/sectionByXMLID/');
        BXClearCache(true, '/allIblockCategories/'.$iblock_id.'/');
        // Телефоны
        if( $iblock_id == project\phone::IBLOCK_ID ){
            global $APPLICATION;
            $APPLICATION->throwException("Запрещено удалять категории инфоблока \"Телефоны\"");
            return false;
            /////////////
            BXClearCache(true, "/phone_categories/");
        }

        // Статьи
        if( $iblock_id == project\article::IBLOCK_ID ){
            BXClearCache(true, "/article_sections/");
        }
        // Акции
        if( $iblock_id == project\action::IBLOCK_ID ){
            BXClearCache(true, "/action_sections/");
        }
        // Кредиты и рассрочки
        if( $iblock_id == project\credit_program::IBLOCK_ID ){
            BXClearCache(true, "/credit_sections/");
        }
        // Баннеры
        if( $iblock_id == project\banner::IBLOCK_ID ){
            BXClearCache(true, "/all_banner_types/");
        }
    }




    static function OnSuccessCatalogImport1C( $arParams, $filePath ){

        BXClearCache(true, "/catalog_iblocks/");
        BXClearCache(true, "/iblock_prop_list/");

        global $DB;
        $catalogIblocks = project\catalog::iblocks();
        foreach ( $catalogIblocks as $iblock ){
            $where = ' WHERE IBLOCK_ID = '.$iblock['ID'];
            $strSql = "UPDATE b_iblock_element SET TMP_ID = 0".$where;
            $res = $DB->Query($strSql, false);
        }

        /*\Bitrix\Main\Loader::includeModule('iblock');
        $xml_content = file_get_contents($filePath);
        $objXML = new \CDataXML();
        $objXML->LoadString($xml_content);
        $arData = $objXML->GetArray();
        $xml_ids = [];
        if ($goodsBlock = $objXML->SelectNodes('/КоммерческаяИнформация/Каталог/Товары')) {
            $goods = $goodsBlock->children();
            foreach ( $goods as $key => $good ){
                $goodFields = $good->children();
                $xml_id = false;
                foreach ( $goodFields as $key => $goodField ){
                    if( $goodField->name == 'Ид' ){   $xml_id = $goodField->textContent();   }
                }
                if( $xml_id ){    $xml_ids[] = $xml_id;    }
            }
            $xml_ids = array_unique($xml_ids);
            if( count($xml_ids) > 0 ){
                $where_xml_ids = [];
                foreach ($xml_ids as $xmlid){   $where_xml_ids[] = 'XML_ID = '.$xmlid;   }
                foreach ( project\catalog::iblocks() as $iblock ){
                    $where = ' WHERE IBLOCK_ID = '.$iblock['ID'].' AND ('.implode(' OR ', $where_xml_ids).')';
                    $strSql = "UPDATE b_iblock_element SET TMP_ID = 0".$where;
                    $res = $DB->Query($strSql, false);
                }
            }
        }*/

    }







    static function OnAfterUserAdd(&$arFields){}
    static function OnAfterUserUpdate(&$arFields){}
    static function OnBeforeUserDelete($ID){
        if( $ID == project\user::SERVICE_USER_ID ){
            global $APPLICATION;
            $APPLICATION->throwException("Служебный пользователь не подлежит удалению");
            return false;
        }
    }





    static function OnGetOptimalPrice(
        $productID,
        $quantity = 1,
        $arUserGroups = array(),
        $renewal = "N",
        $arPrices = array(),
        $siteID = false,
        $arDiscountCoupons = false
    ){

        $product = \AOptima\Tools\el::info($productID);

        $new_catalog_group_id = project\catalog::BASE_PRICE_ID;
        $price = $product[ 'CATALOG_PRICE_' . project\catalog::BASE_PRICE_ID ];

        // Выбор в корзине варианта оплаты "Рассрочка"
        if( $_SESSION['ACTIVE_PS_XML_ID'] == 'rassrochka' ){

            if(
                $product['PROPERTY_'.project\catalog::RASSROCHKA_PROP_CODE.'_VALUE'] == 'Да'
                &&
                $product[ 'CATALOG_PRICE_' . project\catalog::RASSROCHKA_PRICE_ID ]
            ){

                $new_catalog_group_id = project\catalog::RASSROCHKA_PRICE_ID;
                $price = $product[ 'CATALOG_PRICE_' . project\catalog::RASSROCHKA_PRICE_ID ];

            } else {

                $new_catalog_group_id = project\catalog::BASE_PRICE_ID;
                $price = $product[ 'CATALOG_PRICE_' . project\catalog::BASE_PRICE_ID ];

            }

        } else if( $_SESSION['ACTIVE_PS_XML_ID'] == 'credit' ){

            if(
                $product['PROPERTY_'.project\catalog::RASSROCHKA_PROP_CODE.'_VALUE'] == 'Да'
                &&
                $product[ 'CATALOG_PRICE_' . project\catalog::CREDIT_PRICE_ID ]
            ){

                $new_catalog_group_id = project\catalog::CREDIT_PRICE_ID;
                $price = $product[ 'CATALOG_PRICE_' . project\catalog::CREDIT_PRICE_ID ];

            } else {

                $new_catalog_group_id = project\catalog::BASE_PRICE_ID;
                $price = $product[ 'CATALOG_PRICE_' . project\catalog::BASE_PRICE_ID ];

            }

        }

        return array(
            'PRICE' => array(
                "ID" => $productID,
                'CATALOG_GROUP_ID' => $new_catalog_group_id,
                'PRICE' => $price,
                'CURRENCY' => project\catalog::CURRENCY,
                'ELEMENT_IBLOCK_ID' => $productID,
                'VAT_INCLUDED' => "Y",
            ),
            'DISCOUNT' => array(
                'VALUE' => 0,
                'CURRENCY' => project\catalog::CURRENCY,
            ),
        );

    }





    static function clearElCaches( $product_id ){
        $el = tools\el::info($product_id);
        if( intval($el['ID']) > 0 ){
            $cache_name = \AOptima\Tools\el::EL_CACHE_NAME;
            BXClearCache(true, "/".$cache_name."/".$el['ID']."/");
            BXClearCache(true, "/".$cache_name."_by_code/".$el['IBLOCK_ID']."/".$el['CODE']."/");
        }
    }
    static function OnPriceAdd( $event ){
        $params = $event->getParameters();
        $fields = $params['fields'];
        static::clearElCaches( $fields['PRODUCT_ID'] );
    }
    static function OnPriceUpdate( $event ){
        $params = $event->getParameters();
        $fields = $params['fields'];
        static::clearElCaches( $fields['PRODUCT_ID'] );
    }
    static function OnBeforePriceDelete( $event ){
        $params = $event->getParameters();
        $fields = $params['fields'];
        static::clearElCaches( $fields['PRODUCT_ID'] );
    }





	
}