<? namespace AOptima\Project;
use AOptima\Project as project;


use Bitrix\Sale\Compatible\DiscountCompatibility;
use Bitrix\Sale\Basket;
use Bitrix\Sale\Discount\Gift as bxGift;
use Bitrix\Sale\Fuser;


class gift {




    static function getForProduct( $product_id ){
        \Bitrix\Main\Loader::includeModule('sale');
        \Bitrix\Main\Loader::includeModule('catalog');
        $list = [];
        if( intval($product_id) > 0 ){
            DiscountCompatibility::stopUsageCompatible();
            $giftManager = bxGift\Manager::getInstance();
            $potentialBuy = [
                'ID'                     => $product_id,
                'MODULE'                 => 'catalog',
                'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
                'QUANTITY'               => 1,
            ];
            $basket = Basket::loadItemsForFUser(Fuser::getId(), SITE_ID);
            $basketPseudo = $basket->copy();
            foreach ($basketPseudo as $basketItem) {   $basketItem->delete();   }
            $collections = $giftManager->getCollectionsByProduct($basketPseudo, $potentialBuy);
            foreach ($collections as $collection) {
                foreach ($collection as $gift) {    $list[] = $gift->getProductId();    }
            }
            DiscountCompatibility::revertUsageCompatible();
        }
        return $list;
    }




}