<?php

namespace AOptima\Project;
use AOptima\Project as project;


class favorites {

    const COOKIE_NAME = 'favorites';

    const MAX_CNT = 50;



    static function checkCookieIDS(){
        global $APPLICATION;
        $cur_items_str = $APPLICATION->get_cookie(static::COOKIE_NAME);
        $cur_items = [];
        if( strlen($cur_items_str) > 0 ){
            $cur_items = explode('|', $cur_items_str);
        }
        $new_items = static::checkIDs( $cur_items );
        global $APPLICATION;
        $APPLICATION->set_cookie( static::COOKIE_NAME, implode('|', $new_items), time() + 2592000 );
        return $new_items;
    }



    static function checkIDs( $ids ){
        $items = [];
        if( count($ids) > 0 ){
            foreach ( $ids as $key => $id ){
                $el = \AOptima\Tools\el::info($id);
                if( intval($el['ID']) > 0 ){    $items[] = $el['ID'];    }
            }
            $items = array_unique($items);
        }
        return $items;
    }


    /*static function getUserID(){
        global $APPLICATION;
        $fav_user_id = $APPLICATION->get_cookie('fav_user_id');
        if( strlen($fav_user_id) > 0 ){} else {
            $fav_user_id = md5(time());
            $APPLICATION->set_cookie('fav_user_id', $fav_user_id, time() + 2592000);
        }
    }*/




}