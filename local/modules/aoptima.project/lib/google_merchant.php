<?php

namespace AOptima\Project;
use AOptima\Project as project;


class google_merchant {

    const PATH = '/google_merchant/';
    const FILE_NAME = 'feed.php';



    // Отменено, поставили модуль
    static function create_feed (){

        \Bitrix\Main\Loader::includeModule('iblock');
        \Bitrix\Main\Loader::includeModule('aoptima.tools');

        if( !file_exists( $_SERVER["DOCUMENT_ROOT"].static::PATH ) ){
            mkdir($_SERVER["DOCUMENT_ROOT"].static::PATH, 0700);
        }

        $fp = @fopen($_SERVER["DOCUMENT_ROOT"].static::PATH.static::FILE_NAME, "wb");

        @fwrite($fp, '<? header("Content-Type: text/xml; charset=UTF-8");?>');
        @fwrite($fp, '<? echo "<"."?xml version=\"1.0\" encoding=\"UTF-8\"?".">"?>');
        @fwrite($fp, "<!DOCTYPE yml_catalog SYSTEM \"shops.dtd\">"."\n");

        @fwrite($fp, "<rss version=\"2.0\" xmlns:g=\"http://base.google.com/ns/1.0\">"."\n");

        @fwrite($fp, "<channel>"."\n");

        @fwrite($fp, "<title>Мебель для дома</title>"."\n");
        @fwrite($fp, "<link>http://www.examplemebel.ru</link>"."\n");
        @fwrite($fp, "<description>мебель, мебельный магазин, детская мебель, кухни, кухонные гарнитуры, прихожие, прихожая, спальни, кровати, компьютерный стол, кухонные уголки, шкафы-купе, модульные системы мебели, диваны, комоды, полки, кресла</description>"."\n");

        @fwrite($fp, "<items>\n");

        $obIblocks = \CIBlock::GetList(
            Array(),
            Array( 'TYPE' => '1c_catalog' ), true
        );
        while( $ib = $obIblocks->GetNext() ){

            $filter = Array(
            	"IBLOCK_ID" => $ib['ID'],
            	"ACTIVE" => "Y",
                'INCLUDE_SUBSECTIONS' => 'Y',
                'PROPERTY_STATUS_TOVARA' => '_%'
            );
            $fields = [
                "ID", "NAME", "DETAIL_PAGE_URL", "DETAIL_TEXT", "DETAIL_PICTURE",
                "PROPERTY_STATUS_TOVARA"
            ];
            $products = \CIBlockElement::GetList(
            	["SORT"=>"ASC"], $filter, false, ["nTopCount" => 10], $fields
            );
            while ($product = $products->GetNext()){

                @fwrite($fp, "<item>"."\n");

                @fwrite($fp, "<link>https://".\Bitrix\Main\Config\Option::get('main', 'server_name').$product['DETAIL_PAGE_URL']."</link>"."\n");

                @fwrite($fp, "<id>".$product['ID']."</id>"."\n");

                @fwrite($fp, "<title>".\AOptima\Tools\funcs::google_merchant_text2xml($product["~NAME"], true)."</title>\n");

                @fwrite($fp, "<description>".\AOptima\Tools\funcs::google_merchant_text2xml($product["~DETAIL_TEXT"], true)."</description>\n");

                $image_link = null;
                if( intval($product['DETAIL_PICTURE']) > 0 ){
                    $image_link = "https://".\Bitrix\Main\Config\Option::get('main', 'server_name').\CFile::GetPath($product['DETAIL_PICTURE']);
                }
                @fwrite($fp, "<image_link>".$image_link."</image_link>"."\n");

                @fwrite($fp, "</item>"."\n");
            }

        }

        @fwrite($fp, "</items>"."\n");
        @fwrite($fp, "</channel>"."\n");
        @fwrite($fp, "</rss>"."\n");

        @fclose($fp);

    }






}