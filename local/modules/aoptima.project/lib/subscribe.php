<?php
namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;




class subscribe {



    // Сохранение состояния подписки
    public function save( $email, $rubrics = array() ){
		\Bitrix\Main\Loader::includeModule('subscribe');
        $ok = true;
        $subscribers = \CSubscription::GetList(
            array("ID" => "ASC"),
            array( "EMAIL" => $email )
        );
        if ( $subscriber = $subscribers->GetNext() ){
            if( count($rubrics) > 0 ){
                // Вносим изменение в существующего подписчика
                $obSubscribe = new project\subscribe();
                $res =  $obSubscribe->update( $subscriber['ID'], array('RUB_ID' => $rubrics) );
                if( !$res ){    $ok = false;    }
            } else {
                $res = \CSubscription::Delete($subscriber['ID']);
                if( !$res ){    $ok = false;    }
            }
        } else if( count($rubrics) > 0 ){
            // Новый подписчик
            $obSubscribe = new project\subscribe();
            $res = $obSubscribe->add($email, $rubrics);
            if( !$res ){    $ok = false;    }
        }
        return $ok;
    }




    // Новый подписчик
    public function add( string $email, array $rubrics = array() ){
		\Bitrix\Main\Loader::includeModule('subscribe');
        $new_rubrics = array();
        foreach($rubrics as $id){    $new_rubrics[$id] = $id;    }
        $arFields = Array(
            "FORMAT" => "html",
            "EMAIL" => $email,
            "ACTIVE" => "Y",
            "CONFIRMED" => "Y",
            "RUB_ID" => $new_rubrics,
            "SEND_CONFIRM" => "N",
            "ALL_SITES" => "N"
        );
        $obSubscriber = new \CSubscription;
        $id = $obSubscriber->Add($arFields, 's1');
        if( intval($id) > 0 ){
            return true;
        } else {
            tools\logger::addError('Ошибка создания подписчика - '.$obSubscriber->LAST_ERROR);
            return false;
        }
    }



    // Редактирование подписчика
    public function update( int $subscriber_id, array $arFields ){
		\Bitrix\Main\Loader::includeModule('subscribe');
        $obSubscriber = new \CSubscription;
        $res = $obSubscriber->Update($subscriber_id, $arFields, 's1');
        if ($res){
            return true;
        } else {
            tools\logger::addError('Ошибка редактирования подписчика - ' . $obSubscriber->LAST_ERROR);
            return false;
        }
    }





    // getEmailRubrics
    public function getEmailRubrics( string $email ){
		\Bitrix\Main\Loader::includeModule('subscribe');
        $rubrics = array();
        $subscribers = \CSubscription::GetList(
            array("ID" => "ASC"),
            array(
                "CONFIRMED" => "Y",
                "ACTIVE" => "Y",
                "EMAIL" => $email
            )
        );
        while ( $subscriber = $subscribers->GetNext() ){
            $rubs = \CSubscription::GetRubricArray($subscriber['ID']);
            $rubrics = array_merge($rubrics, $rubs);
        }
        $rubrics = array_unique($rubrics);
        return $rubrics;
    }




    // rubricsList
    public function rubricsList( $show_all = false ){
		\Bitrix\Main\Loader::includeModule('subscribe');
        $list = array();
        $stop_codes = array('main');
        $arOrder = Array("SORT"=>"ASC", "NAME"=>"ASC");
        $arFilter = Array("ACTIVE" => "Y", "LID" => 's1');
        $rsRubric = \CRubric::GetList($arOrder, $arFilter);
        while( $arRubric = $rsRubric->GetNext() ){
            if( !in_array($arRubric['CODE'], $stop_codes) || $show_all ){
                $list[$arRubric['ID']] = $arRubric;
            }
        }
        return $list;
    }















}