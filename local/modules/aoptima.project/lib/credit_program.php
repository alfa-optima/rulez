<?php

namespace AOptima\Project;
use AOptima\Project as project;


class credit_program {

    const IBLOCK_ID = 50;



    static function getTree (){
        $sections = static::getSections();
        $elements = static::getList();
        foreach ( $elements as $el_id => $el ){
            $sections[$el['IBLOCK_SECTION_ID']]['items'][$el_id] = $el;
        }
        foreach ( $sections as $key => $section ){
            //if( !$section['items'] ){    unset($sections[$key]);    }
        }
        return $sections;
    }



    static function getList (){
        \Bitrix\Main\Loader::includeModule('iblock');
        $list = [];
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'credit_programs';
        $cache_path = '/credit_programs/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
        	$filter = Array(
        		"IBLOCK_ID" => static::IBLOCK_ID,
        		"ACTIVE" => "Y"
        	);
        	$fields = Array(
        	    "ID", "NAME", "IBLOCK_SECTION_ID", "PREVIEW_PICTURE", "PREVIEW_TEXT", "PROPERTY_SROK"
            );
        	$dbElements = \CIBlockElement::GetList(
        		array("SORT"=>"ASC"), $filter, false, false, $fields
        	);
        	while ($element = $dbElements->Fetch()){
                $list[$element['ID']] = $element;
        	}
        $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }



    static function getSections (){
        \Bitrix\Main\Loader::includeModule('iblock');
        $list = [];
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'credit_sections';
        $cache_path = '/credit_sections/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $filter = array(
            	"ACTIVE" => "Y",
            	"DEPTH_LEVEL" => 1,
            	"IBLOCK_ID" => static::IBLOCK_ID
            );
            $dbSections = \CIBlockSection::GetList( array("SORT"=>"ASC"), $filter, false );
            while ($section = $dbSections->Fetch()){
                $list[$section['ID']] = $section;
            }
            $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }



}