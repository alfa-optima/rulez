<?php

namespace AOptima\Project;
use AOptima\Project as project;


class product_review_file {

    use project\files___trait;

    const MAX_FILE_SIZE = 5242880;
    const MIN_WIDTH = 100;
    const MIN_HEIGHT = 100;

    const TEMP_UPLOAD_DIR = "productReviewFilesTMP";
    const TEMP_UPLOAD_PATH = "/upload/productReviewFilesTMP/";


    protected $FILES = false;
    protected $doc_root = false;
    protected $fileTypes = array(
        'jpg', 'jpeg', 'png'
    );
    protected $fileTypesInc = array();

    protected $file_name = false;
    protected $new_file_name = false;
    protected $file_path = false;
    protected $width = false;
    protected $height = false;
    protected $file_moved = false;
    protected $file_extension = false;





    // конструктор объекта
    function __construct($files = false){
        $this->doc_root = $_SERVER["DOCUMENT_ROOT"];
        // Перемещение файла во временную папку
        if ( $this->moveFile($files) ){
            $this->file_moved = true;
        }
    }


    public function getFileTypes(){
        return $this->fileTypes;
    }


    public function getFilePath(){
        return $this->file_path;
    }


    public function check(){
        $check_status = $this->checkAsPhoto();
        return $check_status;
    }




}