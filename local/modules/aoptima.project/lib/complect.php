<?php

namespace AOptima\Project;
use AOptima\Project as project;



class complect {


    const IBLOCK_ID = 123;




    //
    static function getProducts( $complect_id ){
        \Bitrix\Main\Loader::includeModule('catalog');
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        $list = [];   $sorts = [];
        $complects_child = \CCatalogProductSet::GetList(
            array(),
            array(
                'TYPE' => \CCatalogProductSet::TYPE_SET,
                '!SET_ID' => 0,
                'OWNER_ID' => $complect_id
            ), false, false,
            array( 'ID', 'OWNER_ID', 'SET_ID', 'TYPE', 'ITEM_ID', 'SORT' )
        );
        while ( $complect_item = $complects_child->Fetch() ){
            $list[$complect_item['ITEM_ID']] = $complect_item['ITEM_ID'];
            $sorts[$complect_item['ITEM_ID']] = $complect_item['SORT'];
        }
        if( count($list) > 0 ){
            array_multisort($sorts, SORT_ASC, SORT_NUMERIC, $list);
        }
        return $list;
    }




    static function getForProduct( $product_id ){
        $list = [];
        \Bitrix\Main\Loader::includeModule('catalog');
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        $complects = \CCatalogProductSet::GetList(
            array(),
            array(
                'TYPE' => \CCatalogProductSet::TYPE_SET,
                '!SET_ID' => 0,
                'ITEM_ID' => $product_id
            ), false, ['nTopCount' => 3],
            array( 'ID', 'OWNER_ID', 'SET_ID', 'TYPE', 'ITEM_ID' )
        );
        while ( $complect = $complects->Fetch() ){
            $el = \AOptima\Tools\el::info( $complect['OWNER_ID'] );
            if( intval($el['ID']) > 0 ){
                $items = [];   $sorts = [];
                $complects_child = \CCatalogProductSet::GetList(
                    array(),
                    array(
                        'TYPE' => \CCatalogProductSet::TYPE_SET,
                        '!SET_ID' => 0,
                        'OWNER_ID' => $complect['OWNER_ID']
                    ), false, false,
                    array( 'ID', 'OWNER_ID', 'SET_ID', 'TYPE', 'ITEM_ID', 'SORT' )
                );
                while ( $complect_item = $complects_child->Fetch() ){
                    $product = \AOptima\Tools\el::info($complect_item['ITEM_ID']);
                    if( intval($product['ID']) > 0 ){
                        $items[$complect_item['ITEM_ID']] = $product;
                        $sorts[$complect_item['ITEM_ID']] = $complect_item['SORT'];
                    }
                }
                if( count($items) > 0 ){
                    array_multisort($sorts, SORT_ASC, SORT_NUMERIC, $items);
                    $el['ITEMS'] = $items;
                }
                if( $el['ITEMS'] ){
                    $products_sum = 0;
                    foreach ( $el['ITEMS'] as $key => $item ){
                        $products_sum += $item['CATALOG_PRICE_'.project\catalog::RASSROCHKA_PRICE_ID];
                    }
                    if( $products_sum > $el['CATALOG_PRICE_'.project\catalog::RASSROCHKA_PRICE_ID] ){
                        $list[$el['ID']] = $el;
                    }
                }
            }
        }
        return $list;
    }




    // Сохранение товаров в комплект
    static function save( $complect_id, $items ){
        \Bitrix\Main\Loader::includeModule('catalog');
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        $result = false;
        if( intval($complect_id) > 0 ){
            if(
                is_array($items)
                &&
                count($items) > 0
            ){
                // Проверяем наличие комплекта
                $complects = \CCatalogProductSet::GetList(
                    array(),
                    array(
                        'TYPE' => \CCatalogProductSet::TYPE_SET,
                        'SET_ID' => 0,
                        'OWNER_ID' => $complect_id
                    ), false, false,
                    array( 'ID', 'OWNER_ID', 'SET_ID', 'TYPE' )
                );
                // Комплект уже есть
                if ($complect = $complects->Fetch()){

                    // Обновляем комплект
                    $fields = [
                        'ITEM_ID' => $complect_id,
                        'ACTIVE' => 'Y',
                        'TYPE' => 1
                    ];
                    foreach ( $items as $key => $item ){
                        $fields['ITEMS'][] = [
                            'ITEM_ID' => $item['PRODUCT_ID'],
                            'QUANTITY' => $item['QUANTITY'],
                            'SORT' => $item['SORT'],
                        ];
                    }
                    $result = \CCatalogProductSet::update( $complect['ID'], $fields );
                    \CCatalogProductSet::recalculateSetsByProduct($complect_id);

                // Комплекта ещё нет
                } else {
                    
                    // Создаём комплект
                    $fields = [
                        'ITEM_ID' => $complect_id,
                        'ACTIVE' => 'Y',
                        'TYPE' => 1
                    ];
                    foreach ( $items as $key => $item ){
                        $fields['ITEMS'][] = [
                            'ITEM_ID' => $item['PRODUCT_ID'],
                            'QUANTITY' => $item['QUANTITY'],
                            'SORT' => $item['SORT'],
                        ];
                    }
                    $result = \CCatalogProductSet::add( $fields );
                    \CCatalogProductSet::recalculateSetsByProduct($complect_id);
                }
            } else {
                // Проверяем наличие комплекта
                $complects = \CCatalogProductSet::GetList(
                    array(),
                    array(
                        'TYPE' => \CCatalogProductSet::TYPE_SET,
                        'SET_ID' => 0,
                        'OWNER_ID' => $complect_id
                    ), false, false,
                    array( 'ID', 'OWNER_ID', 'SET_ID', 'TYPE' )
                );
                // Комплект уже есть
                if ($complect = $complects->Fetch()){
                    // Удаляем комплект
                    $result = \CCatalogProductSet::delete( $complect['ID'] );
                }
            }
        }
        return $result;
    }




    static function get_items( $complect_id ){
        \Bitrix\Main\Loader::includeModule('catalog');
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        $items = [];  $cnt = 0;
        if( intval($complect_id) > 0 ){
            $el = \AOptima\Tools\el::info( $complect_id );
            if(
                intval($el['ID']) > 0
                &&
                is_array($el['PROPERTY_CML2_TRAITS_VALUE'])
                &&
                count($el['PROPERTY_CML2_TRAITS_VALUE']) > 0
            ){
                $is_complect = false;
                foreach ( $el['PROPERTY_CML2_TRAITS_VALUE'] as $key => $value ){
                    $value = strtolower(trim($value));
                    $type = $el['PROPERTY_CML2_TRAITS_DESCRIPTION'][$key];
                    if(
                        strtolower(trim($type)) == 'типноменклатуры'
                        &&
                        strtolower(trim($value)) == 'комплект'
                    ){   $is_complect = true;   }
                }
                if( $is_complect ){

                    foreach ( $el['PROPERTY_CML2_TRAITS_VALUE'] as $key => $value ){
                        $value = strtolower(trim($value));
                        $type = $el['PROPERTY_CML2_TRAITS_DESCRIPTION'][$key];
                        if(
                            strtolower(trim($type)) == 'комплектующее'
                            &&
                            strlen($value) > 0
                        ){

                            if( substr_count($value, '*') ){
                                $ar = explode('*', $value);
                                $xml_id = $ar[0];
                                $quantity = $ar[1];
                            } else {
                                $xml_id = $value;
                                $quantity = 1;
                            }

                            $catalog_iblocks = project\catalog::iblocks();
                            foreach ( $catalog_iblocks as $key => $iblock ){
                                if( $iblock['ID'] != project\catalog::GAME_ZONE_IBLOCK_ID ){
                                    $filter = Array(
                                        "IBLOCK_ID" => $iblock['ID'],
                                        "XML_ID" => $xml_id
                                    );
                                    $fields = Array( "ID", "IBLOCK_ID", "XML_ID" );
                                    $products = \CIBlockElement::GetList(
                                        array("SORT"=>"ASC"), $filter, false, array("nTopCount" => 1), $fields
                                    );
                                    if ($product = $products->GetNext()){  $cnt++;

                                        $items[] = [
                                            'PRODUCT_ID' => $product['ID'],
                                            'SORT' => $cnt * 100,
                                            'QUANTITY' => $quantity,
                                        ];

                                    }
                                }
                            }

                        }
                    }
                }
            }
        }
        return $items;
    }





//    static function get_items( $arFields ){
//        \Bitrix\Main\Loader::includeModule('aoptima.tools');
//        $items = [];  $cnt = 0;
//        if( intval($arFields['ID']) > 0 ){
//            $rekv_prop = \AOptima\Tools\prop::getByCode( static::IBLOCK_ID, 'CML2_TRAITS' );
//            foreach ( $arFields['PROPERTY_VALUES'] as $prop_id => $arProp ){
//                if( $prop_id == $rekv_prop['ID'] ){
//                    if(
//                        is_array($arProp)
//                        &&
//                        count($arProp) > 0
//                    ){
//                        $is_complect = false;
//                        foreach ( $arProp as $ar ){
//                            $value = strtolower(trim($ar['VALUE']));
//                            $description = strtolower(trim($ar['DESCRIPTION']));
//                            if(
//                                $description == 'типноменклатуры'
//                                &&
//                                $value == 'комплект'
//                            ){   $is_complect = true;   }
//                        }
//                        if( $is_complect ){
//                            foreach ( $arProp as $ar ){
//                                $value = strtolower(trim($ar['VALUE']));
//                                $description = strtolower(trim($ar['DESCRIPTION']));
//                                if(
//                                    $description == 'комплектующее'
//                                    &&
//                                    strlen($value) > 0
//                                ){
//                                    if( substr_count($value, '*') ){
//                                        $ar = explode('*', $value);
//                                        $xml_id = $ar[0];
//                                        $quantity = $ar[1];
//                                    } else {
//                                        $xml_id = $value;
//                                        $quantity = 1;
//                                    }
//                                    $catalog_iblocks = project\catalog::iblocks();
//                                    foreach ( $catalog_iblocks as $key => $iblock ){
//                                        $filter = Array(
//                                            "IBLOCK_ID" => $iblock['ID'],
//                                            "XML_ID" => $xml_id
//                                        );
//                                        $fields = Array( "ID", "IBLOCK_ID", "XML_ID" );
//                                        $products = \CIBlockElement::GetList(
//                                            array("SORT"=>"ASC"), $filter, false, array("nTopCount" => 1), $fields
//                                        );
//                                        if ($product = $products->GetNext()){  $cnt++;
//                                            $items[] = [
//                                                'PRODUCT_ID' => $product['ID'],
//                                                'SORT' => $cnt * 100,
//                                                'QUANTITY' => $quantity,
//                                            ];
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        return $items;
//    }





}