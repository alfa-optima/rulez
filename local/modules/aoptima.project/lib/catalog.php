<? namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');  use AOptima\Tools as tools;



class catalog {

    const IBLOCK_TYPE = '1c_catalog';

    const PRICE_ROUND = 2;

    const BASE_PRICE_CODE = 'BASE';
    const BASE_PRICE_ID = 1;

    const RASSROCHKA_PRICE_CODE = 'RASSROCHKA';
    const RASSROCHKA_PRICE_ID = 2;
    const RASSROCHKA_PROP_CODE = 'RASSROCHKA';

    const CREDIT_PRICE_CODE = 'CREDIT';
    const CREDIT_PRICE_ID = 4;
    const CREDIT_PROP_CODE = 'CREDIT';

    const ACTION_PROP_CODE = 'AKTSIYA';

    const CURRENCY = 'BYN';

    const GOODS_CNT = 18;
    const COMPLECTS_CNT = 10;
    const FILTER_NAME = 'catalog';
    const CACHE_TYPE = 'A';
    const CACHE_TIME = 36000000;
    const COMPLECTS_CACHE_TIME = 300;
    const COMPLECTS_CACHE_TYPE = 'A';

    const RASSROCHKA_XML_ID = 'rassrochka';

    const GAME_ZONE_SECT_CODE = 'game_zone';
    const GAME_ZONE_IBLOCK_ID = 126;

    const SOPUT_PROP_CODE = 'SOPUTSTVUYUSHCHIETOVARY';
    const SIMILAR_PROP_CODE = 'POKHOZHIETOVARY';

    const SHORT_LIST_PROP_CODE = 'SPISKISVOYSTV';

    const COURIER_MINSK_DELIVERY_PRICE = 5;

    const DETAIL_STOP_PROPS_IBLOCK_ID = 128;
    const VIDEO_PROP_CODE = 'YOUTUBE';

    const IS_RULIK_REC_PROP_CODE = 'RULIKREKOMENDUET';
    const IS_RULIK_REC_PROP_VALUE = 'Да';
    const IS_RULIK_REC_DESCR_PROP_CODE = 'OPISANIERULIKREKOMENDUET';
    const REC_PRODUCT_PROP_CODE = 'REKOMENDUEMYETOVARY';

    const IS_RULIK_REC_PAYMENT_TITLE_PROP_CODE = 'REKOMENDOVANNYYSPOSOBOPLATY';
    const IS_RULIK_REC_PAYMENT_DESCR_PROP_CODE = 'OPISANIEREKOMENDOVANNYYSPOSOBOPLATY';

    const STATUS_PROP_CODE = 'STATUS_TOVARA';





    static function detailCardStopProps(){
        $list = [];
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'detailCardStopProps';
        $cache_path = '/detailCardStopProps/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            \Bitrix\Main\Loader::includeModule('iblock');
            $filter = Array(
            	"IBLOCK_ID" => static::DETAIL_STOP_PROPS_IBLOCK_ID,
            	"ACTIVE" => "Y"
            );
            $fields = Array( "ID", "NAME" );
            $dbElements = \CIBlockElement::GetList(
            	array("SORT"=>"ASC"), $filter, false, false, $fields
            );
            while ($element = $dbElements->GetNext()){
                $list[] = $element['NAME'];
            }
            $obCache->EndDataCache(array('list' => $list));
        }
        $list = array_unique($list);
        return $list;
    }



    static $nal_statuses = [ 'В наличии' ];

    static $sort_fields = [
        'popular' => [
            'name' => 'Популярность',
            'code' => 'popular',
            'default_order' => 'asc'
        ],
        'price' => [
            'name' => 'Цена',
            'code' => 'price',
            'default_order' => 'asc'
        ],
        'new' => [
            'name' => 'Новизна',
            'code' => 'new',
            'default_order' => 'desc'
        ],
    ];

    static function defaultSortField(){
        $defaultSortField = static::$sort_fields[array_keys(static::$sort_fields)[0]];
        return $defaultSortField;
    }
    static function getSortField(){
        $sortFieldGET = trim(strip_tags( $_GET['sort_field'] ));
        $sortFieldPOST = trim(strip_tags( $_POST['sort_field'] ));
        if(
            strlen( $sortFieldGET ) > 0
            &&
            static::getCatSortFieldCode( $sortFieldGET )
        ){
            $sortField = $sortFieldGET;
        } else if(
            strlen( $sortFieldPOST ) > 0
            &&
            static::getCatSortFieldCode( $sortFieldPOST )
        ){
            $sortField = $sortFieldPOST;
        } else {
            $sortField = static::defaultSortField()['code'];
        }
//        global $APPLICATION;
//        $sortField = $APPLICATION->get_cookie('sort_field');
//        if( !$sortField ){
//            $sortField = static::defaultSortField()['code'];
//        }
        return $sortField;
    }
    static function getSortOrder(){
        $sortOrderGET = trim(strip_tags($_GET['sort_order']));
        $sortOrderPOST = trim(strip_tags($_POST['sort_order']));
        if( strlen( $sortOrderGET ) > 0 ){
            $sortOrder = $sortOrderGET;
        } else if( strlen( $sortOrderPOST ) > 0 ){
            $sortOrder = $sortOrderPOST;
        } else {
            $sortOrder = static::$sort_fields[static::getSortField()]['default_order'];
        }
//        global $APPLICATION;
//        $sortOrder = $APPLICATION->get_cookie('sort_order');
//        if( !$sortOrder ){
//            $sortOrder = static::defaultSortField()['default_order'];
//        }
        return $sortOrder;
    }
    static function getCatSortFieldCode( $fieldCode ){
        $codes = [
            'popular' => 'PROPERTY_POPULYARNOST',
            'price' => 'CATALOG_PRICE_'.static::BASE_PRICE_ID,
            'new' => 'ID'
        ];
        return $codes[$fieldCode];
    }



    // Каталожные инфоблоки
    static function iblocks(){
        \Bitrix\Main\Loader::includeModule('iblock');
        $iblocks = [];
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'catalog_iblocks';
        $cache_path = '/catalog_iblocks/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $obIblocks = \CIBlock::GetList(
                Array( 'SORT' => 'ASC' ),
                Array( 'TYPE' => static::IBLOCK_TYPE ), true
            );
            while($iblock = $obIblocks->GetNext()){
                $iblocks[$iblock['ID']] = $iblock;
            }
            $obCache->EndDataCache(array('iblocks' => $iblocks));
        }
        return $iblocks;
    }



    // Категория по XML_ID
    static function getSectionByXMLID( $sectXMLID ){
        $sect = false;
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'sectionByXMLID_'.$sectXMLID;
        $cache_path = '/sectionByXMLID/'.$sectXMLID.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            foreach ( static::iblocks() as $iblock ){
                $f = array(
                    "ACTIVE" => "Y",
                    "XML_ID" => $sectXMLID,
                    "IBLOCK_ID" => $iblock['ID']
                );
                $dbSections = \CIBlockSection::GetList(
                    array("SORT"=>"ASC"), $f, false
                );
                if ( $sect = $dbSections->GetNext() ){
                    return $sect;
                }
            }
        $obCache->EndDataCache(array('sect' => $sect));
        }
        return $sect;
    }



    // Каталожные категории 1 уровня
    static function firstLevelCategory( $iblock_id ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $category = false;
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'iblockFirstLevelCategory_'.$iblock_id;
        $cache_path = '/iblockFirstLevelCategory/'.$iblock_id.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $filter = array(
                "ACTIVE" => "Y",
                "DEPTH_LEVEL" => 1,
                "IBLOCK_ID" => $iblock_id
            );
            $dbSections = \CIBlockSection::GetList(
                array("SORT"=>"ASC"),
                $filter, false, array('UF_*'),
                array('nTopCount' => 1)
            );
            if ($section = $dbSections->GetNext()){
                $category = $section;
            }
            $obCache->EndDataCache(array('category' => $category));
        }
        return $category;
    }



    // Все категории инфоблока
    static function allIblockCategories( $iblock_id ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $list = [];
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'allIblockCategories_'.$iblock_id;
        $cache_path = '/allIblockCategories/'.$iblock_id.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $filter = array(
                "ACTIVE" => "Y",
                "IBLOCK_ID" => $iblock_id
            );
            $dbSections = \CIBlockSection::GetList(
                array("DEPTH_LEVEL" => "ASC", "SORT" => "ASC"),
                $filter, false, array('UF_MAIN_PAGE_MENU', 'UF_MAIN_MENU_SVG', 'UF_TOP_MENU', 'UF_SHORT_TITLE')
            );
            while ($section = $dbSections->GetNext()){
                $list[] = $section;
            }
            $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }



    static function product_photo_block( $el ){
        global $APPLICATION;
        ob_start();
            // product_photos
            $APPLICATION->IncludeComponent(
                "aoptima:product_photos", "",
                array('el' => $el)
            );
            $product_photos = ob_get_contents();
        ob_end_clean();
        return $product_photos;
    }



    static function product_services_block( $el ){
        global $APPLICATION;
        ob_start();
        // product_services
        $APPLICATION->IncludeComponent(
            "aoptima:product_services", "",
            array('el' => $el)
        );
        $product_services = ob_get_contents();
        ob_end_clean();
        return $product_services;
    }



    // Проверка наличия пользовательского свойства раздела инфоблока
    static function getSectUserField($iblock_id, $field_name){
        $userFields = \CUserTypeEntity::GetList(
            array($by=>$order),
            array(
                'ENTITY_ID' => 'IBLOCK_'.$iblock_id.'_SECTION',
                'FIELD_NAME' => $field_name
            )
        );
        if($userField = $userFields->Fetch()){
            return $userField;
        }
        return false;
    }



    // Создание пользовательских полей типа строка (string)
    // Пример:
    // project\catalog::addSectUserFieldsString( 'UF_SHORT_TITLE', 'Название (сокращённая форма)', '' );
    static function addSectUserFieldsString(
        $field_name,
        $ru_name,
        $default_value
    ){
        // Каталожные инфоблоки
        $iblocks = static::iblocks();
        // Перебираем инфоблоки
        foreach ( $iblocks as $iblock_id => $iblock ){
            // Проверка наличия поля
            $userField = static::getSectUserField($iblock_id, $field_name);
            if( intval($userField['ID']) > 0 ){
                // Обновим
                $oUserTypeEntity = new \CUserTypeEntity();
                $oUserTypeEntity->Update(
                    $userField['ID'],
                    array(
                        'EDIT_FORM_LABEL'   => array(
                            'ru'    => $ru_name,
                            'en'    => $ru_name
                        ),
                        'LIST_COLUMN_LABEL' => array(
                            'ru'    => $ru_name,
                            'en'    => $ru_name
                        ),
                        'LIST_FILTER_LABEL' => array(
                            'ru'    => $ru_name,
                            'en'    => $ru_name
                        )
                    )
                );
                echo "Инфоблок \"".$iblock['NAME']."\" [".$iblock['ID']."] - UPDATED;"."\n";
            } else {
                // Создание
                $oUserTypeEntity    = new \CUserTypeEntity();
                $aUserFields = array(
                    'ENTITY_ID'         => 'IBLOCK_'.$iblock_id.'_SECTION',
                    'FIELD_NAME'        => $field_name,
                    'USER_TYPE_ID'      => 'string',
                    'XML_ID'            => '',
                    'SORT'              => 500,
                    'MULTIPLE'          => 'N',
                    'MANDATORY'         => 'N',
                    'SHOW_FILTER'       => 'N',
                    'SHOW_IN_LIST'      => 'Y',
                    'EDIT_IN_LIST'      => 'Y',
                    'IS_SEARCHABLE'     => 'N',
                    'SETTINGS'          => array(
                        'DEFAULT_VALUE' => $default_value
                    ),
                    'EDIT_FORM_LABEL'   => array(
                        'ru'    => $ru_name,
                        'en'    => $ru_name
                    ),
                    'LIST_COLUMN_LABEL' => array(
                        'ru'    => $ru_name,
                        'en'    => $ru_name
                    ),
                    'LIST_FILTER_LABEL' => array(
                        'ru'    => $ru_name,
                        'en'    => $ru_name
                    )
                );
                $iUserFieldId = $oUserTypeEntity->Add( $aUserFields );
                if( intval($iUserFieldId) > 0 ){
                    echo "Инфоблок \"".$iblock['NAME']."\" [".$iblock['ID']."] - ОК;"."\n";
                } else {
                    echo "Инфоблок \"".$iblock['NAME']."\" [".$iblock['ID']."] - ERROR;"."\n";
                }
            }
        }
    }




    // Создание пользовательских полей типа Да/Нет (boolean)
    // Пример:
    // project\catalog::addSectUserFieldsBoolean( 'UF_TOP_MENU', 'Включить в верхнее меню', 0 );
    static function addSectUserFieldsBoolean(
        $field_name,
        $ru_name,
        $default_value
    ){
        // Каталожные инфоблоки
        $iblocks = static::iblocks();
        // Перебираем инфоблоки
        foreach ( $iblocks as $iblock_id => $iblock ){
            // Проверка наличия поля
            $userField = static::getSectUserField($iblock_id, $field_name);
            if( intval($userField['ID']) > 0 ){
                // Обновим
                $oUserTypeEntity = new \CUserTypeEntity();
                $oUserTypeEntity->Update(
                    $userField['ID'],
                    array(
                        'EDIT_FORM_LABEL'   => array(
                            'ru'    => $ru_name,
                            'en'    => $ru_name
                        ),
                        'LIST_COLUMN_LABEL' => array(
                            'ru'    => $ru_name,
                            'en'    => $ru_name
                        ),
                        'LIST_FILTER_LABEL' => array(
                            'ru'    => $ru_name,
                            'en'    => $ru_name
                        )
                    )
                );
                echo "Инфоблок \"".$iblock['NAME']."\" [".$iblock['ID']."] - UPDATED;"."\n";
            } else {
                // Создание
                $oUserTypeEntity    = new \CUserTypeEntity();
                $aUserFields = array(
                    'ENTITY_ID'         => 'IBLOCK_'.$iblock_id.'_SECTION',
                    'FIELD_NAME'        => $field_name,
                    'USER_TYPE_ID'      => 'boolean',
                    'XML_ID'            => '',
                    'SORT'              => 500,
                    'MULTIPLE'          => 'N',
                    'MANDATORY'         => 'N',
                    'SHOW_FILTER'       => 'N',
                    'SHOW_IN_LIST'      => 'Y',
                    'EDIT_IN_LIST'      => 'Y',
                    'IS_SEARCHABLE'     => 'N',
                    'SETTINGS'          => array(
                        'DEFAULT_VALUE' => $default_value,
                        'DISPLAY' => 'CHECKBOX',
                    ),
                    'EDIT_FORM_LABEL'   => array(
                        'ru'    => $ru_name,
                        'en'    => $ru_name
                    ),
                    'LIST_COLUMN_LABEL' => array(
                        'ru'    => $ru_name,
                        'en'    => $ru_name
                    ),
                    'LIST_FILTER_LABEL' => array(
                        'ru'    => $ru_name,
                        'en'    => $ru_name
                    )
                );
                $iUserFieldId = $oUserTypeEntity->Add( $aUserFields );
                if( intval($iUserFieldId) > 0 ){
                    echo "Инфоблок \"".$iblock['NAME']."\" [".$iblock['ID']."] - ОК;"."\n";
                } else {
                    echo "Инфоблок \"".$iblock['NAME']."\" [".$iblock['ID']."] - ERROR;"."\n";
                }
            }
        }
    }


    // Создание пользовательских полей типа HTML
    // Пример:
    // project\catalog::addSectUserFieldsHTML( 'UF_TOP_MENU_SVG', 'SVG для верхнего меню', '' );
    static function addSectUserFieldsHTML(
        $field_name,
        $ru_name,
        $default_value
    ){
        // Каталожные инфоблоки
        $iblocks = static::iblocks();
        // Перебираем инфоблоки
        foreach ( $iblocks as $iblock_id => $iblock ){
            // Проверка наличия поля
            $userField = static::getSectUserField($iblock_id, $field_name);
            if( intval($userField['ID']) > 0 ){
                // Обновим
                $oUserTypeEntity = new \CUserTypeEntity();
                $oUserTypeEntity->Update(
                    $userField['ID'],
                    array(
                        'EDIT_FORM_LABEL'   => array(
                            'ru'    => $ru_name,
                            'en'    => $ru_name
                        ),
                        'LIST_COLUMN_LABEL' => array(
                            'ru'    => $ru_name,
                            'en'    => $ru_name
                        ),
                        'LIST_FILTER_LABEL' => array(
                            'ru'    => $ru_name,
                            'en'    => $ru_name
                        )
                    )
                );
                echo "Инфоблок \"".$iblock['NAME']."\" [".$iblock['ID']."] - UPDATED;"."\n";
            } else {
                // Создание
                $oUserTypeEntity    = new \CUserTypeEntity();
                $aUserFields = array(
                    'ENTITY_ID'         => 'IBLOCK_'.$iblock_id.'_SECTION',
                    'FIELD_NAME'        => $field_name,
                    'USER_TYPE_ID'      => 'customhtml',
                    'XML_ID'            => '',
                    'SORT'              => 500,
                    'MULTIPLE'          => 'N',
                    'MANDATORY'         => 'N',
                    'SHOW_FILTER'       => 'N',
                    'SHOW_IN_LIST'      => 'Y',
                    'EDIT_IN_LIST'      => 'Y',
                    'IS_SEARCHABLE'     => 'N',
                    'SETTINGS'          => array(
                        'DEFAULT_VALUE' => $default_value
                    ),
                    'EDIT_FORM_LABEL'   => array(
                        'ru'    => $ru_name,
                        'en'    => $ru_name
                    ),
                    'LIST_COLUMN_LABEL' => array(
                        'ru'    => $ru_name,
                        'en'    => $ru_name
                    ),
                    'LIST_FILTER_LABEL' => array(
                        'ru'    => $ru_name,
                        'en'    => $ru_name
                    )
                );
                $iUserFieldId = $oUserTypeEntity->Add( $aUserFields );
                if( intval($iUserFieldId) > 0 ){
                    echo "Инфоблок \"".$iblock['NAME']."\" [".$iblock['ID']."] - ОК;"."\n";
                } else {
                    echo "Инфоблок \"".$iblock['NAME']."\" [".$iblock['ID']."] - ERROR;"."\n";
                }
            }
        }
    }



    // Лучшие цены
    static $bestPricesMaxCnt = 5;
    const BEST_PRICE_PROP_CODE = 'LUCHSHAYA_TSENA';


    static function bestPricesIblocks( ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $list = [];
        $params['settings'] = [
            'PROP_CODE' => static::BEST_PRICE_PROP_CODE,
            'PROP_VALUE' => 'Да',
            'MAX_CNT' => static::$bestPricesMaxCnt
        ];
        $params['fields'] = Array( "ID", "PROPERTY_".$params['settings']['PROP_CODE'] );
        $params_hash = md5(json_encode($params));
        // Каталожные инфоблоки
        $iblocks = static::iblocks();
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 60*60;
        $cache_id = 'catalog_best_prices_iblocks_'.$params_hash;
        $cache_path = '/catalog_best_prices_iblocks/'.$params_hash.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            // Перебираем инфоблоки
            foreach ( $iblocks as $iblock_id => $iblock ){
                $prop = tools\prop::getByCode($iblock_id, project\catalog::BEST_PRICE_PROP_CODE);
                if( intval($prop['ID']) > 0 ){
                    if( $iblock['ID'] != project\complect::IBLOCK_ID ){
                        $filter = Array(
                            "IBLOCK_ID" => $iblock['ID'],
                            "ACTIVE" => "Y",
                            "PROPERTY_".$params['settings']['PROP_CODE']."_VALUE" => $params['settings']['PROP_VALUE']
                        );
                        $item = [
                            'ID' => $iblock['ID'],
                            'NAME' => $iblock['NAME'],
                            'IDS' => []
                        ];
                        $dbElements = \CIBlockElement::GetList(
                            array("RAND" => "ASC"), $filter, false,
                            array("nTopCount" => $params['settings']['MAX_CNT']), $params['fields']
                        );
                        while ($element = $dbElements->GetNext()){
                            $item['IDS'][] = $element['ID'];
                        }
                        if( count($item['IDS']) > 0 ){    $list[$item['ID']] = $item;    }
                    }
                }
            }
            $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }



    static function bestPricesCategories( ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $products = [];    $categories = [];
        $params['settings'] = [
            'PROP_CODE' => static::BEST_PRICE_PROP_CODE,
            'PROP_VALUE' => 'Да'
        ];
        $params['fields'] = Array( "ID", "PROPERTY_".$params['settings']['PROP_CODE'] );
        $params_hash = md5(json_encode($params));
        // Каталожные инфоблоки
        $iblocks = static::iblocks();
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 60*60;
        $cache_id = 'catalog_best_prices_categories_'.$params_hash;
        $cache_path = '/catalog_best_prices_categories/'.$params_hash.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            // Перебираем инфоблоки
            foreach ( $iblocks as $iblock_id => $iblock ){
                $prop = tools\prop::getByCode($iblock_id, project\catalog::BEST_PRICE_PROP_CODE);
                if( intval($prop['ID']) > 0 ){
                    if( $iblock['ID'] != project\complect::IBLOCK_ID ){
                        $filter = Array(
                            "IBLOCK_ID" => $iblock['ID'],
                            "ACTIVE" => "Y",
                            "PROPERTY_".$params['settings']['PROP_CODE']."_VALUE" => $params['settings']['PROP_VALUE']
                        );
                        $dbElements = \CIBlockElement::GetList(
                            ["RAND" => "ASC"], $filter, false, false, $params['fields']
                        );
                        while ($element = $dbElements->GetNext()){
                            $products[$element['ID']] = $element;
                        }
                    }
                }
            }
            $obCache->EndDataCache(array('products' => $products));
        }
        if( count($products) > 0 ){
            foreach ( $products as $product ){
                $sect = tools\el::sections($product['ID'])[0];
                if( !$categories[$sect['ID']] ){    $categories[$sect['ID']] = $sect;    }
                $categories[$sect['ID']]['PRODUCTS'][] = $product['ID'];
            }
        }
        return $categories;
    }



    // Идеи для подарков
    static $giftIdeasMaxCnt = 4;
    const GIFT_IDEAS_PRICE_PROP_CODE = 'IDEI_DLYA_PODARKOV';

    static function giftIdeasIblocks(){
        \Bitrix\Main\Loader::includeModule('iblock');
        $list = [];
        $params['settings'] = [
            'PROP_CODE' => static::GIFT_IDEAS_PRICE_PROP_CODE,
            'PROP_VALUE' => 'Да',
            'MAX_CNT' => static::$giftIdeasMaxCnt
        ];
        $params['fields'] = Array( "ID", "PROPERTY_".$params['settings']['PROP_CODE'] );
        $params_hash = md5(json_encode($params));
        // Каталожные инфоблоки
        $iblocks = static::iblocks();
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 60*60;
        $cache_id = 'catalog_gift_ideas_iblocks_'.$params_hash;
        $cache_path = '/catalog_gift_ideas_iblocks/'.$params_hash.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            // Перебираем инфоблоки
            foreach ( $iblocks as $iblock_id => $iblock ){
                $prop = tools\prop::getByCode($iblock_id, project\catalog::GIFT_IDEAS_PRICE_PROP_CODE);
                if( intval($prop['ID']) > 0 ){
                    if( $iblock['ID'] != project\complect::IBLOCK_ID ){
                        $filter = Array(
                            "IBLOCK_ID" => $iblock['ID'],
                            "ACTIVE" => "Y",
                            "PROPERTY_".$params['settings']['PROP_CODE']."_VALUE" => $params['settings']['PROP_VALUE']
                        );
                        $item = [
                            'ID' => $iblock['ID'],
                            'NAME' => $iblock['NAME'],
                            'IDS' => []
                        ];
                        $dbElements = \CIBlockElement::GetList(
                            array("RAND" => "ASC"), $filter, false,
                            array("nTopCount" => $params['settings']['MAX_CNT']), $params['fields']
                        );
                        while ($element = $dbElements->GetNext()){
                            $item['IDS'][] = $element['ID'];
                        }
                        if( count($item['IDS']) > 0 ){
                            $list[$item['ID']] = $item;
                        }
                    }
                }
            }
            $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }


    static function giftIdeasCategories(){
        \Bitrix\Main\Loader::includeModule('iblock');
        $products = [];    $categories = [];
        $params['settings'] = [
            'PROP_CODE' => static::GIFT_IDEAS_PRICE_PROP_CODE,
            'PROP_VALUE' => 'Да'
        ];
        $params['fields'] = Array( "ID", "PROPERTY_".$params['settings']['PROP_CODE'] );
        $params_hash = md5(json_encode($params));
        // Каталожные инфоблоки
        $iblocks = static::iblocks();
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 60*60;
        $cache_id = 'catalog_gift_ideas_categories_'.$params_hash;
        $cache_path = '/catalog_gift_ideas_categories/'.$params_hash.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            // Перебираем инфоблоки
            foreach ( $iblocks as $iblock_id => $iblock ){
                $prop = tools\prop::getByCode($iblock_id, project\catalog::GIFT_IDEAS_PRICE_PROP_CODE);
                if( intval($prop['ID']) > 0 ){
                    if( $iblock['ID'] != project\complect::IBLOCK_ID ){
                        $filter = Array(
                            "IBLOCK_ID" => $iblock['ID'],
                            "ACTIVE" => "Y",
                            "PROPERTY_".$params['settings']['PROP_CODE']."_VALUE" => $params['settings']['PROP_VALUE']
                        );
                         $dbElements = \CIBlockElement::GetList(
                            ["RAND" => "ASC"], $filter, false, false, $params['fields']
                        );
                        while ($element = $dbElements->GetNext()){
                            $products[$element['ID']] = $element;
                        }
                    }
                }
            }
            $obCache->EndDataCache(array('products' => $products));
        }
        if( count($products) > 0 ){
            foreach ( $products as $product ){
                $sect = tools\el::sections($product['ID'])[0];
                if( !$categories[$sect['ID']] ){    $categories[$sect['ID']] = $sect;    }
                $categories[$sect['ID']]['PRODUCTS'][] = $product['ID'];
            }
        }
        return $categories;
    }


    static function getDeliveryPrice( $ds, $bInfo ){
        $delivPrice = 0;
        $arBasket = $bInfo['arBasket'];
        // Курьер по РБ
        if( $ds['XML_ID'] == project\order::COURIER_DS_XMLID ){
            $deliv_cost_file = $_SERVER['DOCUMENT_ROOT'].'/deliverycost.csv';
            $costs = [];
            if (file_exists($deliv_cost_file)){
                $rows = array();
                $csvFile = new \CCSVData('R', false);
                $csvFile->LoadFile($deliv_cost_file);
                $csvFile->SetDelimiter(';');
                $cnt = 0;
                while ($arRes = $csvFile->Fetch()){ $cnt++;
                    if( $cnt != 1 ){
                        $costs[iconv('windows-1251', 'UTF-8', $arRes[0])] = iconv('windows-1251', 'UTF-8', $arRes[1]);
                    }
                }
            }
            $prices = [];
            foreach ( $arBasket as $bItem ){
                $el = $bItem->el;
                $sect_id = tools\el::sections($el['ID'])[0]['ID'];
                $chain = tools\section::chain($sect_id);
                $chain = array_reverse($chain);
                $price = false;
                foreach ( $chain as $sect ){
                    foreach ( $costs as $xml_id => $cost ){
                        if( $xml_id == $sect['XML_ID'] ){     $price = $cost;     }
                    }
                }
                if( !$price ){
                    $price = \Bitrix\Main\Config\Option::get('aoptima.project', 'DEFAULT_COURIER_DELIERY_PRICE');
                }
                $price = str_replace([' '], '', $price);
                $price = str_replace([','], '.', $price);
                $prices[] = $price;
            }
            arsort($prices);
            $delivPrice = $prices[0];

        // Курьер по Минску
        } else if( $ds['XML_ID'] == project\order::COURIER_MINSK_DS_XMLID ){

            $delivPrice = \Bitrix\Main\Config\Option::get('aoptima.project', 'COURIER_MINSK_DELIVERY_PRICE');
            $porog_order_sum = \Bitrix\Main\Config\Option::get('aoptima.project', 'COURIER_MINSK_POROG_SUM');
            if( $bInfo['basket_disc_sum'] > $porog_order_sum ){    $delivPrice = 0;    }

        } else {
            $delivPrice = $bInfo['calcInfo']['DELIVERY_DISC_PRICE'];
        }
        return $delivPrice;
    }



    static function getPropGroups($el){
        $prop_groups = [];
        if ( strlen($el['IBLOCK_EXTERNAL_ID']) > 0 ){
            $file_path = $_SERVER['DOCUMENT_ROOT'].'/upload/propGroup/'.$el['IBLOCK_EXTERNAL_ID'].'.csv';
            if (
                strlen($el['IBLOCK_EXTERNAL_ID']) > 0
                &&
                file_exists($file_path)
            ){
                $rows = array();
                $csvFile = new \CCSVData('R', false);
                $csvFile->LoadFile($file_path);
                $csvFile->SetDelimiter(';');
                $cnt = 0;
                while ($arRes = $csvFile->Fetch()){ $cnt++;
                    if( $cnt != 1 ){
                        $prop_groups[iconv('windows-1251', 'UTF-8', $arRes[0])] = iconv('windows-1251', 'UTF-8', $arRes[1]);
                    }
                }
            }
        }
        asort($prop_groups);
        return $prop_groups;
    }



    static function getFilterProps( $section_xml_id ){
        $props = [];
        if ( strlen($section_xml_id) > 0 ){
            $file_path = $_SERVER['DOCUMENT_ROOT'].'/upload/filterProps/'.$section_xml_id.'.csv';
            if ( file_exists($file_path) ){
                $rows = array();
                $csvFile = new \CCSVData('R', false);
                $csvFile->LoadFile($file_path);
                $csvFile->SetDelimiter(';');
                $cnt = 0;
                while ($arRes = $csvFile->Fetch()){ $cnt++;
                    if( $cnt != 1 ){
                        $props[] = iconv('windows-1251', 'UTF-8', $arRes[0]);
                    }
                }
            }
        }
        $props = array_unique($props);
        return $props;
    }





    static function hasNextProducts( $filter, $list_cnt = false ){
        global $USER;
        \Bitrix\Main\Loader::includeModule('iblock');
        $fields = [ "ID" ];
        foreach ( $filter as $key => $value ){
            if( substr_count( $key, "PROPERTY" ) ){
                $fields[] = preg_replace('/[^0-9_A-Z]/', '', $key);
            } else {
                $fields[] = $key;
            }
        }
        
        if( intval($list_cnt) > 0 ){
            $dbElements = \CIBlockElement::GetList(
                [ "ID" => "ASC" ], $filter, false, [ "nTopCount" => ($list_cnt + 1) ], $fields
            );
            return ($dbElements->SelectedRowsCount()==($list_cnt + 1))?'Y':'N';
        } else {
            $dbElements = \CIBlockElement::GetList(
                [ "ID" => "ASC" ], $filter, false, [ "nTopCount" => 1 ], $fields
            );
            return $dbElements->GetNext()?'Y':'N';
        }
    }





}