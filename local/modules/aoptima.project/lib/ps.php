<?php

namespace AOptima\Project;
use AOptima\Project as project;



class ps {


    const GROUPS_IBLOCK_ID = 52;

    static $stop_ids = [1,2];



    // Группы вариантов оплаты
    static function groups( $ds_id, $ps_list = false ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $groups = [];
        if( !$ps_list ){
            $ps_list = static::getFilteredtList( $ds_id );
        }
        $filter = Array(
        	"IBLOCK_ID" => static::GROUPS_IBLOCK_ID,
        	"ACTIVE" => "Y"
        );
        $fields = Array( "ID", "NAME", "CODE", "PREVIEW_TEXT", "PROPERTY_PS", 'TITLE' );
        $ob_groups = \CIBlockElement::GetList(
        	array("SORT"=>"ASC"), $filter, false, false, $fields
        );
        while ($group = $ob_groups->Fetch()){
            $payments = [];
            if(
                is_array($group['PROPERTY_PS_VALUE'])
                &&
                count($group['PROPERTY_PS_VALUE']) > 0
            ){
                foreach ( $group['PROPERTY_PS_VALUE'] as $ps_id ){
                    if( $ps_list[$ps_id] ){
                        $payments[$ps_id] = $ps_list[$ps_id];
                    }
                }
            }
            if( count($payments) > 0 ){
                $groups[$group['ID']] = [
                    'ID' => $group['ID'],
                    'NAME' => $group['NAME'],
                    'CODE' => $group['CODE'],
                    'DESCRIPTION' => $group['PREVIEW_TEXT'],
                    'PAYMENTS' => $payments,
                ];
            }
        }
        return $groups;
    }



    static function getByID( $id ){
        \Bitrix\Main\Loader::includeModule('sale');
        $filter = Array( "ACTIVE"=>"Y", "ID" => $id );
        $db_ptype = \CSalePaySystem::GetList(
            Array(
                "SORT"=>"ASC",
                "PSA_NAME"=>"ASC"
            ),
            $filter, false, false,
            array('ID', 'NAME', 'XML_ID', 'CODE', 'PSA_LOGOTIP', 'DESCRIPTION', 'PSA_NAME')
        );
        while ($ps = $db_ptype->GetNext()){
            return $ps;
        }
        return false;
    }



    // Перечень вариантов оплаты
    static function getList( $person_type_id = 1 ){
        \Bitrix\Main\Loader::includeModule('sale');
        $list = array();
        $filter = Array( "ACTIVE"=>"Y", "!ID" => static::$stop_ids );
        if( intval($person_type_id) > 0 ){
            $filter['PSA_PERSON_TYPE_ID'] = $person_type_id;
        }
        $db_ptype = \CSalePaySystem::GetList(
            Array(
                "SORT"=>"ASC",
                "PSA_NAME"=>"ASC"
            ),
            $filter, false, false,
            array('ID', 'NAME', 'XML_ID', 'CODE', 'PSA_LOGOTIP', 'DESCRIPTION', 'PSA_NAME')
        );
        while ($ps = $db_ptype->GetNext()){
            $list[$ps['ID']] = $ps;
        }
        return  $list;
    }



    static function getFilteredtList( $ds_id, $person_type_id = false ){
        \Bitrix\Main\Loader::includeModule('sale');
        $list = array();
        $all_ps = static::getList( $person_type_id );
        $res = \CSaleDelivery::GetDelivery2PaySystem();
        while ($item = $res->GetNext()){
            if(
                $ds_id == $item['DELIVERY_ID']
                &&
                $all_ps[$item['PAYSYSTEM_ID']]
            ){
                $ps = $all_ps[$item['PAYSYSTEM_ID']];
                $list[$ps['ID']] = $ps;
            }
        }
        return  $list;
    }







}