<?php

namespace AOptima\Project;
use AOptima\Project as project;
use AOptima\Tools as tools;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;


class vozvrat_form {


    const TEMP_UPLOAD_DIR = "vozvratFormFilesTMP";
    const TEMP_UPLOAD_PATH = "/upload/vozvratFormFilesTMP/";


    protected $formFields;



    function __construct($files = false) {


        $this->formFields = array(
            'fio' => array(
                'NAME' => 'ФИО / Организация',
                'CODE' => 'fio',
                'required' => true,
            ),
            'address' => array(
                'NAME' => 'Адрес',
                'CODE' => 'address',
                'required' => true,
            ),
            'contact_phone' => array(
                'NAME' => 'Контактный номер телефона',
                'CODE' => 'contact_phone',
                'check_reg' => '^[0-9()+ -]{1,99}$',
                'check_error' => 'В номере телефона допускаются только цифры, а также символы "+", "-", пробел и круглые скобки!',
                'required' => true,
            ),
            'order_phone' => array(
                'NAME' => 'Номер телефона, на который был оформлен заказ',
                'CODE' => 'order_phone',
                'check_reg' => '^[0-9()+ -]{1,99}$',
                'check_error' => 'В номере телефона допускаются только цифры, а также символы "+", "-", пробел и круглые скобки!',
                'required' => true,
            ),
            'order_date' => array(
                'NAME' => 'Дата покупки',
                'CODE' => 'order_date',
                'required' => true,
            ),
            'product_model' => array(
                'NAME' => 'Наименование и модель изделия',
                'CODE' => 'product_model',
                'required' => true,
            ),
            'problem_description' => array(
                'NAME' => 'Описание проблемы',
                'CODE' => 'problem_description',
                'TYPE' => 'textarea',
                'required' => true,
            ),
            'sc_info' => array(
                'NAME' => 'Информация о сервисном центре',
                'CODE' => 'sc_info',
                'TYPE' => 'textarea',
                'required' => false,
            ),
            'defect_photos' => array(
                'NAME' => 'Фотографии дефекта (jpeg, png)',
                'CODE' => 'defect_photos',
                'TYPE' => 'file',
                'FILE_TYPES' => ['image/jpeg', 'image/png'],
                'multiple' => true,
                'required' => false,
            ),
            'garant_scan' => array(
                'NAME' => 'Скан гарантийного талона (jpeg, png)',
                'CODE' => 'garant_scan',
                'TYPE' => 'file',
                'FILE_TYPES' => ['image/jpeg', 'image/png'],
                'required' => false,
            ),
            'zakluchenie_sc' => array(
                'NAME' => 'Заключение сервисного центра (pdf)',
                'CODE' => 'zakluchenie_sc',
                'TYPE' => 'file',
                'FILE_TYPES' => ['application/pdf'],
                'required' => false,
            ),
        );

    }



    public function getFormFields(){
        return $this->formFields;
    }



    public function send(){

        \Bitrix\Main\Loader::includeModule('iblock');
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';

        $response = json_encode([
            'status' => 'error',
            'text' => 'Ошибка отправки формы'
        ]);

        if( count($_POST) > 0 ){

            $vozvratForm = new project\vozvrat_form();
            $fields = $vozvratForm->getFormFields();

            $errors = [];
            $files = [];

            foreach ( $_FILES as $input_name => $arFile ){
                if( is_array($arFile['name']) ){
                    foreach ( $arFile['name'] as $k => $file_name ){
                        if(
                            $arFile['size'][$k] > 0
                            &&
                            strlen($file_name) > 0
                        ){
                            if( $arFile['error'][$k] == 0 ){
                                $files[$input_name][] = [
                                    'name' => $arFile['name'][$k],
                                    'type' => $arFile['type'][$k],
                                    'tmp_name' => $arFile['tmp_name'][$k],
                                    'error' => $arFile['error'][$k],
                                    'size' => $arFile['size'][$k],
                                ];
                                if(
                                    is_array($fields[$input_name]['FILE_TYPES'])
                                    &&
                                    !in_array($arFile['type'][$k], $fields[$input_name]['FILE_TYPES'])
                                ){
                                    $errors[] = 'Поле "'.$fields[$input_name]['NAME'].'": не допустимый формат файла "'.strip_tags($file_name).'"';
                                }
                            } else {
                                $errors[] = 'Ошибка загрузки файла "'.strip_tags($file_name).'"';
                            }
                        }
                    }
                } else {
                    if(
                        $arFile['size'] > 0
                        &&
                        strlen($arFile['name']) > 0
                    ){
                        if( $arFile['error'] == 0 ){
                            $files[$input_name][] = [
                                'input_name' => $input_name,
                                'name' => $arFile['name'],
                                'type' => $arFile['type'],
                                'tmp_name' => $arFile['tmp_name'],
                                'error' => $arFile['error'],
                                'size' => $arFile['size'],
                            ];
                            if(
                                is_array($fields[$input_name]['FILE_TYPES'])
                                &&
                                !in_array($arFile['type'], $fields[$input_name]['FILE_TYPES'])
                            ){
                                $errors[] = 'Поле "'.$fields[$input_name]['NAME'].'": не допустимый формат файла "'.strip_tags($arFile['name']).'"';
                            }
                        } else {
                            $errors[] = 'Ошибка загрузки Файла "'.strip_tags($arFile['name']).'"';
                        }
                    } else {
                        unset($_FILES[$input_name]);
                    }
                }
            }

            if ( count($errors) == 0 ){

                $client = new Client(array('timeout' => 5));
                try {
                    $response = $client->request(
                        'POST',
                        'https://www.google.com/recaptcha/api/siteverify',
                        array(
                            'form_params' => array(
                                'secret' => \Bitrix\Main\Config\Option::get('aoptima.project', 'GOOGLE_RECAPTCHA_SECRET_KEY'),
                                'response' => strip_tags($_POST['g-recaptcha-response']),
                                'remoteip' => $_SERVER['REMOTE_ADDR']
                            )
                        )
                    );
                    if ($response->getStatusCode() == 200){
                        $body = $response->getBody();
                        $json = $body->getContents();
                        $res = tools\funcs::json_to_array($json);
                        if ($res['success'] == true){


                            $content = [];
                            foreach ( $_POST as $key => $value ){
                                if( !in_array($key, ['g-recaptcha-response']) ){
                                    $content[] = strtoupper($fields[$key]['NAME']).":\n".$value."\n\n----------------------------------------------\n";
                                }
                            }
                            $content = implode("\n", $content);


                            $path = $_SERVER['DOCUMENT_ROOT'].project\vozvrat_form::TEMP_UPLOAD_PATH;
                            if ( !file_exists( $path ) ){    mkdir( $path, 0700 );    }

                            $files_to_unlink = [];
                            foreach ( $files['defect_photos'] as $input_name => $item ){
                                $arPath = explode('.', $item['name']);
                                if( count($arPath) > 0 ){
                                    $ex = $arPath[count($arPath)-1];
                                    $file_path = $_SERVER['DOCUMENT_ROOT'].project\vozvrat_form::TEMP_UPLOAD_PATH.md5($item['name'].time()).'.'.$ex;
                                    if(
                                        move_uploaded_file($item['tmp_name'], $file_path)
                                        &&
                                        file_exists($file_path)
                                    ){
                                        $PROP['defect_photos'][] = \CFile::MakeFileArray($file_path);
                                        $files_to_unlink[] = $file_path;
                                    }
                                }
                            }
                            foreach ( $files['garant_scan'] as $input_name => $item ){
                                $arPath = explode('.', $item['name']);
                                if( count($arPath) > 0 ){
                                    $ex = $arPath[count($arPath)-1];
                                    $file_path = $_SERVER['DOCUMENT_ROOT'].project\vozvrat_form::TEMP_UPLOAD_PATH.md5($item['name'].time()).'.'.$ex;
                                    if(
                                        move_uploaded_file($item['tmp_name'], $file_path)
                                        &&
                                        file_exists($file_path)
                                    ){
                                        $PROP['garant_scan'] = \CFile::MakeFileArray($file_path);
                                        $files_to_unlink[] = $file_path;
                                    }
                                }
                            }
                            foreach ( $files['zakluchenie_sc'] as $input_name => $item ){
                                $arPath = explode('.', $item['name']);
                                if( count($arPath) > 0 ){
                                    $ex = $arPath[count($arPath)-1];
                                    $file_path = $_SERVER['DOCUMENT_ROOT'].project\vozvrat_form::TEMP_UPLOAD_PATH.md5($item['name'].time()).'.'.$ex;
                                    if(
                                        move_uploaded_file($item['tmp_name'], $file_path)
                                        &&
                                        file_exists($file_path)
                                    ){
                                        $PROP['zakluchenie_sc'] = \CFile::MakeFileArray($file_path);
                                        $files_to_unlink[] = $file_path;
                                    }
                                }
                            }

                            $element = new \CIBlockElement;
                            $fields = [
                                "IBLOCK_ID"				=> 127,
                                "NAME"					=> 'Новый запрос',
                                "ACTIVE"				=> "Y",
                                "PROPERTY_VALUES"       => $PROP,
                                "PREVIEW_TEXT"          => $content,
                            ];

                            $id = $element->add($fields);
                            if( intval($id) > 0 ){

                                foreach ( $files_to_unlink as $file_path ){    unlink($file_path);    }

                                ob_start();
                                    // vozvrat_form
                                    global $APPLICATION;
                                    $APPLICATION->IncludeComponent(
                                        "aoptima:vozvrat_form", ""
                                    );
                                    $html = ob_get_contents();
                                ob_end_clean();

                                $response = json_encode([
                                    'status' => 'ok',
                                    'html' => $html
                                ]);

                            } else {

                                $response = json_encode([
                                    'status' => 'error',
                                    'text' => 'Ошибка отправки сообщения'
                                ]);
                            }

                        } else {

                            $response = json_encode([
                                'status' => 'error',
                                'text' => 'Ошибка проверки пользователя'
                            ]);
                        }
                    } else {

                        $response = json_encode([
                            'status' => 'error',
                            'text' => 'Ошибка проверки пользователя'
                        ]);
                    }
                } catch (RequestException $ex) {

                    $response = json_encode([
                        'status' => 'error',
                        'text' => 'Ошибка проверки пользователя'
                    ]);
                }
            } else {

                $response = json_encode([
                    'status' => 'error',
                    'text' => implode('<br>', $errors)
                ]);
            }
        }

        echo '<script type="text/javascript">
        window.parent.vozvratFormResponse('.$response.');
        </script>';
    }





}