<?php

namespace AOptima\Project;
use AOptima\Project as project;


class manager {

    const IBLOCK_ID = 58;



    static function getList(){
        $list = [];
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'managers';
        $cache_path = '/managers/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            \Bitrix\Main\Loader::includeModule('iblock');
            $filter = Array(
            	"IBLOCK_ID" => static::IBLOCK_ID,
            	"ACTIVE" => "Y"
            );
            $fields = Array(
                "ID", "NAME", "PREVIEW_PICTURE", "PROPERTY_SECTION_XML_ID", "PROPERTY_PHONE",
                "PROPERTY_TELEGRAM", "PROPERTY_VIBER"
            );
            $dbElements = \CIBlockElement::GetList(
            	array("SORT"=>"ASC"), $filter, false, false, $fields
            );
            while ($element = $dbElements->GetNext()){
                $list[$element['ID']] = $element;
            }
        $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }



}