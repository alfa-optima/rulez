<?php


namespace AOptima\Project;
use AOptima\Project as project;


use Bitrix\Main\Entity;

class SeopagecategoryTable extends Entity\DataManager {

    public static function getTableName(){
        return 'aoptima_seo_pages_categories';
    }

    public static function getMap(){
        return array(
            new Entity\IntegerField( 'ID', array(
                'primary' => true,
                'autocomplete' => true,
            )),
            new Entity\IntegerField( 'SORT', array(
                'default_value' => 500
            )),
            new Entity\StringField( 'NAME', array(
                'required' => true
            )),
        );
    }


    public static function checkTable(){
        $addTableSQL = static::getEntity()->compileDbTableStructureDump();
        if( is_array($addTableSQL) ){
            foreach ( $addTableSQL as $key => $sql ){
                $sql = str_replace('CREATE TABLE', 'CREATE TABLE IF NOT EXISTS', $sql);
                global $DB;
                $res = $DB->Query( $sql );
            }
        }
    }



    public static function addItem( $fields ){
        $result = static::add($fields);
        if ($result->isSuccess()){
            $id = $result->getId();
            return $id;
        }
        return false;
    }


    public static function updateItem( $id, $fields ){
        $result = static::update( $id, $fields );
        return $result;
    }







    public static function all_items(){
        $list = [];
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'seo_page_categories';
        $cache_path = '/seo_page_categories/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $categories = project\SeopagecategoryTable::getList([
                'select' => [ 'ID', 'NAME', 'SORT' ],
                'filter' => [],
                'order' => array('SORT' => 'ASC'),
            ]);
            while( $category = $categories->fetch() ){
                $list[$category['ID']] = [
                    'ID' => $category['ID'],
                    'NAME' => $category['NAME'],
                    'SORT' => $category['SORT'],
                ];
            }
        $obCache->EndDataCache(array('list' => $list));
        }
        return $list;
    }




}