<?php

namespace AOptima\Project;
use AOptima\Project as project;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;




class delivery_price_courier extends delivery_price {

    const BASE_URL = 'http://api.xn--80ablsb0bbv6f.xn--90ais';
    const API_KEY = 'd035815d87b5e68e013bfb2f9fa92ab7';



    // Поиск населённого пункта
    public function searchCities( $q ){
        require_once $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        $path = static::BASE_URL.'/api/city/getByName?name='.$q;
        try {
            $client = new Client(array( 'timeout' => 5 ));
            $response = $client->request(
                'GET', $path, [
                    'headers' => [
                        'X-Token' => static::API_KEY,
                        'Accept' => 'application/json',
                    ]
                ]
            );
            $body = $response->getBody();
            $content = $body->getContents();
            if( $response->getStatusCode() == 200 ){
                $data = \AOptima\Tools\funcs::json_to_array($content);
                return $data;
            } else {
                \AOptima\Tools\logger::addError( 'курьербай searchCities - код ответа '.$response->getStatusCode() );
            }
        } catch( RequestException $ex ){
            \AOptima\Tools\logger::addError( 'курьербай searchCities - '.$ex->getMessage() );
        } catch ( \Exception $ex ){
            \AOptima\Tools\logger::addError( 'курьербай searchCities - '.$ex->getMessage() );
        }
        return false;
    }



}