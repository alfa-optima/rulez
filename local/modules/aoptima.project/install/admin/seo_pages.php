<? use Bitrix\Main,  Bitrix\Main\Loader;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

$CURRENCY_RIGHT = $APPLICATION->GetGroupRight("aoptima.project");

if ($CURRENCY_RIGHT=="D") $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('iblock');


// Автосоздание таблиц в БД
project\SeopagecategoryTable::checkTable();
project\SeopageTable::checkTable();

$categories = project\SeopagecategoryTable::all_items(); ?>


<link rel="stylesheet" href="/local/modules/aoptima.project/install/admin/styles.css?v=<?=time()?>">

<? if( count($categories) == 0 ){ ?>

    <div style="margin-bottom: 20px;">
        <input class="adm-btn-save to_add_category_button" type="button" value="Добавить категорию">
    </div>

    <? echo BeginNote();
    echo "<b>Для работы с SEO-страницами сначала нужно создать категории</b>";
    echo EndNote();

} else {

    // Новая запись
    if( $_GET['type'] == 'add' ){

        $APPLICATION->SetTitle('Создание SEO-страницы'); ?>

        <form onsubmit="return false;">
            <table class="add___table">
                <tr>
                    <th class="first_td">Поле</th>
                    <th>Значение</th>
                </tr>
                <tr>
                    <td class="first_td">Название страницы</td>
                    <td>
                        <div class="inputs_block">
                            <input type="text" name="NAME">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="first_td">Уникальный символьный код</td>
                    <td>
                        <div class="inputs_block">
                            <input type="text" name="CODE">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="first_td">Категория SEO-страницы</td>
                    <td>
                        <div class="inputs_block">
                            <select name="CATEGORY">
                                <option value="empty">Выбор категории</option>
                                <? foreach( $categories as $key => $category ){ ?>
                                    <option value="<?=$category['ID']?>"><?=$category['NAME']?></option>
                                <? } ?>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="first_td">Внешний код категории товаров</td>
                    <td>
                        <div class="inputs_block">
                            <input type="text" name="SECTION_XML_ID">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="first_td">Показывать в блоке меню на главной</td>
                    <td>
                        <div class="inputs_block">
                            <input type="checkbox" name="SHOW_ON_MAIN_PAGE" value="Y">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="first_td">Title</td>
                    <td>
                        <div class="inputs_block">
                            <input type="text" name="TITLE">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="first_td">Description</td>
                    <td>
                        <div class="inputs_block">
                            <input type="text" name="DESCRIPTION">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="first_td">Keywords</td>
                    <td>
                        <div class="inputs_block">
                            <input type="text" name="KEYWORDS">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="first_td">Текст страницы</td>
                    <td>
                        <div class="inputs_block">
                            <textarea name="TEXT"></textarea>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="first_td">Фильтрация по цене</td>
                    <td>
                        <div class="inputs_block">
                            <p><strong>Цена "от", руб. :</strong></p>
                            <p><input type="text" class="number___input" name="PRICE_FROM" placeholder='Цена "от", руб.'></p>
                            <p><strong>Цена "до", руб. :</strong></p>
                            <p><input type="text" class="number___input" name="PRICE_TO" placeholder='Цена "до", руб.'></p>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="first_td">Фильтры по свойствам</td>
                    <td>
                        <input class="adm-btn-save add_prop___button" type="button" value="Добавить">
                    </td>
                </tr>

                <tr>
                    <td colspan="2"><p class="error___p"></p></td>
                </tr>
                <tr>
                    <td colspan="2" class="buttons_td">
                        <input class="adm-btn-save seo_page_add___button to___process" type="button" value="Сохранить">
                        <input class="prim seo_page_add___button to___process" type="button" value="Применить">
                        <input type="button" value="Отмена" onclick="window.location.href = '/bitrix/admin/aoptima.project_seo_pages.php';">
                    </td>
                </tr>
            </table>
        </form>

    <? // Редактирование записи
    } else if(
        $_GET['type'] == 'update'
        &&
        intval($_GET['id']) > 0
    ){


        $APPLICATION->SetTitle('Редактирование SEO-страницы');

        // Ищём запись по ID
        $res = project\SeopageTable::getList([
            'select' => [
                'ID', 'CODE', 'NAME', 'TITLE', 'DESCRIPTION', 'KEYWORDS',
                'TEXT', 'SECTION_XML_ID', 'FILTER_JSON', 'CATEGORY', 'SHOW_ON_MAIN_PAGE'
            ],
            'filter' => [ 'ID' => $_GET['id'] ],
            'limit' => 1,
        ]);

        // Если запись найдена
        if( $element = $res->fetch() ){
            
            $section = project\catalog::getSectionByXMLID( $element['SECTION_XML_ID'] );

            $filter = [];
            if( strlen($element['FILTER_JSON']) > 0 ){
                $filter = tools\funcs::json_to_array($element['FILTER_JSON']);
            } ?>

            <form onsubmit="return false;">
                <table class="add___table">
                    <tr>
                        <th class="first_td">Поле</th>
                        <th>Значение</th>
                    </tr>
                    <tr>
                        <td class="first_td">Название страницы</td>
                        <td>
                            <div class="inputs_block">
                                <input type="text" name="NAME" value="<?=$element['NAME']?>">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="first_td">Уникальный символьный код</td>
                        <td>
                            <div class="inputs_block">
                                <input type="text" name="CODE" value="<?=$element['CODE']?>">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="first_td">Категория SEO-страницы</td>
                        <td>
                            <div class="inputs_block">
                                <select name="CATEGORY">
                                    <option value="empty">Выбор категории</option>
                                    <? foreach( $categories as $key => $category ){ ?>
                                        <option <? if( $element['CATEGORY'] == $category['ID'] ){ echo 'selected'; } ?> value="<?=$category['ID']?>"><?=$category['NAME']?></option>
                                    <? } ?>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="first_td">Внешний код категории товаров</td>
                        <td>
                            <div class="inputs_block">
                                <input type="text" name="SECTION_XML_ID" value="<?=$element['SECTION_XML_ID']?>">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="first_td">Показывать в блоке меню на главной</td>
                        <td>
                            <div class="inputs_block">
                                <input type="checkbox" name="SHOW_ON_MAIN_PAGE" value="Y" <? if($element['SHOW_ON_MAIN_PAGE']=='Y'){  echo 'checked';  } ?>>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="first_td">Title</td>
                        <td>
                            <div class="inputs_block">
                                <input type="text" name="TITLE" value="<?=$element['TITLE']?>">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="first_td">Description</td>
                        <td>
                            <div class="inputs_block">
                                <input type="text" name="DESCRIPTION" value="<?=$element['DESCRIPTION']?>">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="first_td">Keywords</td>
                        <td>
                            <div class="inputs_block">
                                <input type="text" name="KEYWORDS" value="<?=$element['KEYWORDS']?>">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="first_td">Текст страницы</td>
                        <td>
                            <div class="inputs_block">
                                <textarea name="TEXT"><?=$element['TEXT']?></textarea>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="first_td">Фильтрация по цене</td>
                        <td>
                            <div class="inputs_block">
                                <p><strong>Цена "от", руб. :</strong></p>
                                <p><input type="text" class="number___input" name="PRICE_FROM" placeholder='Цена "от", руб.' value="<?=$filter['PRICE_FROM']?>"></p>
                                <p><strong>Цена "до", руб. :</strong></p>
                                <p><input type="text" class="number___input" name="PRICE_TO" placeholder='Цена "до", руб.' value="<?=$filter['PRICE_TO']?>"></p>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td class="first_td">Фильтры по свойствам</td>
                        <td>

                            <? if( is_array($filter['PROP_VALUES']) ){

                                foreach ( $filter['PROP_VALUES'] as $prop_code => $values ){

                                    $prop = tools\prop::getByCode( $section['IBLOCK_ID'], $prop_code );

                                    if( intval($prop['ID']) > 0 ){ ?>

                                        <div class="inputs_block props_block">

                                            <div class="prop_code_block">
                                                <p><strong>Код свойства:</strong></p>
                                                <p><input type="text" class="prop_code_input" placeholder='Код свойства' value="<?=$prop_code?>"></p>
                                            </div>

                                            <? $APPLICATION->IncludeComponent(
                                                "aoptima:seo_page_prop_item", "",
                                                [
                                                    'prop' => $prop,
                                                    'values' => $values,
                                                    'section' => $section
                                                ]
                                            ); ?>

                                        </div>

                                    <? }
                                }
                            } ?>

                            <input class="add_prop___button" type="button" value="+ Добавить свойство">
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2"><p class="error___p"></p></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="buttons_td">
                            <input class="adm-btn-save seo_page_update___button to___process" type="button" value="Сохранить">
                            <input class="prim seo_page_update___button to___process" type="button" value="Применить">
                            <input type="button" value="Отмена" onclick="window.location.href = '/bitrix/admin/aoptima.project_seo_pages.php';">
                        </td>
                    </tr>
                </table>

                <input type="hidden" name="item_id" value="<?=$element['ID']?>">

            </form>

        <? } else {

            echo BeginNote();
            echo "<b>Запись не найдена</b>";
            echo EndNote();

        } ?>


    <? // Список записей
    } else {


        if( !isset($_GET['PAGEN_1']) ){
            LocalRedirect( '/bitrix/admin/aoptima.project_seo_pages.php?PAGEN_1=1' );
        }

        $APPLICATION->SetTitle('SEO-страницы');

        $list = [];
        $elements = project\SeopageTable::getList([
            'select' => [
                'ID', 'CODE', 'NAME'
            ],
            'order' => array('ID' => 'DESC'),
        ]);
        while( $element = $elements->fetch() ){
            $list[$element['ID']] =  $element;
        } ?>

        <div style="margin-bottom: 20px;">
            <input class="adm-btn-save to_add_page_button" type="button" value="Добавить страницу">
        </div>

        <? if( count($list) > 0 ){

            $r_elements = new CDBResult;
            $r_elements->InitFromArray($list);
            $r_elements->NavStart(20);
            $NAV_STRING = $r_elements->GetPageNavStringEx($navComponentObject, false, false); ?>

            <table class="list___table">

                <tr>
                    <th class="first_td">ID</th>
                    <th>Название страницы</th>
                    <th>Уникальный символьный код</th>
                    <th class="buttons_td"></th>
                    <th class="buttons_td"></th>
                </tr>

                <? while ( $item = $r_elements->Fetch() ){ ?>

                    <tr class="iblock_tr" iblock_id="<?=$iblock['ID']?>">
                        <td class="first_td">
                            <?=$item['ID']?>
                        </td>
                        <td>
                            <?=$item['NAME']?>
                        </td>
                        <td>
                            <?=$item['CODE']?>
                        </td>
                        <td class="buttons_td">
                            <a href="?type=update&id=<?=$item['ID']?>">Редактировать</a>
                        </td>
                        <td class="buttons_td">
                            <a class="delete___seo_page_button to___process" style="cursor: pointer" item_id="<?=$item['ID']?>">Удалить</a>
                        </td>
                    </tr>

                <? } ?>

            </table>


            <? echo $NAV_STRING; ?>


        <? } else {

            echo BeginNote();
            echo "<b>На данный момент не создано ни одной SEO-страницы</b>";
            echo EndNote();

        } ?>

    <? }
} ?>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="/local/modules/aoptima.project/install/admin/funcs.js?v=<?=time()?>"></script>
<script type="text/javascript" src="/local/modules/aoptima.project/install/admin/scripts.js?v=<?=time()?>"></script>



<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");