<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

$arResult['PROP'] = $arParams['prop'];
$arResult['VALUES'] = $arParams['values'];

// Список
if( $arResult['PROP']['PROPERTY_TYPE'] == 'L' ){
    $arResult['ENUMS'] = [];
    $enums = \CIBlockPropertyEnum::GetList(Array("VALUE"=>"ASC"), Array("IBLOCK_ID"=>$section['IBLOCK_ID'], "CODE" => $arResult['PROP']['CODE']));
    while($enum = $enums->GetNext()){    $arResult['ENUMS'][] = $enum;    }
}




$this->IncludeComponentTemplate();