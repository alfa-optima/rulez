<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;

$module_id = 'aoptima.project';

Loc::loadMessages($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");
Loc::loadMessages(__FILE__);

if ( $APPLICATION->GetGroupRight($module_id) < "S" ){
    $APPLICATION->AuthForm('Access denied');
}

\Bitrix\Main\Loader::includeModule($module_id);


$request = \Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest();

#Описание опций

$aTabs = array(
    
	array(
        'DIV' => 'edit1',
        'TAB' => Loc::getMessage("TAB_1_NAME"),
        'OPTIONS' => array(
            array(
				'COMPANY_NAME',
				'Название компании',
                'ООО "Компания"',
                array('text', 38)
			),
        )
    ),

    array(
        'DIV' => 'edit2',
        'TAB' => 'Формы',
        'OPTIONS' => array(
            array(
                'CALLBACK_EMAIL_TO',
                'Email для запросов обратного звонка',
                '',
                array('text', 38)
            ),
            array(
                'QUICK_ORDER_EMAIL',
                'Email для заказов в 1 клик',
                '',
                array('text', 38)
            ),
            array(
                'ORDER_EMAIL',
                'Email для заказов',
                '',
                array('text', 38)
            ),
        )
    ),

    array(
        'DIV' => 'edit3',
        'TAB' => 'Email',
        'OPTIONS' => array(
            array(
                'GENERAL_EMAIL',
                'Email для обратной связи (шапка сайта)',
                '',
                array('text', 38)
            ),
        )
    ),

    array(
        'DIV' => 'edit4',
        'TAB' => 'Google',
        'OPTIONS' => array(
            array(
                'GOOGLE_RECAPTCHA_SITE_KEY',
                'ReCaptcha site key',
                '',
                array('text', 38)
            ),
            array(
                'GOOGLE_RECAPTCHA_SECRET_KEY',
                'ReCaptcha secret key',
                '',
                array('text', 38)
            ),
        )
    ),

    array(
        'DIV' => 'edit5',
        'TAB' => 'Мессенджеры',
        'OPTIONS' => array(
            array(
                'TELEGRAM_CHAT',
                'ID чата Telegram',
                '',
                array('text', 38)
            ),
            array(
                'VIBER_CHAT',
                'ID чата VIBER',
                '',
                array('text', 38)
            ),
            array(
                'WATSAPP_PHONE',
                'Номер телефона для Watsapp',
                '',
                array('text', 38)
            ),
        )
    ),

    array(
        'DIV' => 'edit6',
        'TAB' => 'Рассрочка',
        'OPTIONS' => array(
            array(
                'RASSROCHKA_PROCENT',
                '% рассрочки',
                0,
                array('text', 38)
            ),
            array(
                'RASSROCHKA_MONTHS',
                'Месяцы рассрочки',
                '',
                array('text', 38)
            ),
//            array(
//                'RASSROCHKA_MIN_MONTHS',
//                'Мин. количество месяцев',
//                2,
//                array('text', 38)
//            ),
//            array(
//                'RASSROCHKA_MAX_MONTHS',
//                'Макс. количество месяцев',
//                11,
//                array('text', 38)
//            ),
            array(
                'RASSROCHKA_MIN_VZNOS',
                'Первонач. взнос (мин.)',
                30,
                array('text', 38)
            ),
            array(
                'RASSROCHKA_MAX_VZNOS',
                'Первонач. взнос (макс.)',
                50,
                array('text', 38)
            ),
        )
    ),

    array(
        'DIV' => 'edit7',
        'TAB' => 'Доставка',
        'OPTIONS' => array(
            array(
                'DEFAULT_COURIER_DELIERY_PRICE',
                'Курьер РБ - стоимость доставки поумолчанию, руб.',
                14.5,
                array('text', 38)
            ),
            array(
                'COURIER_MINSK_DELIVERY_PRICE',
                'Курьер Минск - стоимость доставки, руб.',
                5,
                array('text', 38)
            ),
            array(
                'COURIER_MINSK_POROG_SUM',
                'Курьер Минск - мин. сумма заказа (для бесплатной доставки), руб.',
                300,
                array('text', 38)
            ),
            array(
                'DELIVERY_RB_PLUS_DAYS',
                'Разница между Минском и РБ по доставке, дней',
                1,
                array('text', 38)
            ),
        )
    ),

);





if ($APPLICATION->GetGroupRight($module_id)=="W"){
	
	$aTabs[] = array(
        "DIV" => "edit100",
        "TAB" => Loc::getMessage("MAIN_TAB_RIGHTS"),
        "TITLE" => Loc::getMessage("MAIN_TAB_TITLE_RIGHTS")
    );
	
}


#Сохранение
if ( 
	$request->isPost()
	&&
	$request['Update']
	&&
	check_bitrix_sessid()
){

    foreach ($aTabs as $aTab){
		
        //Или можно использовать __AdmSettingsSaveOptions($MODULE_ID, $arOptions);
        foreach ($aTab['OPTIONS'] as $arOption){
			
            if (!is_array($arOption)) //Строка с подсветкой. Используется для разделения настроек в одной вкладке
                continue;

            if ($arOption['note']) //Уведомление с подсветкой
                continue;

            //Или __AdmSettingsSaveOption($MODULE_ID, $arOption);
            $optionName = $arOption[0];

            $optionValue = $request->getPost($optionName);

            Option::set($module_id, $optionName, is_array($optionValue) ? implode(",", $optionValue):$optionValue);
			
        }
    }
	
}

#Визуальный вывод

$tabControl = new CAdminTabControl('tabControl', $aTabs);

?>

<? $tabControl->Begin(); ?>

<form method='post' action='<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($request['mid'])?>&amp;lang=<?=$request['lang']?>' name='AOPTIMA_PROJECT_settings'>

    <? foreach ($aTabs as $aTab):
		if($aTab['OPTIONS']):?>
			<? $tabControl->BeginNextTab(); ?>
			<? __AdmSettingsDrawList($module_id, $aTab['OPTIONS']); ?>
		<? endif;
     endforeach; ?>

    <? $tabControl->BeginNextTab();
	
	if ($APPLICATION->GetGroupRight($module_id)=="W"){
		require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/admin/group_rights.php');
	}


    $tabControl->Buttons(); ?>

    <input type="submit" name="Update" value="<?echo GetMessage('MAIN_SAVE')?>">
    <input type="reset" name="reset" value="<?echo GetMessage('MAIN_RESET')?>">
	
    <?=bitrix_sessid_post();?>
	
</form>

<? $tabControl->End(); ?>

