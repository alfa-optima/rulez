<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if( count($arResult['CATEGORIES']) > 0 ){ ?>

    <section class="main-section main-section_topics tabs-js max-wrap layout dop-tabs-js">

        <div class="caption-box">

            <h2 class="caption-box__title">Лучшая цена</h2>

            <div class="caption-box__options">
                <div class="topics-nav">

                    <div class="topics-thumbs-select tabs__select-js"></div>

                    <div class="topics-thumbs tabs__thumbs-js tabs__select-drop-js">

                        <? $cnt = 0;
                        foreach( $arResult['CATEGORIES'] as $category ){ $cnt++;
                            if( $cnt <= 4 ){ ?>

                                <a href="#best_price_<?=$category['ID']?>" <? if( $cnt == 1 ){ ?>class="tabs-active"<? } ?>><span><?=$category['NAME']?></span></a>

                            <? }
                        } ?>

                    </div>
                </div>
            </div>

            <a href="/best_prices/" class="link-all-2">
                <span>Все</span>
                <svg height="6" width="10" fill="none">
                    <use xlink:href="#icon-angle"></use>
                </svg>
            </a>

        </div>

        <div class="topics-tabs">
            <div class="topics-tabs__panels tabs__panels-js">

                <? $cnt = 0;
                foreach( $arResult['CATEGORIES'] as $category ){ $cnt++;
                    if( $cnt <= 4 ){

                        $GLOBALS['main_best_prices']['ID'] = $category['PRODUCTS'];
                        $GLOBALS['main_best_prices']['INCLUDE_SUBSECTIONS'] = 'Y';

                        $APPLICATION->IncludeComponent(
                            "bitrix:catalog.section", "main_best_prices",
                            Array(
                                "CATEGORY_ID" => $category['ID'],
                                "IBLOCK_TYPE" => project\catalog::IBLOCK_TYPE,
                                "IBLOCK_ID" => $category['IBLOCK_ID'],
                                "SECTION_USER_FIELDS" => array(),
                                "ELEMENT_SORT_FIELD" => 'RAND',
                                "ELEMENT_SORT_ORDER" => 'ASC',
                                "ELEMENT_SORT_FIELD2" => "NAME",
                                "ELEMENT_SORT_ORDER2" => 'ASC',
                                "FILTER_NAME" => 'main_best_prices',
                                "HIDE_NOT_AVAILABLE" => "N",
                                "PAGE_ELEMENT_COUNT" => project\catalog::$bestPricesMaxCnt,
                                "LINE_ELEMENT_COUNT" => 3,
                                "PROPERTY_CODE" => array('STATUS_TOVARA'),
                                "OFFERS_LIMIT" => 0,
                                "TEMPLATE_THEME" => "",
                                "PRODUCT_SUBSCRIPTION" => "N",
                                "SHOW_DISCOUNT_PERCENT" => "N",
                                "SHOW_OLD_PRICE" => "N",
                                "MESS_BTN_BUY" => "Купить",
                                "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                                "MESS_BTN_SUBSCRIBE" => "Подписаться",
                                "MESS_BTN_DETAIL" => "Подробнее",
                                "MESS_NOT_AVAILABLE" => "Нет в наличии",
                                "SECTION_URL" => "",
                                "DETAIL_URL" => "",
                                "SECTION_ID_VARIABLE" => "SECTION_ID",
                                "AJAX_MODE" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "AJAX_OPTION_HISTORY" => "N",
                                "CACHE_TYPE" => project\catalog::CACHE_TYPE,
                                "CACHE_TIME" => project\catalog::CACHE_TIME,
                                "CACHE_GROUPS" => "Y",
                                "SET_META_KEYWORDS" => "N",
                                "META_KEYWORDS" => "",
                                "SET_META_DESCRIPTION" => "N",
                                "META_DESCRIPTION" => "",
                                "BROWSER_TITLE" => "-",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "DISPLAY_COMPARE" => "N",
                                "SET_TITLE" => "N",
                                "SET_STATUS_404" => "N",
                                "CACHE_FILTER" => "Y",
                                "PROPERTY_CODE" => array('STATUS_TOVARA'),
                                "PRICE_CODE" => array(
                                    project\catalog::RASSROCHKA_PRICE_CODE,
                                    project\catalog::BASE_PRICE_CODE
                                ),
                                "USE_PRICE_COUNT" => "N",
                                "SHOW_PRICE_COUNT" => "1",
                                "PRICE_VAT_INCLUDE" => "Y",
                                "CONVERT_CURRENCY" => "N",
                                "BASKET_URL" => "/personal/basket.php",
                                "ACTION_VARIABLE" => "action",
                                "PRODUCT_ID_VARIABLE" => "id",
                                "USE_PRODUCT_QUANTITY" => "N",
                                "ADD_PROPERTIES_TO_BASKET" => "Y",
                                "PRODUCT_PROPS_VARIABLE" => "prop",
                                "PARTIAL_PRODUCT_PROPERTIES" => "N",
                                "PRODUCT_PROPERTIES" => "",
                                "PAGER_TEMPLATE" => "",
                                "DISPLAY_TOP_PAGER" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                "PAGER_TITLE" => "Товары",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "Y",
                                "ADD_PICT_PROP" => "-",
                                "LABEL_PROP" => "-",
                                'INCLUDE_SUBSECTIONS' => "Y",
                                'SHOW_ALL_WO_SECTION' => "Y"
                            )
                        );

                    }
                } ?>

            </div>
        </div>
    </section>

<? } ?>