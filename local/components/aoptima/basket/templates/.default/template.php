<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project'); use AOptima\Project as project;
\Bitrix\Main\Loader::includeModule('aoptima.tools'); use AOptima\Tools as tools; ?>

<basketBlock>

    <!--cart-->
    <div class="cart cart-js">

        <!--cart base-->
        <div class="cart__base">

            <!--cart main-->
            <div class="cart__main">

                <!--cart heading-->
                <div class="main-heading">
                    <h1 class="main-heading__title">Корзина</h1>
                </div>
                <!--cart heading end-->

                <? if( count($arResult['arBasket']) > 0 ){ ?>

                    <!--cart list-->
                    <section class="cart__list cart__list-js">

                        <? foreach( $arResult['arBasket'] as $key => $basketItem ){
                            $el = $basketItem->el; ?>

                            <div class="cart-item cart__list-item cart-item-js">
                                <div class="cart-item__box">
                                    <div class="cart-item__main">
                                        <a <? if( $el['IBLOCK_ID'] != project\complect::IBLOCK_ID ){ ?>href="<?=$el['DETAIL_PAGE_URL']?>"<? } ?> class="cart-item__figure">
                                            <? if( intval($el['DETAIL_PICTURE']) > 0 ){ ?>
                                                <img class="cart-item__figure-img lozad" src="<?=SITE_TEMPLATE_PATH?>/img/preloader.svg" data-src="<?=tools\funcs::rIMGG($el['DETAIL_PICTURE'], 4, 212, 139)?>"/>
                                            <? } else { ?>
                                                <img class="cart-item__figure-img lozad" src="<?=SITE_TEMPLATE_PATH?>/img/preloader.svg" data-src="<?=SITE_TEMPLATE_PATH?>/img/no-photo.png"/>
                                            <? } ?>
                                        </a>
                                        <div class="cart-item__caption">
                                            <a <? if( $el['IBLOCK_ID'] != project\complect::IBLOCK_ID ){ ?>href="<?=$el['DETAIL_PAGE_URL']?>"<? } ?> class="cart-item__title"><?=$el['NAME']?></a>
                                            <? if( $el['IBLOCK_ID'] != project\complect::IBLOCK_ID ){ ?>
                                                <div class="cart-item__rate p-rate">
                                                    <span style="width: <?=round($basketItem->el['rating']/5*100, 0)?>%;"><?=$basketItem->el['rating']?></span>
                                                </div>
                                            <? } ?>
                                        </div>
                                    </div>
                                    <div class="cart-item__order">
                                        <div class="cart-item__stock p-stock on"><i>&nbsp;</i>В наличии</div>
                                        <div class="cart-item__price">
                                            <?php if( isset($basketItem->oldPrice) ){ ?>
                                                <div class="cart-item__price-old"><strong><?=$basketItem->oldPriceFormat?></strong>&nbsp;<em>р.</em></div>
                                            <? } ?>
                                            <div class="cart-item__price-new"><strong class="cart-item__price-js" data-price="<?=$basketItem->discPrice?>"><?=$basketItem->discPriceFormat?></strong>&nbsp;<em>р.</em></div>
                                            <?php if( isset($basketItem->oldPrice) ){ ?>
                                                <div class="cart-item__price-note">Спецпредложение</div>
                                            <? } ?>
                                        </div>
                                        <div class="cart-item__counter cart-item__counter-js">
                                            <input class="spinner spinner-js basketCntInput" max="1000" min="1" type="number" name="value" value="<?=round($basketItem->getQuantity(), 0)?>" data-only-number item_id="<?=$arResult['basket'][$key]->getID()?>">
                                        </div>
                                        <div class="cart-item__sum" <? if( $basketItem->price == $basketItem->sum ){ ?>style="display: none"<? } ?>>
                                            <strong class="cart-item__sum-js" data-total="<?=$basketItem->sum?>"><?=$basketItem->sumFormat?></strong> <em>р.</em>
                                        </div>
                                    </div>
                                </div>
                                <a style="cursor: pointer;" class="cart-item__remove basketRemoveButton to___process" item_id="<?=$arResult['basket'][$key]->getID()?>">
                                    <svg width="8" height="8" viewBox="0 0 8 8" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M0.646447 1.35355L6.64645 7.35355L7.35355 6.64645L1.35355 0.646447L0.646447 1.35355ZM6.64645 0.646447L0.646447 6.64645L1.35355 7.35355L7.35355 1.35355L6.64645 0.646447Z"></path>
                                    </svg>
                                    <span>Удалить из корзины</span>
                                </a>
                            </div>

                        <? } ?>

                        <div class="loader-cover">&nbsp;</div>

                    </section>
                    <!--cart list end-->

                <? } else { ?>

                    <!--cart empty note-->
                    <div class="cart__empty" style="display: block;">В корзине нет товаров</div>
                    <!--cart empty note end-->

                <? } ?>

            </div>
            <!--cart main end-->

            <? if( count($arResult['arBasket']) > 0 ){ ?>

                <? // basket_recommend_block
                $APPLICATION->IncludeComponent(
                    "aoptima:basket_recommend_block", "",
                    array( 'arBasket' => $arResult['arBasket'] )
                ); ?>

                <? // advBanner
                $APPLICATION->IncludeComponent(
                    "aoptima:advBanner", "", array('type' => 'basket')
                ); ?>

            <? } ?>

        </div>
        <!--cart base end-->

        <? if( count($arResult['arBasket']) > 0 ){ ?>

            <!--cart aside / clearance-->
            <div class="clearance cart__aside">
                <form class="validate-js">
                    <div class="clearance__box">

                        <div class="clearance__meta">

                            <div class="clearance__caption">
                                <div class="clearance__caption-icon">
                                    <i class="icon-free"></i>
                                </div>
                                <div class="clearance__caption-text">Доступные варианты доставки:</div>
                            </div>

                            <div class="delivery clearance__delivery">

                                <? foreach( $arResult['DS_LIST'] as $ds ){ ?>

                                    <div class="delivery-item">
                                        <div class="delivery__icon">
                                            <!-- <i class="icon-delivery"></i>-->
                                        </div>
                                        <div class="delivery__text"><strong><?=$ds['NAME']?></strong><?=$ds['DELIVERY_PRICE']>0?($ds['DELIVERY_PRICE'].' руб.'):'бесплатно'?></div>
                                    </div>

                                <? } ?>

                            </div>

                        </div>

                        <div class="clearance__main">

                            <div class="single-form clearance__promo-code">
                                <div class="single-form__layout single-form_has-btn">

                                    <label for="promo-code" class="single-form__label">Промокод</label>

                                    <input class="single-form__input field-js cart__promo-code-js" id="promo-code" name="coupon" type="text" value="<?=$arResult['ACTIVE_COUPON']['COUPON']?>">

                                    <div class="error-note single-form__error-note">Промокод неверный</div>

                                    <a style="cursor: pointer" class="single-form__send-btn saveCouponButton to___process">
                                        <svg width="19" height="13" viewBox="0 0 19 13" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M12.8021 0.19552C12.5342 -0.0651732 12.1126 -0.0651732 11.8447 0.19552C11.5859 0.447405 11.5859 0.866628 11.8447 1.11793L16.6993 5.84269H0.679892C0.306465 5.84269 0 6.13157 0 6.49501C0 6.85845 0.306465 7.15672 0.679892 7.15672H16.6993L11.8447 11.8727C11.5859 12.1334 11.5859 12.5532 11.8447 12.8045C12.1126 13.0652 12.5342 13.0652 12.8021 12.8045L18.8059 6.9612C19.0647 6.70932 19.0647 6.2901 18.8059 6.0388L12.8021 0.19552Z"></path>
                                        </svg>
                                        <span class="hide-text">Отправить</span>
                                    </a>

                                </div>
                            </div>

                            <div class="summary clearance__summary cart__summary-js">
<!--                                <div class="summary__item">-->
<!--                                    <div>Товары (<span class="cart__total-length-js">--><?//=$arResult['basket_cnt']?><!--</span>)</div>-->
<!--                                    <div><span class="cart__total-price-js">--><?//=number_format($arResult['basket_disc_sum'], project\catalog::PRICE_ROUND, ",", " ")?><!--</span> р.</div>-->
<!--                                </div>-->
<!--                                <div class="summary__item">-->
<!--                                    <div>Доставка</div>-->
<!--                                    <div class="cart__delivery-js" data-deliver="--><?//=$arResult['DELIVERY_PRICE']?><!--"><mark>--><?//=$arResult['DELIVERY_PRICE']?><!--</mark></div>-->
<!--                                </div>-->
                                <div class="summary__total">
                                    <div>Итого:</div>
                                    <div><span class="cart__result-price-js"><?=number_format($arResult['basket_disc_sum']+$arResult['DELIVERY_PRICE'], project\catalog::PRICE_ROUND, ",", " ")?></span> р.</div>
                                </div>
                            </div>

                            <div class="buttons clearance__buttons">

                                <div class="buttons__item">
                                    <a href="/order/" class="btn-def cart__button-js">Продолжить оформление</a>
                                </div>

<!--                                <div class="buttons__item">-->
<!--                                    <a href="/order/" class="btn-alt cart__button-js">Оформить в рассрочку</a>-->
<!--                                </div>-->

                            </div>

                            <? if( 1==2 ){ ?>
                                <div class="single-form clearance__quick-order">
                                    <div class="single-form__layout single-form_has-btn">
                                        <label for="quick-order" class="single-form__label">Быстрый заказ</label>
                                        <input class="single-form__input field-js cart__quick-order-js" id="quick-order" name="quick-order" placeholder="+375" type="text" required>
                                        <div class="error-note single-form__error-note">Введите корректный номер телефона в формате +375 и 9 цифр</div>
                                        <!--<div class="success-note single-form__success-note">Success text</div>-->
                                        <a href="#" class="single-form__send-btn">
                                            <svg width="19" height="13" viewBox="0 0 19 13" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M12.8021 0.19552C12.5342 -0.0651732 12.1126 -0.0651732 11.8447 0.19552C11.5859 0.447405 11.5859 0.866628 11.8447 1.11793L16.6993 5.84269H0.679892C0.306465 5.84269 0 6.13157 0 6.49501C0 6.85845 0.306465 7.15672 0.679892 7.15672H16.6993L11.8447 11.8727C11.5859 12.1334 11.5859 12.5532 11.8447 12.8045C12.1126 13.0652 12.5342 13.0652 12.8021 12.8045L18.8059 6.9612C19.0647 6.70932 19.0647 6.2901 18.8059 6.0388L12.8021 0.19552Z"></path>
                                            </svg>
                                            <span class="hide-text">Отправить</span>
                                        </a>
                                    </div>
                                </div>
                            <? } ?>

                        </div>

                    </div>
                </form>
            </div>
            <!--cart aside / clearance end-->

        <? } ?>

    </div>
    <!--cart end-->

    <? if( count($arResult['arBasket']) > 0 ){
        // bottom_advantages
        $APPLICATION->IncludeComponent(
            "bitrix:news.list", "bottom_advantages",
            array(
                "COMPONENT_TEMPLATE" => "bottom_advantages",
                "IBLOCK_TYPE" => "content",
                "IBLOCK_ID" => "53",
                "NEWS_COUNT" => "4",
                "SORT_BY1" => "SORT",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "NAME",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "",
                "FIELD_CODE" => array(),
                "PROPERTY_CODE" => array('ICON_CLASS'),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "Y",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_BROWSER_TITLE" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "INCLUDE_SUBSECTIONS" => "Y",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "PAGER_TEMPLATE" => ".default",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_ADDITIONAL" => "undefined",
                "SET_LAST_MODIFIED" => "N",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "SHOW_404" => "N",
                "MESSAGE_404" => ""
            ),
            false
        );
    } ?>

</basketBlock>
