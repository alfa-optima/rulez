<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('sale');
use Bitrix\Main;
use Bitrix\Sale;
use Bitrix\Sale\DiscountCouponsManager;


// Инфо о корзине
$arResult = project\user_basket::info();


// Получение рейтингов товаров
foreach ( $arResult['arBasket'] as $key => $basketItem ){
    $arResult['arBasket'][$key]->el['rating'] = (new project\product_rating($arResult['arBasket'][$key]->el['ID']))->get();
}


$arResult['DELIVERY_PRICE'] = 0;


// Варианты доставки
$arResult['DS_LIST'] = project\ds::getList();
foreach( $arResult['DS_LIST'] as $key => $ds ){
    $bInfo = project\user_basket::info( false, $ds['ID'] );
    //$delivPrice = project\catalog::getDeliveryPrice($ds, $bInfo);
    $delivPrice = $bInfo['calcInfo']['DELIVERY_PRICE'];
    $arResult['DS_LIST'][$key]['DELIVERY_PRICE'] = $delivPrice;
}



$arResult['ACTIVE_COUPON'] = project\coupon::get_active_coupon();





$this->IncludeComponentTemplate();