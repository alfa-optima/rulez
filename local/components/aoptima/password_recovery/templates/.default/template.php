<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>

<div class="content">

    <style>
        .content-align::before {
            border: none!important;
        }
        .content-inner {
            margin-left: 0!important;
        }
    </style>

    <div class="content-align">

        <section class="content-inner article-content">

            <h1>Восстановление пароля</h1>

            <div class="cabinet-profile-form">

                <form class="simple-form validate-js password_update_block" onsubmit="return false;">

                    <? if( !$arResult['ERROR'] ){ ?>

                        <div class="simple-form__group">

                            <label class="input-wrap simple-form__input">
                                <input class="input-def" name="PASSWORD" type="password">
                                <span class="label-float">Новый пароль</span>
                            </label>

                            <label class="input-wrap simple-form__input">
                                <input class="input-def" name="CONFIRM_PASSWORD" type="password">
                                <span class="label-float">Повтор пароля</span>
                            </label>

                        </div>

                        <p class="error___p"></p>

                        <input type="hidden" name="code" value="<?=$arResult['KOD']?>">

                        <div class="input-wrap input-wrap_btn simple-form__input simple-form__input_btn">
                            <input class="btn-alt simple-form__btn password___recovery_save_button to___process" type="button" value="Сохранить">
                        </div>

                    <? } else { ?>

                        <p class="error___p" style="font-size: 18px;"><?=$arResult['ERROR']?></p>

                    <? } ?>

                </form>

            </div>

        </section>

    </div>

</div>

