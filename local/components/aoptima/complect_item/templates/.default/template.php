<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools; ?>

<div class="sets__item catalog___item" item_id="<?=$arResult['ID']?>">

    <div class="m-heading">
        <h2><a href="<?=$arResult['DETAIL_PAGE_URL']?>"><?=$arResult['NAME']?></a></h2>
    </div>

    <div class="set equal-height-js">

        <? $cnt = 0;
        foreach( $arResult['COMPLECT_PRODUCTS'] as $product ){ $cnt++;

            if( $cnt != 1 ){  echo '<div class="set__sign set__sign_plus">+</div>';  } ?>

            <div class="set__item">

                <div itemscope itemtype="http://schema.org/product-short" class="product-short product-short-js">

                    <div class="product-short__main">

                        <? // product_labels
                        $APPLICATION->IncludeComponent(
                            "aoptima:product_labels", "catalog_item",
                            array( 'element' => $product )
                        ); ?>

                        <a itemprop="url" href="<?=$product['DETAIL_PAGE_URL']?>">
                            <div class="product-short__figure">
                                <? if( intval($product['DETAIL_PICTURE']) > 0 ){ ?>
                                    <img itemprop="image" src="<?=SITE_TEMPLATE_PATH?>/img/preloader.svg" data-src="<?=tools\funcs::rIMGG($product['DETAIL_PICTURE'], 4, 125, 90)?>">
                                <? } else { ?>
                                    <img itemprop="image" src="<?=SITE_TEMPLATE_PATH?>/img/preloader.svg" data-src="<?=SITE_TEMPLATE_PATH?>/img/no-photo.png" style="max-width: 125px">
                                <? } ?>
                            </div>
<!--                            <div class="product-short__category">Сумки</div>-->
                            <h3 itemprop="name" class="product-short__title"><span><?=$product['NAME']?></span></h3>
                        </a>
                    </div>

                    <div class="product-short__footer">
                        <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="product-short__offer">
                            <div class="p-short-price">

<!--                                <div class="p-short-price__old"><strong class="p-short-price__old-val">1457</strong>&nbsp<em class="p-short-price__old-unit">р.</em></div>-->

                                <div class="p-short-price__new">
                                    <strong class="p-short-price__new-val" itemprop="price" content="<?=$product['CATALOG_PRICE_'.project\catalog::BASE_PRICE_ID]?>"><?=number_format($product['CATALOG_PRICE_'.project\catalog::BASE_PRICE_ID], project\catalog::PRICE_ROUND, ".", " ")?></strong>&nbsp;<em class="p-short-price__new-unit" itemprop="priceCurrency" content="BYN">р.</em>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

            </div>

        <? } ?>

        <div class="set__sign set__sign_equal">=</div>

        <div class="set__result">
            <div class="set-result">

                <? if( $arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['VALUE'] > 0 ){ ?>

                    <div class="set-result-price set-result__price">

                        <?php if(
                            $arResult['COMPLECT_PRODUCTS_SUM'] > 0
                            &&
                            $arResult['COMPLECT_PRODUCTS_SUM'] > $arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['DISCOUNT_VALUE_VAT']
                        ){ ?>

                            <div class="set-result-price__old">
                                <strong class="set-result-price__old-val"><?=number_format($arResult['COMPLECT_PRODUCTS_SUM'], project\catalog::PRICE_ROUND, ".", " ")?></strong>&nbsp;<em class="set-result-price__old-unit">р.</em>
                            </div>

                        <? } ?>

                        <div class="set-result-price__new">
                            <strong class="set-result-price__new-val"><?=number_format($arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['DISCOUNT_VALUE_VAT'], project\catalog::PRICE_ROUND, ".", " ")?></strong>&nbsp;<em class="set-result-price__new-unit">р.</em>
                        </div>

                    </div>

                <? } ?>

                <? if( $arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['VALUE'] > 0 ){ ?>

                    <div class="set-result-options">

                        <div class="set-result-options__item">
                            <a style="cursor:pointer;" class="btn-buy set-result__btn  add_to_basket_button to___process" item_id="<?=$arResult['ID']?>">
                                <span class="btn-buy__buy">Купить</span>
                                <span class="btn-buy__in-cart">В&nbsp;корзине</span>
                                <i>
                                    <svg class="svg-ico-cart" xmlns="http://www.w3.org/2000/svg" width="23" height="21" viewBox="0 0 23 21">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M23 0.902583C23 1.40102 22.6057 1.80517 22.1192 1.80517H20.3482L14.4767 15.6447H2.34872L0 6.01718H11.4035C11.89 6.01718 12.2842 6.42117 12.2842 6.91976C12.2842 7.4182 11.89 7.82235 11.4035 7.82235H2.25645L3.72437 13.8396H13.3215L19.1931 0H22.1192C22.6059 0 23 0.403934 23 0.902583ZM7.84736 18.5028C7.84736 19.8797 6.75405 21 5.41057 21C4.06724 21 2.97399 19.8797 2.97399 18.5028C2.97399 17.1258 4.06724 16.0055 5.41057 16.0055C6.75405 16.0055 7.84736 17.1258 7.84736 18.5028ZM5.41057 17.2089C6.10667 17.2089 6.6731 17.7895 6.6731 18.5028C6.6731 19.2163 6.10667 19.7966 5.41057 19.7966C4.71467 19.7966 4.14824 19.2163 4.14824 18.5028C4.14824 17.7895 4.71467 17.2089 5.41057 17.2089ZM14.3235 18.5028C14.3235 19.8797 13.23 21 11.8866 21C10.5433 21 9.45009 19.8797 9.45009 18.5028C9.45009 17.1258 10.5433 16.0055 11.8866 16.0055C13.23 16.0055 14.3235 17.1258 14.3235 18.5028ZM11.8866 17.2089C12.5828 17.2089 13.1493 17.7895 13.1493 18.5028C13.1493 19.2163 12.5827 19.7966 11.8866 19.7966C11.1906 19.7966 10.6243 19.2163 10.6243 18.5028C10.6243 17.7895 11.1906 17.2089 11.8866 17.2089Z"></path>
                                    </svg>
                                </i>
                            </a>
                        </div>

                        <? if( $arResult['PRICES'][ project\catalog::RASSROCHKA_PRICE_CODE]['VALUE'] > 0 && 0 ){ ?>

                            <div class="set-result-options__item">
                                <a style="cursor: pointer" class="btn-alt set-result__btn add_to_basket_button to___process" item_id="<?=$arResult['ID']?>">Купить в рассрочку</a>
                            </div>

                        <? } ?>

                    </div>

                <? } ?>

            </div>
        </div>

    </div>

</div>