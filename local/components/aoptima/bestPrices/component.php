<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$arResult['IS_DETAIL'] = 'N';
$arResult['IS_404'] = 'N';

$arURI = tools\funcs::arURI(tools\funcs::pureURL());


if( $arURI[3] ){

    $arResult['IS_404'] = 'Y';

} else if( $arURI[2] ){

    $sectCode = strip_tags($arURI[2]);

    // Каталожные инфоблоки
    $catalogIblocks = project\catalog::iblocks();

    $section = false;   $iblock = false;
    foreach ( $catalogIblocks as $iblock_id => $ib ){
        if( !$section ){
            $section = tools\section::info_by_code($sectCode, $iblock_id);
            if( intval($section['ID']) > 0 ){   $iblock = $ib;   }
        }
    }

    if( intval($section['ID']) > 0 ){

        $arResult['IS_DETAIL'] = 'Y';
        $arResult['SECTION'] = $section;

        $APPLICATION->SetPageProperty("title", 'Лучшие цены в категории "'.$section["NAME"].'"');

    } else {

        $arResult['IS_404'] = 'Y';

    }

} else {

    $arResult['IBLOCKS'] = project\catalog::bestPricesIblocks();

    foreach ( $arResult['IBLOCKS'] as $iblock_id => $ar ){
        $fistLevelSect = project\catalog::firstLevelCategory($iblock_id);
        $arResult['IBLOCKS'][$iblock_id]['SECTION'] = $fistLevelSect;
    }

}



$this->IncludeComponentTemplate();