<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');  use AOptima\Project as project;
\Bitrix\Main\Loader::includeModule('aoptima.tools');  use AOptima\Tools as tools;

if( count($arResult['CATEGORIES']) > 0 ){ ?>

    <section class="cart__similar tabs-js">

        <div class="cart__similar-caption">
            <h3 class="cart__similar-title">Подходящие товары</h3>
            <div class="simple-tabs__nav">
                <div class="simple-tabs__select tabs__select-js"></div>
                <div class="simple-tabs-thumbs tabs__thumbs-js tabs__select-drop-js">
                    <? $cnt = 0;
                    foreach( $arResult['CATEGORIES'] as $category ){ $cnt++; ?>
                        <a href="#basket_soput_<?=$category['ID']?>" <? if( $cnt == 1 ){ ?>class="tabs-active"<? } ?>><span><?=$category['NAME']?></span></a>
                    <? } ?>
                </div>
            </div>
        </div>

        <div class="tabs__panels-js">

            <? foreach( $arResult['CATEGORIES'] as $category ){

                $GLOBALS['basket_soput_goods']['ID'] = array_keys($category['ITEMS']);
                $APPLICATION->IncludeComponent(
                    "bitrix:catalog.section", "basket_soput_goods",
                    Array(
                        "category" => $category,
                        "IBLOCK_TYPE" => project\catalog::IBLOCK_TYPE,
                        "IBLOCK_ID" => $category['IBLOCK_ID'],
                        "SECTION_USER_FIELDS" => array(),
                        "ELEMENT_SORT_FIELD" => 'NAME',
                        "ELEMENT_SORT_ORDER" => 'ASC',
                        "ELEMENT_SORT_FIELD2" => "NAME",
                        "ELEMENT_SORT_ORDER2" => 'ASC',
                        "FILTER_NAME" => 'basket_soput_goods',
                        "HIDE_NOT_AVAILABLE" => "N",
                        "PAGE_ELEMENT_COUNT" => count($category['ITEMS']),
                        "LINE_ELEMENT_COUNT" => "3",
                        "PROPERTY_CODE" => array('STATUS_TOVARA'),
                        "OFFERS_LIMIT" => 0,
                        "TEMPLATE_THEME" => "",
                        "PRODUCT_SUBSCRIPTION" => "N",
                        "SHOW_DISCOUNT_PERCENT" => "N",
                        "SHOW_OLD_PRICE" => "N",
                        "MESS_BTN_BUY" => "Купить",
                        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                        "MESS_BTN_SUBSCRIBE" => "Подписаться",
                        "MESS_BTN_DETAIL" => "Подробнее",
                        "MESS_NOT_AVAILABLE" => "Нет в наличии",
                        "SECTION_URL" => "",
                        "DETAIL_URL" => "",
                        "SECTION_ID_VARIABLE" => "SECTION_ID",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "CACHE_TYPE" => project\catalog::CACHE_TYPE,
                        "CACHE_TIME" => project\catalog::CACHE_TIME,
                        "CACHE_GROUPS" => "Y",
                        "SET_META_KEYWORDS" => "N",
                        "META_KEYWORDS" => "",
                        "SET_META_DESCRIPTION" => "N",
                        "META_DESCRIPTION" => "",
                        "BROWSER_TITLE" => "-",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "DISPLAY_COMPARE" => "N",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "CACHE_FILTER" => "Y",
                        "PROPERTY_CODE" => array('STATUS_TOVARA'),
                        "PRICE_CODE" => array(
                            project\catalog::RASSROCHKA_PRICE_CODE,
                            project\catalog::BASE_PRICE_CODE
                        ),
                        "USE_PRICE_COUNT" => "N",
                        "SHOW_PRICE_COUNT" => "1",
                        "PRICE_VAT_INCLUDE" => "Y",
                        "CONVERT_CURRENCY" => "N",
                        "BASKET_URL" => "/personal/basket.php",
                        "ACTION_VARIABLE" => "action",
                        "PRODUCT_ID_VARIABLE" => "id",
                        "USE_PRODUCT_QUANTITY" => "N",
                        "ADD_PROPERTIES_TO_BASKET" => "Y",
                        "PRODUCT_PROPS_VARIABLE" => "prop",
                        "PARTIAL_PRODUCT_PROPERTIES" => "N",
                        "PRODUCT_PROPERTIES" => "",
                        "PAGER_TEMPLATE" => "",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Товары",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "Y",
                        "ADD_PICT_PROP" => "-",
                        "LABEL_PROP" => "-",
                        'INCLUDE_SUBSECTIONS' => "Y",
                        'SHOW_ALL_WO_SECTION' => "Y"
                    )
                );

            } ?>

        </div>
    </section>

<? } ?>