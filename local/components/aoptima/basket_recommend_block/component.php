<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;


$arResult['arBasket'] = $arParams['arBasket'];

$arResult['CATEGORIES'] = [];

if( count($arResult['arBasket']) > 0 ){
    foreach ( $arResult['arBasket'] as $basketItem ){
        $product_id = $basketItem->getProductID();
        $product = tools\el::info($product_id);
        if( intval($product['ID']) > 0 ){
            
            if( is_array($product['PROPERTY_'.project\catalog::SOPUT_PROP_CODE.'_VALUE']) ){
                foreach ( $product['PROPERTY_'.project\catalog::SOPUT_PROP_CODE.'_VALUE'] as $xml_id ){
                    $xml_el = false;
                    $catalogIblocks = project\catalog::iblocks();
                    foreach ( $catalogIblocks as $iblock_id => $iblock ){
                        if( !$xml_el ){
                            $el = tools\el::info_by_xml_id($xml_id, $iblock_id);
                            if( intval($el['ID']) > 0 ){    $xml_el = $el;    }
                        }
                    }
                    if( $xml_el ){
                        $section = tools\el::sections($xml_el['ID'])[0];
                        if( !$arResult['CATEGORIES'][$section['ID']] ){
                            $arResult['CATEGORIES'][$section['ID']] = $section;
                        }
                        $arResult['CATEGORIES'][$section['ID']]['ITEMS'][$xml_el['ID']] = $xml_el;
                    }
                }
            }
        }
    }
}





$this->IncludeComponentTemplate();