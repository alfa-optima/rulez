<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>

<!--CONTENT-->
<div class="content">

    <? if( $arResult['IS_AUTH'] == 'Y' ){ ?>

        <div class="content-align">

            <div class="content-inner article-content">

                <h1><?$APPLICATION->ShowTitle()?></h1>

                <div class="cabinet-profile-form">

                    <form onsubmit="return false;" class="simple-form validate-js" style="margin-bottom: 40px;">

                        <h2>Личные данные</h2>

                        <div class="simple-form__group">
                            <label class="input-wrap simple-form__input">
                                <input class="input-def" name="NAME" type="text" value="<?=$arResult['USER']['NAME']?>">
                                <span class="label-float">Имя</span>
                                <span class="error-note">Текст ошибки</span>
                                <span class="success-note">Текст успешной отправки</span>
                            </label>
                            <label class="input-wrap simple-form__input">
                                <input class="input-def" name="LAST_NAME" type="text" value="<?=$arResult['USER']['LAST_NAME']?>">
                                <span class="label-float">Фамилия</span>
                                <span class="error-note">Текст ошибки</span>
                                <span class="success-note">Текст успешной отправки</span>
                            </label>
                            <label class="input-wrap simple-form__input">
                                <input class="input-def" name="SECOND_NAME" type="text" value="<?=$arResult['USER']['SECOND_NAME']?>">
                                <span class="label-float">Отчество</span>
                                <span class="error-note">Текст ошибки</span>
                                <span class="success-note">Текст успешной отправки</span>
                            </label>

                            <? //if( $arResult['USER']['EXTERNAL_AUTH_ID'] != 'socservices' ){ ?>

                                <label class="input-wrap simple-form__input">
                                    <input class="input-def" name="EMAIL" type="email" value="<?=$arResult['USER']['EMAIL']?>">
                                    <span class="label-float">E-mail *</span>
                                    <span class="error-note">Текст ошибки</span>
                                    <span class="success-note">Текст успешной отправки</span>
                                </label>

                            <? //} ?>

                            <label class="input-wrap simple-form__input">
                                <input class="input-def phone___input" name="PERSONAL_PHONE" type="text" value="<?=$arResult['USER']['PERSONAL_PHONE']?>">
                                <span class="label-float">Номер телефона</span>
                                <span class="error-note">Текст ошибки</span>
                                <span class="success-note">Текст успешной отправки</span>
                            </label>
                            <label class="input-wrap simple-form__input">
                                <input class="input-def phone___input" name="UF_DOP_PHONE" type="text" value="<?=$arResult['USER']['UF_DOP_PHONE']?>">
                                <span class="label-float">Доп. номер телефона</span>
                                <span class="error-note">Текст ошибки</span>
                                <span class="success-note">Текст успешной отправки</span>
                            </label>
                            <label class="input-wrap simple-form__input">
                                <input class="input-def js-datepicker" name="PERSONAL_BIRTHDATE" type="text" data-locale="ru" data-date-format="d.m.Y" value="<?=$arResult['USER']['PERSONAL_BIRTHDATE']?>">
                                <span class="label-float">Дата рождения</span>
                                <span class="error-note">Текст ошибки</span>
                                <span class="success-note">Текст успешной отправки</span>
                            </label>
                        </div>

                        <p class="error___p"></p>

                        <div class="input-wrap input-wrap_btn simple-form__input simple-form__input_btn">
                            <input class="btn-alt simple-form__btn personal___data_button to___process" type="button" value="Сохранить">
                        </div>

                    </form>

                    <? if( $arResult['USER']['EXTERNAL_AUTH_ID'] != 'socservices' ){ ?>

                        <form onsubmit="return false;" class="simple-form validate-js">

                            <h2>Смена пароля</h2>
                            <div class="simple-form__group">
                                <label class="input-wrap simple-form__input">
                                    <input class="input-def" name="PASSWORD" type="password" required>
                                    <span class="label-float">Новый пароль</span>
                                    <span class="error-note">Текст ошибки</span>
                                    <span class="success-note">Текст успешной отправки</span>
                                </label>
                                <label class="input-wrap simple-form__input">
                                    <input class="input-def" name="CONFIRM_PASSWORD" type="password" required>
                                    <span class="label-float">Подтверждение пароля</span>
                                    <span class="error-note">Текст ошибки</span>
                                    <span class="success-note">Текст успешной отправки</span>
                                </label>
                            </div>

                            <p class="error___p"></p>

                            <div class="input-wrap input-wrap_btn simple-form__input simple-form__input_btn">
                                <input class="btn-alt simple-form__btn personal___password_button to___process" type="button" value="Сохранить">
                            </div>

                        </form>

                    <? } ?>

                </div>
            </div>

        </div>

        <aside class="sidebar">
            <? // personal
            $APPLICATION->IncludeComponent(
                "bitrix:menu",  "personal", // Шаблон меню
                Array(
                    "ROOT_MENU_TYPE" => "personal", // Тип меню
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "CACHE_SELECTED_ITEMS" => "N",
                    "MENU_CACHE_GET_VARS" => array(""),
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "left",
                    "USE_EXT" => "N",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N",
                    "REQUEST_URI" => $_SERVER['REQUEST_URI']
                )
            ); ?>
        </aside>


    <? } else {

        include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/auth_form.php';

    } ?>

</div>