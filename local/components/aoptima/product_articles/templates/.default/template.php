<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if( count($arResult['ITEMS']) > 0 ){ ?>

    <section class="section main-section_articles">

        <h2 class="section__title">Статьи и обзоры, полезные советы</h2>

        <div class="articles-tiles">
            <div class="articles-tiles__list grid-js">
                
                <? $cnt = 0;
                foreach( $arResult['ITEMS'] as $key => $arItem ){ $cnt++;
                    if( $cnt == 1 ){ ?>

                        <div class="articles-tiles__item articles-tiles__item_wide grid-item-js" data-align="bottom left">
                            <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="articles-tiles-elem" style="color: #fff;">
                                <div class="articles-tiles-elem__figure lozad" data-background-image="<?=tools\funcs::rIMGG($arItem['PREVIEW_PICTURE'], 5, 946, 840)?>" style="background-color: #bacbdf;">&nbsp;</div>
                                <div class="articles-tiles-elem__content">
                                    <time class="articles-tiles-elem__date" datetime="<?=ConvertDateTime($arItem['PROPERTY_SORT_DATE_VALUE'], "YYYY-MM-DD", "ru")?>"><?=ConvertDateTime($arItem['PROPERTY_SORT_DATE_VALUE'], "DD.MM.YYYY", "ru")?></time>
                                    <? if( strlen($arItem['PROPERTY_SECTION_NAME_VALUE']) > 0 ){ ?>
                                        <span class="articles-tiles-elem__label"><?=$arItem['PROPERTY_SECTION_NAME_VALUE']?></span>
                                    <? } ?>
                                    <h3 class="articles-tiles-elem__title"><span><?=$arItem['NAME']?></span></h3>
                                </div>
                            </a>
                        </div>

                    <? } else if( $cnt == 2 ){ ?>

                        <div class="articles-tiles__item articles-tiles__item_wide grid-item-js" data-align="top left">
                            <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="articles-tiles-elem" style="color: #000;">
                                <div class="articles-tiles-elem__figure lozad" data-background-image="<?=tools\funcs::rIMGG($arItem['PREVIEW_PICTURE'], 5, 950, 414)?>" style="background-color: #f8f4ed;">&nbsp;</div>
                                <div class="articles-tiles-elem__content">
                                    <? if( strlen($arItem['PROPERTY_SECTION_NAME_VALUE']) > 0 ){ ?>
                                        <span class="articles-tiles-elem__label"><?=$arItem['PROPERTY_SECTION_NAME_VALUE']?></span>
                                    <? } ?>
                                    <h3 class="articles-tiles-elem__title"><span><?=$arItem['NAME']?></span></h3>
                                </div>
                            </a>
                        </div>

                    <? } else if( $cnt == 3 ){ ?>

                        <div class="articles-tiles__item grid-item-js" data-align="top left">
                            <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="articles-tiles-elem" style="color: #000;">
                                <div class="articles-tiles-elem__figure lozad" data-background-image="<?=tools\funcs::rIMGG($arItem['PREVIEW_PICTURE'], 5, 460, 844)?>" style="background-color: #eaf4ff;">&nbsp;</div>
                                <div class="articles-tiles-elem__content">
                                    <? if( strlen($arItem['PROPERTY_SECTION_NAME_VALUE']) > 0 ){ ?>
                                        <span class="articles-tiles-elem__label"><?=$arItem['PROPERTY_SECTION_NAME_VALUE']?></span>
                                    <? } ?>
                                    <h3 class="articles-tiles-elem__title"><span><?=$arItem['NAME']?></span></h3>
                                </div>
                            </a>
                        </div>

                    <? } else if( $cnt == 4 ){ ?>

                        <div class="articles-tiles__item articles-tiles__item_text-small grid-item-js" data-align="bottom left">
                            <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="articles-tiles-elem" style="color: #fff;">
                                <div class="articles-tiles-elem__figure lozad" data-background-image="<?=tools\funcs::rIMGG($arItem['PREVIEW_PICTURE'], 5, 466, 414)?>" style="background-color: #fddb00;">&nbsp;</div>
                                <div class="articles-tiles-elem__content">
                                    <? if( strlen($arItem['PROPERTY_SECTION_NAME_VALUE']) > 0 ){ ?>
                                        <span class="articles-tiles-elem__label"><?=$arItem['PROPERTY_SECTION_NAME_VALUE']?></span>
                                    <? } ?>
                                    <h3 class="articles-tiles-elem__title"><span><?=$arItem['NAME']?></span></h3>
                                </div>
                            </a>
                        </div>

                    <? } else if( $cnt == 5 ){ ?>

                        <div class="articles-tiles__item articles-tiles__item_text-small grid-item-js" data-align="bottom left">
                            <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="articles-tiles-elem" style="color: #fff;">
                                <div class="articles-tiles-elem__figure lozad" data-background-image="<?=tools\funcs::rIMGG($arItem['PREVIEW_PICTURE'], 5, 464, 414)?>" style="background-color: #7c4b9c;">&nbsp;</div>
                                <div class="articles-tiles-elem__content">
                                    <? if( strlen($arItem['PROPERTY_SECTION_NAME_VALUE']) > 0 ){ ?>
                                        <span class="articles-tiles-elem__label"><?=$arItem['PROPERTY_SECTION_NAME_VALUE']?></span>
                                    <? } ?>
                                    <h3 class="articles-tiles-elem__title"><span><?=$arItem['NAME']?></span></h3>
                                </div>
                            </a>
                        </div>

                    <? }
                } ?>

            </div>
        </div>
    </section>

<? } ?>