<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */

\Bitrix\Main\Loader::includeModule('aoptima.tools');   use AOptima\Tools as tools;
\Bitrix\Main\Loader::includeModule('aoptima.project');   use AOptima\Project as project;

$arResult['CUR_URL'] = $_SERVER['REQUEST_URI'];

$arResult['CATALOG_IBLOCKS'] = project\catalog::iblocks();


// Категории 1 уровня каталога
$arResult['CATEGORIES_1_LEVEL'] = [];
foreach ( $arResult['CATALOG_IBLOCKS'] as $iblock_id => $iblock ){

    if( $iblock_id != project\complect::IBLOCK_ID ){

        $category = project\catalog::firstLevelCategory($iblock_id);

        // Подкатегории
        $category['SUB_SECTIONS'] = tools\section::sub_sects( $category['ID'] );

        $seoPages = project\SeopageTable::getBySectionXMLID($category['XML_ID']);
        if( count($seoPages) > 0 ){
            $category['SUB_SECTIONS'] = array_merge($category['SUB_SECTIONS'], $seoPages);
        }

        if( count($category['SUB_SECTIONS']) > 0 ){
            foreach ( $category['SUB_SECTIONS'] as $key => $sub_section ){

                $sub_section['SUB_SECTIONS'] = tools\section::sub_sects( $sub_section['ID'] );
                $category['SUB_SECTIONS'][$key] = $sub_section;

            }
        }

        if( intval($category['ID']) > 0 ){
            $arResult['CATEGORIES_1_LEVEL'][$category['CODE']] = $category;
        }

    }

}





$this->IncludeComponentTemplate();