<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>

<div class="catalog-menu__list-wrap">
    <ul class="catalog-menu__list catalog-list-rolls-js">

        <? foreach(
            $arResult['CATEGORIES_1_LEVEL']
            as
            $section_code => $section_1
        ){ ?>

            <li class="catalog-menu__item <? if( substr_count($arResult['CUR_URL'], $section_1['SECTION_PAGE_URL']) ){  echo 'current';  } ?>">

                <div class="catalog-menu__title">
                    <a href="<?=$section_1['SECTION_PAGE_URL']?>">
                        <?=html_entity_decode($section_1['UF_TOP_MENU_SVG'])?>
                        <span><?=$section_1['NAME']?></span>
                    </a>
                    <? if( count($section_1['SUB_SECTIONS']) > 0 ){ ?>
                        <em class="catalog-menu-angle catalog-list__angle catalog-list-angle-js">
                            <i class="icon-angle-color catalog-menu-angle__icon"></i>
                        </em>
                    <? } ?>
                </div>

                <? if( count($section_1['SUB_SECTIONS']) > 0 ){ ?>

                    <ul class="catalog-menu__links catalog-links-rolls-js">

                        <? foreach( $section_1['SUB_SECTIONS'] as $section_2 ){ ?>

                            <li <? if( count($section_2['SUB_SECTIONS']) > 0 ){ ?>class="has-items"<? } ?>>

                                <a href="<?=$section_2['SECTION_PAGE_URL']?>"><span><?=$section_2['NAME']?></span></a>

                                <? if( count($section_2['SUB_SECTIONS']) > 0 ){ ?>

                                    <em class="catalog-menu-angle catalog-menu__angle catalog-menu-angle-js">
                                        <i class="icon-angle-color catalog-menu-angle__icon"></i>
                                    </em>

                                    <ul>
                                        <? foreach( $section_2['SUB_SECTIONS'] as $section_3 ){ ?>
                                            <li>
                                                <a href="<?=$section_3['SECTION_PAGE_URL']?>"><span><?=$section_3['NAME']?></span></a>
                                            </li>
                                        <? } ?>
                                    </ul>

                                <? } ?>

                            </li>

                        <? } ?>

                    </ul>

                <? } ?>

            </li>

        <? } ?>

    </ul>
</div>


<? // hidden_top_menu
$APPLICATION->IncludeComponent(
    "bitrix:menu",  "hidden_top_menu", // Шаблон меню
    Array(
        "ROOT_MENU_TYPE" => "bottom", // Тип меню
        "MENU_CACHE_TYPE" => "A",
        "MENU_CACHE_TIME" => "3600",
        "MENU_CACHE_USE_GROUPS" => "N",
        "CACHE_SELECTED_ITEMS" => "N",
        "MENU_CACHE_GET_VARS" => array(""),
        "MAX_LEVEL" => "1",
        "CHILD_MENU_TYPE" => "left",
        "USE_EXT" => "N",
        "DELAY" => "N",
        "ALLOW_MULTI_SELECT" => "N"
    )
); ?>




