<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>

<ul class="articles-nav">

    <? foreach( $arResult['CATEGORIES_1_LEVEL'] as $section_code => $section_1 ){ ?>

        <li class="current">

            <a href="<?=$section_1['SECTION_PAGE_URL']?>"><?=$section_1['NAME']?></a>

            <? if( count($section_1['SUB_SECTIONS']) > 0 ){ ?>

                <ul>

                    <? foreach( $section_1['SUB_SECTIONS'] as $section_2 ){ ?>

                        <li class="current">

                            <a href="<?=$section_2['SECTION_PAGE_URL']?>"><?=$section_2['NAME']?></a>

                            <? if( count($section_2['SUB_SECTIONS']) > 0 ){ ?>

                                <ul>

                                    <? foreach( $section_2['SUB_SECTIONS'] as $section_3 ){ ?>

                                        <li>
                                            <a href="<?=$section_3['SECTION_PAGE_URL']?>"><?=$section_3['NAME']?></a>
                                        </li>

                                    <? } ?>

                                </ul>

                            <? } ?>

                        </li>

                    <? } ?>

                </ul>

            <? } ?>

        </li>

    <? } ?>

</ul>
