<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use AOptima\Project as project;
use AOptima\Tools as tools;

if(
    \Bitrix\Main\Loader::includeModule('aoptima.project')
    &&
    \Bitrix\Main\Loader::includeModule('aoptima.tools')
){ ?>
    <basket_small>
        <a href="/basket/" class="cart-link cart-keeper-js" title="Оформить заказа">
            <i>
                <svg class="svg-ico-cart" xmlns="http://www.w3.org/2000/svg" width="30" height="30"
                     viewBox="0 0 30 30">
                    <path class="svg-ico-cart-circle" fill-rule="evenodd" clip-rule="evenodd" d="M15 30C23.2843 30 30 23.2843 30 15C30 6.71573 23.2843 0 15 0C6.71573 0 0 6.71573 0 15C0 23.2843 6.71573 30 15 30Z"></path>
                    <path class="svg-ico-cart-cart" fill-rule="evenodd" clip-rule="evenodd" d="M30 5.90258C30 6.40102 29.6057 6.80517 29.1192 6.80517H27.3482L21.4767 20.6447H9.34872L7 11.0172H18.4035C18.89 11.0172 19.2842 11.4212 19.2842 11.9198C19.2842 12.4182 18.89 12.8223 18.4035 12.8223H9.25645L10.7244 18.8396H20.3215L26.1931 5H29.1192C29.6059 5 30 5.40393 30 5.90258ZM14.8474 23.5028C14.8474 24.8797 13.7541 26 12.4106 26C11.0672 26 9.97399 24.8797 9.97399 23.5028C9.97399 22.1258 11.0672 21.0055 12.4106 21.0055C13.7541 21.0055 14.8474 22.1258 14.8474 23.5028ZM12.4106 22.2089C13.1067 22.2089 13.6731 22.7895 13.6731 23.5028C13.6731 24.2163 13.1067 24.7966 12.4106 24.7966C11.7147 24.7966 11.1482 24.2163 11.1482 23.5028C11.1482 22.7895 11.7147 22.2089 12.4106 22.2089ZM21.3235 23.5028C21.3235 24.8797 20.23 26 18.8866 26C17.5433 26 16.4501 24.8797 16.4501 23.5028C16.4501 22.1258 17.5433 21.0055 18.8866 21.0055C20.23 21.0055 21.3235 22.1258 21.3235 23.5028ZM18.8866 22.2089C19.5828 22.2089 20.1493 22.7895 20.1493 23.5028C20.1493 24.2163 19.5827 24.7966 18.8866 24.7966C18.1906 24.7966 17.6243 24.2163 17.6243 23.5028C17.6243 22.7895 18.1906 22.2089 18.8866 22.2089Z"></path>
                </svg>
            </i>
            <div class="cart-link__text">
                <strong>Корзина</strong>
                <span class="basket_small_sum_block"><?=number_format($arResult['calcInfo']['BASKET_DISC_SUM'], project\catalog::PRICE_ROUND, ",", " ")?> руб</span>
            </div>
            <em class="counter counter-js basket_small_cnt_block"><?=round($arResult['basket_cnt'])?></em>
        </a>
        <div class="quick-cart quick-cart-js stop-remove-class">
            <div class="quick-cart__overflow">
                <div class="quick-cart__list">
                    <? if( count($arResult['arBasket']) > 0 ){
                        foreach( $arResult['arBasket'] as $key => $basketItem ){ ?>
                            <a class="quick-cart__item quick-cart__item-js" product_id="<?=$basketItem->el['ID']?>" quantity="<?=round($basketItem->getQuantity(), 0)?>">
                                <div class="quick-cart__item-figure">
                                    <? if( intval($basketItem->el['DETAIL_PICTURE']) > 0 ){ ?>
                                        <img src="<?=tools\funcs::rIMGG($basketItem->el['DETAIL_PICTURE'], 4, 118, 78)?>">
                                    <? } else { ?>
                                        <img src="<?=SITE_TEMPLATE_PATH?>/img/no-photo_small_basket.png">
                                    <? } ?>
                                </div>
                                <div class="quick-cart__item-content">

                                    <strong class="quick-cart__item-title"><?=$basketItem->el['NAME']?></strong>

                                    <div class="quick-cart__item-meta">

                                        <div class="quick-cart__item-count"><?=$basketItem->getQuantity()?> шт</div>

                                        <div class="quick-cart__item-price">

                                            <?php if( isset($basketItem->oldPrice) ){ ?>

                                                <div class="quick-cart__item-price-old">
                                                    <?=$basketItem->oldPriceFormat?> р.
                                                </div>
                                                <div class="quick-cart__item-price-new">
                                                    <?=number_format(($basketItem->discPrice * $basketItem->getQuantity()), project\catalog::PRICE_ROUND, ",", " ")?> р.
                                                </div>

                                            <?php } else {

                                                echo number_format(($basketItem->discPrice * $basketItem->getQuantity()), project\catalog::PRICE_ROUND, ",", " ")." р.";

                                            } ?>

                                        </div>

                                    </div>

                                    <?php if( isset($basketItem->oldPrice) ){ ?>
                                        <div class="quick-cart__item-note">Спецпредложение</div>
                                    <? } ?>

                                </div>
                                <div class="quick-cart__item-remove smallBasketRemoveButton to___process" item_id="<?=$arResult['basket'][$key]->getField('ID')?>">
                                    <em>x</em>
                                    <span>Удалить из корзины</span>
                                </div>
                            </a>
                        <? }
                    } else { ?>
                        <div class="quick-cart__empty quick-cart__empty-js" style="display:block; text-align: center;">В корзине нет товаров</div>
                    <? } ?>
                </div>
            </div>
            <? if( count($arResult['arBasket']) > 0 ){ ?>
                <div class="quick-cart__footer">
                    <a href="/basket/" class="btn-def quick-cart__btn quick-cart__btn-js">Оформить заказ</a>
                </div>
            <? } ?>
        </div>
    </basket_small>
<? } ?>