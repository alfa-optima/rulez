<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if(
    count($arResult['seo_pages']) > 0
    ||
    count($arResult['goods']['items']) > 0
    ||
    count($arResult['articles']) > 0
){ ?>

    <div class="s-result stop-remove-class js-search-fast-result s-result_show quickSearchBlock">
        <div class="s-result__layout max-wrap layout">
            <div class="s-result__holder">
                <div class="s-result__content">

                    <? if( count($arResult['seo_pages']) > 0 ){ ?>

                        <div class="s-result__title">Теги:</div>
                        <div class="s-result__row">
                            <ul class="float-list">

                                <? foreach( $arResult['seo_pages'] as $seo_page ){ ?>
                                    <li>
                                        <a href="/<?=$seo_page['CODE']?>/"><span><?=$seo_page['NAME']?></span></a>
                                    </li>
                                <? } ?>

                            </ul>
                        </div>

                    <? } ?>

                    <? if( count($arResult['goods']['items']) > 0 ){ ?>

                        <div class="s-result__title">Товары:</div>

                        <div class="s-result__row">
                            <div class="s-result__list">

                                <? foreach( $arResult['goods']['items'] as $el_id => $iblock_id ){

                                    $GLOBALS['quick_search_one_good']['ID'] = $el_id;
                                    $APPLICATION->IncludeComponent(
                                        "bitrix:catalog.section", "quick_search_one_good",
                                        Array(
                                            "SEARCH_Q" => $arResult['q'],
                                            "IBLOCK_TYPE" => project\catalog::IBLOCK_TYPE,
                                            "IBLOCK_ID" => $iblock_id,
                                            "SECTION_USER_FIELDS" => array(),
                                            "ELEMENT_SORT_FIELD" => 'NAME',
                                            "ELEMENT_SORT_ORDER" => 'ASC',
                                            "ELEMENT_SORT_FIELD2" => "NAME",
                                            "ELEMENT_SORT_ORDER2" => 'ASC',
                                            "FILTER_NAME" => 'quick_search_one_good',
                                            "HIDE_NOT_AVAILABLE" => "N",
                                            "PAGE_ELEMENT_COUNT" => 1,
                                            "LINE_ELEMENT_COUNT" => "3",
                                            "PROPERTY_CODE" => array('STATUS_TOVARA'),
                                            "OFFERS_LIMIT" => 0,
                                            "TEMPLATE_THEME" => "",
                                            "PRODUCT_SUBSCRIPTION" => "N",
                                            "SHOW_DISCOUNT_PERCENT" => "N",
                                            "SHOW_OLD_PRICE" => "N",
                                            "MESS_BTN_BUY" => "Купить",
                                            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                                            "MESS_BTN_SUBSCRIBE" => "Подписаться",
                                            "MESS_BTN_DETAIL" => "Подробнее",
                                            "MESS_NOT_AVAILABLE" => "Нет в наличии",
                                            "SECTION_URL" => "",
                                            "DETAIL_URL" => "",
                                            "SECTION_ID_VARIABLE" => "SECTION_ID",
                                            "AJAX_MODE" => "N",
                                            "AJAX_OPTION_JUMP" => "N",
                                            "AJAX_OPTION_STYLE" => "Y",
                                            "AJAX_OPTION_HISTORY" => "N",
                                            "CACHE_TYPE" => project\catalog::CACHE_TYPE,
                                            "CACHE_TIME" => project\catalog::CACHE_TIME,
                                            "CACHE_GROUPS" => "Y",
                                            "SET_META_KEYWORDS" => "N",
                                            "META_KEYWORDS" => "",
                                            "SET_META_DESCRIPTION" => "N",
                                            "META_DESCRIPTION" => "",
                                            "BROWSER_TITLE" => "-",
                                            "ADD_SECTIONS_CHAIN" => "N",
                                            "DISPLAY_COMPARE" => "N",
                                            "SET_TITLE" => "N",
                                            "SET_STATUS_404" => "N",
                                            "CACHE_FILTER" => "Y",
                                            "PRICE_CODE" => array(
                                                project\catalog::RASSROCHKA_PRICE_CODE,
                                                project\catalog::BASE_PRICE_CODE
                                            ),
                                            "USE_PRICE_COUNT" => "N",
                                            "SHOW_PRICE_COUNT" => "1",
                                            "PRICE_VAT_INCLUDE" => "Y",
                                            "CONVERT_CURRENCY" => "N",
                                            "BASKET_URL" => "/personal/basket.php",
                                            "ACTION_VARIABLE" => "action",
                                            "PRODUCT_ID_VARIABLE" => "id",
                                            "USE_PRODUCT_QUANTITY" => "N",
                                            "ADD_PROPERTIES_TO_BASKET" => "Y",
                                            "PRODUCT_PROPS_VARIABLE" => "prop",
                                            "PARTIAL_PRODUCT_PROPERTIES" => "N",
                                            "PRODUCT_PROPERTIES" => "",
                                            "PAGER_TEMPLATE" => "",
                                            "DISPLAY_TOP_PAGER" => "N",
                                            "DISPLAY_BOTTOM_PAGER" => "Y",
                                            "PAGER_TITLE" => "Товары",
                                            "PAGER_SHOW_ALWAYS" => "N",
                                            "PAGER_DESC_NUMBERING" => "N",
                                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                            "PAGER_SHOW_ALL" => "Y",
                                            "ADD_PICT_PROP" => "-",
                                            "LABEL_PROP" => "-",
                                            'INCLUDE_SUBSECTIONS' => "Y",
                                            'SHOW_ALL_WO_SECTION' => "Y"
                                        )
                                    );

                                } ?>

                                <? if( $arResult['goods']['has_more'] ){ ?>

                                    <div class="s-result__list-item">

                                        <a href="/search/?q=<?=$arResult['q']?>" class="s-result__list-inner" style="color: #6e52dd;">Смотреть больше товаров &#8594;</a>

                                    </div>

                                <? } ?>

                            </div>
                        </div>

                    <? } ?>

                    <? if( count($arResult['articles']) > 0 ){ ?>

                        <div class="s-result__title">Статьи:</div>
                        <div class="s-result__row">
                            <div class="s-result__list">

                                <? foreach( $arResult['articles'] as $article ){ ?>

                                    <div class="s-result__list-item">
                                        <a href="<?=$article['DETAIL_PAGE_URL']?>" class="s-result__list-inner"><?=$article['NAME']?></a>
                                    </div>

                                <? } ?>

                            </div>
                        </div>

                    <? } ?>

                </div>
            </div>
        </div>
    </div>

<? } ?>