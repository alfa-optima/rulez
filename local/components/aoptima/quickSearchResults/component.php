<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('search');


$catalogIblocksIDS = [];
foreach ( project\catalog::iblocks() as $iblock ){
    $catalogIblocksIDS[] = $iblock['ID'];
}

$arResult['q'] = strip_tags(trim($_POST['q']));

$maxGoodsCNT = 10;  $goods_cnt = 0;
$maxArticlesCNT = 10;

$arResult['seo_pages'] = [];
$arResult['goods'] = [
    'items' => [],
    'has_more' => false
];
$arResult['articles'] = [];

$qHasRu = preg_match("/[а-яёА-ЯЁ]/", $arResult['q'], $matches, PREG_OFFSET_CAPTURE);


// Поиск сео-страниц
$all_seo_pages = project\SeopageTable::allList();
foreach ( $all_seo_pages as $seo_page ){
    $section = project\catalog::getSectionByXMLID( $seo_page['SECTION_XML_ID'] );
    if(
        tools\funcs::string_begins_with(
            trim(strtolower($arResult['q'])),
            trim(strtolower($seo_page['NAME']))
        )
        &&
        intval($section['ID']) > 0
    ){     $arResult['seo_pages'][$seo_page['ID']] = $seo_page;     }
}
foreach ( $all_seo_pages as $seo_page ){
    $section = project\catalog::getSectionByXMLID( $seo_page['SECTION_XML_ID'] );
    if(
        substr_count( trim(strtolower($seo_page['NAME'])), trim(strtolower($arResult['q'])) )
        &&
        intval($section['ID']) > 0
        &&
        !$arResult['seo_pages'][$seo_page['ID']]
    ){     $arResult['seo_pages'][$seo_page['ID']] = $seo_page;     }
}



// Поиск товаров
// Поиск по индексу
$obSearch = new \CSearch;
$obSearch->Search(
    [
        "QUERY" => $arResult['q'],
        "SITE_ID" => LANG,
        [
            [
                "=MODULE_ID" => "iblock",
                "!ITEM_ID" => "S%",
                "PARAM2" => $catalogIblocksIDS
            ]
        ]
    ],
    ["TITLE_RANK" => 'DESC'],
    ['STEMMING' => true]
);
if ($obSearch->errorno != 0){} else {
    while($item = $obSearch->GetNext()){
        if( substr_count( strtolower($item['TITLE']), strtolower(trim($arResult['q'])) ) ){
            $goods_cnt++;
            if( $goods_cnt <= $maxGoodsCNT ){
                $arResult['goods']['items'][$item['ITEM_ID']] = $item['PARAM2'];
            }
        }
    }
}


if(
    count( $arResult['goods']['items']) < $maxGoodsCNT
    &&
    // Если есть русские буквы
    $qHasRu
){
    // в т.ч. по транслиту
    $obSearch = new \CSearch;
    $obSearch->Search(
        [
            "QUERY" => tools\funcs::translit($arResult['q']),
            "SITE_ID" => LANG,
            [
                [
                    "=MODULE_ID" => "iblock",
                    "!ITEM_ID" => "S%",
                    "PARAM2" => $catalogIblocksIDS
                ]
            ]
        ],
        [ "RANK" => "DESC", "TITLE_RANK" => "DESC" ],
        [ 'STEMMING' => true ]
    );
    if ($obSearch->errorno != 0){} else {
        while($item = $obSearch->GetNext()){
            if( substr_count( strtolower($item['TITLE']), strtolower(tools\funcs::translit(trim($arResult['q']))) ) ){
                $goods_cnt++;
                if( $goods_cnt <= $maxGoodsCNT ){
                    $arResult['goods']['items'][$item['ITEM_ID']] = $item['PARAM2'];
                }
            }
        }
    }
}
if( count( $arResult['goods']['items']) == 0 ){
    $obSearch = new \CSearch;
    $obSearch->Search(
        [
            "QUERY" => $arResult['q'],
            "SITE_ID" => LANG,
            [
                [
                    "=MODULE_ID" => "iblock",
                    "!ITEM_ID" => "S%",
                    "PARAM2" => $catalogIblocksIDS
                ]
            ]
        ],
        ["TITLE_RANK" => 'DESC'],
        ['STEMMING' => false]
    );
    if ($obSearch->errorno != 0){} else {
        while($item = $obSearch->GetNext()){
            if( substr_count( strtolower($item['TITLE']), strtolower(trim($arResult['q'])) ) ){
                $goods_cnt++;
                if( $goods_cnt <= $maxGoodsCNT ){
                    $arResult['goods']['items'][$item['ITEM_ID']] = $item['PARAM2'];
                }
            }
        }
    }
}
// Если по индексу на найдено
// Попытка найти по заголовку
if( count( $arResult['goods']['items']) == 0 ){
    foreach ( project\catalog::iblocks() as $iblock ){
        $firstLevelCategory = project\catalog::firstLevelCategory($iblock['ID']);
        $filter = Array(
            "IBLOCK_ID" => $iblock['ID'],
            "SECTION_ID" => $firstLevelCategory['ID'],
            "INCLUDE_SUBSECTIONS" => "Y",
            "ACTIVE" => "Y",
            project\funcs::searchLogic($arResult['q'])
        );
        $fields = Array( "ID", "NAME", "IBLOCK_ID" );
        $dbElements = \CIBlockElement::GetList(
            array("SORT"=>"ASC"), $filter, false, array("nTopCount" => ($maxGoodsCNT-count($arResult['goods']['items'])+1) ), $fields
        );
        while ($element = $dbElements->GetNext()){
            $goods_cnt++;
            if( $goods_cnt <= $maxGoodsCNT ){
                $arResult['goods']['items'][$element['ID']] = $iblock['ID'];
            }
        }
    }
}
if( count( $arResult['goods']['items']) == 0 ){
    foreach ( project\catalog::iblocks() as $iblock ){
        $firstLevelCategory = project\catalog::firstLevelCategory($iblock['ID']);
        $filter = Array(
            "IBLOCK_ID" => $iblock['ID'],
            "SECTION_ID" => $firstLevelCategory['ID'],
            "INCLUDE_SUBSECTIONS" => "Y",
            "ACTIVE" => "Y",
            project\funcs::searchLogic($arResult['q'], true)
        );
        $fields = Array( "ID", "NAME", "IBLOCK_ID" );
        $dbElements = \CIBlockElement::GetList(
            array("SORT"=>"ASC"), $filter, false, array("nTopCount" => ($maxGoodsCNT-count($arResult['goods']['items'])+1) ), $fields
        );
        while ($element = $dbElements->GetNext()){
            $goods_cnt++;
            if( $goods_cnt <= $maxGoodsCNT ){
                $arResult['goods']['items'][$element['ID']] = $iblock['ID'];
            }
        }
    }
}
// Попытка найти по транслиту
if( count( $arResult['goods']['items']) == 0 ){
    foreach ( project\catalog::iblocks() as $iblock ){
        $firstLevelCategory = project\catalog::firstLevelCategory($iblock['ID']);
        $filter = Array(
            "IBLOCK_ID" => $iblock['ID'],
            "SECTION_ID" => $firstLevelCategory['ID'],
            "INCLUDE_SUBSECTIONS" => "Y",
            "ACTIVE" => "Y",
            project\funcs::searchLogic(tools\funcs::translit($arResult['q']))
        );
        $fields = Array( "ID", "NAME", "IBLOCK_ID" );
        $dbElements = \CIBlockElement::GetList(
            array("SORT"=>"ASC"), $filter, false, array("nTopCount" => ($maxGoodsCNT-count($arResult['goods']['items'])+1) ), $fields
        );
        while ($element = $dbElements->GetNext()){
            $goods_cnt++;
            if( $goods_cnt <= $maxGoodsCNT ){
                $arResult['goods']['items'][$element['ID']] = $iblock['ID'];
            }
        }
    }
}
if( count($arResult['goods']['items']) == 0 ){
    foreach ( project\catalog::iblocks() as $iblock ){
        $firstLevelCategory = project\catalog::firstLevelCategory($iblock['ID']);
        $filter = Array(
            "IBLOCK_ID" => $iblock['ID'],
            "SECTION_ID" => $firstLevelCategory['ID'],
            "INCLUDE_SUBSECTIONS" => "Y",
            "ACTIVE" => "Y",
            project\funcs::searchLogic(tools\funcs::translit($arResult['q']), true)
        );
        $fields = Array( "ID", "NAME", "IBLOCK_ID" );
        $dbElements = \CIBlockElement::GetList(
            array("SORT"=>"ASC"), $filter, false, array("nTopCount" => ($maxGoodsCNT-count($arResult['goods']['items'])+1) ), $fields
        );
        while ($element = $dbElements->GetNext()){
            $goods_cnt++;
            if( $goods_cnt <= $maxGoodsCNT ){
                $arResult['goods']['items'][$element['ID']] = $iblock['ID'];
            }
        }
    }
}
// Поиск по коду
foreach ( project\catalog::iblocks() as $iblock ){
    $firstLevelCategory = project\catalog::firstLevelCategory($iblock['ID']);
    $filter = Array(
        "IBLOCK_ID" => $iblock['ID'],
        "SECTION_ID" => $firstLevelCategory['ID'],
        "INCLUDE_SUBSECTIONS" => "Y",
        "ACTIVE" => "Y",
        "PROPERTY_CML2_ARTICLE" => $arResult['q']
    );
    $fields = Array( "ID", "NAME", "IBLOCK_ID", "PROPERTY_CML2_ARTICLE" );
    $dbElements = \CIBlockElement::GetList(
        array("SORT"=>"ASC"), $filter, false, array("nTopCount" => ($maxGoodsCNT-count($arResult['goods']['items'])+1) ), $fields
    );
    while ($element = $dbElements->GetNext()){
        $goods_cnt++;
        if( $goods_cnt <= $maxGoodsCNT ){
            $arResult['goods']['items'][$element['ID']] = $iblock['ID'];
        }
    }
}
if( $goods_cnt  > $maxGoodsCNT ){     $arResult['goods']['has_more'] = true;     }



// Статьи
$filter = Array(
	"IBLOCK_ID" => project\article::IBLOCK_ID,
	"ACTIVE" => "Y",
	"ACTIVE_DATE" => "Y",
    [
        "LOGIC" => "OR",
        [ "NAME" => $arResult['q']."%" ],
        [ "NAME" => "%".$arResult['q']."%" ],
        [ "NAME" => "%".$arResult['q'] ],
    ],
);
$fields = Array( "ID", "NAME", "DETAIL_PAGE_URL" );
$articles = \CIBlockElement::GetList(
	[ "SORT"=>"ASC" ], $filter, false,
    [ "nTopCount" => ($maxArticlesCNT - count($arResult['articles'])) ],
    $fields
);
while ($article = $articles->GetNext()){
    $arResult['articles'][$article['ID']] = $article;
}




$this->IncludeComponentTemplate();