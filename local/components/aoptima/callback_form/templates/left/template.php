<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>


<a style="cursor: pointer;" class="advise__callback advise-callback-open-js" onclick="dataLayer.push({'event': 'event-callback'});"><span>Заказать звонок</span></a>

<div class="advise-callback stop-remove-class advise-callback-js">
    <form onsubmit="return false" class="callback___form">
        <label class="input-wrap advise-callback__form-input">
            <input class="input-def" name="name" type="text">
            <span class="label-float">Ваше имя</span>
            <span class="error-note">Текст ошибки</span>
            <span class="success-note">Текст успешной отправки</span>
        </label>
        <label class="input-wrap advise-callback__form-input">
            <input class="input-def" name="phone" type="text">
            <span class="label-float">Телефон</span>
            <span class="error-note">Текст ошибки</span>
            <span class="success-note">Текст успешной отправки</span>
        </label>

        <p class="error___p"></p>

        <div class="input-wrap input-wrap_btn advise-callback__form-input advise-callback__form-input_btn">
            <input class="btn-alt advise-callback-form__btn callback___button to___process" type="button" value="Заказать">
        </div>
        <div class=""></div>
    </form>
    <a href="#" class="advise-callback-close advise-callback-close-js">
        <svg width="8" height="8" viewBox="0 0 8 8" xmlns="http://www.w3.org/2000/svg">
            <path d="M0.646447 1.35355L6.64645 7.35355L7.35355 6.64645L1.35355 0.646447L0.646447 1.35355ZM6.64645 0.646447L0.646447 6.64645L1.35355 7.35355L7.35355 1.35355L6.64645 0.646447Z"></path>
        </svg>
        <span>Закрыть</span>
    </a>
</div>