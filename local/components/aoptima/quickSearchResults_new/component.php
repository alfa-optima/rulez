<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('iblock');

$ag_module = 'arturgolubev.smartsearch';

if(!\CModule::IncludeModule("search")){
    ShowError(GetMessage("SEARCH_MODULE_UNAVAILABLE"));
    return;
}

if(!\CModule::IncludeModule($ag_module)){
    global $USER;
    if($USER->IsAdmin()){
        ShowError(GetMessage("ARTURGOLUBEV_SMARTSEARCH_MODULE_UNAVAILABLE"));
    }
    return;
}

$catalogIblocksIDS = [];
foreach ( project\catalog::iblocks() as $iblock ){
    $catalogIblocksIDS[] = $iblock['ID'];
}

$arResult['q'] = strip_tags(trim($_POST['q']));

$maxGoodsCNT = 10;  $goods_cnt = 0;
$maxArticlesCNT = 10;

$arResult['seo_pages'] = [];
$arResult['goods'] = [
    'items' => [],
    'has_more' => false
];
$arResult['articles'] = [];

$qHasRu = preg_match("/[а-яёА-ЯЁ]/", $arResult['q'], $matches, PREG_OFFSET_CAPTURE);



// Поиск сео-страниц
$all_seo_pages = project\SeopageTable::allList();
foreach ( $all_seo_pages as $seo_page ){
    $section = project\catalog::getSectionByXMLID( $seo_page['SECTION_XML_ID'] );
    if(
        tools\funcs::string_begins_with(
            trim(strtolower($arResult['q'])),
            trim(strtolower($seo_page['NAME']))
        )
        &&
        intval($section['ID']) > 0
    ){     $arResult['seo_pages'][$seo_page['ID']] = $seo_page;     }
}
foreach ( $all_seo_pages as $seo_page ){
    $section = project\catalog::getSectionByXMLID( $seo_page['SECTION_XML_ID'] );
    if(
        substr_count( trim(strtolower($seo_page['NAME'])), trim(strtolower($arResult['q'])) )
        &&
        intval($section['ID']) > 0
        &&
        !$arResult['seo_pages'][$seo_page['ID']]
    ){     $arResult['seo_pages'][$seo_page['ID']] = $seo_page;     }
}



// Поиск по поисковому индексу
if( $arResult['q'] ){
    $tmp_q = '"'.str_replace(' ', '" "', $arResult['q']).'"';
} else {
    $tmp_q = false;
}
$arFilter = [
    "QUERY" => $tmp_q,
    "SITE_ID" => LANG,
    "TAGS" => false
];
$arSort = [ "CUSTOM_RANK" => "DESC", "RANK" => "DESC", "DATE_CHANGE" => "DESC" ];
$exFILTER = [
    [
        '=MODULE_ID' => 'iblock',
        'PARAM1' => '1c_catalog',
        'PARAM2' => $catalogIblocksIDS,
    ]
];

// Поиск по индексу
$obSearch = new \CSearch;
$obSearch->SetOptions([
    "ERROR_ON_EMPTY_STEM" => true,
    "NO_WORD_LOGIC" => false,
]);
$obSearch->Search( $arFilter, $arSort, $exFILTER );
if ($obSearch->errorno != 0){} else {
    while($item = $obSearch->GetNext()){

        $goods_cnt++;
        if( $goods_cnt <= $maxGoodsCNT ){
            $arResult['goods']['items'][$item['ITEM_ID']] = $item['PARAM2'];
        }

    }
}


// Поиск ошибок
if(
    count($arResult['goods']['items']) == 0
    &&
    count( $arResult['goods']['items']) < $maxGoodsCNT
){

    $arSmartParams = array();
    $arSmartParams["query_words"] = explode(' ', strtoupper($arResult['q']));
    $arSmartParams["SETTINGS"]["WORDS"] = CArturgolubevSmartsearch::prepareQueryWords($arSmartParams["query_words"]);

    if( !empty($arSmartParams["SETTINGS"]["WORDS"]) ){

        $arLavelsWords = CArturgolubevSmartsearch::getSimilarWordsList($arSmartParams["SETTINGS"]["WORDS"]);

        if( !empty($arLavelsWords) ){
            foreach( $arLavelsWords as $level => $searchArray ){
                $arFilter["QUERY"] = implode(' | ', $searchArray);
                // Повторный поиск по индексу
                $obSearch = new \CSearch;
                $obSearch->SetOptions([
                    "ERROR_ON_EMPTY_STEM" => true,
                    "NO_WORD_LOGIC" => false,
                ]);
                $obSearch->Search( $arFilter, $arSort, $exFILTER );
                if ($obSearch->errorno != 0){} else {
                    while($item = $obSearch->GetNext()){

                        $goods_cnt++;
                        if( $goods_cnt <= $maxGoodsCNT ){
                            $arResult['goods']['items'][$item['ITEM_ID']] = $item['PARAM2'];
                        }

                    }
                }
            }
        }
    }
}


// Раскладка
if(
    count($arResult['goods']['items']) == 0
    &&
    count( $arResult['goods']['items']) < $maxGoodsCNT
){
    $arLang = \CSearchLanguage::GuessLanguage($arResult['q']);
    if( is_array($arLang) && $arLang["from"] != $arLang["to"] ){
        $arResult['lang_q'] = CSearchLanguage::ConvertKeyboardLayout($arResult['q'], $arLang["from"], $arLang["to"]);
        if( $arResult['lang_q'] != $arResult['q'] ){
            $arFilter["QUERY"] = $arResult['lang_q'];
            // Повторный поиск по индексу
            $obSearch = new \CSearch;
            $obSearch->SetOptions([
                "ERROR_ON_EMPTY_STEM" => true,
                "NO_WORD_LOGIC" => false,
            ]);
            $obSearch->Search( $arFilter, $arSort, $exFILTER );
            if ($obSearch->errorno != 0){} else {
                while($item = $obSearch->GetNext()){

                    $goods_cnt++;
                    if( $goods_cnt <= $maxGoodsCNT ){
                        $arResult['goods']['items'][$item['ITEM_ID']] = $item['PARAM2'];
                    }

                }
            }

            // Поиск ошибок
            if(
                count($arResult['goods']['items']) == 0
                &&
                count( $arResult['goods']['items']) < $maxGoodsCNT
            ){

                $query_words = explode(' ', strtoupper($arResult['lang_q']));
                $words = CArturgolubevSmartsearch::prepareQueryWords($query_words);

                if( !empty($words) ){

                    $arLavelsWords = CArturgolubevSmartsearch::getSimilarWordsList($words);

                    if( !empty($arLavelsWords) ){
                        foreach( $arLavelsWords as $level=>$searchArray ){
                            $arFilter["QUERY"] = implode(' | ', $searchArray);
                            // Повторный поиск по индексу
                            $obSearch = new \CSearch;
                            $obSearch->SetOptions([
                                "ERROR_ON_EMPTY_STEM" => true,
                                "NO_WORD_LOGIC" => false,
                                "STEMMING" => false,
                            ]);
                            $obSearch->Search( $arFilter, $arSort, $exFILTER );
                            if ($obSearch->errorno != 0){} else {
                                while($item = $obSearch->GetNext()){

                                    $goods_cnt++;
                                    if( $goods_cnt <= $maxGoodsCNT ){
                                        $arResult['goods']['items'][$item['ITEM_ID']] = $item['PARAM2'];
                                    }

                                }
                            }
                        }
                    }
                }
            }

        }
    }
}



// Статьи
$filter = Array(
	"IBLOCK_ID" => project\article::IBLOCK_ID,
	"ACTIVE" => "Y",
	"ACTIVE_DATE" => "Y",
    [
        "LOGIC" => "OR",
        [ "NAME" => $arResult['q']."%" ],
        [ "NAME" => "%".$arResult['q']."%" ],
        [ "NAME" => "%".$arResult['q'] ],
    ],
);
$fields = Array( "ID", "NAME", "DETAIL_PAGE_URL" );
$articles = \CIBlockElement::GetList(
	[ "SORT"=>"ASC" ], $filter, false,
    [ "nTopCount" => ($maxArticlesCNT - count($arResult['articles'])) ],
    $fields
);
while ($article = $articles->GetNext()){
    $arResult['articles'][$article['ID']] = $article;
}



$this->IncludeComponentTemplate();