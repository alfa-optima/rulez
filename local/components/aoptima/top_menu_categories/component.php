<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;



$arResult['CATALOG_IBLOCKS'] = project\catalog::iblocks();

// Категории 1 уровня каталога
$arResult['CATEGORIES'] = [];
foreach ( $arResult['CATALOG_IBLOCKS'] as $iblock_id => $iblock ){

    if( $iblock['ID'] != project\complect::IBLOCK_ID ){

        $iblock_sections = project\catalog::allIblockCategories($iblock['ID']);

        if( count($iblock_sections) > 0 ){

            foreach ( $iblock_sections as $section ){
                if( $section['UF_TOP_MENU'] ){
                    $arResult['CATEGORIES'][$section['CODE']] = $section;
                }
            }

        }

    }

}





$this->IncludeComponentTemplate();