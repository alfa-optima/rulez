<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (count($arResult["CATEGORIES"]) > 0){ ?>

    <!--navigation-->
    <div class="c-nav-wrap">
        <div class="c-nav">
            <ul class="c-nav__list">

                <? foreach ($arResult["CATEGORIES"] as $category){ ?>

                    <li>
                        <a href="<?=$category['SECTION_PAGE_URL']?>"><span><?=strlen($category['UF_SHORT_TITLE'])>0?$category['UF_SHORT_TITLE']:$category['NAME']?></span></a>
                    </li>

                <? } ?>

            </ul>
        </div>
    </div>
    <!--navigation end-->

<? } ?>