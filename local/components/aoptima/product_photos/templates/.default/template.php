<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools; ?>

<? if( count($arResult['MEDIA_ITEMS']) > 0 ){ ?>

    <!--product card gallery-->
    <div class="p-card-gallery p-card-gallery-js">

        <div class="swiper-container p-card-gallery-images p-card-gallery-images-js">
            <div class="swiper-wrapper">

                <? foreach( $arResult['MEDIA_ITEMS'] as $item ){ ?>

                    <? if( $item['TYPE'] == 'PICTURE' ){ ?>

                        <div class="swiper-slide p-card-gallery-images__item">

                            <a href="<?=tools\funcs::rIMGG($item['PICTURE_ID'], 4, 800, 700)?>">

                                <img itemprop="image" src="<?=tools\funcs::rIMGG($item['PICTURE_ID'], 4, 700, 500)?>" data-thumb="<?=tools\funcs::rIMGG($item['PICTURE_ID'], 4, 50, 50)?>" class="swiper-lazy" alt="image description">
                                <div class="swiper-lazy-preloader"></div>
                                <noscript class="noscript">
                                    <img src="<?=tools\funcs::rIMGG($item['PICTURE_ID'], 4, 700, 500)?>" alt="<?=$arResult['NAME']?>">
                                </noscript>

                            </a>

                        </div>

                    <? } else if( $item['TYPE'] == 'YOUTUBE_VIDEO' ){ ?>

                        <div class="swiper-slide p-card-gallery-images__item">
                            <a href="https://www.youtube.com/watch?v=<?=$item['YOUTUBE_VIDEO_ID']?>">
                                <img itemprop="image" src="<?=SITE_TEMPLATE_PATH?>/img/empty.png" data-src="<?=SITE_TEMPLATE_PATH?>/img/img-video.png" data-thumb="<?=SITE_TEMPLATE_PATH?>/img/img-video.png" class="swiper-lazy" alt="View Video">
                                <div class="swiper-lazy-preloader"></div>
                                <noscript class="noscript">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/img/img-video.png" alt="View Video">
                                </noscript>
                            </a>
                        </div>

                    <? } ?>

                <? } ?>

            </div>
        </div>

    </div>
    <!--product card gallery end-->

<? } ?>