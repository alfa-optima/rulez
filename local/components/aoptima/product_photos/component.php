<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$arResult = $arParams['el'];

$arResult['MEDIA_ITEMS'] = [];

$start_media_key = 1;
$media_key = false;
if( intval($arResult['DETAIL_PICTURE']) > 0 ){
    if( intval($media_key) > 0 ){  $media_key += 2;  } else {  $media_key = $start_media_key;  }
    $arResult['MEDIA_ITEMS'][$media_key] = [
        'TYPE' => 'PICTURE',
        'PICTURE_ID' => $arResult['DETAIL_PICTURE']
    ];
}

if( is_array( $arResult['PROPERTY_MORE_PHOTO_VALUE'] ) ){
    foreach ( $arResult['PROPERTY_MORE_PHOTO_VALUE'] as $photo_id ){
        if( intval($photo_id) > 0 ){
            if( intval($media_key) > 0 ){  $media_key += 2;  } else {  $media_key = $start_media_key;  }
            $arResult['MEDIA_ITEMS'][$media_key] = [
                'TYPE' => 'PICTURE',
                'PICTURE_ID' => $photo_id
            ];
        }
    }
}

$start_media_key = 2;
$media_key = false;
if( strlen( trim($arResult['PROPERTY_'.project\catalog::VIDEO_PROP_CODE.'_VALUE']) ) > 0 ){
    if( substr_count($arResult['PROPERTY_'.project\catalog::VIDEO_PROP_CODE.'_VALUE'], '|') ){
        $ar = explode('|', trim($arResult['PROPERTY_'.project\catalog::VIDEO_PROP_CODE.'_VALUE']));
        foreach ( $ar as $key => $item ){
            if( strlen(trim($item)) > 0 ){
                if( intval($media_key) > 0 ){  $media_key += 2;  } else {  $media_key = $start_media_key;  }
                $arResult['MEDIA_ITEMS'][$media_key] = [
                    'TYPE' => 'YOUTUBE_VIDEO',
                    'YOUTUBE_VIDEO_ID' => $item
                ];
            }
        }
    } else {
        if( intval($media_key) > 0 ){  $media_key += 2;  } else {  $media_key = $start_media_key;  }
        $arResult['MEDIA_ITEMS'][$media_key] = [
            'TYPE' => 'YOUTUBE_VIDEO',
            'YOUTUBE_VIDEO_ID' => trim($arResult['PROPERTY_'.project\catalog::VIDEO_PROP_CODE.'_VALUE'])
        ];
    }
}

if( count($arResult['MEDIA_ITEMS']) > 0 ){
    ksort($arResult['MEDIA_ITEMS']);
}

//echo "<pre>"; print_r( $arResult['MEDIA_ITEMS'] ); echo "</pre>";


$this->IncludeComponentTemplate();