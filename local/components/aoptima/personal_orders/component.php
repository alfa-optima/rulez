<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('sale');
use Bitrix\Sale;
use Bitrix\Sale\Internals;

\Bitrix\Main\Loader::includeModule('aoptima.tools');  use AOptima\Tools as tools;
\Bitrix\Main\Loader::includeModule('aoptima.project');  use AOptima\Project as project;

$url = $_SERVER['REQUEST_URI'];
while( substr_count( $url, '//' ) ){
    $url = str_replace('//', "/", $url);
    LocalRedirect( $url );
}


$arResult['MAX_CNT'] = 10;
$arResult['DOP_CNT'] = 0;

$arResult['IS_AUTH'] = $USER->IsAuthorized()?'Y':'N';

if( $arResult['IS_AUTH'] == 'Y' ){

    if( $arParams['IS_AJAX'] != 'Y' ){
        $arResult['TOTAL_CNT'] = 0;
        $filter_array = array( 'USER_ID' => $USER->GetID() );
        $orders = \Bitrix\Sale\OrderTable::getList(array(
            'filter' => $filter_array,
            'select' => array( 'ID', 'USER_ID' ),
            'order' => array('ID' => 'DESC'),
        ));
        while ($arOrder = $orders->fetch()){  $arResult['TOTAL_CNT']++;  }
    }

    $arResult['ORDERS'] = [];

    // Получаем заказы пользователя
    $filter_array = array(
        'USER_ID' => $USER->GetID()
    );
    if( is_array( $_POST['stop_ids'] ) ){
        $filter_array['!ID'] = $_POST['stop_ids'];
    }
    $orders = \Bitrix\Sale\OrderTable::getList(array(
        'filter' => $filter_array,
        //'select' => array( 'ID', 'USER_ID' ),
        'order' => array('ID' => 'DESC'),
        'limit' => $arResult['MAX_CNT'] + 1
    ));
    while ($arOrder = $orders->fetch()){
        
        $arOrder['order'] = Sale\Order::load($arOrder['ID']);

        $status_code = 'A';
        if( $arOrder['order']->isPaid() ){      $status_code = 'P';     }
        if( $arOrder['ALLOW_DELIVERY'] == 'Y' ){       $status_code = 'D';     }
        if( $arOrder['STATUS_ID'] == 'F' ){     $status_code = 'Z';     }
        if( $arOrder['CANCELED'] == 'Y' ){      $status_code = 'C';     }
        
        $arOrder['status'] = project\order::$statuses[$status_code];

        $arOrder['PICTURES'] = [];
        $arOrder['PICTURE'] = false;

        $arOrder['basket'] = $arOrder['order']->getBasket();
        foreach ( $arOrder['basket'] as $key => $bItem ){

            $product_id = $bItem->getProductId();
            $el = tools\el::info($product_id);

            if( intval($el['ID']) > 0 ){

                $el_pics = [];
                if( intval($el['DETAIL_PICTURE']) > 0 ){
                    $arOrder['PICTURES'][round($bItem->getPrice(), 0)][] = $el['DETAIL_PICTURE'];
                    $el_pics[] = $el['DETAIL_PICTURE'];
                }
                if( is_array( $el['PROPERTY_MORE_PHOTO_VALUE'] ) ){
                    foreach ( $el['PROPERTY_MORE_PHOTO_VALUE'] as $photo_id ){
                        if( intval($photo_id) > 0 ){
                            $arOrder['PICTURES'][round($bItem->getPrice(), 0)][] = $photo_id;
                            $el_pics[] = $photo_id;
                        }
                    }
                }

                if( count($el_pics) > 0 ){
                    $pic_id = $el_pics[0];
                    $el['PICTURE'] = tools\funcs::rIMGG($pic_id, 4, 90, 70);
                }
                
                $arOrder['basket'][$key]->el = $el;
            }

            arsort($arOrder['PICTURES']);
        }

        if( count($arOrder['PICTURES']) > 0 ){
            $pic_id = $arOrder['PICTURES'][array_keys($arOrder['PICTURES'])[0]][0];
            $arOrder['PICTURE'] = tools\funcs::rIMGG($pic_id, 4, 100, 69);
        }

        $arResult['ORDERS'][$arOrder['ID']] = $arOrder;
    }
    
}




$this->IncludeComponentTemplate();