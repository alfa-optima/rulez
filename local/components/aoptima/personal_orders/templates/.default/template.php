<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

if( $arParams['IS_AJAX'] == 'Y' ){

    $cnt = 0;
    foreach( $arResult['ORDERS'] as $key => $arOrder ){ $cnt++;
        if( $cnt <= $arResult['MAX_CNT'] ){ ?>

            <div class="rolls__item rolls__item-js order___item" item_id="<?=$arOrder['ID']?>">

                <div class="rolls__header rolls__header-js">
                    <div class="oh-meta">
                        <div class="oh-meta__figure">
                            <? if( $arOrder['PICTURE'] ){ ?>
                                <img src="<?=$arOrder['PICTURE']?>">
                            <? } ?>
                        </div>
                        <div class="oh-meta__content">
                            <div class="oh-meta__title"><a href="/personal/orders/<?=$arOrder['ID']?>/">Заказ от <?=ConvertDateTime($arOrder['order']->getDateInsert(), "DD.MM.YYYY", "ru")?></a></div>
                            <div class="oh-meta__add">№ <?=$arOrder['ID']?></div>
                            <div class="oh-meta__status <?=$arOrder['status']['CLASS']?>"><?=$arOrder['status']['NAME']?></div>
                        </div>
                    </div>
                    <em class="rolls__angle rolls__hand-js">&#9660;</em>
                </div>

                <div class="rolls__panel typography rolls__panel-js">
                    <div class="oh-content">
                        <div class="oh-products">

                            <? foreach( $arOrder['basket'] as $key => $bItem ){ ?>

                                <div class="oh-products__item">
                                    <a href="<?=$bItem->el['DETAIL_PAGE_URL']?>" class="oh-product">
                                        <div class="oh-product__figure">
                                            <? if( $bItem->el['PICTURE'] ){ ?>
                                                <img src="<?=$bItem->el['PICTURE']?>">
                                            <? } else { ?>
                                                <img src="<?=SITE_TEMPLATE_PATH?>/img/no-photo.png">
                                            <? } ?>
                                        </div>
                                        <div class="oh-product__title"><?=$bItem->el['NAME']?></div>
                                    </a>
                                </div>

                            <? } ?>

                        </div>
                    </div>
                </div>

            </div>

        <? } else {
            echo '<ost></ost>';
        }
    }

} else { ?>

    <!--CONTENT-->
    <div class="content">

        <? if( $arResult['IS_AUTH'] == 'Y' ){ ?>

            <div class="content-align">
                <!--article content-->

                <div class="content-inner article-content">

                    <h1>Заказы <sup><?=$arResult['TOTAL_CNT']?></sup></h1>

                    <div class="rolls layout-text payment__methods rolls-js orders_load_area">

                        <? $cnt = 0;
                        foreach( $arResult['ORDERS'] as $key => $arOrder ){ $cnt++;
                            if( $cnt <= $arResult['MAX_CNT'] ){ ?>

                                <div class="rolls__item rolls__item-js order___item" item_id="<?=$arOrder['ID']?>">

                                    <div class="rolls__header rolls__header-js">
                                        <div class="oh-meta">
                                            <div class="oh-meta__figure">
                                                <? if( $arOrder['PICTURE'] ){ ?>
                                                    <img src="<?=$arOrder['PICTURE']?>">
                                                <? } ?>
                                            </div>
                                            <div class="oh-meta__content">
                                                <div class="oh-meta__title"><a href="/personal/orders/<?=$arOrder['ID']?>/">Заказ от <?=ConvertDateTime($arOrder['order']->getDateInsert(), "DD.MM.YYYY", "ru")?></a></div>
                                                <div class="oh-meta__add">№ <?=$arOrder['ID']?></div>
                                                <div class="oh-meta__status <?=$arOrder['status']['CLASS']?>"><?=$arOrder['status']['NAME']?></div>
                                            </div>
                                        </div>
                                        <em class="rolls__angle rolls__hand-js">&#9660;</em>
                                    </div>

                                    <div class="rolls__panel typography rolls__panel-js">
                                        <div class="oh-content">
                                            <div class="oh-products">

                                                <? foreach( $arOrder['basket'] as $key => $bItem ){ ?>

                                                    <div class="oh-products__item">
                                                        <a href="<?=$bItem->el['DETAIL_PAGE_URL']?>" class="oh-product">
                                                            <div class="oh-product__figure">
                                                                <? if( $bItem->el['PICTURE'] ){ ?>
                                                                    <img src="<?=$bItem->el['PICTURE']?>">
                                                                <? } else { ?>
                                                                    <img src="<?=SITE_TEMPLATE_PATH?>/img/no-photo.png">
                                                                <? } ?>
                                                            </div>
                                                            <div class="oh-product__title"><?=$bItem->el['NAME']?></div>
                                                        </a>
                                                    </div>

                                                <? } ?>

                                            </div>
                                        </div>
                                    </div>

                                </div>

                            <? }
                        } ?>

                    </div>

                    <div class="load-more" <? if( $cnt <= $arResult['MAX_CNT'] ){ ?>style="display:none;"<? } ?>>
                        <a style="cursor: pointer; " class="btn-def load-more__btn ordersMoreButton to___process">Показать еще</a>
                    </div>

                </div>

            </div>

            <aside class="sidebar">
                <? // personal
                $APPLICATION->IncludeComponent(
                    "bitrix:menu",  "personal", // Шаблон меню
                    Array(
                        "ROOT_MENU_TYPE" => "personal", // Тип меню
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "N",
                        "CACHE_SELECTED_ITEMS" => "N",
                        "MENU_CACHE_GET_VARS" => array(""),
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "left",
                        "USE_EXT" => "N",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N",
                        "REQUEST_URI" => $_SERVER['REQUEST_URI']
                    )
                ); ?>
            </aside>

        <? } else {

            include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/auth_form.php';

        } ?>

    </div>

<? } ?>