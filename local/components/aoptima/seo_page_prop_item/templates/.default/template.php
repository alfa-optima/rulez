<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>

<div class="prop_values_block">

    <p><strong>Значения:</strong></p>

    <p>

        <? if( $arResult['PROP']['PROPERTY_TYPE'] == 'L' ){ ?>

            <select multiple name="PROP_VALUES[<?=$arResult['PROP']['CODE']?>][]" style="height: 180px;">
                <option>Выбор варианта</option>
                <? foreach( $arResult['ENUMS'] as $key => $enum ){ ?>
                    <option <? if( in_array( $enum['ID'], $arResult['VALUES'] ) ){ echo 'selected'; } ?> value="<?=$enum['ID']?>"><?=$enum['VALUE']?></option>
                <? } ?>
            </select>

        <? } else if( $arResult['PROP']['PROPERTY_TYPE'] == 'N' ){ ?>

            <input type="text" class="prop_value_input number___input" name="PROP_VALUES[<?=$arResult['PROP']['CODE']?>][MIN]" placeholder="Значение 'от'" value="<?=$arResult['VALUES']['MIN']?>">

            <input type="text" class="prop_value_input number___input" name="PROP_VALUES[<?=$arResult['PROP']['CODE']?>][MAX]" placeholder="Значение 'до'" value="<?=$arResult['VALUES']['MAX']?>">

        <? } else { ?>

            <? if( is_array($arResult['VALUES']) && count($arResult['VALUES']) > 0 ){
                foreach ( $arResult['VALUES'] as $key => $value ){ ?>
                    <input type="text" class="prop_value_input string___input" name="PROP_VALUES[<?=$arResult['PROP']['CODE']?>][]" placeholder="Значение свойства" value="<?=$value?>">
                <? }
            } ?>

            <input type="text" class="prop_value_input string___input" name="PROP_VALUES[<?=$arResult['PROP']['CODE']?>][]" placeholder="Значение свойства">

            <div style="width:100%; text-align: right;">
                <input class="add_string_value___button" type="button" value="+ Добавить значение">
            </div>

        <? } ?>

    </p>

</div>