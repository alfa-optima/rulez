<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$arResult['PRODUCTS_CNT'] = $arParams['productsCNT'];
$arResult['PAGES_CNT'] = $arParams['pagesCNT'];
$arResult['PAGE_NUMBER'] = intval($arParams['pageNumber'])>0?intval($arParams['pageNumber']):1;

$countPagesDisplay = 3;

$i = 1;
$left_border = $arResult['PAGE_NUMBER'];
$right_border = $arResult['PAGE_NUMBER'];
$max = $arResult['PAGES_CNT'];

while($i < $countPagesDisplay){
    if($i % 2 == 0){
        $lb = $left_border;
        $left_border = tools\funcs::addLeftBorder($left_border);
        if ($left_border == $lb){
            $rb = $right_border;
            $right_border = tools\funcs::addRightBorder($right_border, $max);
            if ($right_border == $rb){   $i = $countPagesDisplay;   }
        }
    } else {
        $rb = $right_border;
        $right_border = tools\funcs::addRightBorder($right_border, $max);
        if ($right_border == $rb){
            $lb = $left_border;
            $left_border = tools\funcs::addLeftBorder($left_border);
            if ($left_border == $lb){   $i = $countPagesDisplay;   }
        }
    }
    $i++;
}


$arResult["nStartPage"] = $left_border;
$arResult["nEndPage"] = $right_border;


if( $arResult['PAGES_CNT'] > 1 ){      $this->IncludeComponentTemplate();      }

