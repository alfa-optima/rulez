<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if( $arResult['PAGES_CNT'] > 1 ){ ?>

    <div class="products-pagination <?=$arParams['class']?> paginationBlock <?
    if( $arParams['hidden'] ){
        echo 'hiddenPaginationBlock';
    } ?>"
    >
        <div class="pagination layout-text">

            <div class="pg-left">

                <? if ($arResult['PAGE_NUMBER'] > 1){ ?>

                    <a href="<?=tools\funcs::addToRequestURI($arParams['CUR_URL'], 'PAGEN_1', $arResult['PAGE_NUMBER']-1)?>" class="pg-prev" style="cursor: pointer" data-pagenumber="<?=($arResult['PAGE_NUMBER']-1)?>">
                        <span>Назад</span>
                        <i class="pg-prev-arr">&nbsp;</i>
                    </a>

                <? } else { ?>

                    <a class="pg-prev disabled">
                        <span>Назад</span>
                        <i class="pg-prev-arr">&nbsp;</i>
                    </a>

                <? } ?>

            </div>

            <ul class="pg-list">

                <? if ($arResult['PAGE_NUMBER'] > 1): ?>

                    <?if ($arResult["nStartPage"] != 1):?>

                        <li>
                            <a href="<?=tools\funcs::addToRequestURI($arParams['CUR_URL'], 'PAGEN_1', 1)?>" style="cursor: pointer" data-pagenumber="1">1</a>
                        </li>

                    <?endif?>

                <?endif?>

                <?if ($arResult["nStartPage"] > 2):?>

                    <li>
                        <span class="dots">...</span>
                    </li>

                <?endif?>

                <? while($arResult["nStartPage"] <= $arResult["nEndPage"]): ?>

                    <?if ($arResult["nStartPage"] == $arResult['PAGE_NUMBER']):?>

                        <li class="active"><span><?=$arResult["nStartPage"]?></span></li>

                    <?elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):?>

                        <li>
                            <a href="<?=tools\funcs::addToRequestURI($arParams['CUR_URL'], 'PAGEN_1', $arResult["nStartPage"])?>" style="cursor: pointer" data-pagenumber="<?=$arResult["nStartPage"]?>"><?=$arResult["nStartPage"]?></a>
                        </li>

                    <?else:?>

                        <li>
                            <a href="<?=tools\funcs::addToRequestURI($arParams['CUR_URL'], 'PAGEN_1', $arResult["nStartPage"])?>" style="cursor: pointer" data-pagenumber="<?=$arResult["nStartPage"]?>" style="cursor: pointer" data-pagenumber="<?=$arResult["nStartPage"]?>"><?=$arResult["nStartPage"]?></a>
                        </li>

                    <?endif?>

                    <?$arResult["nStartPage"]++?>

                <?endwhile?>

                <?if ($arResult["nEndPage"] < ($arResult['PAGES_CNT'] - 1)):?>

                    <li>
                        <span class="dots">...</span>
                    </li>

                <?endif?>

                <?if($arResult['PAGE_NUMBER'] < $arResult['PAGES_CNT']):?>
                    <?if($arResult["nEndPage"] < $arResult['PAGES_CNT']):?>

                        <li>
                            <a href="<?=tools\funcs::addToRequestURI($arParams['CUR_URL'], 'PAGEN_1', $arResult['PAGES_CNT'])?>" style="cursor: pointer" data-pagenumber="<?=$arResult['PAGES_CNT']?>"><?=$arResult['PAGES_CNT']?></a>
                        </li>

                    <?endif?>

                <?endif?>

            </ul>

            <div class="pg-right">

                <?if($arResult['PAGE_NUMBER'] < $arResult['PAGES_CNT']):?>

                    <?if($arResult["nEndPage"] < $arResult['PAGES_CNT']):?>

                        <a href="<?=tools\funcs::addToRequestURI($arParams['CUR_URL'], 'PAGEN_1', $arResult['PAGE_NUMBER']+1)?>" class="pg-next" style="cursor: pointer" data-pagenumber="<?=($arResult['PAGE_NUMBER']+1)?>">
                            <span>Вперед</span>
                            <i class="pg-next-arr">&nbsp;</i>
                        </a>

                    <?else:?>

                        <a href="<?=tools\funcs::addToRequestURI($arParams['CUR_URL'], 'PAGEN_1', $arResult['PAGE_NUMBER']+1)?>" class="pg-next" style="cursor: pointer" data-pagenumber="<?=($arResult['PAGE_NUMBER']+1)?>">
                            <span>Вперед</span>
                            <i class="pg-next-arr">&nbsp;</i>
                        </a>

                    <?endif?>

                <?else:?>

                    <a class="pg-next disabled">
                        <span>Вперед</span>
                        <i class="pg-next-arr">&nbsp;</i>
                    </a>

                <?endif?>

            </div>

        </div>
    </div>

<?php } ?>

