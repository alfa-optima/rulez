<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$compareIDS = project\compare::checkCookieIDS();

$arResult['GOODS'] = [];
foreach ( $compareIDS as $key => $tov_id ){
    $el = tools\el::info($tov_id);
    if( intval($el['ID']) > 0 ){
        $el['PICTURE'] = false;
        $el_pics = [];
        if( intval($el['DETAIL_PICTURE']) > 0 ){
            $el_pics[] = $el['DETAIL_PICTURE'];
        }
        if( is_array( $el['PROPERTY_MORE_PHOTO_VALUE'] ) ){
            foreach ( $el['PROPERTY_MORE_PHOTO_VALUE'] as $photo_id ){
                if( intval($photo_id) > 0 ){
                    $el_pics[] = $photo_id;
                }
            }
        }
        if( count($el_pics) > 0 ){
            $pic_id = $el_pics[0];
            $el['PICTURE'] = tools\funcs::rIMGG($pic_id, 4, 175, 120);
        }
        $arResult['GOODS'][$el['ID']] = $el;
    }
}



ob_start();
    if( count($arResult['GOODS']) > 0 ){

        $GLOBALS['compare']['ID'] = array_keys($arResult['GOODS']);

        $APPLICATION->IncludeComponent(
            "bitrix:catalog.section", "compare",
            Array(
                "stopProps" => project\catalog::detailCardStopProps(),
                "IBLOCK_TYPE" => project\catalog::IBLOCK_TYPE,
                "IBLOCK_ID" => $arResult['GOODS'][array_keys($arResult['GOODS'])[0]]['IBLOCK_ID'],
                "SECTION_USER_FIELDS" => array(),
                "ELEMENT_SORT_FIELD" => 'NAME',
                "ELEMENT_SORT_ORDER" => 'ASC',
                "ELEMENT_SORT_FIELD2" => "NAME",
                "ELEMENT_SORT_ORDER2" => 'ASC',
                "FILTER_NAME" => 'compare',
                "HIDE_NOT_AVAILABLE" => "N",
                "PAGE_ELEMENT_COUNT" => 100,
                "LINE_ELEMENT_COUNT" => "3",
                "PROPERTY_CODE" => array('STATUS_TOVARA'),
                "OFFERS_LIMIT" => 0,
                "TEMPLATE_THEME" => "",
                "PRODUCT_SUBSCRIPTION" => "N",
                "SHOW_DISCOUNT_PERCENT" => "N",
                "SHOW_OLD_PRICE" => "N",
                "MESS_BTN_BUY" => "Купить",
                "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                "MESS_BTN_SUBSCRIBE" => "Подписаться",
                "MESS_BTN_DETAIL" => "Подробнее",
                "MESS_NOT_AVAILABLE" => "Нет в наличии",
                "SECTION_URL" => "",
                "DETAIL_URL" => "",
                "SECTION_ID_VARIABLE" => "SECTION_ID",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => project\catalog::CACHE_TYPE,
                "CACHE_TIME" => project\catalog::CACHE_TIME,
                "CACHE_GROUPS" => "Y",
                "SET_META_KEYWORDS" => "N",
                "META_KEYWORDS" => "",
                "SET_META_DESCRIPTION" => "N",
                "META_DESCRIPTION" => "",
                "BROWSER_TITLE" => "-",
                "ADD_SECTIONS_CHAIN" => "N",
                "DISPLAY_COMPARE" => "N",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "CACHE_FILTER" => "Y",
                "PROPERTY_CODE" => array('STATUS_TOVARA'),
                "PRICE_CODE" => array(
                    project\catalog::RASSROCHKA_PRICE_CODE,
                    project\catalog::BASE_PRICE_CODE
                ),
                "USE_PRICE_COUNT" => "N",
                "SHOW_PRICE_COUNT" => "1",
                "PRICE_VAT_INCLUDE" => "Y",
                "CONVERT_CURRENCY" => "N",
                "BASKET_URL" => "/personal/basket.php",
                "ACTION_VARIABLE" => "action",
                "PRODUCT_ID_VARIABLE" => "id",
                "USE_PRODUCT_QUANTITY" => "N",
                "ADD_PROPERTIES_TO_BASKET" => "Y",
                "PRODUCT_PROPS_VARIABLE" => "prop",
                "PARTIAL_PRODUCT_PROPERTIES" => "N",
                "PRODUCT_PROPERTIES" => "",
                "PAGER_TEMPLATE" => "",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Товары",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "Y",
                "ADD_PICT_PROP" => "-",
                "LABEL_PROP" => "-",
                'INCLUDE_SUBSECTIONS' => "Y",
                'SHOW_ALL_WO_SECTION' => "Y"
            )
        );
        $arResult['HTML'] = ob_get_contents();

    } else {

        $arResult['HTML'] = '<div class="content"><div class="article-content"><h1 class="full-width">Сравнение товаров</h1><div class="products-compare products-compare-js"><div class="products-compare__empty products-compare__empty-js" style="display: block;"><a href="/">Выберите товары для сравнения</a></div></div></div></div>';

    }
ob_end_clean();




$this->IncludeComponentTemplate();