<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$vozvratForm = new project\vozvrat_form();

$arResult['FIELDS'] = $vozvratForm->getFormFields();


$this->IncludeComponentTemplate();