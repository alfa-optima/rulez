<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>

<form class="order validate-js order___form vozvratForm" action="/ajax/sendVozvratForm.php" method="post" target="vozvratFormFrame" enctype="multipart/form-data" onsubmit="return vozvratFormSend($(this));">

    <script>
        vozvratFormFields = <?=CUtil::PhpToJsObject($arResult['FIELDS'])?>;
    </script>

    <div class="main-heading">
        <h1 class="main-heading__title">Заявка на возврат</h1>
    </div>

    <section class="checkout">

        <div class="checkout__row">

            <div class="checkout__col">

                <? foreach( $arResult['FIELDS'] as $key => $field ){

                    if ( !$field['TYPE'] ){ ?>

                        <label class="input-wrap">
                            <input class="input-def" name="<?=$field['CODE']?>" type="text">
                            <span class="label-float"><?=$field['NAME']?></span>
                        </label>

                    <? } else if( $field['TYPE'] == 'file' ){ ?>

                        <label class="input-wrap">
                            <p style="margin:0"><?=$field['NAME']?>:</p>
                            <input placeholder="<?=$field['NAME']?>" class="input-def" name="<?=$field['CODE']?><? if( $field['multiple'] ){ echo '[]'; } ?>" <? if( $field['multiple'] ){ echo 'multiple'; } ?> type="file">
                        </label>

                    <? } else if( $field['TYPE'] == 'textarea' ){ ?>

                        <label class="input-wrap">
                            <textarea class="input-def" name="<?=$field['CODE']?>" type="text"></textarea>
                            <span class="label-float"><?=$field['NAME']?></span>
                        </label>

                    <? }
                } ?>

                <label class="input-wrap">
                    <div class="g-recaptcha" data-sitekey="<?=\Bitrix\Main\Config\Option::get('aoptima.project', 'GOOGLE_RECAPTCHA_SITE_KEY')?>"></div>
                </label>

                <p class="error___p"></p>

                <div class="buttons clearance__buttons">
                    <div class="buttons__item">
                        <button type="submit" class="btn-def vozvratFormButton to___process">Отправить</button>
                    </div>
                </div>

            </div>

        </div>

    </section>

    <script src='https://www.google.com/recaptcha/api.js'></script>

</form>

