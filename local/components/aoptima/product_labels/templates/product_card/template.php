<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

if( count($arResult['ITEMS']) > 0 ){ ?>

    <div class="p-stickers p-card-stickers">

        <? foreach( $arResult['ITEMS'] as $arItem ){ ?>

            <div class="p-sticker p-stickers__item <?=$arItem['CSS_CLASS']?>"><?=$arItem['TITLE']?></div>

        <? } ?>

    </div>

<? } ?>