<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$arResult['el'] = $arParams['~element'];


$labels = [
    [
        'PROP_CODE' => 'POSLEDNIY_TOVAR',
        'CSS_CLASS' => 'p-sticker_text p-sticker_last',
        'TITLE' => 'Последний товар',
    ],
    [
        'PROP_CODE' => 'EKSKLYUZIV',
        'CSS_CLASS' => 'p-sticker_text p-sticker_exclusive',
        'TITLE' => 'Эксклюзив',
    ],
    [
        'PROP_CODE' => 'PREMIUM',
        'CSS_CLASS' => 'p-sticker_text p-sticker_premium',
        'TITLE' => 'Премиум',
    ],
//    [
//        'PROP_CODE' => project\catalog::RASSROCHKA_PROP_CODE,
//        'CSS_CLASS' => 'p-sticker_text p-sticker_installment',
//        'TITLE' => 'Рассрочка 0%',
//    ],
    [
        'PROP_CODE' => project\catalog::ACTION_PROP_CODE,
        'CSS_CLASS' => 'p-sticker_text p-sticker_action',
        'TITLE' => 'Акция',
    ],
    [
        'PROP_CODE' => 'NEW',
        'CSS_CLASS' => 'p-sticker_new',
        'TITLE' => 'new',
    ],
    [
        'PROP_CODE' => 'TOP',
        'CSS_CLASS' => 'p-sticker_top',
        'TITLE' => 'ТОП',
    ],
    [
        'PROP_CODE' => 'WOW',
        'CSS_CLASS' => 'p-sticker_wow',
        'TITLE' => 'WOW!',
    ],
];

$arResult['ITEMS'] = [];
foreach ( $labels as $label ){
    if( $arResult['el']['PROPERTIES'][$label['PROP_CODE']]['VALUE'] == 'Да' ){
        $arResult['ITEMS'][] = $label;
    }
}
foreach ( $labels as $label ){
    if( $arResult['el']['PROPERTY_'.$label['PROP_CODE'].'_VALUE'] == 'Да' ){
        $arResult['ITEMS'][] = $label;
    }
}

if(
    $arResult['el']['PROPERTIES'][project\catalog::RASSROCHKA_PROP_CODE]['VALUE'] == 'Да'
    &&
    $arResult['el']['PRICES'][project\catalog::RASSROCHKA_PRICE_CODE]['DISCOUNT_VALUE_VAT'] > 0
){
    $arResult['ITEMS'][] = [
        'PROP_CODE' => project\catalog::RASSROCHKA_PROP_CODE,
        'CSS_CLASS' => 'p-sticker_text p-sticker_installment',
        'TITLE' => 'Рассрочка',
    ];
}

if(
    $arResult['el']['PRICES'][project\catalog::RASSROCHKA_PRICE_CODE]['DISCOUNT_VALUE_VAT'] > 0
    &&
    $arResult['el']['PROPERTIES'][project\catalog::RASSROCHKA_PROP_CODE]['VALUE'] == 'Да'
    &&
    $arResult['el']['PRICES'][project\catalog::RASSROCHKA_PRICE_CODE]['DISCOUNT_VALUE_VAT']
    >
    $arResult['el']['PRICES'][project\catalog::BASE_PRICE_CODE]['DISCOUNT_VALUE_VAT']
){
    $arResult['ITEMS'][] = [
        'CSS_CLASS' => 'p-sticker_sale',
        'TITLE' => '%',
    ];
}


$this->IncludeComponentTemplate();