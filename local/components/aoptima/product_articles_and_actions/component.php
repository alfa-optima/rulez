<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');  use AOptima\Project as project;
\Bitrix\Main\Loader::includeModule('aoptima.tools');  use AOptima\Tools as tools;

$arResult['ARTICLES'] = [];
$arResult['ACTIONS'] = [];

if( intval($arParams['product']['ID']) > 0 ){

    $arResult['PRODUCT'] = $arParams['~product'];

    $sect = tools\el::sections($arResult['PRODUCT']['ID'])[0];

    $arResult['ARTICLES'] = project\product_advice::getList( $arResult['PRODUCT'], $sect['ID'] );

    $arResult['ACTIONS'] = project\product_action::getList( $arResult['PRODUCT'], $sect['ID'] );
    
}




$this->IncludeComponentTemplate();