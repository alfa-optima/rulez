<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$cnt = 0;

if( count($arResult['ARTICLES']) > 0 || count($arResult['ACTIONS']) > 0 ){ ?>

    <div class="p-card__advice">

        <? foreach( $arResult['ARTICLES'] as $key => $arItem ){ $cnt++; ?>

            <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="advice <? if( $cnt%2 == 1 ){ echo 'advice_alt'; } ?> advice_small">
                <div class="advice__layout">
                    <? if( intval($arItem['PROPERTY_EXPERT_PHOTO_VALUE']) > 0 ){ ?>
                        <div class="advice__figure">
                            <img src="<?=tools\funcs::rIMGG($arItem['PROPERTY_EXPERT_PHOTO_VALUE'], 5, 65, 63)?>">
                        </div>
                    <? } ?>
                    <div class="advice__label advice__label_alt" <? if( intval($arItem['PROPERTY_EXPERT_PHOTO_VALUE']) > 0 ){} else { echo 'style="margin:0;"'; } ?>>Полезные советы</div>
                    <div class="advice__text advice__text_alt" <? if( intval($arItem['PROPERTY_EXPERT_PHOTO_VALUE']) > 0 ){} else { echo 'style="margin:0;"'; } ?>><?=$arItem['NAME']?></div>
                </div>
            </a>

        <? } ?>

        <? foreach( $arResult['ACTIONS'] as $key => $arItem ){ $cnt++; ?>

            <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="advice <? if( $cnt%2 == 1 ){ echo 'advice_alt'; } ?> advice_small">
                <div class="advice__layout">
                    <? if( intval($arItem['PROPERTY_EXPERT_PHOTO_VALUE']) > 0 ){ ?>
                        <div class="advice__figure">
                            <img src="<?=tools\funcs::rIMGG($arItem['PROPERTY_EXPERT_PHOTO_VALUE'], 5, 65, 63)?>">
                        </div>
                    <? } ?>
                    <div class="advice__label advice__label_alt" <? if( intval($arItem['PROPERTY_EXPERT_PHOTO_VALUE']) > 0 ){} else { echo 'style="margin:0;"'; } ?>>Акции</div>
                    <div class="advice__text advice__text_alt" <? if( intval($arItem['PROPERTY_EXPERT_PHOTO_VALUE']) > 0 ){} else { echo 'style="margin:0;"'; } ?>><?=$arItem['NAME']?></div>
                </div>
            </a>

        <? } ?>

    </div>

<?php } ?>
