<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');  use AOptima\Project as project;
\Bitrix\Main\Loader::includeModule('aoptima.tools');  use AOptima\Tools as tools;

if( count($arResult['complects']) > 0 ){ ?>

    <div class="p-prompts-tabs__panel" id="promptsSets">
        <div class="sets-slider sets-slider-js swiper-container">
            <div class="swiper-wrapper">

                <? foreach( $arResult['complects'] as $key => $complect ){

                    $products_sum = 0;
                    foreach ( $complect['ITEMS'] as $key => $item ){
                        $products_sum += $item['CATALOG_PRICE_'.project\catalog::BASE_PRICE_ID];
                    }

                    if( $products_sum > $complect['CATALOG_PRICE_'.project\catalog::BASE_PRICE_ID] ){ ?>

                        <div class="swiper-slide sets-slider__item">
                            <div class="set equal-height-js">

                                <? $cnt = 0;
                                foreach ( $complect['ITEMS'] as $key => $item ){ $cnt++;
                                    $section = tools\el::sections($item['ID']);

                                    if( $cnt != 1 ){
                                        echo '<div class="set__sign set__sign_plus">+</div>';
                                    }

                                    $GLOBALS['productComplectBlockItem']['ID'] = $item['ID'];
                                    $APPLICATION->IncludeComponent(
                                        "bitrix:catalog.section", "productComplectBlockItem",
                                        Array(
                                            'section' => $section,
                                            "IBLOCK_TYPE" => project\catalog::IBLOCK_TYPE,
                                            "IBLOCK_ID" => $item['IBLOCK_ID'],
                                            "SECTION_USER_FIELDS" => array(),
                                            "ELEMENT_SORT_FIELD" => 'NAME',
                                            "ELEMENT_SORT_ORDER" => 'ASC',
                                            "ELEMENT_SORT_FIELD2" => "NAME",
                                            "ELEMENT_SORT_ORDER2" => 'ASC',
                                            "FILTER_NAME" => 'productComplectBlockItem',
                                            "HIDE_NOT_AVAILABLE" => "N",
                                            "PAGE_ELEMENT_COUNT" => 1,
                                            "LINE_ELEMENT_COUNT" => "3",
                                            "PROPERTY_CODE" => array('STATUS_TOVARA'),
                                            "OFFERS_LIMIT" => 0,
                                            "TEMPLATE_THEME" => "",
                                            "PRODUCT_SUBSCRIPTION" => "N",
                                            "SHOW_DISCOUNT_PERCENT" => "N",
                                            "SHOW_OLD_PRICE" => "N",
                                            "MESS_BTN_BUY" => "Купить",
                                            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                                            "MESS_BTN_SUBSCRIBE" => "Подписаться",
                                            "MESS_BTN_DETAIL" => "Подробнее",
                                            "MESS_NOT_AVAILABLE" => "Нет в наличии",
                                            "SECTION_URL" => "",
                                            "DETAIL_URL" => "",
                                            "SECTION_ID_VARIABLE" => "SECTION_ID",
                                            "AJAX_MODE" => "N",
                                            "AJAX_OPTION_JUMP" => "N",
                                            "AJAX_OPTION_STYLE" => "Y",
                                            "AJAX_OPTION_HISTORY" => "N",
                                            "CACHE_TYPE" => project\catalog::CACHE_TYPE,
                                            "CACHE_TIME" => project\catalog::CACHE_TIME,
                                            "CACHE_GROUPS" => "Y",
                                            "SET_META_KEYWORDS" => "N",
                                            "META_KEYWORDS" => "",
                                            "SET_META_DESCRIPTION" => "N",
                                            "META_DESCRIPTION" => "",
                                            "BROWSER_TITLE" => "-",
                                            "ADD_SECTIONS_CHAIN" => "N",
                                            "DISPLAY_COMPARE" => "N",
                                            "SET_TITLE" => "N",
                                            "SET_STATUS_404" => "N",
                                            "CACHE_FILTER" => "Y",
                                            "PROPERTY_CODE" => array('STATUS_TOVARA'),
                                            "PRICE_CODE" => array(
                                                project\catalog::RASSROCHKA_PRICE_CODE,
                                                project\catalog::BASE_PRICE_CODE
                                            ),
                                            "USE_PRICE_COUNT" => "N",
                                            "SHOW_PRICE_COUNT" => "1",
                                            "PRICE_VAT_INCLUDE" => "Y",
                                            "CONVERT_CURRENCY" => "N",
                                            "BASKET_URL" => "/personal/basket.php",
                                            "ACTION_VARIABLE" => "action",
                                            "PRODUCT_ID_VARIABLE" => "id",
                                            "USE_PRODUCT_QUANTITY" => "N",
                                            "ADD_PROPERTIES_TO_BASKET" => "Y",
                                            "PRODUCT_PROPS_VARIABLE" => "prop",
                                            "PARTIAL_PRODUCT_PROPERTIES" => "N",
                                            "PRODUCT_PROPERTIES" => "",
                                            "PAGER_TEMPLATE" => "",
                                            "DISPLAY_TOP_PAGER" => "N",
                                            "DISPLAY_BOTTOM_PAGER" => "Y",
                                            "PAGER_TITLE" => "Товары",
                                            "PAGER_SHOW_ALWAYS" => "N",
                                            "PAGER_DESC_NUMBERING" => "N",
                                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                            "PAGER_SHOW_ALL" => "Y",
                                            "ADD_PICT_PROP" => "-",
                                            "LABEL_PROP" => "-",
                                            'INCLUDE_SUBSECTIONS' => "Y",
                                            'SHOW_ALL_WO_SECTION' => "Y"
                                        )
                                    );

                                } ?>

                                <div class="set__sign set__sign_equal">=</div>

                                <div class="set__result">
                                    <div class="set-result">

                                        <div class="set-result-price set-result__price">

                                            <div class="set-result-price__old"><strong class="set-result-price__old-val"><?=number_format($products_sum, project\catalog::PRICE_ROUND, ".", " ")?></strong>&nbsp;<em class="set-result-price__old-unit">р.</em></div>

                                            <div class="set-result-price__new"><strong class="set-result-price__new-val"><?=number_format($complect['CATALOG_PRICE_'.project\catalog::BASE_PRICE_ID], project\catalog::PRICE_ROUND, ".", " ")?></strong>&nbsp;<em class="set-result-price__new-unit">р.</em></div>

                                        </div>

                                        <div class="set-result-options">

                                            <div class="set-result-options__item">
                                                <a style="cursor: pointer" class="btn-buy btn-buy_stretch add_to_basket_button to___process" item_id="<?=$complect['ID']?>">
                                                    <span class="btn-buy__buy">Купить</span>
                                                    <span class="btn-buy__in-cart">В&nbsp;корзине</span>
                                                    <i>
                                                        <svg class="svg-ico-cart" xmlns="http://www.w3.org/2000/svg" width="23" height="21" viewBox="0 0 23 21">
                                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M23 0.902583C23 1.40102 22.6057 1.80517 22.1192 1.80517H20.3482L14.4767 15.6447H2.34872L0 6.01718H11.4035C11.89 6.01718 12.2842 6.42117 12.2842 6.91976C12.2842 7.4182 11.89 7.82235 11.4035 7.82235H2.25645L3.72437 13.8396H13.3215L19.1931 0H22.1192C22.6059 0 23 0.403934 23 0.902583ZM7.84736 18.5028C7.84736 19.8797 6.75405 21 5.41057 21C4.06724 21 2.97399 19.8797 2.97399 18.5028C2.97399 17.1258 4.06724 16.0055 5.41057 16.0055C6.75405 16.0055 7.84736 17.1258 7.84736 18.5028ZM5.41057 17.2089C6.10667 17.2089 6.6731 17.7895 6.6731 18.5028C6.6731 19.2163 6.10667 19.7966 5.41057 19.7966C4.71467 19.7966 4.14824 19.2163 4.14824 18.5028C4.14824 17.7895 4.71467 17.2089 5.41057 17.2089ZM14.3235 18.5028C14.3235 19.8797 13.23 21 11.8866 21C10.5433 21 9.45009 19.8797 9.45009 18.5028C9.45009 17.1258 10.5433 16.0055 11.8866 16.0055C13.23 16.0055 14.3235 17.1258 14.3235 18.5028ZM11.8866 17.2089C12.5828 17.2089 13.1493 17.7895 13.1493 18.5028C13.1493 19.2163 12.5827 19.7966 11.8866 19.7966C11.1906 19.7966 10.6243 19.2163 10.6243 18.5028C10.6243 17.7895 11.1906 17.2089 11.8866 17.2089Z"></path>
                                                        </svg>
                                                    </i>
                                                </a>
                                            </div>

                                            <!--                <div class="set-result-options__item">-->
                                            <!--                    <a href="#" class="btn-alt btn-alt_full-width">Купить в рассрочку</a>-->
                                            <!--                </div>-->

                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>

                    <? }
                } ?>

            </div>

            <div class="sets-slider__prev sets-slider__prev-js">
                <svg width="19" height="13" viewBox="0 0 19 13" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6.1979 12.8045C6.46575 13.0652 6.88744 13.0652 7.1553 12.8045C7.4141 12.5526 7.4141 12.1334 7.1553 11.8821L2.30074 7.15731L18.3201 7.15731C18.6935 7.15731 19 6.86843 19 6.50499C19 6.14155 18.6935 5.84328 18.3201 5.84328L2.30074 5.84328L7.1553 1.12732C7.4141 0.866628 7.4141 0.446818 7.1553 0.195519C6.88744 -0.0651731 6.46575 -0.0651731 6.1979 0.195519L0.194104 6.0388C-0.0647013 6.29068 -0.0647013 6.7099 0.194104 6.9612L6.1979 12.8045Z"></path>
                </svg>
                <span>Prev</span>
            </div>

            <div class="sets-slider__next sets-slider__next-js">
                <svg width="19" height="13" viewBox="0 0 19 13" xmlns="http://www.w3.org/2000/svg">
                    <path d="M12.8021 0.19552C12.5342 -0.0651732 12.1126 -0.0651732 11.8447 0.19552C11.5859 0.447405 11.5859 0.866628 11.8447 1.11793L16.6993 5.84269H0.679892C0.306465 5.84269 0 6.13157 0 6.49501C0 6.85845 0.306465 7.15672 0.679892 7.15672H16.6993L11.8447 11.8727C11.5859 12.1334 11.5859 12.5532 11.8447 12.8045C12.1126 13.0652 12.5342 13.0652 12.8021 12.8045L18.8059 6.9612C19.0647 6.70932 19.0647 6.2901 18.8059 6.0388L12.8021 0.19552Z"></path>
                </svg>
                <span>Next</span>
            </div>

        </div>
    </div>

<? } ?>