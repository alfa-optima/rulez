<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

if (count($arResult["CATEGORIES"]) > 0){ ?>

    <ul class="h-contacts__phones">

        <? foreach( $arResult['CATEGORIES'] as $category_id => $category ){
            //if( $category['CODE'] == 'a1' ){ ?>

                <li>

                    <? $APPLICATION->IncludeComponent(
                        "creativebz:phone", "headerList",
                        Array(
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "N",
                            "CLASS" => $category['UF_CSS_CLASS'],
                            "ID" => "",
                            "SECTION_ID" => $category_id
                        )
                    ); ?>

                </li>

            <? //}
        } ?>

    </ul>

<? } ?>