<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

if (count($arResult["CATEGORIES"]) > 0){

    $icons = [];
    foreach( $arResult["CATEGORIES"] as $category ){
        if(
            strlen($category['UF_CSS_CLASS_2']) > 0
            &&
            $category['CODE'] == 'a1'
        ){
            $icons[] = $category['UF_CSS_CLASS_2'];
        }
    }
    $icons = array_unique($icons);

    $key = array_keys($arResult["CATEGORIES"])[0];
    $category = $arResult["CATEGORIES"][$key]; ?>

    <? $APPLICATION->IncludeComponent(
        "creativebz:phone", "headerDefaultPhone",
        Array(
            "ICONS" => $icons,
            "CACHE_TIME" => "36000000",
            "CACHE_TYPE" => "N",
            "CLASS" => $category['UF_CSS_CLASS_2'],
            "ID" => "",
            "SECTION_ID" => $category['ID']
        )
    ); ?>

<? } ?>