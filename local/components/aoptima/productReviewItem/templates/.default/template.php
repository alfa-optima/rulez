<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools; ?>

<div class="u-reviews__item u-review productReviewItem" item_id="<?=$arResult['ID']?>">
    <div class="u-review__aside">

        <? if( intval($arResult['USER']['PERSONAL_PHOTO']) > 0 ){ ?>

            <a class="u-review__avatar">
                <img class="u-review__avatar-img" src="<?=tools\funcs::rIMGG($arResult['USER']['PERSONAL_PHOTO'], 5, 168, 168)?>" />
            </a>

        <? } else { ?>

            <a class="u-review__avatar">
                <div class="u-review__no-avatar"><span>Mr</span></div>
            </a>

        <? } ?>

    </div>
    <div class="u-review__body">
        <div class="u-review__header">
            <div class="u-review__meta">
                <div class="u-review__author-name"><a><?=$arResult['PROPERTY_NAME_VALUE']?></a></div>
                <time class="u-review__date" datetime="<?=ConvertDateTime($arResult['DATE_CREATE'], "YYYY-MM-DD", "ru")?>T<?=ConvertDateTime($arResult['DATE_CREATE'], "HH:MI", "ru")?>"><?=ConvertDateTime($arResult['DATE_CREATE'], "DD.MM.YYYY HH:MI", "ru")?></time>
            </div>
            <div class="u-review__rating-wrap">
                <div class="p-rate u-review__rating">
                    <span content="<?=$arResult['PROPERTY_VOTE_VALUE']?>" style="width: <?=round($arResult['PROPERTY_VOTE_VALUE']/5*100, 0)?>%"><?=$arResult['PROPERTY_VOTE_VALUE']?></span>
                </div>
            </div>
        </div>
        <div class="u-review__content">

            <? if( strlen($arResult['PROPERTY_PLUSES_VALUE']['TEXT']) > 0 ){ ?>
                <div class="u-review__condition">
                    <h4 class="u-review__condition-title">Достоинства:</h4>
                    <p><?=$arResult['PROPERTY_PLUSES_VALUE']['TEXT']?></p>
                </div>
            <? } ?>

            <? if( strlen($arResult['PROPERTY_MINUSES_VALUE']['TEXT']) > 0 ){ ?>
                <div class="u-review__condition">
                    <h4 class="u-review__condition-title">Недостатки:</h4>
                    <p><?=$arResult['PROPERTY_MINUSES_VALUE']['TEXT']?></p>
                </div>
            <? } ?>

            <? if( strlen($arResult['PREVIEW_TEXT']) > 0 ){ ?>
                <div class="u-review__condition">
                    <h4 class="u-review__condition-title">Комментарий:</h4>
                    <p><?=html_entity_decode($arResult['PREVIEW_TEXT'])?></p>
                </div>
            <? } ?>

            <? if( intval($arResult['PROPERTY_FILE_VALUE']) > 0 ){ ?>
                <div class="u-review__condition">
                    <h4 class="u-review__condition-title">Прикреплённое фото:</h4>
                    <div class="u-review__gallery u-review-gallery-js">
                        <a href="<?=tools\funcs::rIMGG($arResult['PROPERTY_FILE_VALUE'], 4, 800, 800)?>" class="u-review__gallery-item">
                            <img src="<?=tools\funcs::rIMGG($arResult['PROPERTY_FILE_VALUE'], 4, 90, 90)?>" class="u-review__gallery-img" alt="image description" />
                        </a>

                    </div>
                </div>
            <? } ?>

        </div>

        <div class="u-review__footer">

            <div class="u-review__footer-c">
                <div class="u-review__reply">
                    <? if( $arResult['IS_AUTH'] == 'Y' ){ ?>
                        <a style="cursor: pointer;" class="add-new-review-answer-js" item_id="<?=$arResult['ID']?>">Ответить</a>
                    <? } ?>
                </div>
            </div>

            <div class="u-review__voting">
                <div class="u-review__voting-label">Полезно?</div>
                <a style="cursor: pointer;" title="Да, полезно" class="u-review__voting-btn u-review__voting-plus productReviewVotePlus to___process" item_id="<?=$arResult['ID']?>">
                    <i class="u-review__voting-icon icon-voting-plus"></i>
                    <span class="u-review__voting-count" data-count="<?=$arResult['PLUS_VOTES_CNT']?>"><?=$arResult['PLUS_VOTES_CNT']?></span>
                </a>
                <a style="cursor: pointer;" title="Нет, бесполезно" class="u-review__voting-btn u-review__voting-minus productReviewVoteMinus to___process" item_id="<?=$arResult['ID']?>">
                    <i class="u-review__voting-icon icon-voting-minus"></i>
                    <span class="u-review__voting-count" data-count="<?=$arResult['MINUS_VOTES_CNT']?>"><?=$arResult['MINUS_VOTES_CNT']?></span>
                </a>
            </div>

        </div>

        <div class="u-review__talk productReviewSubItemsArea">

            <? if(
                is_array($arResult['SUB_ITEMS'])
                &&
                count($arResult['SUB_ITEMS']) > 0
            ){
                
                foreach( $arResult['SUB_ITEMS'] as $sub_review_id => $sub_review ){

                    // productSubReviewItem
                    $APPLICATION->IncludeComponent(
                        "aoptima:productSubReviewItem", "",
                        array('review' => $sub_review)
                    );

                }
            } ?>

        </div>

    </div>
</div>