<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>

<div class="content">

    <style>
        .content-align::before {
            border: none!important;
        }
        .content-inner {
            margin-left: 0!important;
        }
    </style>

    <div class="content-align">

        <section class="content-inner article-content">

            <h1>Подтверждение регистрации</h1>

            <div class="cabinet-profile-form">

                <? if( $arResult['ERROR'] ){ ?>

                    <p class="error___p" style="font-size: 18px;"><?=$arResult['ERROR']?></p>

                <? } else if( $arResult['SUCCESS'] ){ ?>

                    <p class="success___p"><?=$arResult['SUCCESS']?></p>

                <? } ?>

                <p class="success___p">
                    <a href="#popup-login" class="open-popup-def-js" style="color: green; text-decoration: underline;">Авторизация</a>
                </p>

            </div>

        </section>

    </div>

</div>

