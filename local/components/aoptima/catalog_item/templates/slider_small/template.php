<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');  use AOptima\Project as project;
\Bitrix\Main\Loader::includeModule('aoptima.tools');  use AOptima\Tools as tools; ?>


<div class="swiper-slide shot-slider__item catalog___item" item_id="<?=$arResult['ID']?>">
    <div itemscope itemtype="http://schema.org/product-short" class="product-short product-short-js">
        <div class="product-short__main">

            <? // product_labels
            $APPLICATION->IncludeComponent(
                "aoptima:product_labels", "catalog_item_short",
                array( 'element' => $arResult )
            ); ?>

            <a itemprop="url" href="<?=$arResult['DETAIL_PAGE_URL']?>" title="<?=$arResult['NAME']?>">
                <div class="product-short__figure ">
                    <? if( intval($arResult['DETAIL_PICTURE']['ID']) > 0 ){ ?>
                        <img src="<?=tools\funcs::rIMGG($arResult['DETAIL_PICTURE']['ID'], 4, 100, 100)?>">
                    <? } else { ?>
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/no-photo.png">
                    <? } ?>
                </div>
                <div class="product-short__category"><?=$arParams['~category']['NAME']?></div>
                <h3 itemprop="name" class="product-short__title"><span><?=$arResult['~NAME']?></span></h3>
            </a>

        </div>

        <div class="product-short__footer has-cart-btn">

            <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="product-short__offer">
                <div class="p-short-price">

                    <? if( $arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['VALUE'] > 0 ){ ?>

                        <? if(
                            $arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['VALUE_VAT']
                            !=
                            $arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['DISCOUNT_VALUE_VAT']
                        ){ ?>

                            <div class="p-short-price__old"><strong class="p-short-price__old-val"><?=number_format($arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['VALUE_VAT'], project\catalog::PRICE_ROUND, ".", " ")?></strong>&nbsp<em class="p-short-price__old-unit">р.</em></div>

                        <? } ?>

                        <div class="p-short-price__new"><strong class="p-short-price__new-val" itemprop="price" content="<?=$arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['DISCOUNT_VALUE_VAT']?>"><?=number_format($arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['DISCOUNT_VALUE_VAT'], project\catalog::PRICE_ROUND, ".", " ")?></strong>&nbsp;<em class="p-short-price__new-unit" itemprop="priceCurrency" content="<?=project\catalog::CURRENCY?>">р.</em></div>

                    <? } ?>

                </div>
            </div>

            <? if(
                in_array($arResult['PROPERTIES']['STATUS_TOVARA']['VALUE'], project\catalog::$nal_statuses)
                &&
                $arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['VALUE'] > 0
            ){ ?>

                <a style="cursor: pointer" class="purchase-sm-btn add_to_basket_button to___process <? if( $arParams['isInBasket'] == 'Y' ){ echo 'isInBasket'; } ?>" item_id="<?=$arResult['ID']?>">
                    <span class="purchase-sm-btn__to-cart">Добавить в&nbsp;корзину</span>
                    <i class="icon-cart-color"></i>
                </a>

            <? } ?>

        </div>
    </div>
</div>