<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');  use AOptima\Project as project;
\Bitrix\Main\Loader::includeModule('aoptima.tools');  use AOptima\Tools as tools; ?>

<?php if(
    trim(strtolower($arResult["PROPERTIES"][project\catalog::IS_RULIK_REC_PROP_CODE]['VALUE']))
    ==
    trim(strtolower(project\catalog::IS_RULIK_REC_PROP_VALUE))
){ ?>
    <div class="flip-product js-flip-product">
    <div class="flip-product-frame">
    <div class="flip-product-card flip-product-card_front">
<?php } ?>

<div itemscope itemtype="http://schema.org/Product" class="product product-js catalog___item <?php if(
    trim(strtolower($arResult["PROPERTIES"][project\catalog::IS_RULIK_REC_PROP_CODE]['VALUE']))
    ==
    trim(strtolower(project\catalog::IS_RULIK_REC_PROP_VALUE))
){ ?>product_mark<? } ?>" item_id="<?=$arResult['ID']?>">

    <?php if(
        trim(strtolower($arResult["PROPERTIES"][project\catalog::IS_RULIK_REC_PROP_CODE]['VALUE']))
        ==
        trim(strtolower(project\catalog::IS_RULIK_REC_PROP_VALUE))
    ){ ?>
        <div class="product__mark-label js-flip-btn js-rulik-bubble" data-bubble="Слабо нажать?">
            <img src="<?=SITE_TEMPLATE_PATH?>/img/rulik.png">
        </div>
    <?php } ?>

    <div class="product__main">

        <? // product_labels
        $APPLICATION->IncludeComponent(
            "aoptima:product_labels", "catalog_item",
            array( 'element' => $arResult )
        ); ?>

        <a itemprop="url" href="<?=$arResult['DETAIL_PAGE_URL']?>">
            <div class="product__figure product-figure-js">
                <? if(
                    intval($arResult['DETAIL_PICTURE']['ID']) > 0
                    &&
                    $arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['VALUE'] > 0
                ){ ?>
                    <img itemprop="image" src="<?=SITE_TEMPLATE_PATH?>/img/preloader.svg" data-src="<?=tools\funcs::rIMGG($arResult['DETAIL_PICTURE']['ID'], 4, 261, 178)?>">
                <? } else { ?>
                    <img itemprop="image" src="<?=SITE_TEMPLATE_PATH?>/img/preloader.svg" data-src="<?=SITE_TEMPLATE_PATH?>/img/no-photo.png">
                <? } ?>
            </div>
            <h3 itemprop="name" class="product__title"><span><?=$arResult['~NAME']?></span></h3>
        </a>

        <div class="product__meta">
            <div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating" class="p-rate product__rate">
                <span itemprop="ratingValue" content="0" style="width: 0%">0</span>
                <meta itemprop="ratingCount" content="0">
            </div>
        </div>

        <? if(
            count($arResult['SHORT_PROPS_LIST']) > 0
            &&
            $arParams['hideChars'] != 'Y'
        ){ ?>
            <ul class="products__features">
                <? foreach( $arResult['SHORT_PROPS_LIST'] as $prop_name => $prop_value ){ ?>
                    <li style="text-align: left;"><?=$prop_name?>: <span><?=$prop_value?></span></li>
                <? } ?>
            </ul>
        <? } ?>

    </div>

    <div class="product__footer">
        <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="product__offer">

            <? if( in_array($arResult['PROPERTIES']['STATUS_TOVARA']['VALUE'], project\catalog::$nal_statuses)  ){ ?>
                <div itemprop="availability" content="http://schema.org/InStock" class="p-stock product__stock on"><i>&nbsp;</i>В наличии</div>
            <? } else { ?>
                <div itemprop="availability" content="http://schema.org/InStock" class="p-stock product__stock on" style="padding-left: 0;">Нет в наличии</div>
            <? } ?>

            <div class="product__price">

                <? if( $arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['VALUE'] > 0 ){ ?>

                    <? if( $arParams['isGift'] == 'Y' ){ ?>

                        <div class="product__price-old"><strong><?=number_format($arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['VALUE_VAT'], project\catalog::PRICE_ROUND, ".", " ")?></strong>&nbsp;<em>р.</em></div>
                        <div class="product__price-new"><strong itemprop="price" content="0.00"><?=number_format(0, project\catalog::PRICE_ROUND, ".", " ")?></strong>&nbsp;<em itemprop="priceCurrency" content="<?=project\catalog::CURRENCY?>">р.</em></div>

                    <? } else { ?>

                        <? if(
                            $arResult['PRICES'][project\catalog::RASSROCHKA_PRICE_CODE]['VALUE_VAT'] > 0
                            &&
                            $arResult['PROPERTIES'][project\catalog::RASSROCHKA_PROP_CODE]['VALUE'] == 'Да'
                            &&
                            $arResult['PRICES'][project\catalog::RASSROCHKA_PRICE_CODE]['VALUE_VAT']
                            >
                            $arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['DISCOUNT_VALUE_VAT']
                        ){ ?>
                            <div class="product__price-old"><strong><?=number_format($arResult['PRICES'][project\catalog::RASSROCHKA_PRICE_CODE]['VALUE_VAT'], project\catalog::PRICE_ROUND, ".", " ")?></strong>&nbsp;<em>р.</em></div>
                        <? } ?>

                        <div class="product__price-new"><strong itemprop="price" content="<?=$arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['DISCOUNT_VALUE_VAT']?>"><?=number_format($arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['DISCOUNT_VALUE_VAT'], project\catalog::PRICE_ROUND, ".", " ")?></strong>&nbsp;<em itemprop="priceCurrency" content="<?=project\catalog::CURRENCY?>">р.</em></div>

                        <? if(
                            $arResult['PRICES'][project\catalog::RASSROCHKA_PRICE_CODE]['VALUE_VAT'] > 0
                            &&
                            $arResult['PROPERTIES'][project\catalog::RASSROCHKA_PROP_CODE]['VALUE'] == 'Да'
                            &&
                            $arResult['PRICES'][project\catalog::RASSROCHKA_PRICE_CODE]['VALUE_VAT']
                            >
                            $arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['DISCOUNT_VALUE_VAT']
                        ){ ?>
                            <div class="product__price-note">Спецпредложение</div>
                        <?php }

                    }

                } ?>

            </div>
        </div>
        <div class="product__options">
            <div class="product__action">
                <a style="cursor: pointer;" class="action-btn action-btn_compare product__action-btn product__options-btn_compare compare___button" data-active-text="Добавлен к сравнению" data-inactive-text="Сравнить" item_id="<?=$arResult['ID']?>">
                    <span>Сравнить</span>
                    <svg class="svg-ico-compare" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M0 5.89474V16H3.36842V5.89474H0ZM0.842105 6.73684V15.1579H2.52632V6.73684H0.842105ZM4.21053 16V0H7.57895V16H4.21053ZM5.05263 15.1579V0.842105H6.73684V15.1579H5.05263ZM8.42105 2.52632V16H11.7895V2.52632H8.42105ZM9.26316 3.36842V15.1579H10.9474V3.36842H9.26316ZM12.6316 16V7.57895H16V16H12.6316ZM13.4737 15.1579V8.42105H15.1579V15.1579H13.4737Z"></path>
                    </svg>
                </a>
                <a style="cursor: pointer;" class="action-btn action-btn_like product__action-btn product__options-btn product__options-btn_like fav___button" data-active-text="Мне нравится" data-inactive-text="В избранное" item_id="<?=$arResult['ID']?>"><span>В избранное</span>
                    <svg class="svg-ico-like" xmlns="http://www.w3.org/2000/svg" width="19" height="17" viewBox="0 0 19 17">
                        <path d="M18 5.67611C18 3.09352 15.9446 1 13.4091 1C11.7535 1 10.3077 1.89561 9.49995 3.23407C8.69232 1.89561 7.24584 1 5.59027 1C3.05483 1 1 3.09352 1 5.67611C1 7.08219 1.61169 8.34013 2.57576 9.19676L9.08102 15.8233C9.19212 15.9364 9.34279 16 9.49995 16C9.65711 16 9.80778 15.9364 9.91888 15.8233L16.4241 9.19676C17.3882 8.34013 18 7.08219 18 5.67611Z"></path>
                    </svg>
                </a>
            </div>

            <? if(
                in_array($arResult['PROPERTIES']['STATUS_TOVARA']['VALUE'], project\catalog::$nal_statuses)
                &&
                $arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['VALUE'] > 0
            ){ ?>

                <div class="product__purchase">
                    <a style="cursor:pointer;" class="purchase-btn product__options-btn add_to_basket_button to___process" item_id="<?=$arResult['ID']?>">
                        <span class="purchase-btn__to-cart">В&nbsp;корзину</span>
                        <span class="purchase-btn__buy">Купить</span>
                        <span class="purchase-btn__in-cart">В&nbsp;корзине</span>
                        <i class="icon-cart-color"></i>
                    </a>
                </div>

                <? if(
                    0
                    &&
                    $arResult['PROPERTIES'][ project\catalog::RASSROCHKA_PROP_CODE]['VALUE'] == 'Да'
                    &&
                    $arResult['PRICES'][ project\catalog::RASSROCHKA_PRICE_CODE]['VALUE'] > 0
                ){ ?>

                    <div class="product__installment">
                        <a style="cursor: pointer" class="btn-alt add_to_basket_button to___process" item_id="<?=$arResult['ID']?>">Купить в рассрочку</a>
                    </div>

                <? } ?>

            <? } ?>

        </div>
    </div>

</div>

<?php if(
    trim(strtolower($arResult["PROPERTIES"][project\catalog::IS_RULIK_REC_PROP_CODE]['VALUE']))
    ==
    trim(strtolower(project\catalog::IS_RULIK_REC_PROP_VALUE))
){ ?>

    </div>

    <div class="flip-product-card flip-product-card_back flip-product-card_mark">
        <div class="flip-product-card__alight">
            <div class="recommend recommend_align-center">
                <div class="recommend__align">
                    <div class="recommend__title">Рулик рекомендует</div>
                    <div class="recommend__figure">
                        <div class="recommend__figure-img">
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/rulik_purple.png" alt="Рулик рекомендует">
                        </div>
                    </div>
                    <div class="recommend__text"><?=$arResult["PROPERTIES"][project\catalog::IS_RULIK_REC_DESCR_PROP_CODE]['VALUE']?></div>
                    <div class="recommend__footer">
                        <a href="<?=$arResult["DETAIL_PAGE_URL"]?>" class="btn-alt js-flip-btn">Перевернуть</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>
    </div>

<?php } ?>