<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$arResult = $arParams['arItem'];
$arResult['component'] = $arParams['component'];
//unset($arParams['component']);


$arResult['SHORT_PROPS_LIST'] = [];
if(
    is_array( $arResult["PROPERTIES"][project\catalog::SHORT_LIST_PROP_CODE]['VALUE'] )
    &&
    count($arResult["PROPERTIES"][project\catalog::SHORT_LIST_PROP_CODE]['VALUE']) > 0
){
    foreach ( $arResult["PROPERTIES"][project\catalog::SHORT_LIST_PROP_CODE]['VALUE'] as $prop_xml_id ){
        foreach ( $arResult["PROPERTIES"] as $prop_code => $prop ){
            if(
                $prop_xml_id == $prop['XML_ID']
                &&
                strlen($prop['VALUE']) > 0
            ){
                $arResult['SHORT_PROPS_LIST'][$prop['NAME']] = $prop['VALUE'];
            }
        }
    }
}




$this->IncludeComponentTemplate();