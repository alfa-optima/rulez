<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>

    <h3>Отзывы</h3>

<? if( $arResult['reviews_cnt'] == 0 ){ ?>
    <div class="u-reviews-empty">К этому товару нет отзывов.
        <? if( $arResult['IS_AUTH'] == 'Y' ){ ?>
            <a href="#" class="add-new-review-js">Будь первым!</a>
        <? } ?>
    </div>
<? } ?>


<div class="new-u-review">
    <? if( $arResult['IS_AUTH'] != 'Y' ){ ?>
        <div class="new-u-review__note">Для написания отзыва необходимо <a href="#popup-login" class="open-popup-def-js login___link" style="cursor: pointer">авторизоваться</a> на сайте.</div>
    <? } else { ?>
        <div class="new-u-review__note">Поделитесь своим мнением о товаре: <?=$arResult['el']['NAME']?></div>
        <div class="new-u-review__footer">
            <a style="cursor: pointer;" class="btn-def new-u-review__btn add-new-review-js">Написать отзыв</a>
        </div>
    <? } ?>
</div>

<? if( $arResult['IS_AUTH'] == 'Y' ){ ?>

    <? // productReviewFormBlock
    $APPLICATION->IncludeComponent(
        "aoptima:productReviewFormBlock", "",
        array( 'PRODUCT' => $arResult['el'] )
    ); ?>

    <? // productReviewAnswerFormBlock
    $APPLICATION->IncludeComponent(
        "aoptima:productReviewAnswerFormBlock", "",
        array( 'PRODUCT' => $arResult['el'] )
    ); ?>

<? } ?>

<? // Отзывы к товарам
$APPLICATION->IncludeComponent(
    "aoptima:productReviews", "",
    array('PRODUCT' => $arResult['el'])
); ?>