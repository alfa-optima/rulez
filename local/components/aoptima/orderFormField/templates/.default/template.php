<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

if( $arResult ){ ?>

    <label class="input-wrap" <? if( $arResult['HIDE'] ){ ?>style="display:none;"<?php } ?>>
        <input
            class="input-def <?=$arResult['CSS_CLASSES']?>"
            name="<?=$arResult['CODE']?>"
            type="<?=$arResult['type']?>"
            value="<?=$arResult['VALUE']?>"
            <? if( $arResult['REQUIRED'] && !$USER->IsAdmin() && 0 ){ echo 'required'; } ?>
        >
        <span class="label-float"><?=$arResult['NAME']?></span>
    </label>

<?php } ?>