<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$allFields = $arParams['ALL_FIELDS'];

if( strlen( $arParams['FIELD_CODE'] ) > 0 ){

    $arResult = $allFields[$arParams['FIELD_CODE']];

    $arResult['VALUE'] = $arParams['FIELD_VALUE'];

    $arResult['type'] = strlen($arResult['type'])>0?$arResult['type']:'text';

    $arResult['REQUIRED'] = $allFields[$arParams['FIELD_CODE']]['REQUIRED'];
    $arResult['HIDE'] = $allFields[$arParams['FIELD_CODE']]['HIDE'];

}







$this->IncludeComponentTemplate();