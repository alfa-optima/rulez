<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;


$arResult['MANAGER'] = false;
$arResult['ALL_MANAGERS'] = project\manager::getList();

if( count($arResult['ALL_MANAGERS']) > 0 ){

    if( intval($arParams['section_id']) > 0 ){
        $section = tools\section::info($arParams['section_id']);


        foreach ( $arResult['ALL_MANAGERS'] as $key => $manager ){
            if(
                $manager['PROPERTY_SECTION_XML_ID_VALUE'] == $section['XML_ID']
                &&
                !$arResult['MANAGER']
            ){      $arResult['MANAGER'] = $manager;      }
        }

        if( !$arResult['MANAGER'] ){
            $chain = tools\section::chain($arParams['section_id']);
            $chain = array_reverse($chain);
            foreach ( $chain as $sect ){
                foreach ( $arResult['ALL_MANAGERS'] as $key => $manager ){
                    if(
                        $manager['PROPERTY_SECTION_XML_ID_VALUE'] == $sect['XML_ID']
                        &&
                        !$arResult['MANAGER']
                    ){      $arResult['MANAGER'] = $manager;      }
                }
            }
        }
    }

    if( !$arResult['MANAGER'] ){
        $arResult['MANAGER'] = $arResult['ALL_MANAGERS'][array_keys($arResult['ALL_MANAGERS'])[0]];
    }

}

$this->IncludeComponentTemplate();