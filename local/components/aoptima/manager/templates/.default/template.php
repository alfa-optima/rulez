<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if( $arResult['MANAGER'] ){ ?>

   <div class="advise__contact managerBlock">

        <div class="advise__figure">
            <img src="<?=tools\funcs::rIMGG($arResult['MANAGER']['PREVIEW_PICTURE'], 5, 168, 168)?>" alt="image description" />
        </div>

        <strong class="advise__name"><?=$arResult['MANAGER']['NAME']?></strong> <span class="advise__past">Специалист</span>

        <div class="advise-chat">
            <ul class="advise-chat__list">
                <? if( strlen($arResult['MANAGER']['PROPERTY_VIBER_VALUE']) > 0 ){ ?>
                    <li>
                        <a href="https://www.viber.com/<?=$arResult['MANAGER']['PROPERTY_VIBER_VALUE']?>" target="_blank" class="chat-telegram"><span>Viber</span></a>
                    </li>
                <? }
                if( strlen($arResult['MANAGER']['PROPERTY_TELEGRAM_VALUE']) > 0 ){ ?>
                    <li>
                        <a class="chat-viber" href="http://t.me/<?=$arResult['MANAGER']['PROPERTY_TELEGRAM_VALUE']?>" target="_blank" ><span>Telegram</span></a>
                    </li>
                <? }
                $watsapp_phone = $arResult['MANAGER']['PROPERTY_PHONE_VALUE'];
                $watsapp_phone = preg_replace('/[^0-9]/', '', $watsapp_phone);
                if( strlen( $watsapp_phone ) > 0 ){ ?>
                    <li>
                        <a href="https://wa.me/<?=$watsapp_phone?>" target="_blank" class="chat-whatsapp"><span>Whatsapp</span></a>
                    </li>
                <? } ?>
            </ul>
        </div>

    </div>

<? } ?>