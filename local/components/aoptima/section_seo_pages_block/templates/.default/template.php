<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

if( count($arResult['SEO_PAGES_CATEGORIES']) > 0 ){ ?>

    <!--- Готовые подборки --->
    <section class="section section_collections tabs-js">
        <div class="collections">

            <h2 class="collections__title">Готовые подборки</h2>

            <div class="collections__content">

                <div class="collections__nav">
                    <div class="collections__select tabs__select-js"></div>
                    <div class="collections__thumbs tabs__thumbs-js tabs__select-drop-js">
                        <? $cnt = 0;
                        foreach( $arResult['SEO_PAGES_CATEGORIES'] as $key => $category ){ $cnt++; ?>
                            <a href="#category_<?=$category['ID']?>" <? if( $cnt == 1 ){ ?>class="tabs-active"<? } ?>><span><?=$category['NAME']?></span></a>
                        <? } ?>
                    </div>
                </div>

                <div class="tabs__panels-js">

                    <? foreach( $arResult['ALL_CATEGORIES'] as $key => $cat ){
                        $category = $arResult['SEO_PAGES_CATEGORIES'][$cat['ID']]; ?>

                        <div id="category_<?=$category['ID']?>">
                            <ul class="float-list">

                                <? foreach( $arResult['SEO_PAGES'] as $seo_page ){
                                    if( $seo_page['CATEGORY'] == $category['ID'] ){ ?>
                                        <li>
                                            <a href="<?=$seo_page['SECTION_PAGE_URL']?>"><span><?=$seo_page['NAME']?></span></a>
                                        </li>
                                    <? }
                                } ?>

                            </ul>
                        </div>

                    <? } ?>

                </div>
            </div>
        </div>
    </section>

<? } ?>