<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;


$arResult['SECTION'] = $arParams['section'];

$arResult['ALL_CATEGORIES'] = project\SeopagecategoryTable::all_items();
$arResult['SEO_PAGES'] = project\SeopageTable::getBySectionXMLID( $arResult['SECTION']['XML_ID'] );


$arResult['SEO_PAGES_CATEGORIES'] = [];
foreach ( $arResult['SEO_PAGES'] as $seo_page ){
    $category = $arResult['ALL_CATEGORIES'][$seo_page['CATEGORY']];
    if(
        intval($seo_page['CATEGORY']) > 0
        &&
        $category
    ){    $arResult['SEO_PAGES_CATEGORIES'][$category['ID']] = $category;    }
}







$this->IncludeComponentTemplate();