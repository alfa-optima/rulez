<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;
\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$arResult['BANNER'] = false;

$all_banners = project\banner::allList();

if( $USER->IsAdmin() ){
    //echo "<pre>"; print_r( $all_banners ); echo "</pre>";
}

// отсеем истекшие и не наступившие
foreach ( $all_banners as $key => $banner ){
    $time_from = strtotime($banner['DATE_ACTIVE_FROM']);
    if( strlen($time_from) > 0 && $time_from > time() ){    unset($all_banners[$key]);    }
}
foreach ( $all_banners as $key => $banner ){
    $time_to = strtotime($banner['DATE_ACTIVE_TO']);
    if( strlen($time_to) > 0 && $time_to < time() ){    unset($all_banners[$key]);    }
}

if( intval($arParams['catalog_section']['ID']) > 0 ){
    $catalog_section = $arParams['~catalog_section'];
    foreach ( $all_banners as $key => $banner ){
        if(
            strlen($banner['PROPERTY_SECTION_XML_ID_VALUE']) > 0
            &&
            $banner['PROPERTY_SECTION_XML_ID_VALUE']
            !=
            $catalog_section['XML_ID']
        ){   unset($all_banners[$key]);   }
    }
}


$all_banner_types = project\banner::types();

if( count($all_banners) > 0 ){
    $arResult['BANNERS'] = [];
    foreach ( $all_banners as $banner ){
        $banner_sects = tools\el::sections($banner['ID']);
        foreach ( $banner_sects as $banner_sect ){
            $arResult['BANNERS'][$banner_sect['CODE']][$banner['ID']] = $banner;
        }
    }

    if( $arParams['type'] ){
        if( $arResult['BANNERS'][$arParams['type']] ){
            shuffle($arResult['BANNERS'][$arParams['type']]);
            $arResult['BANNER'] = $arResult['BANNERS'][$arParams['type']][array_keys($arResult['BANNERS'][$arParams['type']])[0]];
        }
    } else {
        shuffle($all_banners);
        $arResult['BANNER'] = $all_banners[array_keys($all_banners)[0]];
    }

}

//echo "<pre>"; print_r( $arResult['BANNERS'] ); echo "</pre>";

$this->IncludeComponentTemplate();