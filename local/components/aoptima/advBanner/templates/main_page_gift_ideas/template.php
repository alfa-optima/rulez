<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(  $arResult['BANNER'] ){
    
    echo '<article class="topics__item">';

        echo '<a '.($arResult['BANNER']['PROPERTY_LINK_VALUE']?('href="'.$arResult['BANNER']['PROPERTY_LINK_VALUE'].'" target="_blank"'):'').' class="topic-banner">';

            echo '<img src="'.SITE_TEMPLATE_PATH.'/img/preloader.svg" data-src="'.\CFile::GetPath($arResult['BANNER']['DETAIL_PICTURE']).'"/>';

            echo '<div class="topic-banner__title"><span>'.$arResult['BANNER']['NAME'].'</span></div>';

        echo '</a>';

    echo '</article>';

}