<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(  $arResult['BANNER'] ){

    echo '<a '.($arResult['BANNER']['PROPERTY_LINK_VALUE']?('href="'.$arResult['BANNER']['PROPERTY_LINK_VALUE'].'" target="_blank"'):'').' class="banner">';

        echo '<img class="no-mobile-only lozad" src="'.SITE_TEMPLATE_PATH.'/img/preloader.svg" data-src="'.\CFile::GetPath($arResult['BANNER']['DETAIL_PICTURE']).'">';

        echo '<img class="mobile-only lozad" src="'.\CFile::GetPath($arResult['BANNER']['PREVIEW_PICTURE']).'" data-src="'.\CFile::GetPath($arResult['BANNER']['PREVIEW_PICTURE']).'">';

    echo '</a>';

}