<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project'); use AOptima\Project as project;
\Bitrix\Main\Loader::includeModule('aoptima.tools'); use AOptima\Tools as tools;

$arResult['SECTION'] = $arParams['SECTION'];
if( intval($_POST['section_id']) > 0 ){
    $arResult['SECTION'] =  tools\section::info($_POST['section_id']);
}

$APPLICATION->AddChainItem($arResult['SECTION']["NAME"], $arResult['SECTION']["SECTION_PAGE_URL"]);

$arResult['IBLOCKS'] = project\catalog::giftIdeasIblocks();

foreach ( $arResult['IBLOCKS'] as $iblock_id => $ar ){
    $fistLevelSect = project\catalog::firstLevelCategory($iblock_id);
    $arResult['IBLOCKS'][$iblock_id]['SECTION'] = $fistLevelSect;
}





$this->IncludeComponentTemplate();