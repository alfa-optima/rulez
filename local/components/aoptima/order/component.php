<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$APPLICATION->AddChainItem('Корзина', '/basket/');
$APPLICATION->AddChainItem('Оформление заказа', false);
$APPLICATION->SetPageProperty("title", 'Оформление заказа');

$arResult['POST'] = $_POST;

$arResult['form_data'] = [];
parse_str($arResult['POST']["form_data"], $arResult['form_data']);

if( $USER->IsAuthorized() ){
    $arResult['USER'] = tools\user::info($USER->GetID());
}

if( $arParams['IS_AJAX'] != 'Y' ){
    if( !$arResult['form_data']['name'] ){
        $arResult['form_data']['name'] = $arResult['USER']['NAME'];
    }
    if( !$arResult['form_data']['phone'] ){
        $arResult['form_data']['phone'] = $arResult['USER']['PERSONAL_PHONE'];
    }
    if( !$arResult['form_data']['email'] ){
        $arResult['form_data']['email'] = $arResult['USER']['EMAIL'];
    }
}

// Варианты доставки
$arResult['DS_LIST'] = project\ds::getList();
// Активный вариант доставки
if( strlen($arResult['form_data']['ds_xml_id']) > 0 ){
    $arResult['ACTIVE_DS_XMLID'] = strip_tags($arResult['form_data']['ds_xml_id']);
} else {
    $arResult['ACTIVE_DS_XMLID'] = project\user_basket::DEFAULT_DELIVERY_XMLID;
}
$arResult['ACTIVE_DS'] = project\ds::getByXMLID( $arResult['ACTIVE_DS_XMLID'] );

$arResult['SAMOVYVOZ_DS_XMLID'] = project\order::SAMOVYVOZ_DS_XMLID;
$arResult['COURIER_DS_XMLID'] = project\order::COURIER_DS_XMLID;
$arResult['COURIER_MINSK_DS_XMLID'] = project\order::COURIER_MINSK_DS_XMLID;
$arResult['POCHTA_DS_XMLID'] = project\order::POCHTA_DS_XMLID;

// Варианты оплаты
$arResult['PS_LIST'] = project\ps::getFilteredtList( $arResult['ACTIVE_DS']['ID'] );

// Группы вариантов оплаты
$arResult['PS_GROUPS'] = project\ps::groups( $arResult['ACTIVE_DS']['ID'], $arResult['PS_LIST'] );
$groups_pss = [];
foreach ( $arResult['PS_GROUPS'] as $group_key => $group ){
    foreach ( $group['PAYMENTS'] as $ps ){   $groups_pss[] = $ps['ID'];   }
}
$groups_pss = array_unique($groups_pss);
$other_pss = [];
foreach ( $arResult['PS_LIST'] as $key => $ps ){
    if( !in_array($ps['ID'], $groups_pss) ){   $other_pss[$ps['ID']] = $ps;   }
}
if( count($other_pss) > 0 ){
    $arResult['PS_GROUPS']['other'] = [
        'ID' => 'other',
        'NAME' => 'Другое',
        'CODE' => 'other',
        'PAYMENTS' => $other_pss
    ];
}



// Определение активной группы
if( intval($arResult['form_data']['ps_id']) > 0 ){
    foreach ( $arResult['PS_GROUPS'] as $ps_group ){
        if( $ps_group['PAYMENTS'][$arResult['form_data']['ps_id']] ){
            $arResult['ACTIVE_PS_GROUP'] = $ps_group['CODE'];
        }
        foreach ( $ps_group['PAYMENTS'] as $key => $ps ){
            if( $ps['ID'] == $arResult['form_data']['ps_id'] ){
                $arResult['ACTIVE_PS'] = project\ps::getByID($arResult['form_data']['ps_id']);
                $_SESSION['ACTIVE_PS_XML_ID'] = $arResult['ACTIVE_PS']['XML_ID'];
            }
        }
    }
}



// Инфо о корзине
$arResult['bInfo'] = project\user_basket::info(
    false,
    $arResult['ACTIVE_DS']['ID'],
    $arResult['ACTIVE_PS']['ID']
);



// Расчёт стоимости доставки
//$arResult['DELIVERY_PRICE'] = project\catalog::getDeliveryPrice($arResult['ACTIVE_DS'], $arResult['bInfo']);
$arResult['DELIVERY_PRICE'] = $arResult['bInfo']['calcInfo']['DELIVERY_PRICE'];
$arResult['DELIVERY_PRICE_FORMAT'] = number_format($arResult['DELIVERY_PRICE'], project\catalog::PRICE_ROUND, ",", " ");


$arResult['PAY_SUM'] = $arResult['bInfo']['calcInfo']['BASKET_DISC_SUM'] + $arResult['DELIVERY_PRICE'];
$arResult['PAY_SUM_FORMAT'] = number_format($arResult['PAY_SUM'], project\catalog::PRICE_ROUND, ",", " ");


//if( $arResult['bInfo']['basket_cnt'] == 0 ){    LocalRedirect( '/basket/' );    }


$arResult['DISABLE_RASSROCHKA_PS'] = false;
$arResult['DISABLE_CREDIT_PS'] = false;
$arResult['DISABLE_YUR_PS'] = false;

$arResult['POPUP_MESSAGE'] = '';
$arResult['POPUP_TITLE'] = 'Внимание!';

// Проверка наличия товаров, недоступных для оформления в рассрочку
$arResult['HAS_NO_RASSROCHKA_PRODUCTS'] = false;
// перебираем корзину
foreach( $arResult['bInfo']['arBasket'] as $key => $bItem ){
    if(
        $bItem->el['PROPERTY_'.project\catalog::RASSROCHKA_PROP_CODE.'_VALUE'] == 'Да'
        &&
        $bItem->el['CATALOG_PRICE_'.project\catalog::RASSROCHKA_PRICE_ID] > 0
    ){
        $arResult['bInfo']['arBasket'][$key]->RASSROCHKA_AVAILABLE = true;
    } else {
        $arResult['bInfo']['arBasket'][$key]->RASSROCHKA_AVAILABLE = false;
        $arResult['HAS_NO_RASSROCHKA_PRODUCTS'] = true;
    }
}

// Проверка наличия товаров, недоступных для оформления в кредит
$arResult['HAS_NO_CREDIT_PRODUCTS'] = false;
// перебираем корзину
foreach( $arResult['bInfo']['arBasket'] as $key => $bItem ){
    if(
        $bItem->el['PROPERTY_'.project\catalog::RASSROCHKA_PROP_CODE.'_VALUE'] == 'Да'
        &&
        $bItem->el['CATALOG_PRICE_'.project\catalog::CREDIT_PRICE_ID] > 0
    ){
        $arResult['bInfo']['arBasket'][$key]->CREDIT_AVAILABLE = true;
    } else {
        $arResult['bInfo']['arBasket'][$key]->CREDIT_AVAILABLE = false;
        $arResult['HAS_NO_CREDIT_PRODUCTS'] = true;
    }
}

// Проверка наличия товаров, недоступных для оформления юридическими лицами
$arResult['HAS_NO_YUR_PRODUCTS'] = false;
// перебираем корзину
foreach( $arResult['bInfo']['arBasket'] as $key => $bItem ){
    // Так попросил клиент (тоже привязка к свойству рассрочки)
    if(
        $bItem->el['PROPERTY_'.project\catalog::RASSROCHKA_PROP_CODE.'_VALUE'] == 'Да'
        &&
        $bItem->el['CATALOG_PRICE_'.project\catalog::RASSROCHKA_PRICE_ID] > 0
    ){
        $arResult['bInfo']['arBasket'][$key]->YUR_AVAILABLE = true;
    } else {
        $arResult['bInfo']['arBasket'][$key]->YUR_AVAILABLE = false;
        $arResult['HAS_NO_YUR_PRODUCTS'] = true;
    }
}

// Если в корзине только 1 товар и он не доступен в рассрочку
// то деактивируем рассрочные варианты оплаты
if(
    $arResult['HAS_NO_RASSROCHKA_PRODUCTS']
    &&
    count($arResult['bInfo']['arBasket']) == 1
){    $arResult['DISABLE_RASSROCHKA_PS'] = true;    }

// Если в корзине только 1 товар и он не доступен в кредит
// то деактивируем кредитные варианты оплаты
if(
    $arResult['HAS_NO_CREDIT_PRODUCTS']
    &&
    count($arResult['bInfo']['arBasket']) == 1
){    $arResult['DISABLE_CREDIT_PS'] = true;    }

// Если в корзине только 1 товар и он не доступен для юр.лиц
// то деактивируем юрид. варианты оплаты
if(
    $arResult['HAS_NO_YUR_PRODUCTS']
    &&
    count($arResult['bInfo']['arBasket']) == 1
){    $arResult['DISABLE_YUR_PS'] = true;    }

// если выбран вариант оплаты рассрочка
if(
    $arParams['IS_AJAX'] == 'Y'
    &&
    $arResult['POST']['isPaymentSelect'] == 'Y'
){
    if( $arResult['ACTIVE_PS']['XML_ID'] == 'rassrochka' ){
        if( $arResult['HAS_NO_RASSROCHKA_PRODUCTS'] ){
            $arResult['POPUP_MESSAGE'] .= "<p>Проверьте список товаров в корзине, возможно какой-то из них не доступен в рассрочку!</p>";
        } else {
            $arResult['POPUP_MESSAGE'] .= "<p>При оплате в рассрочку или картой рассрочки отменяется действующая в корзине скидка!</p>";
        }
    }
    if( $arResult['ACTIVE_PS']['XML_ID'] == 'credit' ){
        if( $arResult['HAS_NO_CREDIT_PRODUCTS'] ){
            $arResult['POPUP_MESSAGE'] .= "<p>Проверьте список товаров в корзине, возможно какой-то из них не доступен в кредит!</p>";
        }
    }
    if( $arResult['ACTIVE_PS']['XML_ID'] == 'yur' ){
        if( $arResult['HAS_NO_YUR_PRODUCTS'] ){
            $arResult['POPUP_MESSAGE'] .= "<p>Проверьте список товаров в корзине, возможно какой-то из них не доступен для оформление юр.лицом!</p>";
        }
    }
}



$order = new project\order();
$arResult['FORM_FIELDS'] = $order->getFormFields();
foreach ( $arResult['FORM_FIELDS'] as $key => $field ){
    // name
    if( $field['CODE'] == 'name' ){
        $field['REQUIRED'] = true;
        $field['HIDE'] = false;
    // phone
    } else if( $field['CODE'] == 'phone' ){
        $field['REQUIRED'] = true;
        $field['HIDE'] = false;
    // email
    } else if( $field['CODE'] == 'email' ){
        $field['REQUIRED'] = true;
        $field['HIDE'] = false;
    // np_name
    } else if( $field['CODE'] == 'np_name' ){
        $field['REQUIRED'] = $arResult['ACTIVE_DS']['XML_ID'] != $arResult['SAMOVYVOZ_DS_XMLID'] && $arResult['ACTIVE_DS']['XML_ID'] != $arResult['COURIER_MINSK_DS_XMLID'];
        $field['HIDE'] = $arResult['ACTIVE_DS']['XML_ID'] == $arResult['SAMOVYVOZ_DS_XMLID'] || $arResult['ACTIVE_DS']['XML_ID'] == $arResult['COURIER_MINSK_DS_XMLID'];
    // index
    } else if( $field['CODE'] == 'index' ){
        $field['REQUIRED'] = $arResult['ACTIVE_DS']['XML_ID']==
            $arResult['POCHTA_DS_XMLID'];
        $field['HIDE'] = $arResult['ACTIVE_DS']['XML_ID'] != $arResult['POCHTA_DS_XMLID'];
    // address
    } else if( $field['CODE'] == 'address' ){
        $field['REQUIRED'] = $arResult['ACTIVE_DS']['XML_ID'] != $arResult['SAMOVYVOZ_DS_XMLID'];
        $field['HIDE'] = $arResult['ACTIVE_DS']['XML_ID'] == $arResult['SAMOVYVOZ_DS_XMLID'];
    // kv
    } else if( $field['CODE'] == 'kv' ){
        $field['REQUIRED'] = false;
        $field['HIDE'] = $arResult['ACTIVE_DS']['XML_ID'] == $arResult['SAMOVYVOZ_DS_XMLID'];
    // floor
    } else if( $field['CODE'] == 'floor' ){
        $field['REQUIRED'] = false;
        $field['HIDE'] = $arResult['ACTIVE_DS']['XML_ID'] == $arResult['SAMOVYVOZ_DS_XMLID'];
    }
    $arResult['FORM_FIELDS'][$key] = $field;
}




// Активный купон
$arResult['ACTIVE_COUPON'] = project\coupon::get_active_coupon()['COUPON'];





$this->IncludeComponentTemplate();