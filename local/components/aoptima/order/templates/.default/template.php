<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project'); use AOptima\Project as project;
\Bitrix\Main\Loader::includeModule('aoptima.tools'); use AOptima\Tools as tools; ?>

<orderBlock>


    <script>
        orderFormFields = <?=CUtil::PhpToJsObject($arResult['FORM_FIELDS'])?>;
    </script>


    <? if( $arResult['bInfo']['basket_cnt'] > 0 ){ ?>

        <form class="order validate-js order___form" onsubmit="order( $(this) ); return false;">

            <div class="order__main">

                <div class="main-heading">
                    <h1 class="main-heading__title">Оформление заказа</h1>
                </div>

                <section class="checkout">

                    <h2 class="checkout__title hide">Личные данные:</h2>

                    <div class="checkout__row">

                        <div class="checkout__col">

                            <h3 class="checkout__subtitle">Личные данные:</h3>

                            <? // Ваше имя
                            $APPLICATION->IncludeComponent(
                                "aoptima:orderFormField", "",
                                [
                                    'FIELD_CODE' => 'name',
                                    'FIELD_VALUE' => $arResult['form_data']['name'],
                                    'ALL_FIELDS' => $arResult['FORM_FIELDS']
                                ]
                            ); ?>

                            <? // Ваш телефон
                            $APPLICATION->IncludeComponent(
                                "aoptima:orderFormField", "",
                                [
                                    'FIELD_CODE' => 'phone',
                                    'FIELD_VALUE' => $arResult['form_data']['phone'],
                                    'ALL_FIELDS' => $arResult['FORM_FIELDS']
                                ]
                            ); ?>

                            <? // Ваш Email
                            $APPLICATION->IncludeComponent(
                                "aoptima:orderFormField", "",
                                [
                                    'FIELD_CODE' => 'email',
                                    'FIELD_VALUE' => $arResult['form_data']['email'],
                                    'ALL_FIELDS' => $arResult['FORM_FIELDS']
                                ]
                            ); ?>

                        </div>

                        <div class="checkout__col">

                            <h3 class="checkout__subtitle">Доставка:</h3>

                            <div class="radiocheck-text-group radiocheck-text-group_three">

                                <? foreach( $arResult['DS_LIST'] as $ds ){ ?>

                                    <label class="radiocheck-text">
                                        <input type="radio" name="ds_xml_id" value="<?=$ds['XML_ID']?>" <? if( $ds['XML_ID'] == $arResult['ACTIVE_DS']['XML_ID'] ){ echo 'checked'; } ?>>
                                        <span><?=$ds['NAME']?></span>
                                    </label>

                                <? } ?>

                            </div>

                            <? // Населённый пункт
                            $APPLICATION->IncludeComponent(
                                "aoptima:orderFormField", "",
                                [
                                    'FIELD_CODE' => 'np_name',
                                    'FIELD_VALUE' => $arResult['form_data']['np_name'],
                                    'ALL_FIELDS' => $arResult['FORM_FIELDS']
                                ]
                            ); ?>

                            <? // Почтовый индекс
                            $APPLICATION->IncludeComponent(
                                "aoptima:orderFormField", "",
                                [
                                    'FIELD_CODE' => 'index',
                                    'FIELD_VALUE' => $arResult['form_data']['index'],
                                    'ALL_FIELDS' => $arResult['FORM_FIELDS']
                                ]
                            ); ?>

                            <? // Улица, дом, корпус
                            $APPLICATION->IncludeComponent(
                                "aoptima:orderFormField", "",
                                [
                                    'FIELD_CODE' => 'address',
                                    'FIELD_VALUE' => $arResult['form_data']['address'],
                                    'ALL_FIELDS' => $arResult['FORM_FIELDS']
                                ]
                            ); ?>

                            <div class="form-row" <? if( $arResult['ACTIVE_DS']['XML_ID'] == $arResult['SAMOVYVOZ_DS_XMLID'] ){ ?>style="display:none;"<? } ?>>

                                <? // Квартира
                                $APPLICATION->IncludeComponent(
                                    "aoptima:orderFormField", "",
                                    [
                                        'FIELD_CODE' => 'kv',
                                        'FIELD_VALUE' => $arResult['form_data']['kv'],
                                        'ALL_FIELDS' => $arResult['FORM_FIELDS']
                                    ]
                                ); ?>

                                <? // Этаж
                                $APPLICATION->IncludeComponent(
                                    "aoptima:orderFormField", "",
                                    [
                                        'FIELD_CODE' => 'floor',
                                        'FIELD_VALUE' => $arResult['form_data']['floor'],
                                        'ALL_FIELDS' => $arResult['FORM_FIELDS']
                                    ]
                                ); ?>

                            </div>

                        </div>
                    </div>
                </section>

                <section class="payment">
                    <h2 class="payment__title">Оплата:</h2>
                    <div class="rolls payment__methods rolls-js">

                        <? foreach( $arResult['PS_GROUPS'] as $ps_group ){ ?>

                            <div class="rolls__item rolls__item-js">

                                <div class="rolls__header rolls__header-js rolls__hand-js <? if( $ps_group['CODE'] == $arResult['ACTIVE_PS_GROUP'] || count($arResult['PS_GROUPS']) == 1 ){  echo 'rolls-active';  } ?>">
                                    <span><?=$ps_group['NAME']?></span>
                                    <em class="rolls__angle">&#9660;</em>
                                </div>

                                <div class="rolls__panel typography rolls__panel-js <? if( $ps_group['CODE'] == $arResult['ACTIVE_PS_GROUP'] || count($arResult['PS_GROUPS']) == 1 ){  echo 'rolls-active';  } ?>">

                                    <? if( count($ps_group['PAYMENTS']) > 0 ){ ?>

                                        <div class="radiocheck-img-group radiocheck-img-group_two">

    <? foreach( $ps_group['PAYMENTS'] as $ps ){ ?>

        <label class="radiocheck-img">
            <input <?
            if(
                ($arResult['DISABLE_RASSROCHKA_PS'] && $ps['XML_ID'] == 'rassrochka')
                ||
                ($arResult['DISABLE_CREDIT_PS'] && $ps['XML_ID'] == 'credit')
                ||
                ($arResult['DISABLE_YUR_PS'] && $ps['XML_ID'] == 'yur')
            ){  echo 'disabled';  } ?> type="radio" name="ps_id" value="<?=$ps['ID']?>" <?
            if(
                (
                    $ps['ID'] == $arResult['form_data']['ps_id']
                    ||
                    (
                        count($arResult['PS_GROUPS']) == 1
                        &&
                        count($ps_group['PAYMENTS']) == 1
                    )
                )
                &&
                !( $arResult['DISABLE_RASSROCHKA_PS'] && $ps['XML_ID'] == 'rassrochka' )
                &&
                !( $arResult['DISABLE_CREDIT_PS'] && $ps['XML_ID'] == 'credit' )
                &&
                !( $arResult['DISABLE_YUR_PS'] && $ps['XML_ID'] == 'yur' )
            ){    echo 'checked';    } ?>>

            <em <? if( $arResult['DISABLE_RASSROCHKA_PS'] && $ps['XML_ID'] == 'rassrochka' ){
                echo 'style="background-color: #dbdbdb; color: #ffffff;" title="Для выбранных товаров не доступно оформление в рассрочку"';
            } else if( $arResult['DISABLE_CREDIT_PS'] && $ps['XML_ID'] == 'credit' ){
                echo 'style="background-color: #dbdbdb; color: #ffffff;" title="Для выбранных товаров не доступно оформление в кредит"';
            } else if( $arResult['DISABLE_YUR_PS'] && $ps['XML_ID'] == 'yur' ){
                echo 'style="background-color: #dbdbdb; color: #ffffff;" title="Для выбранных товаров не доступно оформление юр.лицом"';
            } ?>>
                <? if( intval($ps['PSA_LOGOTIP']) > 0 && 0 ){ ?>
                    <img src="<?=tools\funcs::rIMGG($ps['PSA_LOGOTIP'], 4, 300, 34)?>" alt="<?=$ps['PSA_NAME']?>" />
                <? } else { ?>
                    <?=$ps['PSA_NAME']?>
                <? } ?>
            </em>

            <span><?=$ps['PSA_NAME']?></span>
        </label>

    <? } ?>

                                        </div>

                                    <? } ?>

                                    <? if( strlen($ps_group['DESCRIPTION']) > 0 ){ ?>
                                        <p style="margin-top:20px;"><?=$ps_group['DESCRIPTION']?></p>
                                   <? } ?>

                                </div>

                            </div>

                        <? } ?>

                    </div>
                </section>

            </div>

            <div class="clearance order__aside">
                <div class="clearance__box">
                    <div class="clearance__meta">

                        <? if( strlen($arResult['ACTIVE_COUPON']) > 0 ){ ?>
                            <div class="clearance__caption">
                                <div class="clearance__caption-icon">
                                    <i class="icon-free"></i>
                                </div>
                                <div class="clearance__caption-text">Применён купон: <?=$arResult['ACTIVE_COUPON']?></div>
                            </div>
                        <? } ?>

<div class="consist clearance__consist">
    <h3 class="consist__title">Состав заказа:</h3>
    <div class="consist__overflow">
        <div class="consist__list">

            <? foreach( $arResult['bInfo']['arBasket'] as $key => $bItem ){ ?>

                <div
                    <? if(
                        $bItem->el['IBLOCK_ID'] != project\complect::IBLOCK_ID
                    ){ ?>href="<?=$bItem->el['DETAIL_PAGE_URL']?>"<? } ?>
                    target="_blank"
                    class="consist__item order___b_item <?
                    if(
                        (
                            $arResult['ACTIVE_PS']['XML_ID'] == 'rassrochka'
                            &&
                            !$bItem->RASSROCHKA_AVAILABLE
                        )
                        ||
                        (
                            $arResult['ACTIVE_PS']['XML_ID'] == 'credit'
                            &&
                            !$bItem->CREDIT_AVAILABLE
                        )
                        ||
                        (
                            $arResult['ACTIVE_PS']['XML_ID'] == 'yur'
                            &&
                            !$bItem->YUR_AVAILABLE
                        )
                    ){
                        echo ' consist__item_no-available';
                    } ?>"
                    <? if(
                        $arResult['ACTIVE_PS']['XML_ID'] == 'rassrochka'
                        &&
                        !$bItem->RASSROCHKA_AVAILABLE
                    ){
                        echo 'title="Рассрочка не доступна"';
                    } else if(
                        $arResult['ACTIVE_PS']['XML_ID'] == 'credit'
                        &&
                        !$bItem->CREDIT_AVAILABLE
                    ){
                        echo 'title="Кредит не доступен"';
                    } else if(
                        $arResult['ACTIVE_PS']['XML_ID'] == 'yur'
                        &&
                        !$bItem->YUR_AVAILABLE
                    ){
                        echo 'title="Для юр.лиц не доступен"';
                    } ?>
                >
                    <div class="consist__item-figure">
                        <? if( intval($bItem->el['DETAIL_PICTURE']) > 0 ){ ?>
                            <img src="<?=tools\funcs::rIMGG($bItem->el['DETAIL_PICTURE'], 4, 100, 66)?>">
                        <? } else { ?>
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/no-photo.png"/>
                        <? } ?>
                    </div>
                    <div class="consist__item-content">
                        <strong class="consist__item-title"><?=$bItem->el['NAME']?></strong>
                        <div class="consist__item-meta">
                            <div class="consist__item-count"><?=$bItem->getQuantity()?> шт</div>
                            <div class="consist__item-price">

                                <?php if( isset($bItem->oldSum) ){ ?>

                                    <div class="consist__item-price-old">
                                        <?=$bItem->oldSumFormat?> р.
                                    </div>
                                    <div class="consist__item-price-new">
                                        <?=$bItem->discSumFormat?> р.
                                    </div>

                                <?php } else {

                                    echo $bItem->discSumFormat." р.";

                                } ?>

                            </div>
                        </div>

                        <?php if( isset($bItem->oldSum) ){ ?>
                            <div class="consist__item-note">Спецпредложение</div>
                        <? } ?>

                    </div>

                    <? if(
                        $arResult['ACTIVE_PS']['XML_ID'] == 'rassrochka'
                        &&
                        !$bItem->RASSROCHKA_AVAILABLE
                    ){
                        echo '<div class="consist__item-warning">Рассрочка не доступна</div>';
                    } else if(
                        $arResult['ACTIVE_PS']['XML_ID'] == 'credit'
                        &&
                        !$bItem->CREDIT_AVAILABLE
                    ){
                        echo '<div class="consist__item-warning">Кредит не доступен</div>';
                    } else if(
                        $arResult['ACTIVE_PS']['XML_ID'] == 'yur'
                        &&
                        !$bItem->YUR_AVAILABLE
                    ){
                        echo '<div class="consist__item-warning">Для юр.лиц не доступен</div>';
                    } ?>

                    <div class="consist__item-remove orderBasketItemRemove to___process" data-itemid="<?=$arResult['bInfo']['basket'][$key]->getID()?>">
                        <em>x</em>
                        <span>Удалить из корзины</span>
                    </div>

                </div>

            <? } ?>

        </div>
    </div>
</div>

                        <div class="clearance__divider"></div>
                        <div class="delivery clearance__delivery">

                            <? if( intval($arResult['ACTIVE_DS']['ID']) > 0 ){ ?>
                                <div class="delivery-item">
                                    <div class="delivery__icon">
                                        <i class="icon-helper-delivery-color"></i>
                                    </div>
                                    <div class="delivery__text"><strong><?=$arResult['ACTIVE_DS']['NAME']?></strong> <mark><?=$arResult['DELIVERY_PRICE']>0?($arResult['DELIVERY_PRICE_FORMAT'].' руб.'):'бесплатно'?></mark></div>
                                </div>
                            <? } ?>

                            <? if( intval($arResult['ACTIVE_PS']['ID']) > 0 ){ ?>
                                <div class="delivery-item">
                                    <div class="delivery__icon">
                                        <i class="icon-cards"></i>
                                    </div>
                                    <div class="delivery__text"><strong><?=$arResult['ACTIVE_PS']['PSA_NAME']?></strong></div>
                                </div>
                            <? } ?>

                        </div>
                        <div class="clearance__divider"></div>
                    </div>

                    <div class="clearance__main">

                        <? if( $arResult['bInfo']['calcInfo']['BASKET_DISC_SUM'] > 0 ){ ?>

                            <div class="summary clearance__summary">
                                <div class="summary__item">
                                    <div>Товары<!--(<?=$arResult['bInfo']['basket_cnt']?>)-->:</div>
                                    <div><?=$arResult['bInfo']['calcInfo']['BASKET_DISC_SUM_FORMAT']?> р.</div>
                                </div>
                                <div class="summary__item">
                                    <div>Доставка:</div>
                                    <div><?=$arResult['DELIVERY_PRICE_FORMAT']?> р.</div>
                                </div>
                                <div class="summary__total">
                                    <div>Итого:</div>
                                    <div><?=$arResult['PAY_SUM_FORMAT']?> р.</div>
                                </div>
                            </div>

                        <? } ?>

                        <div class="clearance__box">
                            <div class="comment-form clearance__comment">
                                <label for="comment-order">Комментарий к заказу:</label>
                                <textarea name="comment" class="single-form__input field-js" id="comment-order" placeholder="Текст сообщения"><?=$arResult['form_data']['comment']?></textarea>
                            </div>
                        </div>

                        <p class="error___p"></p>

                        <div class="buttons clearance__buttons">
                            <div class="buttons__item">
                                <button class="btn-def order___button to___process" <? if( $arParams['IS_AJAX'] != 'Y' ){ echo 'disabled'; } ?>>Оформить заказ</button>
                            </div>
                        </div>

                    </div>
                </div>



            </div>

        </form>


        <? // bottom_advantages
        $APPLICATION->IncludeComponent(
            "bitrix:news.list", "bottom_advantages",
            array(
                "COMPONENT_TEMPLATE" => "bottom_advantages",
                "IBLOCK_TYPE" => "content",
                "IBLOCK_ID" => "53",
                "NEWS_COUNT" => "4",
                "SORT_BY1" => "SORT",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "NAME",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "",
                "FIELD_CODE" => array(),
                "PROPERTY_CODE" => array('ICON_CLASS'),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "Y",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_BROWSER_TITLE" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "INCLUDE_SUBSECTIONS" => "Y",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "PAGER_TEMPLATE" => ".default",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_ADDITIONAL" => "undefined",
                "SET_LAST_MODIFIED" => "N",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "SHOW_404" => "N",
                "MESSAGE_404" => ""
            ), false
        ); ?>

        <div class="loader-cover">&nbsp;</div>


    <? } else { ?>


        <div class="cart cart-js">
            <div class="cart__base">
                <div class="cart__main">

                    <!--cart heading-->
                    <div class="main-heading">
                        <h1 class="main-heading__title">Оформление заказа</h1>
                    </div>
                    <!--cart heading end-->

                    <div class="cart__empty" style="display: block;">В корзине нет товаров</div>

                </div>
            </div>
        </div>


    <? } ?>


    <?php if( strlen($arResult['POPUP_MESSAGE']) > 0 ){ ?>

        <script>
        var modalHTML = '';
        <?php if( strlen($arResult['POPUP_TITLE']) > 0 ){ ?>
            modalHTML += '<div class="modal-title h2"><div class="modal-title__inner"><?=$arResult['POPUP_TITLE']?></div></div>';
        <? } ?>
        modalHTML += '<?=$arResult['POPUP_MESSAGE']?>';
        $('#orderModal').html(modalHTML);
        $.fancybox.open({ src  : '#orderModal', type : 'inline' });
        </script>

    <?php } ?>


</orderBlock>