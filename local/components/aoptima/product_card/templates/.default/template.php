<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>



<? echo $arResult['HTML']; ?>



<? if( \CModule::IncludeModule("arturgolubev.ecommerce") ){
    $APPLICATION->IncludeComponent(
        "arturgolubev:ecommerce.detail", ".default",
        array(
            "COMPONENT_TEMPLATE" => ".default",
            "OFFERS_CART_PROPERTIES" => $arParams['OFFERS_CART_PROPERTIES'],
            "PRODUCT_ID" => $arResult['el']['ID'],
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "360000"
        ),
        $arResult['component']
    );
} ?>



<?php if( $arResult['IS_COMPLECT'] != 'Y' ){ ?>

    <iframe id="productReviewAnswerFrame" name="productReviewAnswerFrame" style="display: none"></iframe>
    <iframe id="productReviewFrame" name="productReviewFrame" style="display: none"></iframe>

<?php } ?>



<? // product_articles
$APPLICATION->IncludeComponent(
    "aoptima:product_articles", "", array( 'product' => $arResult['el'] )
); ?>



<input type="hidden" name="product_id" value="<?=$arResult['el']['ID']?>">

