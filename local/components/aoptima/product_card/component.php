<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('iblock');

$arResult['component'] = $component;

$arResult['fullURL'] = $_SERVER['REQUEST_URI'];
$arResult['pureURL'] = tools\funcs::pureURL();
$arResult['arURI'] = tools\funcs::arURI( $arResult['pureURL'] );

$arResult['product_code'] = strip_tags($arResult['arURI'][2]);

if(
    $arResult['arURI'][1] == 'product'
    &&
    strlen($arResult['product_code']) > 0
){

    // каталожные инфоблоки
    $arResult['catalogIblocks'] = project\catalog::iblocks();

    $arResult['el'] = false;   $arResult['product_iblock'] = false;
    foreach ( $arResult['catalogIblocks'] as $key => $iblock ){
        if( !$arResult['el'] ){
            $arResult['el'] = tools\el::info_by_code($arResult['product_code'], $iblock['ID']);
            if( intval($arResult['el']['ID']) > 0 ){   $arResult['product_iblock'] = $iblock;   }
        }
    }

    if(
        intval($arResult['el']['ID']) > 0
        &&
        $arResult['el']['DETAIL_PAGE_URL'] == $arResult['pureURL']
    ){

        $arResult['IS_COMPLECT'] = $arResult['el']['IBLOCK_ID']==project\complect::IBLOCK_ID?'Y':'N';

        $arResult['ipropValues'] = new \Bitrix\Iblock\InheritedProperty\ElementValues( $arResult['el']["IBLOCK_ID"], $arResult['el']["ID"] );
        $arResult['meta_tags'] = $arResult['ipropValues']->getValues();

        $APPLICATION->SetPageProperty("title", $arResult['meta_tags']["ELEMENT_META_TITLE"]?$arResult['meta_tags']["ELEMENT_META_TITLE"]:$arResult['el']["NAME"]);
        $APPLICATION->SetPageProperty("description", $arResult['meta_tags']["ELEMENT_META_DESCRIPTION"]?$arResult['meta_tags']["ELEMENT_META_DESCRIPTION"]:$arResult['el']["NAME"]);
        $APPLICATION->SetPageProperty("keywords", $arResult['meta_tags']["ELEMENT_META_KEYWORDS"]?$arResult['meta_tags']["ELEMENT_META_KEYWORDS"]:$arResult['el']["NAME"]);

        // Раздел товара
        $arResult['sect_id'] = tools\el::sections($arResult['el']["ID"])[0]['ID'];
        $arResult['section'] = tools\section::info($arResult['sect_id']);

        // Цепочка разделов
        $arResult['sect_chain'] = tools\section::chain($arResult['section']['ID']);
        foreach ( $arResult['sect_chain'] as $key => $sect ){
            $APPLICATION->AddChainItem($sect["NAME"], $sect["SECTION_PAGE_URL"]);
        }
        $APPLICATION->AddChainItem($arResult['el']["NAME"], $arResult['el']["DETAIL_PAGE_URL"]);


        if( $arResult['el']['IBLOCK_ID'] != project\complect::IBLOCK_ID ){

            // Отзывы к товару
            $arResult['reviews_cnt'] = count( project\product_review::getList($arResult['el']['ID']) );

        }

        $cacheTime = $arResult['el']['IBLOCK_ID']==project\complect::IBLOCK_ID?project\catalog::COMPLECTS_CACHE_TIME:project\catalog::CACHE_TIME;
        $cacheType = $arResult['el']['IBLOCK_ID']==project\complect::IBLOCK_ID?project\catalog::COMPLECTS_CACHE_TYPE:project\catalog::CACHE_TYPE;

        //echo "<pre>"; print_r( project\credit_program::getTree() ); echo "</pre>";

        ob_start();
            $APPLICATION->IncludeComponent(
                "aoptima:product_articles_and_actions", "",
                array( 'product' => $arResult['el'] )
            );
        	$product_articles_and_actions = ob_get_contents();
        ob_end_clean();

        $arResult['componentElementParams'] = Array(
            'DELIVERY_RB_PLUS_DAYS' => \Bitrix\Main\Config\Option::get('aoptima.project', 'DELIVERY_RB_PLUS_DAYS'),
            'version' => 2,
            'IS_AUTH' => $USER->IsAuthorized()?'Y':'N',
            'reviews_cnt' => $arResult['reviews_cnt'],
            'prop_groups' => project\catalog::getPropGroups($arResult['el']),
            'stopProps' => project\catalog::detailCardStopProps(),
            'product_rating' => (new project\product_rating($arResult['el']['ID']))->get(),
            'product_photos' => '' /*project\catalog::product_photo_block($arResult['el'])*/,
            'product_services' => project\catalog::product_services_block($arResult['el']),
            'credit_sections' => project\credit_program::getTree(),
            'rassrochkaMonths' => project\calculator::rassrochkaMonths(),
            'product_articles_and_actions' => $product_articles_and_actions,
            'IBLOCK_TYPE' => project\catalog::IBLOCK_TYPE,
            'IBLOCK_ID' => $arResult['product_iblock']['ID'],
            "ACTION_VARIABLE" => "action",
            "ADD_DETAIL_TO_SLIDER" => "N",
            "ADD_ELEMENT_CHAIN" => "N",
            "ADD_PICT_PROP" => "-",
            "ADD_PROPERTIES_TO_BASKET" => "Y",
            "ADD_SECTIONS_CHAIN" => "N",
            "ADD_TO_BASKET_ACTION" => array("BUY"),
            "BACKGROUND_IMAGE" => "-",
            "BASKET_URL" => "/personal/basket.php",
            "BRAND_USE" => "N",
            "BROWSER_TITLE" => "-",
            "CACHE_GROUPS" => "Y",
            'CACHE_TYPE' => $cacheType,
            'CACHE_TIME' => $cacheTime,
            "CHECK_SECTION_ID_VARIABLE" => "N",
            "CONVERT_CURRENCY" => "N",
            "DETAIL_PICTURE_MODE" => "IMG",
            "DETAIL_URL" => "",
            "DISABLE_INIT_JS_IN_COMPONENT" => "N",
            "DISPLAY_COMPARE" => "N",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PREVIEW_TEXT_MODE" => "E",
            "ELEMENT_CODE" => "",
            "ELEMENT_ID" => $arResult['el']['ID'],
            "GIFTS_DETAIL_BLOCK_TITLE" => "Выберите один из подарков",
            "GIFTS_DETAIL_HIDE_BLOCK_TITLE" => "N",
            "GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => "3",
            "GIFTS_DETAIL_TEXT_LABEL_GIFT" => "Подарок",
            "GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => "Выберите один из товаров, чтобы получить подарок",
            "GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE" => "N",
            "GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => "3",
            "GIFTS_MESS_BTN_BUY" => "Выбрать",
            "GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
            "GIFTS_SHOW_IMAGE" => "Y",
            "GIFTS_SHOW_NAME" => "Y",
            "GIFTS_SHOW_OLD_PRICE" => "Y",
            "HIDE_NOT_AVAILABLE" => "N",
            "LABEL_PROP" => "-",
            "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
            "LINK_IBLOCK_ID" => "",
            "LINK_IBLOCK_TYPE" => "",
            "LINK_PROPERTY_SID" => "",
            "MESSAGE_404" => "",
            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
            "MESS_BTN_BUY" => "Купить",
            "MESS_BTN_COMPARE" => "Сравнить",
            "MESS_BTN_SUBSCRIBE" => "Подписаться",
            "MESS_NOT_AVAILABLE" => "Нет в наличии",
            "META_DESCRIPTION" => "-",
            "META_KEYWORDS" => "-",
            "OFFERS_LIMIT" => "0",
            "PARTIAL_PRODUCT_PROPERTIES" => "N",
            "PRICE_CODE" => array(
                project\catalog::RASSROCHKA_PRICE_CODE,
                project\catalog::BASE_PRICE_CODE,
                project\catalog::CREDIT_PRICE_CODE,
            ),
            "PRICE_VAT_INCLUDE" => "Y",
            "PRICE_VAT_SHOW_VALUE" => "N",
            "PRODUCT_ID_VARIABLE" => "id",
            "PRODUCT_PROPERTIES" => array(),
            "PRODUCT_PROPS_VARIABLE" => "prop",
            "PRODUCT_QUANTITY_VARIABLE" => "",
            "PRODUCT_SUBSCRIPTION" => "N",
            "PROPERTY_CODE" => [],
            "SECTION_ID_VARIABLE" => "SECTION_ID",
            "SECTION_URL" => "",
            "SEF_MODE" => "N",
            "SET_BROWSER_TITLE" => "N",
            "SET_CANONICAL_URL" => "N",
            "SET_LAST_MODIFIED" => "N",
            "SET_META_DESCRIPTION" => "N",
            "SET_META_KEYWORDS" => "N",
            "SET_STATUS_404" => "N",
            "SET_TITLE" => "N",
            "SET_VIEWED_IN_COMPONENT" => "N",
            "SHOW_404" => "N",
            "SHOW_CLOSE_POPUP" => "N",
            "SHOW_DEACTIVATED" => "N",
            "SHOW_DISCOUNT_PERCENT" => "N",
            "SHOW_MAX_QUANTITY" => "N",
            "SHOW_OLD_PRICE" => "N",
            "SHOW_PRICE_COUNT" => "1",
            "TEMPLATE_THEME" => "blue",
            "USE_COMMENTS" => "N",
            "USE_ELEMENT_COUNTER" => "Y",
            "USE_GIFTS_DETAIL" => "Y",
            "USE_GIFTS_MAIN_PR_SECTION_LIST" => "Y",
            "USE_MAIN_ELEMENT_SECTION" => "N",
            "USE_PRICE_COUNT" => "N",
            "USE_PRODUCT_QUANTITY" => "N",
            "USE_VOTE_RATING" => "N"
        );

        if( $arResult['el']['IBLOCK_ID'] == project\complect::IBLOCK_ID ){

            // Товары комплекта

            $arResult['COMPLECT']['PRODUCTS_IDS'] = project\complect::getProducts($arResult['el']['ID']);
            $arResult['COMPLECT']['PRODUCTS_SUM'] = 0;
            foreach ( $arResult['COMPLECT']['PRODUCTS_IDS'] as $id ){
                $element = tools\el::info( $id );
                $arResult['COMPLECT']['PRODUCTS'][$id] = $element;
                $arResult['COMPLECT']['PRODUCTS_SUM'] += $element['CATALOG_PRICE_'.project\catalog::BASE_PRICE_ID];
            }

            $arResult['componentElementParams']['COMPLECT'] = $arResult['COMPLECT'];
        }

        // Блок отзывов
        ob_start();
            $APPLICATION->IncludeComponent(
                "aoptima:productReviewBlock", "",
                [ 'el' => $arResult['el'], 'reviews_cnt' => $arResult['reviews_cnt'] ]
            );
            $arResult['componentElementParams']['productReviewBlock'] = ob_get_contents();
        ob_end_clean();



        $template = $arResult['el']['IBLOCK_ID']==project\complect::IBLOCK_ID?'complect_card':'product_card';

        ob_start();
            $APPLICATION->IncludeComponent(
                'bitrix:catalog.element', $template,
                $arResult['componentElementParams'], $arResult['component']
            );
            $arResult['HTML'] = ob_get_contents();
        ob_end_clean();



        $this->IncludeComponentTemplate();



    } else {

        include($_SERVER["DOCUMENT_ROOT"]."/local/templates/main/include/404include.php");
    }

} else {

    include($_SERVER["DOCUMENT_ROOT"]."/local/templates/main/include/404include.php");
}