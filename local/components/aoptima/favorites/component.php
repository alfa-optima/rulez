<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project'); use AOptima\Project as project;
\Bitrix\Main\Loader::includeModule('aoptima.tools'); use AOptima\Tools as tools;


$arResult['favorites'] = project\favorites::checkCookieIDS();


$arResult['FIRST_LEVEL_SECTIONS'] = [];
if( count($arResult['favorites']) > 0 ){

    foreach ( $arResult['favorites'] as $el_id ){
        $iblock_id = tools\el::getIblock($el_id);
        $firstLevelCategory = project\catalog::firstLevelCategory($iblock_id);
        $arResult['FIRST_LEVEL_SECTIONS'][$firstLevelCategory['ID']] = $firstLevelCategory;
    }

    $arResult['section'] =  $arResult['FIRST_LEVEL_SECTIONS'][array_keys($arResult['FIRST_LEVEL_SECTIONS'])[0]];
}


if( intval($_POST['section_id']) > 0 ){
    $arResult['section'] =  tools\section::info($_POST['section_id']);
}



$this->IncludeComponentTemplate();