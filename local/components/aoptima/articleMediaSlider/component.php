<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

$arResult['el'] = $arParams['el'];

$arResult['MEDIA_ITEMS'] = [];

$start_media_key = 1;
$media_key = false;

if( is_array( $arResult['el']['PROPERTY_OBZ_PHOTOS_VALUE'] ) ){
    foreach ( $arResult['el']['PROPERTY_OBZ_PHOTOS_VALUE'] as $photo_id ){
        if( intval($photo_id) > 0 ){
            if( intval($media_key) > 0 ){  $media_key += 2;  } else {  $media_key = $start_media_key;  }
            $arResult['MEDIA_ITEMS'][$media_key] = [
                'TYPE' => 'PICTURE',
                'PICTURE_ID' => $photo_id
            ];
        }
    }
}

$start_media_key = 2;
$media_key = false;

if(
    is_array($arResult['el']['PROPERTY_OBZ_VIDEOS_VALUE'])
    &&
    count( $arResult['el']['PROPERTY_OBZ_VIDEOS_VALUE'] ) > 0
){
    foreach ( $arResult['el']['PROPERTY_OBZ_VIDEOS_VALUE'] as $key => $item ){
        if( strlen(trim($item)) > 0 ){
            if( intval($media_key) > 0 ){  $media_key += 2;  } else {  $media_key = $start_media_key;  }
            $arResult['MEDIA_ITEMS'][$media_key] = [
                'TYPE' => 'YOUTUBE_VIDEO',
                'YOUTUBE_VIDEO_ID' => trim($item)
            ];
        }
    }
}

if( count($arResult['MEDIA_ITEMS']) > 0 ){
    ksort($arResult['MEDIA_ITEMS']);
}






$this->IncludeComponentTemplate();