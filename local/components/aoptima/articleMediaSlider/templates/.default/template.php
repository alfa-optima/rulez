<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools; ?>

<? if( count($arResult["MEDIA_ITEMS"]) > 0 ){ ?>

    <div class="swiper-container images-gallery images-gallery-js">
        <div class="swiper-wrapper">

            <? foreach( $arResult["MEDIA_ITEMS"] as $key => $item ){ ?>

                <? if( $item['TYPE'] == 'PICTURE' ){ ?>

                    <div class="swiper-slide">
                        <img src="<?=tools\funcs::rIMGG($item['PICTURE_ID'], 5, 1364, 936)?>" />
                    </div>

                <? } else if( $item['TYPE'] == 'YOUTUBE_VIDEO' ){ ?>

                    <div class="swiper-slide images-gallery__slide">
                        <div class="images-gallery__video">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/<?=$item['YOUTUBE_VIDEO_ID']?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>

                <? } ?>

            <? } ?>

        </div>
        <div class="swiper-pagination"></div>
    </div>

<? } ?>
