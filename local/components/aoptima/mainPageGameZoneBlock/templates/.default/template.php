<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if( intval($arResult['SECTION']['ID']) > 0 ){ ?>

    <section class="main-section main-section_game-zone">

        <div class="mark-anchor" id="gameZone">&nbsp;</div>

        <div class="gz layout max-wrap">

            <div class="gz-caption">
                <h2 class="gz-caption__title">Game Zone</h2>

                <? if( count($arResult['SUBSECTIONS_2']) > 0 ){ ?>

                    <div class="gz__options">
                        <div class="gz__links">
                            <? foreach( $arResult['SUBSECTIONS_2'] as $key => $sub_sect ){ ?>
                                <a href="<?=$sub_sect['SECTION_PAGE_URL']?>"><span><?=$sub_sect['NAME']?></span></a>
                            <? } ?>
                        </div>
                    </div>

                <? } ?>

            </div>

            <div class="gz__entry">

                <? $APPLICATION->IncludeFile( SITE_TEMPLATE_PATH."/inc/main_game_zone.inc.php", Array(), Array("MODE"=>"html") ); ?>


            </div>

            <div class="gz-tiles">
                <div class="gz-tiles__list grid-js">

<? $cnt = 0;
foreach( $arResult['SUBSECTIONS_1'] as $key => $sub_sect ){ $cnt++;
    if( $cnt == 1 ){ ?>

        <div class="gz-tiles__item grid-item-js" style="width: 25.6%;">
            <a href="<?=$sub_sect['SECTION_PAGE_URL']?>" class="gz-tiles-elem" data-align="top left" style="padding-top: 101.94%;">
                <? if( intval($sub_sect['PICTURE']) > 0 ){ ?>
                    <div class="gz-tiles-elem__bg lozad" data-background-image="<?=tools\funcs::rIMGG($sub_sect['PICTURE'], 5, 620, 632)?>">&nbsp;</div>
                <? } ?>
                <strong class="gz-tiles-elem__title"><span><?=$sub_sect['NAME']?></span></strong>
            </a>
        </div>

    <? } else if( $cnt == 2 ){ ?>

        <div class="gz-tiles__item grid-item-js" style="width: 24.4%;">
            <a href="<?=$sub_sect['SECTION_PAGE_URL']?>" class="gz-tiles-elem" data-align="top left" style="padding-top: 70.03%;">
                <? if( intval($sub_sect['PICTURE']) > 0 ){ ?>
                    <div class="gz-tiles-elem__bg lozad" data-background-image="<?=tools\funcs::rIMGG($sub_sect['PICTURE'], 5, 566, 436)?>">&nbsp;</div>
                <? } ?>
                <strong class="gz-tiles-elem__title"><span><?=$sub_sect['NAME']?></span></strong>
            </a>
        </div>

    <? } else if( $cnt == 3 ){ ?>

        <div class="gz-tiles__item grid-item-js" style="width: 24.3%;">
            <a href="<?=$sub_sect['SECTION_PAGE_URL']?>" class="gz-tiles-elem" data-align="top left" style="padding-top: 108%;">
                <div class="gz-tiles-elem__bg lozad" data-background-image="<?=tools\funcs::rIMGG($sub_sect['PICTURE'], 5, 578, 634)?>">&nbsp;</div>
                <strong class="gz-tiles-elem__title"><span><?=$sub_sect['NAME']?></span></strong>
            </a>
        </div>

    <? } else if( $cnt == 4 ){ ?>

        <div class="gz-tiles__item grid-item-js" style="width: 25.7%;">
            <a href="<?=$sub_sect['SECTION_PAGE_URL']?>" class="gz-tiles-elem" data-align="top right" style="padding-top: 101.8%;">
                <div class="gz-tiles-elem__bg lozad" data-background-image="<?=tools\funcs::rIMGG($sub_sect['PICTURE'], 5, 620, 642)?>">&nbsp;</div>
                <strong class="gz-tiles-elem__title"><span><?=$sub_sect['NAME']?></span></strong>
            </a>
        </div>

    <? } else if( $cnt == 5 ){ ?>

        <div class="gz-tiles__item grid-item-js" style="width: 24.4%;">
            <a href="<?=$sub_sect['SECTION_PAGE_URL']?>" class="gz-tiles-elem" data-align="top right" style="padding-top: 85.36%;">
                <div class="gz-tiles-elem__bg lozad" data-background-image="<?=tools\funcs::rIMGG($sub_sect['PICTURE'], 5, 566, 482)?>">&nbsp;</div>
                <strong class="gz-tiles-elem__title"><span><?=$sub_sect['NAME']?></span></strong>
            </a>
        </div>

    <? } else if( $cnt == 6 ){ ?>

        <div class="gz-tiles__item grid-item-js" style="width: 25.6%;">
            <a href="<?=$sub_sect['SECTION_PAGE_URL']?>" class="gz-tiles-elem" data-align="middle left" style="padding-top: 45.81%;">
                <div class="gz-tiles-elem__bg lozad" data-background-image="<?=tools\funcs::rIMGG($sub_sect['PICTURE'], 5, 620, 284)?>">&nbsp;</div>
                <strong class="gz-tiles-elem__title"><span><?=$sub_sect['NAME']?></span></strong>
            </a>
        </div>

    <? } else if( $cnt == 7 ){ ?>

        <div class="gz-tiles__item grid-item-js" style="width: 50.0%;">
            <a href="<?=$sub_sect['SECTION_PAGE_URL']?>" class="gz-tiles-elem" data-align="middle left" style="padding-top: 22.8%;">
                <div class="gz-tiles-elem__bg lozad" data-background-image="<?=tools\funcs::rIMGG($sub_sect['PICTURE'], 5, 1230, 294)?>">&nbsp;</div>
                <strong class="gz-tiles-elem__title"><span><?=$sub_sect['NAME']?></span></strong>
            </a>
        </div>

    <? }
} ?>

                </div>
            </div>

        </div>
    </section>

<? } ?>