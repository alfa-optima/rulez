<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$arResult['SECTION'] = false;
$arResult['SUBSECTIONS_1'] = [];
$arResult['SUBSECTIONS_2'] = [];

$catalogIblocks = project\catalog::iblocks();
foreach ( $catalogIblocks as $iblock_id => $iblock ){
    $firstLevelSect = project\catalog::firstLevelCategory($iblock['ID']);
    if( $firstLevelSect['CODE'] == project\catalog::GAME_ZONE_SECT_CODE ){
        $arResult['SECTION'] = tools\section::info($firstLevelSect['ID']);
    }
}

if( intval($arResult['SECTION']['ID']) > 0 ){

    $sub_sects = tools\section::sub_sects($arResult['SECTION']['ID']);

    $cnt = 0;
    foreach ( $sub_sects as $key => $sub_sect ){ $cnt++;
        $sub_sect = tools\section::info( $sub_sect['ID'] );
        if( $cnt <= 7 ){
            $arResult['SUBSECTIONS_1'][$sub_sect['ID']] = $sub_sect;
        } else {
            $arResult['SUBSECTIONS_2'][$sub_sect['ID']] = $sub_sect;
        }
    }

}






$this->IncludeComponentTemplate();