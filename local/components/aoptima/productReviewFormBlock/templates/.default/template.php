<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<div class="u-review-form u-review-form-js productReviewFormBlock" style="display: none;">

    <form class="validate-js productReviewForm" action="/ajax/productReview.php" target="productReviewFrame" method="post" enctype="multipart/form-data" onsubmit="productReviewSend( $(this) );">

        <input type="hidden" name="product_id" value="<?=$arResult['PRODUCT']['ID']?>">

        <div class="u-review-form__title">Новый отзыв</div>

        <div class="input-wrap input-file input-file-js">
            <span class="input-file__title">Фото к отзыву:</span>
            <label>
                <span class="input-file__label input-file-label-js">Выберите файл</span>
                <input type="file" class="input-file-field" name="file" accept="image/jpeg, image/png">
            </label>
        </div>

        <label class="input-wrap">
            <input class="input-def" name="name" type="text" required>
            <span class="label-float">Ваше имя</span>
            <span class="error-note">Текст ошибки</span>
            <span class="success-note">Текст успешной отправки</span>
        </label>

        <label class="input-wrap">
            <input class="input-def" name="email" type="email" required>
            <span class="label-float">E-mail</span>
            <span class="error-note">Текст ошибки</span>
            <span class="success-note">Текст успешной отправки</span>
        </label>

        <label class="input-wrap">
            <textarea class="input-def" name="pluses" required></textarea>
            <span class="label-float">Достоинства</span>
            <span class="error-note">Текст ошибки</span>
            <span class="success-note">Текст успешной отправки</span>
        </label>

        <label class="input-wrap">
            <textarea class="input-def" name="minuses" required></textarea>
            <span class="label-float">Недостатки</span>
            <span class="error-note">Текст ошибки</span>
            <span class="success-note">Текст успешной отправки</span>
        </label>

        <label class="input-wrap">
            <textarea class="input-def" name="comment" required></textarea>
            <span class="label-float">Комментарий</span>
            <span class="error-note">Текст ошибки</span>
            <span class="success-note">Текст успешной отправки</span>
        </label>

        <div class="input-wrap">
            <div class="label-holder"><span class="label">Ваша оценка:</span></div>
            <select class="stars-rating-js" name="vote">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option selected value="5">5</option>
            </select>
        </div>

        <div class="input-wrap" style="float: right;">
            <div class="g-recaptcha" data-sitekey="<?=\Bitrix\Main\Config\Option::get('aoptima.project', 'GOOGLE_RECAPTCHA_SITE_KEY')?>"></div>
        </div>
        <div style="clear:both"></div>

        <p class="error___p" style="height:18px;"></p>

        <div class="u-review-form-footer">

            <div class="u-review-form-footer__holder">
                <label class="check-label">
                    <input type="checkbox" name="recommend" value="Y"/>
                    <span>Рекомендуете данный товар?</span>
                </label>
            </div>

            <div class="u-review-form-footer__aside">
                <input class="btn-alt u-review-form__btn" type="submit" value="Отправить">
            </div>

        </div>

    </form>

</div>

<script src="//www.google.com/recaptcha/api.js"></script>