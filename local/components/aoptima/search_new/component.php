<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;
\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;
\Bitrix\Main\Loader::includeModule('iblock');

$ag_module = 'arturgolubev.smartsearch';

if(!\CModule::IncludeModule("search")){
    ShowError(GetMessage("SEARCH_MODULE_UNAVAILABLE"));   return;
}

if(!\CModule::IncludeModule($ag_module)){
    global $USER;
    if($USER->IsAdmin()){
        ShowError(GetMessage("ARTURGOLUBEV_SMARTSEARCH_MODULE_UNAVAILABLE"));
    }
    return;
}



$arResult['SEARCH_RESULTS'] = [];
$arResult['FIRST_LEVEL_SECTIONS'] = [];

$arResult['NORMAL_SEARCH_RESULTS'] = [];
$arResult['NORMAL_FIRST_LEVEL_SECTIONS'] = [];

$arResult['DOP_SEARCH_RESULTS'] = [];
$arResult['DOP_FIRST_LEVEL_SECTIONS'] = [];

$arResult['SORT'] = project\search::getSort();

$catalogIblocksIDS = [];
foreach ( project\catalog::iblocks() as $iblock ){
    $catalogIblocksIDS[] = $iblock['ID'];
}

if( $arParams['IS_AJAX'] == 'Y' ){

    $arResult['SEARCH_RESULTS'] = $_SESSION['SEARCH_RESULTS'];
    $arResult['q'] = $_SESSION['SEARCH_Q'];

    if( intval($_POST['section_id']) > 0 ){
        $arResult['section'] =  tools\section::info($_POST['section_id']);
    }

} else {

    $arResult['~q'] = strip_tags(trim($_GET['q']));
    $arResult['q'] = htmlspecialcharsex($arResult['~q']);

    $q_words = explode(' ', $arResult['q']);
    $q_words = array_diff($q_words, ['']);

    $qHasRu = preg_match("/[а-яёА-ЯЁ]/", $arResult['q'], $matches, PREG_OFFSET_CAPTURE);
    $_SESSION['SEARCH_Q'] = $arResult['q'];

    if( strlen($arResult['q']) > 0 ){

        // Сделан foreach специально, т.к. в случае передачи массива ID-шников
        // поиск почему-то не искал по части инфоблоков (21.04.2020)
        foreach ( $catalogIblocksIDS as $iblockID ){

            if( $arResult['q'] ){
                $tmp_q = '"'.str_replace(' ', '" "', $arResult['q']).'"';
            } else {
                $tmp_q = false;
            }

            $arFilter = [
                "QUERY" => $tmp_q,
                "SITE_ID" => LANG,
                "TAGS" => false
            ];
            $arSort = [ "CUSTOM_RANK" => "DESC", "RANK" => "DESC", "DATE_CHANGE" => "DESC" ];
            $exFILTER = [
                [
                    '=MODULE_ID' => 'iblock',
                    'PARAM1' => '1c_catalog',
                    'PARAM2' => [ $iblockID ],
                ]
            ];

            $hasNormalResults = false;

            // Поиск по индексу
            $obSearch = new \CSearch;
            $obSearch->SetOptions([
                "ERROR_ON_EMPTY_STEM" => true,
                "NO_WORD_LOGIC" => false,
                //"STEMMING" => false,
            ]);
            $obSearch->Search( $arFilter, $arSort, $exFILTER );
            if ($obSearch->errorno != 0){} else {
                while( $item = $obSearch->GetNext() ){

$hasNormalResults = true;

$firstLevelCategory = project\catalog::firstLevelCategory($item['PARAM2']);
$arResult['NORMAL_FIRST_LEVEL_SECTIONS'][$firstLevelCategory['ID']] = $firstLevelCategory;
$arResult['NORMAL_SEARCH_RESULTS'][$firstLevelCategory['ID']][] = $item['ITEM_ID'];

                }
            }

            // Поиск ошибок
            if(
                count($arResult['SEARCH_RESULTS']) == 0
                &&
                !$hasNormalResults
            ){

                $query_words = explode(' ', strtoupper($arResult['q']));
                $words = CArturgolubevSmartsearch::prepareQueryWords($query_words);

                if( !empty($words) ){

                    $arLavelsWords = CArturgolubevSmartsearch::getSimilarWordsList($words);

                    if( !empty($arLavelsWords) ){
                        foreach( $arLavelsWords as $level => $searchArray ){
                            $arFilter["QUERY"] = implode(' | ', $searchArray);
                            // Повторный поиск по индексу
                            $obSearch = new \CSearch;
                            $obSearch->SetOptions([
                                "ERROR_ON_EMPTY_STEM" => true,
                                "NO_WORD_LOGIC" => false,
                                //"STEMMING" => false,
                            ]);
                            $obSearch->Search( $arFilter, $arSort, $exFILTER );
                            if ($obSearch->errorno != 0){} else {
                                while( $item = $obSearch->GetNext() ){

$firstLevelCategory = project\catalog::firstLevelCategory($item['PARAM2']);
$arResult['DOP_FIRST_LEVEL_SECTIONS'][$firstLevelCategory['ID']] = $firstLevelCategory;
$arResult['DOP_SEARCH_RESULTS'][$firstLevelCategory['ID']][] = $item['ITEM_ID'];

                                }
                            }
                        }
                    }
                }
            }


            // Раскладка
            if(
                count($arResult["SEARCH_RESULTS"]) == 0
                &&
                !$hasNormalResults
            ){

                $arLang = \CSearchLanguage::GuessLanguage($arResult['q']);
                if( is_array($arLang) && $arLang["from"] != $arLang["to"] ){

                    $arResult['lang_q'] = \CSearchLanguage::ConvertKeyboardLayout($arResult['q'], $arLang["from"], $arLang["to"]);

                    if( $arResult['lang_q'] != $arResult['q'] ){

                        // Повторный поиск по индексу
                        $arFilter["QUERY"] = $arResult['lang_q'];
                        $obSearch = new \CSearch;
                        $obSearch->SetOptions([
                            "ERROR_ON_EMPTY_STEM" => true,
                            "NO_WORD_LOGIC" => false,
                            //"STEMMING" => false,
                        ]);

                        $obSearch->Search( $arFilter, $arSort, $exFILTER );
                        if ($obSearch->errorno != 0){} else {
                            while($item = $obSearch->GetNext()){

$firstLevelCategory = project\catalog::firstLevelCategory($item['PARAM2']);
$arResult['DOP_FIRST_LEVEL_SECTIONS'][$firstLevelCategory['ID']] = $firstLevelCategory;
$arResult['DOP_SEARCH_RESULTS'][$firstLevelCategory['ID']][] = $item['ITEM_ID'];

                            }
                        }

                        // Поиск ошибок
                        if( count($arResult['SEARCH_RESULTS']) == 0 ){

                            $query_words = explode(' ', strtoupper($arResult['lang_q']));
                            $words = CArturgolubevSmartsearch::prepareQueryWords($query_words);

                            if( !empty($words) ){

                                $arLavelsWords = CArturgolubevSmartsearch::getSimilarWordsList($words);

                                if( !empty($arLavelsWords) ){
                                    foreach( $arLavelsWords as $level=>$searchArray ){
                                        $arFilter["QUERY"] = implode(' | ', $searchArray);
                                        // Повторный поиск по индексу
                                        $obSearch = new \CSearch;
                                        $obSearch->SetOptions([
                                            "ERROR_ON_EMPTY_STEM" => true,
                                            "NO_WORD_LOGIC" => false,
                                            //"STEMMING" => false,
                                        ]);
                                        $obSearch->Search( $arFilter, $arSort, $exFILTER );
                                        if ($obSearch->errorno != 0){} else {
                                            while($item = $obSearch->GetNext()){

$firstLevelCategory = project\catalog::firstLevelCategory($item['PARAM2']);
$arResult['DOP_FIRST_LEVEL_SECTIONS'][$firstLevelCategory['ID']] = $firstLevelCategory;
$arResult['DOP_SEARCH_RESULTS'][$firstLevelCategory['ID']][] = $item['ITEM_ID'];

                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
            }

        }

        if( count($arResult['NORMAL_SEARCH_RESULTS']) > 0  ){
            $arResult['SEARCH_RESULTS'] = $arResult['NORMAL_SEARCH_RESULTS'];
            $arResult['FIRST_LEVEL_SECTIONS'] = $arResult['NORMAL_FIRST_LEVEL_SECTIONS'];
        } else {
            $arResult['SEARCH_RESULTS'] = $arResult['DOP_SEARCH_RESULTS'];
            $arResult['FIRST_LEVEL_SECTIONS'] = $arResult['DOP_FIRST_LEVEL_SECTIONS'];
        }

        $_SESSION['SEARCH_RESULTS'] = $arResult['SEARCH_RESULTS'];

        if( count( $arResult['FIRST_LEVEL_SECTIONS']) > 0 ){
            $arResult['section'] =  $arResult['FIRST_LEVEL_SECTIONS'][array_keys($arResult['FIRST_LEVEL_SECTIONS'])[0]];
        }
    }
}



foreach ( $arResult['SEARCH_RESULTS'] as $sect_id => $items ){
    $arResult['SEARCH_RESULTS'][$sect_id] = array_unique($items);
}

if(
    $arResult['SORT']['CODE'] == 'RELEVANT'
    &&
    $arResult['SORT']['ORDER'] == 'asc'
    &&
    is_array($arResult['SEARCH_RESULTS'])
){
    foreach ( $arResult['SEARCH_RESULTS'] as $sect_id => $items ){
        $arResult['SEARCH_RESULTS'][$sect_id] = array_reverse($items);
    }
}


$this->IncludeComponentTemplate();