<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;


$arResult['sortFields'] = project\catalog::$sort_fields;


$arResult['sortField'] = project\catalog::getSortField();
$arResult['sortOrder'] = project\catalog::getSortOrder();





$this->IncludeComponentTemplate();