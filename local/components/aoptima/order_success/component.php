<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */

\Bitrix\Main\Loader::includeModule('sale');

use Bitrix\Main,
Bitrix\Main\Localization\Loc as Loc,
Bitrix\Main\Loader,
Bitrix\Main\Config\Option,
Bitrix\Main\Application,
Bitrix\Sale\Delivery,
Bitrix\Sale\PaySystem,
Bitrix\Sale,
Bitrix\Sale\Basket,
Bitrix\Sale\Order,
Bitrix\Sale\DiscountCouponsManager,
Bitrix\Main\Context;

\Bitrix\Main\Loader::includeModule('aoptima.tools');   use AOptima\Tools as tools;
\Bitrix\Main\Loader::includeModule('aoptima.project');   use AOptima\Project as project;

if( intval( $_POST['order_id'] ) > 0 ){

    $arResult['ORDER_ID'] = strip_tags($_POST['order_id']);

    $order = Sale\Order::load($arResult['ORDER_ID']);
    $arResult['ORDER_NUMBER'] = $order->getField('ACCOUNT_NUMBER');
    $arResult['ORDER_SUM'] = $order->getPrice();
    $arResult['ORDER_DATE'] = ConvertDateTime($order->getDateInsert(), "DD.MM.YYYY", "ru");

    $paymentCollection = $order->getPaymentCollection();
    $arResult['PS_ID'] = $paymentCollection[0]->getPaymentSystemId();

    $arResult['PS'] = project\ps::getByID($arResult['PS_ID']);

    $APPLICATION->SetPageProperty("title", 'Заказ '.$arResult['ORDER_NUMBER'].' успешно оформлен');


} else {

    LocalRedirect( '/' );

}






$this->IncludeComponentTemplate();