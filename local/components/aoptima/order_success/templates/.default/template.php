<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>

<div class="content" style="padding: 30px;">

    <h1>Заказ №<?=$arResult['ORDER_ID']?> от <?=$arResult['ORDER_DATE']?> успешно оформлен!</h1>

    <p>Мы свяжемся с Вами в ближайшее время!</p>
    
    <? if( in_array( $arResult['PS']['CODE'], [ 'card', 'halva' ] ) ){ ?>

        <div class="pay___block"></div>

        <script>
        $(document).ready(function(){
            getPaymentButton(<?=$arResult['ORDER_ID']?>);
        })
        </script>

        <script>(window.b24order = window.b24order || []).push({ id: "<?=$arResult['ORDER_ID']?>", sum: "<?=$arResult['ORDER_SUM']?>" });</script>

    <? } ?>
    
</div>
