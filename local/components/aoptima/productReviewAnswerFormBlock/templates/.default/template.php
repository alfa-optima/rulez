<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<div class="u-review-form u-review-form-js productReviewAnswerFormBlock" style="display: none;">

    <form class="validate-js productReviewAnswerForm" action="/ajax/productReviewAnswer.php" target="productReviewAnswerFrame" method="post" enctype="multipart/form-data" onsubmit="productReviewAnswerSend( $(this) );">

        <input type="hidden" name="product_id" value="<?=$arResult['PRODUCT']['ID']?>">
        <input type="hidden" name="review_id">

        <div class="u-review-form__title">Ответ на отзыв</div>

        <div class="input-wrap input-file input-file-js">
            <span class="input-file__title">Фото к отзыву:</span>
            <label>
                <span class="input-file__label input-file-label-js">Выберите файл</span>
                <input type="file" class="input-file-field" name="file" accept="image/jpeg, image/png">
            </label>
        </div>

        <label class="input-wrap">
            <input class="input-def" name="name" type="text" required>
            <span class="label-float">Ваше имя</span>
            <span class="error-note">Текст ошибки</span>
            <span class="success-note">Текст успешной отправки</span>
        </label>

        <label class="input-wrap">
            <input class="input-def" name="email" type="email" required>
            <span class="label-float">E-mail</span>
            <span class="error-note">Текст ошибки</span>
            <span class="success-note">Текст успешной отправки</span>
        </label>

        <label class="input-wrap">
            <textarea class="input-def" name="comment" required></textarea>
            <span class="label-float">Комментарий</span>
            <span class="error-note">Текст ошибки</span>
            <span class="success-note">Текст успешной отправки</span>
        </label>

        <div class="input-wrap" style="float: right;">
            <div class="g-recaptcha" data-sitekey="<?=\Bitrix\Main\Config\Option::get('aoptima.project', 'GOOGLE_RECAPTCHA_SITE_KEY')?>"></div>
        </div>
        <div style="clear:both"></div>

        <p class="error___p" style="height:18px;"></p>

        <div class="u-review-form-footer">

            <div class="u-review-form-footer__holder"></div>

            <div class="u-review-form-footer__aside">
                <input class="btn-alt u-review-form__btn" type="submit" value="Отправить">
            </div>

        </div>

    </form>

</div>

<script src="//www.google.com/recaptcha/api.js"></script>