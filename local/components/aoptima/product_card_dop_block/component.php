<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */


\Bitrix\Main\Loader::includeModule('aoptima.project'); use AOptima\Project as project;
\Bitrix\Main\Loader::includeModule('aoptima.tools'); use AOptima\Tools as tools;
\Bitrix\Main\Loader::includeModule('iblock');

$arResult['el'] = $arParams['~el'];



// Подарки к товару
$arResult['gifts_categories'] = [];
$arResult['gifts'] = project\gift::getForProduct($arResult['el']['ID']);
if(
    is_array($arResult['gifts'])
    &&
    count($arResult['gifts']) > 0
){
    $elements_info = [];
    $chunks = array_chunk($arResult['gifts'], 500);
    foreach ( $chunks as $chunk ){
        $filter = Array( "ACTIVE" => "Y", "ID" => $chunk );
        $fields = Array( "ID", "IBLOCK_ID", "IBLOCK_SECTION_ID" );
        $dbElements = \CIBlockElement::GetList(
        	array("SORT"=>"ASC"), $filter, false, false, $fields
        );
        while ($element = $dbElements->GetNext()){
            $elements_info[$element['ID']] = $element;
        }
    }
    foreach ( $elements_info as $element ){
        if( intval($element['ID']) > 0 ){
            $el_sect = tools\el::sections($element['ID'])[0];
            if( !$arResult['gifts_categories'][$el_sect['ID']] ){
                $arResult['gifts_categories'][$el_sect['ID']] = $el_sect;
            }
            $arResult['gifts_categories'][$el_sect['ID']]['ITEMS'][$element['ID']] = $element;
        }
    }
}



// Комплекты к товару
$arResult['complects'] = project\complect::getForProduct( $arResult['el']['ID'] );



// Похожие товары
$arResult['similar_goods'] = [];
if(
    is_array($arResult['el']['PROPERTY_'.project\catalog::SIMILAR_PROP_CODE.'_VALUE'])
    &&
    count($arResult['el']['PROPERTY_'.project\catalog::SIMILAR_PROP_CODE.'_VALUE']) > 0
){
    $xml_ids = $arResult['el']['PROPERTY_'.project\catalog::SIMILAR_PROP_CODE.'_VALUE'];
    $elements_info = [];
    $chunks = array_chunk($xml_ids, 500);
    foreach ( $chunks as $chunk ){
        $filter = Array( "ACTIVE" => "Y", "XML_ID" => $chunk );
        $fields = Array( "ID", "XML_ID", "IBLOCK_ID", "IBLOCK_SECTION_ID" );
        $dbElements = \CIBlockElement::GetList(
            array("SORT"=>"ASC"), $filter, false, false, $fields
        );
        while ($element = $dbElements->GetNext()){
            $elements_info[$element['XML_ID']] = $element;
        }
        foreach ( $xml_ids as $xml_id ){
            $el = $elements_info[$xml_id];
            if( intval($el['ID']) > 0 ){
                $el_sect = tools\el::sections($el['ID'])[0];
                if( !$arResult['similar_goods'][$el_sect['ID']] ){
                    $arResult['similar_goods'][$el_sect['ID']] = $el_sect;
                }
                $arResult['similar_goods'][$el_sect['ID']]['ITEMS'][$el['ID']] = $el;
            }
        }
    }
}



// Аксессуары
$arResult['accessoires'] = [];
if(
    is_array($arResult['el']['PROPERTY_'.project\catalog::SOPUT_PROP_CODE.'_VALUE'])
    &&
    count($arResult['el']['PROPERTY_'.project\catalog::SOPUT_PROP_CODE.'_VALUE']) > 0
){
    $xml_ids = $arResult['el']['PROPERTY_'.project\catalog::SOPUT_PROP_CODE.'_VALUE'];
    $elements_info = [];
    $chunks = array_chunk($xml_ids, 500);
    foreach ( $chunks as $chunk ){
        $filter = Array( "ACTIVE" => "Y", "XML_ID" => $chunk );
        $fields = Array( "ID", "XML_ID", "IBLOCK_ID", "IBLOCK_SECTION_ID" );
        $dbElements = \CIBlockElement::GetList(
            array("SORT"=>"ASC"), $filter, false, false, $fields
        );
        while ($element = $dbElements->GetNext()){
            $elements_info[$element['XML_ID']] = $element;
        }
        foreach ( $xml_ids as $xml_id ){
            $el = $elements_info[$xml_id];
            if( intval($el['ID']) > 0 ){
                $el_sect = tools\el::sections($el['ID'])[0];
                if( !$arResult['accessoires'][$el_sect['ID']] ){
                    $arResult['accessoires'][$el_sect['ID']] = $el_sect;
                }
                $arResult['accessoires'][$el_sect['ID']]['ITEMS'][$el['ID']] = $el;
            }
        }
    }
}




$this->IncludeComponentTemplate();