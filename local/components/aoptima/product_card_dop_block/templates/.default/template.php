<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');  use AOptima\Project as project;
\Bitrix\Main\Loader::includeModule('aoptima.tools');  use AOptima\Tools as tools; ?>


<section class="section section_p-prompts dop-tabs-js productCardDopBlock" id="productCardDopBlock">

    <? if(
        count($arResult['gifts_categories']) > 0
        ||
        count($arResult['complects']) > 0
        ||
        count($arResult['accessoires']) > 0
        ||
        count($arResult['similar_goods']) > 0
    ){ ?>

        <h2 class="section__title hide">Предложения</h2>
        <div class="p-prompts">

            <div class="divisions-tabs__nav p-prompts-tabs__nav">
                <div class="divisions-tabs__select tabs__select-js"></div>
                <div class="divisions-tabs-thumbs tabs__thumbs-js tabs__select-drop-js product_tabs___block">

                    <? if( count($arResult['gifts_categories']) > 0 ){ ?>
                        <a href="#giftsBlock" class="giftsTabLink"><span>Подарки</span></a>
                    <? } ?>

                    <? if( count($arResult['complects']) > 0 ){ ?>
                        <a href="#promptsSets"><span>Вместе дешевле</span></a>
                    <? } ?>

                    <? if( count($arResult['accessoires']) > 0 ){ ?>
                        <a href="#promptsAccessories"><span>Аксессуары</span></a>
                    <? } ?>

                    <? if( count($arResult['similar_goods']) > 0 ){ ?>
                        <a href="#promptsSimilar"><span>Похожие товары</span></a>
                    <? } ?>

                </div>
            </div>

            <script>
            $(document).ready(function(){
                $('.product_tabs___block a').eq(0).trigger('click');
            })
            </script>

            <div class="tabs__panels-js">


                <? // Подарки
                $APPLICATION->IncludeComponent(
                    "aoptima:productGiftsBlock", "", array(
                        'gifts_categories' => $arResult['gifts_categories']
                    )
                ); ?>


                <? // Товары комплекта
                $APPLICATION->IncludeComponent(
                    "aoptima:productComplect", "", array('arResult' => $arResult)
                ); ?>


                <? // Аксессуары
                $APPLICATION->IncludeComponent(
                    "aoptima:productAccessoires", "", array(
                        'accessoires' => $arResult['accessoires']
                    )
                ); ?>


                <? // Похожие товары
                $APPLICATION->IncludeComponent(
                    "aoptima:productSimilarBlock", "", array(
                        'similar_goods' => $arResult['similar_goods']
                    )
                ); ?>


            </div>
        </div>

    <? } ?>

</section>