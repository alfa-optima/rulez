<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');  use AOptima\Project as project;
\Bitrix\Main\Loader::includeModule('aoptima.tools');  use AOptima\Tools as tools;
\Bitrix\Main\Loader::includeModule('iblock');


if( $arParams['IS_AJAX'] == 'Y' ){

    $arResult['BRAND_CODE'] = $_SESSION['BRAND_CODE'];
    $arResult['BRAND'] = $_SESSION['BRAND'];
    
    if( intval($_POST['section_id']) > 0 ){

        $arResult['section'] = tools\section::info($_POST['section_id']);
        
    }

} else {

    $arResult['404'] = true;

    $pureURL = tools\funcs::pureURL();
    $arResult['arURI'] = tools\funcs::arURI($pureURL);
    $arResult['BRAND_CODE'] = $arResult['arURI'][2];
    $arResult['BRAND'] = tools\el::info_by_code($arResult['BRAND_CODE'], project\brand::IBLOCK_ID);

    if( intval($arResult['BRAND']) > 0 ){

        $_SESSION['BRAND_CODE'] = $arResult['BRAND_CODE'];
        $_SESSION['BRAND'] = $arResult['BRAND'];

        $arResult['404'] = false;

        $APPLICATION->SetPageProperty("title", 'Товары бренда "'.$arResult['BRAND']['NAME'].'"');

        $arResult['FIRST_LEVEL_SECTIONS'] = [];

        // Получим инфоблоки каталога
        $catalogIblocks = project\catalog::iblocks();

        // Делаем поиск по запросу в каждом из инфоблоков
        foreach ( $catalogIblocks as $iblock ){

            $filter = Array(
            	"IBLOCK_ID" => $iblock['ID'],
                "ACTIVE" => "Y",
                "INCLUDE_SUBSECTIONS" => "Y",
                "PROPERTY_".$arResult['BRAND']['PROPERTY_PROP_CODE_VALUE']."_VALUE" => $arResult['BRAND']['PROPERTY_PROP_VALUE_VALUE']
            );
            
            $fields = Array( "ID", "NAME", "PROPERTY_".$arResult['BRAND']['PROPERTY_PROP_CODE_VALUE'] );
            $tovary = \CIBlockElement::GetList(
            	array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>1), $fields
            );
            if ($tovar = $tovary->GetNext()){

                $firstLevelCategory = project\catalog::firstLevelCategory($iblock['ID']);
                $arResult['FIRST_LEVEL_SECTIONS'][$firstLevelCategory['ID']] = $firstLevelCategory;

                $arResult['section'] =  $arResult['FIRST_LEVEL_SECTIONS'][array_keys($arResult['FIRST_LEVEL_SECTIONS'])[0]];
                
            }

        }

    }

}


$this->IncludeComponentTemplate();