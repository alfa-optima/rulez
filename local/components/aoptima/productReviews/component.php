<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$arResult['PRODUCT'] = $arParams['PRODUCT'];
$arResult['IBLOCK_ID'] = project\product_review::IBLOCK_ID;
$arResult['MAX_CNT'] = project\product_review::LIST_CNT;
$arResult['SHOW_BUTTON'] = 'N';
$arResult['IS_AUTH'] = $USER->IsAuthorized()?'Y':'N';


// Отзывы о товаре
if( $arParams['IS_LOAD'] == 'Y' ){
    $arResult['REVIEWS'] = project\product_review::getOrderedList( $arResult['PRODUCT']['ID'], $_POST['stop_ids'] );
} else {
    $arResult['REVIEWS'] = project\product_review::getOrderedList( $arResult['PRODUCT']['ID'] );
}

// Подсчитаем голоса
foreach ( $arResult['REVIEWS'] as $key => $review ){
    $review['PLUS_VOTES_CNT'] = 0;
    $review['MINUS_VOTES_CNT'] = 0;
    // Все голоса за данный отзыв
    $product_review_vote = new project\product_review_vote();
    $votes = $product_review_vote->getList( $review['ID'] );
    foreach ( $votes as $vote ){
        if( $vote['UF_VOTE'] ){
            $review['PLUS_VOTES_CNT']++;
        } else {
            $review['MINUS_VOTES_CNT']++;
        }
    }
    $arResult['REVIEWS'][$key] = $review;
}

$arResult['SUB_ITEMS'] = [];
$cnt = 0;
foreach ( $arResult['REVIEWS'] as $key => $review ){ $cnt++;
    if( $cnt > $arResult['MAX_CNT'] ){
        unset($arResult['REVIEWS'][$key]);
    } else {
        if( intval($review['PROPERTY_LINK_VALUE']) > 0 ){
            if( $arResult['REVIEWS'][$review['PROPERTY_LINK_VALUE']] ){
                $arResult['REVIEWS'][$review['PROPERTY_LINK_VALUE']]['SUB_ITEMS'][$review['ID']] = $review;
            } else {
                $arResult['SUB_ITEMS'][$review['ID']] = $review;
            }
            unset($arResult['REVIEWS'][$key]);
        }
    }
}
if( $cnt > $arResult['MAX_CNT'] ){
    $arResult['SHOW_BUTTON'] = 'Y';
}














$arResult['AUTH'] = $USER->IsAuthorized()?'Y':'N';





$this->IncludeComponentTemplate();


if( $arParams['IS_LOAD'] == 'Y' ){

    return $arResult;

}
