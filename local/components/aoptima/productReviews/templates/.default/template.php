<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if( $arParams['IS_LOAD'] != 'Y' ){


    if( count($arResult['REVIEWS']) > 0 ){ ?>

        <div class="u-reviews productReviewsLoadArea">

            <? foreach( $arResult['REVIEWS'] as $review_id => $review ){

                // productReviewItem
                $APPLICATION->IncludeComponent(
                    "aoptima:productReviewItem", "",
                    array('review' => $review)
                );

            } ?>

        </div>

        <div class="u-reviews-load-more productReviewsMoreBlock" style="<? if( $arResult['SHOW_BUTTON'] == 'N' ){ ?>display:none;<? } ?>">
            <a style="cursor: pointer;" class="btn-def load-more__btn productReviewsMoreButton to___process">Показать еще</a>
        </div>

    <? }


} else if( $arParams['IS_LOAD'] == 'Y' ){

    if( count($arResult['REVIEWS']) > 0 ){

        foreach( $arResult['REVIEWS'] as $review_id => $review ){

            // productReviewItem
            $APPLICATION->IncludeComponent(
                "aoptima:productReviewItem", "",
                array('review' => $review)
            );

        }

    }


}