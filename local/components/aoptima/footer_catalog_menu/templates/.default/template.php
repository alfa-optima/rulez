<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>

<div class="footer-grid__item footer-grid__item_catalog">
    <strong class="footer-grid__title"><a href="#"><span>Каталог</span></a></strong>
    <div class="footer-grid__content">
        <ul class="footer-grid__list">

            <? foreach( $arResult['SECTIONS'] as $key => $sect ){ ?>

                <li>
                    <a href="<?=$sect['SECTION_PAGE_URL']?>"><span><?=$sect['NAME']?></span></a>
                </li>

            <? } ?>

        </ul>
    </div>
</div>
