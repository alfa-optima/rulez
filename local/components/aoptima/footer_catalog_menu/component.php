<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$arResult['SECTIONS'] = [];

$iblocks = project\catalog::iblocks();
foreach ( $iblocks as $key => $iblock ){
    $firstLevelSection = project\catalog::firstLevelCategory($iblock['ID']);
    $arResult['SECTIONS'][] = $firstLevelSection;
}



$this->IncludeComponentTemplate();