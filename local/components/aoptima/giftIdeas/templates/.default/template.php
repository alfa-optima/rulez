<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if( $arResult['IS_404'] == 'Y' ){

    include($_SERVER["DOCUMENT_ROOT"]."/local/templates/main/include/404include.php");

} else if( $arResult['IS_DETAIL'] == 'Y' ){ ?>


    <? // giftIdeasDetail
    $APPLICATION->IncludeComponent(
        "aoptima:giftIdeasDetail", "", array('SECTION' => $arResult['SECTION'])
    ); ?>


<? } else { ?>

    <div class="m-container p-filters-js">

        <div class="m-aside__align">
            <div class="m-aside__holder">

                <ul class="articles-nav">
                    <li class="current">

                        <a>Лучшие цены</a>

                        <? if( count($arResult['IBLOCKS']) > 0 ){ ?>

                            <ul>

                                <? foreach( $arResult['IBLOCKS'] as $key => $iblock ){
                                    $section = $iblock['SECTION']; ?>

                                    <li>
                                        <a href="/gift_ideas/<?=$section['CODE']?>/"><?=$section['NAME']?></a>
                                    </li>

                                <? } ?>

                            </ul>

                        <? } ?>

                    </li>
                </ul>
            </div>
        </div>

        <div class="m-content">

            <div class="m-heading">

                <h1>Лучшие цены</h1>

                <div class="view-switcher m-heading__switcher view-switcher-products-js" data-toggle-view-for="products">
                    <a href="#" class="active" data-mod="grid-view" title="Продукты плиткой">
                        <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 28 28">
                            <path d="M6 7.5C6 6.67157 6.67157 6 7.5 6C8.32843 6 9 6.67157 9 7.5C9 8.32843 8.32843 9 7.5 9C6.67157 9 6 8.32843 6 7.5Z"></path>
                            <path d="M6 13.5C6 12.6716 6.67157 12 7.5 12C8.32843 12 9 12.6716 9 13.5C9 14.3284 8.32843 15 7.5 15C6.67157 15 6 14.3284 6 13.5Z"></path>
                            <path d="M6 20.5C6 19.6716 6.67157 19 7.5 19C8.32843 19 9 19.6716 9 20.5C9 21.3284 8.32843 22 7.5 22C6.67157 22 6 21.3284 6 20.5Z"></path>
                            <path d="M13 7.5C13 6.67157 13.6716 6 14.5 6C15.3284 6 16 6.67157 16 7.5C16 8.32843 15.3284 9 14.5 9C13.6716 9 13 8.32843 13 7.5Z"></path>
                            <path d="M13 13.5C13 12.6716 13.6716 12 14.5 12C15.3284 12 16 12.6716 16 13.5C16 14.3284 15.3284 15 14.5 15C13.6716 15 13 14.3284 13 13.5Z"></path>
                            <path d="M13 20.5C13 19.6716 13.6716 19 14.5 19C15.3284 19 16 19.6716 16 20.5C16 21.3284 15.3284 22 14.5 22C13.6716 22 13 21.3284 13 20.5Z"></path>
                            <path d="M20 7.5C20 6.67157 20.6716 6 21.5 6C22.3284 6 23 6.67157 23 7.5C23 8.32843 22.3284 9 21.5 9C20.6716 9 20 8.32843 20 7.5Z"></path>
                            <path d="M20 13.5C20 12.6716 20.6716 12 21.5 12C22.3284 12 23 12.6716 23 13.5C23 14.3284 22.3284 15 21.5 15C20.6716 15 20 14.3284 20 13.5Z"></path>
                            <path d="M20 20.5C20 19.6716 20.6716 19 21.5 19C22.3284 19 23 19.6716 23 20.5C23 21.3284 22.3284 22 21.5 22C20.6716 22 20 21.3284 20 20.5Z"></path>
                        </svg>
                        <span>Плиткой</span>
                    </a>
                    <a href="#" data-mod="list-view" title="Продукты списком">
                        <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 28 28">
                            <path d="M3 7C3 6.44772 3.44772 6 4 6H24C24.5523 6 25 6.44772 25 7C25 7.55228 24.5523 8 24 8H4C3.44772 8 3 7.55228 3 7Z"></path>
                            <path d="M3 14C3 13.4477 3.44772 13 4 13H24C24.5523 13 25 13.4477 25 14C25 14.5523 24.5523 15 24 15H4C3.44772 15 3 14.5523 3 14Z"></path>
                            <path d="M3 21C3 20.4477 3.44772 20 4 20H24C24.5523 20 25 20.4477 25 21C25 21.5523 24.5523 22 24 22H4C3.44772 22 3 21.5523 3 21Z"></path>
                        </svg>
                        <span>Списком</span>
                    </a>
                </div>

            </div>

            <div class="products" data-toggle-view-id="products">
                <section class="products__list equal-height-js">

                    <? foreach( $arResult['IBLOCKS'] as $key => $iblock ){
                        $section = $iblock['SECTION']; ?>

                        <article class="products__item">
                            <a href="/gift_ideas/<?=$section['CODE']?>/" class="split">

                                <div class="split__figure">

                                    <? if( intval($section['PICTURE']) > 0 ){ ?>
                                        <img itemprop="image" src="<?=SITE_TEMPLATE_PATH?>/img/preloader.svg" data-src="<?=tools\funcs::rIMGG($section['PICTURE'], 4, 261, 178)?>">
                                    <? } else { ?>
                                        <img itemprop="image" src="<?=SITE_TEMPLATE_PATH?>/img/no-photo.png">
                                    <? } ?>

                                </div>

                                <h3 class="split__title"><span><?=$section['NAME']?></span></h3>

                            </a>
                        </article>

                    <? } ?>

                </section>
            </div>

        </div>
    </div>

<? } ?>