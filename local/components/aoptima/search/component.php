<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('search');
\Bitrix\Main\Loader::includeModule('iblock');

$arResult['SEARCH_RESULTS'] = [];
$arResult['FIRST_LEVEL_SECTIONS'] = [];

$arResult['SORT'] = project\search::getSort();


$catalogIblocksIDS = [];
foreach ( project\catalog::iblocks() as $iblock ){
    $catalogIblocksIDS[] = $iblock['ID'];
}

if( $arParams['IS_AJAX'] == 'Y' ){

    $arResult['SEARCH_RESULTS'] = $_SESSION['SEARCH_RESULTS'];
    $arResult['q'] = $_SESSION['SEARCH_Q'];

    if( intval($_POST['section_id']) > 0 ){
        $arResult['section'] =  tools\section::info($_POST['section_id']);
    }

} else {

    $arResult['q'] = strip_tags(trim($_GET['q']));
    $qHasRu = preg_match("/[а-яёА-ЯЁ]/", $arResult['q'], $matches, PREG_OFFSET_CAPTURE);
    $_SESSION['SEARCH_Q'] = $arResult['q'];

    if( strlen($arResult['q']) > 0 ){

        // Поиск по индексу
        $obSearch = new \CSearch;
        $obSearch->Search(
            [
                "QUERY" => $arResult['q'],
                "SITE_ID" => LANG,
                [
                    [
                        "=MODULE_ID" => "iblock",
                        "!ITEM_ID" => "S%",
                        "PARAM2" => $catalogIblocksIDS
                    ]
                ]
            ],
            [ "RANK" => "DESC", "TITLE_RANK" => "DESC" ],
            [ 'STEMMING' => true ]
        );
        if ($obSearch->errorno != 0){} else {
            while($item = $obSearch->GetNext()){
                if( substr_count( strtolower($item['TITLE']), strtolower(trim($arResult['q'])) ) ){
                    $firstLevelCategory = project\catalog::firstLevelCategory($item['PARAM2']);
                    $arResult['FIRST_LEVEL_SECTIONS'][$firstLevelCategory['ID']] = $firstLevelCategory;
                    $arResult['SEARCH_RESULTS'][$firstLevelCategory['ID']][] = $item['ITEM_ID'];
                }
            }
        }
        // в т.ч. по транслиту
        if( $qHasRu ){
            $obSearch = new \CSearch;
            $obSearch->Search(
                [
                    "QUERY" => tools\funcs::translit($arResult['q']),
                    "SITE_ID" => LANG,
                    [
                        [
                            "=MODULE_ID" => "iblock",
                            "!ITEM_ID" => "S%",
                            "PARAM2" => $catalogIblocksIDS
                        ]
                    ]
                ],
                [ "RANK" => "DESC", "TITLE_RANK" => "DESC" ],
                [ 'STEMMING' => true ]
            );
            if ($obSearch->errorno != 0){} else {
                while($item = $obSearch->GetNext()){
                    if( substr_count( strtolower($item['TITLE']), strtolower(tools\funcs::translit(trim($arResult['q']))) ) ){
                        $firstLevelCategory = project\catalog::firstLevelCategory($item['PARAM2']);
                        $arResult['FIRST_LEVEL_SECTIONS'][$firstLevelCategory['ID']] = $firstLevelCategory;
                        $arResult['SEARCH_RESULTS'][$firstLevelCategory['ID']][] = $item['ITEM_ID'];
                    }
                }
            }
        }
        if( count($arResult['SEARCH_RESULTS']) == 0 ){
            $obSearch = new \CSearch;
            $obSearch->Search(
                [
                    "QUERY" => $arResult['q'],
                    "SITE_ID" => LANG,
                    [
                        [
                            "=MODULE_ID" => "iblock",
                            "!ITEM_ID" => "S%",
                            "PARAM2" => $catalogIblocksIDS
                        ]
                    ]
                ],
                [ "RANK" => "DESC", "TITLE_RANK" => "DESC" ],
                [ 'STEMMING' => false ]
            );
            if ($obSearch->errorno != 0){} else {
                while($item = $obSearch->GetNext()){
                    if( substr_count( strtolower($item['TITLE']), strtolower(trim($arResult['q'])) ) ){
                        $firstLevelCategory = project\catalog::firstLevelCategory($item['PARAM2']);
                        $arResult['FIRST_LEVEL_SECTIONS'][$firstLevelCategory['ID']] = $firstLevelCategory;
                        $arResult['SEARCH_RESULTS'][$firstLevelCategory['ID']][] = $item['ITEM_ID'];
                    }
                }
            }
        }
        // Если по индексу не найдено
        // Попытка найти по заголовку
        if( count($arResult['SEARCH_RESULTS']) == 0 ){
            foreach ( project\catalog::iblocks() as $iblock ){
                $firstLevelCategory = project\catalog::firstLevelCategory($iblock['ID']);
                $filter = Array(
                    "IBLOCK_ID" => $iblock['ID'],
                    "SECTION_ID" => $firstLevelCategory['ID'],
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "ACTIVE" => "Y",
                    project\funcs::searchLogic($arResult['q'])
                );
                $fields = Array( "ID", "NAME", "IBLOCK_ID" );
                $dbElements = \CIBlockElement::GetList(
                    array("SORT"=>"ASC"), $filter, false, false, $fields
                );
                while ($element = $dbElements->GetNext()){
                    $arResult['FIRST_LEVEL_SECTIONS'][$firstLevelCategory['ID']] = $firstLevelCategory;
                    $arResult['SEARCH_RESULTS'][$firstLevelCategory['ID']][] = $element['ID'];
                }
            }
        }
        if( count($arResult['SEARCH_RESULTS']) == 0 ){
            foreach ( project\catalog::iblocks() as $iblock ){
                $firstLevelCategory = project\catalog::firstLevelCategory($iblock['ID']);
                $filter = Array(
                    "IBLOCK_ID" => $iblock['ID'],
                    "SECTION_ID" => $firstLevelCategory['ID'],
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "ACTIVE" => "Y",
                    project\funcs::searchLogic($arResult['q'], true)
                );
                $fields = Array( "ID", "NAME", "IBLOCK_ID" );
                $dbElements = \CIBlockElement::GetList(
                    array("SORT"=>"ASC"), $filter, false, false, $fields
                );
                while ($element = $dbElements->GetNext()){
                    $arResult['FIRST_LEVEL_SECTIONS'][$firstLevelCategory['ID']] = $firstLevelCategory;
                    $arResult['SEARCH_RESULTS'][$firstLevelCategory['ID']][] = $element['ID'];
                }
            }
        }
        // Попытка найти по транслиту
        if( count($arResult['SEARCH_RESULTS']) == 0 ){
            foreach ( project\catalog::iblocks() as $iblock ){
                $firstLevelCategory = project\catalog::firstLevelCategory($iblock['ID']);
                $filter = Array(
                    "IBLOCK_ID" => $iblock['ID'],
                    "SECTION_ID" => $firstLevelCategory['ID'],
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "ACTIVE" => "Y",
                    project\funcs::searchLogic(tools\funcs::translit($arResult['q']))
                );
                $fields = Array( "ID", "NAME", "IBLOCK_ID" );
                $dbElements = \CIBlockElement::GetList(
                    array("SORT"=>"ASC"), $filter, false, false, $fields
                );
                while ($element = $dbElements->GetNext()){
                    $arResult['FIRST_LEVEL_SECTIONS'][$firstLevelCategory['ID']] = $firstLevelCategory;
                    $arResult['SEARCH_RESULTS'][$firstLevelCategory['ID']][] = $element['ID'];
                }
            }
        }
        if( count($arResult['SEARCH_RESULTS']) == 0 ){
            foreach ( project\catalog::iblocks() as $iblock ){
                $firstLevelCategory = project\catalog::firstLevelCategory($iblock['ID']);
                $filter = Array(
                    "IBLOCK_ID" => $iblock['ID'],
                    "SECTION_ID" => $firstLevelCategory['ID'],
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "ACTIVE" => "Y",
                    project\funcs::searchLogic(tools\funcs::translit($arResult['q']), true)
                );
                $fields = Array( "ID", "NAME", "IBLOCK_ID" );
                $dbElements = \CIBlockElement::GetList(
                    array("SORT"=>"ASC"), $filter, false, false, $fields
                );
                while ($element = $dbElements->GetNext()){
                    $arResult['FIRST_LEVEL_SECTIONS'][$firstLevelCategory['ID']] = $firstLevelCategory;
                    $arResult['SEARCH_RESULTS'][$firstLevelCategory['ID']][] = $element['ID'];
                }
            }
        }
        // Поиск по коду
        foreach ( project\catalog::iblocks() as $iblock ){
            $firstLevelCategory = project\catalog::firstLevelCategory($iblock['ID']);
            $filter = Array(
                "IBLOCK_ID" => $iblock['ID'],
                "SECTION_ID" => $firstLevelCategory['ID'],
                "INCLUDE_SUBSECTIONS" => "Y",
                "ACTIVE" => "Y",
                "PROPERTY_CML2_ARTICLE" => $arResult['q']
            );
            $fields = Array( "ID", "NAME", "IBLOCK_ID", "PROPERTY_CML2_ARTICLE" );
            $dbElements = \CIBlockElement::GetList(
                array("SORT"=>"ASC"), $filter, false, false, $fields
            );
            while ($element = $dbElements->GetNext()){
                $arResult['FIRST_LEVEL_SECTIONS'][$firstLevelCategory['ID']] = $firstLevelCategory;
                $arResult['SEARCH_RESULTS'][$firstLevelCategory['ID']][] = $element['ID'];
            }
        }

        $_SESSION['SEARCH_RESULTS'] = $arResult['SEARCH_RESULTS'];

        if( count( $arResult['FIRST_LEVEL_SECTIONS']) > 0 ){
            $arResult['section'] =  $arResult['FIRST_LEVEL_SECTIONS'][array_keys($arResult['FIRST_LEVEL_SECTIONS'])[0]];
        }

    }

}

foreach ( $arResult['SEARCH_RESULTS'] as $sect_id => $items ){
    $arResult['SEARCH_RESULTS'][$sect_id] = array_unique($items);
}

if(
    $arResult['SORT']['CODE'] == 'RELEVANT'
    &&
    $arResult['SORT']['ORDER'] == 'asc'
    &&
    is_array($arResult['SEARCH_RESULTS'])
){
    foreach ( $arResult['SEARCH_RESULTS'] as $sect_id => $items ){
        $arResult['SEARCH_RESULTS'][$sect_id] = array_reverse($items);
    }
}






$this->IncludeComponentTemplate();