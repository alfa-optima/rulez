<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;
?>

<!--cs-container-->
<div class="p-sorting cs-container cs-js catalogSortBlock">
    <!--angle icon svg-->
    <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
        <symbol id="icon-sort-angle" viewBox="0 0 10 6">
            <path d="M1 1L5 5L9 1" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
        </symbol>
    </svg>
    <!--angle icon svg end-->
    <!--cs-selector-->
    <div class="cs-selector cs__selector-js">
        <span>Сортировать</span>
        <i><svg height="6" width="10" fill="none">
            <use xlink:href="#icon-sort-angle"></use>
        </svg></i>
    </div>
    <!--cs-drop-->
    <div class="cs-drop cs__drop-js">

        <? // Сортировка
        foreach( $arResult['ITEMS'] as $item ){ ?>

            <div class="cs-drop__item cs__item-js <? if( $arResult['SORT']['CODE'] == $item['CODE'] ){ ?>cs_active<? } ?> <?=$arResult['SORT']['ORDER']?>">
                <a style="cursor:pointer;" sort_field="<?=$item['CODE']?>" sort_order="<?=$arResult['SORT']['ORDER']?>" class="cs-drop__elem cs-drop__elem_toggle sortLink is___search to___process">
                    <span><?=$item['NAME']?></span>
                    <i><svg height="6" width="10" fill="none">
                        <use xlink:href="#icon-sort-angle"></use>
                    </svg></i>
                </a>
                <a style="cursor:pointer;" class="cs-drop__elem cs-drop__elem_option cs__option-js" sort_field="<?=$item['CODE']?>" data-cs-trend="asc"><?=$item['NAME']?> по возрастанию</a>
                <a style="cursor:pointer;" class="cs-drop__elem cs-drop__elem_option cs__option-js" sort_field="<?=$item['CODE']?>" data-cs-trend="desc"><?=$item['NAME']?> по убыванию</a>
            </div>

        <? } ?>

    </div>
</div>
<!--.cs end-->