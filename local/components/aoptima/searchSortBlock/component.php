<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;


$arResult['ITEMS'] = project\search::sortList();
$arResult['SORT'] = project\search::getSort();






$this->IncludeComponentTemplate();