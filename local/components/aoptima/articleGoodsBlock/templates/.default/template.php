<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if(
    is_array($arResult['PROPERTY_GOODS_VALUE'])
    &&
    count($arResult['PROPERTY_GOODS_VALUE']) > 0
){ ?>

    <? if( strlen($arResult['PROPERTY_GOODS_TITLE_VALUE']) > 0 ){ ?>
        <h3 style="margin: 40px 0 0 10px;"><?=$arResult['PROPERTY_GOODS_TITLE_VALUE']?></h3>
    <? } ?>

    <div class="swiper-container shot-slider article-offer-slider-js" style="padding-bottom: 6.1rem;">
        <div class="swiper-wrapper">

            <? foreach( $arResult['PROPERTY_GOODS_VALUE'] as $el_id ){
                $iblock_id = tools\el::getIblock($el_id);

                $category = tools\section::info(tools\el::sections($el_id)[0]['ID']);

                $GLOBALS['article_one_slider_good']['ID'] = $el_id;
                $APPLICATION->IncludeComponent(
                    "bitrix:catalog.section", "article_one_slider_good",
                    Array(
                        "IS_AJAX" => "Y",
                        "category" => $category,
                        "IBLOCK_TYPE" => project\catalog::IBLOCK_TYPE,
                        "IBLOCK_ID" => $iblock_id,
                        "SECTION_USER_FIELDS" => array(),
                        "ELEMENT_SORT_FIELD" => 'NAME',
                        "ELEMENT_SORT_ORDER" => 'ASC',
                        "ELEMENT_SORT_FIELD2" => "NAME",
                        "ELEMENT_SORT_ORDER2" => 'ASC',
                        "FILTER_NAME" => 'article_one_slider_good',
                        "HIDE_NOT_AVAILABLE" => "N",
                        "PAGE_ELEMENT_COUNT" => 1,
                        "LINE_ELEMENT_COUNT" => "3",
                        "PROPERTY_CODE" => array('STATUS_TOVARA'),
                        "OFFERS_LIMIT" => 0,
                        "TEMPLATE_THEME" => "",
                        "PRODUCT_SUBSCRIPTION" => "N",
                        "SHOW_DISCOUNT_PERCENT" => "N",
                        "SHOW_OLD_PRICE" => "N",
                        "MESS_BTN_BUY" => "Купить",
                        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                        "MESS_BTN_SUBSCRIBE" => "Подписаться",
                        "MESS_BTN_DETAIL" => "Подробнее",
                        "MESS_NOT_AVAILABLE" => "Нет в наличии",
                        "SECTION_URL" => "",
                        "DETAIL_URL" => "",
                        "SECTION_ID_VARIABLE" => "SECTION_ID",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "CACHE_TYPE" => project\catalog::CACHE_TYPE,
                        "CACHE_TIME" => project\catalog::CACHE_TIME,
                        "CACHE_GROUPS" => "Y",
                        "SET_META_KEYWORDS" => "N",
                        "META_KEYWORDS" => "",
                        "SET_META_DESCRIPTION" => "N",
                        "META_DESCRIPTION" => "",
                        "BROWSER_TITLE" => "-",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "DISPLAY_COMPARE" => "N",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "CACHE_FILTER" => "Y",
                        "PRICE_CODE" => array(
                            project\catalog::RASSROCHKA_PRICE_CODE,
                            project\catalog::BASE_PRICE_CODE
                        ),
                        "USE_PRICE_COUNT" => "N",
                        "SHOW_PRICE_COUNT" => "1",
                        "PRICE_VAT_INCLUDE" => "Y",
                        "CONVERT_CURRENCY" => "N",
                        "BASKET_URL" => "/personal/basket.php",
                        "ACTION_VARIABLE" => "action",
                        "PRODUCT_ID_VARIABLE" => "id",
                        "USE_PRODUCT_QUANTITY" => "N",
                        "ADD_PROPERTIES_TO_BASKET" => "Y",
                        "PRODUCT_PROPS_VARIABLE" => "prop",
                        "PARTIAL_PRODUCT_PROPERTIES" => "N",
                        "PRODUCT_PROPERTIES" => "",
                        "PAGER_TEMPLATE" => "",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Товары",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "Y",
                        "ADD_PICT_PROP" => "-",
                        "LABEL_PROP" => "-",
                        'INCLUDE_SUBSECTIONS' => "Y",
                        'SHOW_ALL_WO_SECTION' => "Y"
                    )
                );


            } ?>

        </div>

        <? if( count($arResult['PROPERTY_GOODS_VALUE']) > 1 ){ ?>

            <div class="shot-slider__prev cart-similar-slider__prev-js">
                <svg width="19" height="13" viewBox="0 0 19 13" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6.1979 12.8045C6.46575 13.0652 6.88744 13.0652 7.1553 12.8045C7.4141 12.5526 7.4141 12.1334 7.1553 11.8821L2.30074 7.15731L18.3201 7.15731C18.6935 7.15731 19 6.86843 19 6.50499C19 6.14155 18.6935 5.84328 18.3201 5.84328L2.30074 5.84328L7.1553 1.12732C7.4141 0.866628 7.4141 0.446818 7.1553 0.195519C6.88744 -0.0651731 6.46575 -0.0651731 6.1979 0.195519L0.194104 6.0388C-0.0647013 6.29068 -0.0647013 6.7099 0.194104 6.9612L6.1979 12.8045Z"></path>
                </svg>
                <span>Prev</span>
            </div>

            <div class="shot-slider__next cart-similar-slider__next-js">
                <svg width="19" height="13" viewBox="0 0 19 13" xmlns="http://www.w3.org/2000/svg">
                    <path d="M12.8021 0.19552C12.5342 -0.0651732 12.1126 -0.0651732 11.8447 0.19552C11.5859 0.447405 11.5859 0.866628 11.8447 1.11793L16.6993 5.84269H0.679892C0.306465 5.84269 0 6.13157 0 6.49501C0 6.85845 0.306465 7.15672 0.679892 7.15672H16.6993L11.8447 11.8727C11.5859 12.1334 11.5859 12.5532 11.8447 12.8045C12.1126 13.0652 12.5342 13.0652 12.8021 12.8045L18.8059 6.9612C19.0647 6.70932 19.0647 6.2901 18.8059 6.0388L12.8021 0.19552Z"></path>
                </svg>
                <span>Next</span>
            </div>

        <? } ?>

    </div>

<? } ?>