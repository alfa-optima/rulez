<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

$arItem = $arResult; ?>

<div class="p-filters-item p-filters-item-js <? if( $arItem['DISPLAY_EXPANDED'] == 'Y' ){ ?>p-filters-is-open<? } ?> filter_number_block">

    <div class="p-filters-select p-filters-select-js">
        <div class="p-filters-title"><span><?=html_entity_decode(str_replace(' (базовая)', '', $arItem['NAME']))?></span> <span class="p-filters-selected p-filters-selected-js">0</span></div>
        <div class="p-filters-angle">
            <svg height="6" width="10" fill="none">
                <use xlink:href="#icon-arr-filter"></use>
            </svg>
        </div>
    </div>

    <div class="p-filters-drop p-filters-drop-js">
        <ul class="p-filters-drop-list p-filters-group-js" data-filter-group="<?=$arItem["CODE"]?>">
            <li>
                <div class="form-row">
                    <div class="input-wrap has-prefix">
                        <span class="form-prefix">От</span>
                        <input class="form-light form-small filter___number_input" name="<?=$arItem['VALUES']['MIN']['CONTROL_NAME']?>" id="<?=$arItem['VALUES']['MIN']['CONTROL_ID']?>" value="<?=$arItem['VALUES']['MIN']['HTML_VALUE']?floor($arItem['VALUES']['MIN']['HTML_VALUE']):($arResult['PARAMS']['VALUE_FROM']?$arResult['PARAMS']['VALUE_FROM']:'')?>" type="text" placeholder="<?=floor($arItem['VALUES']['MIN']['VALUE'])?>" data-filter-default="" data-filter-tag="От" data-only-number>
                    </div>
                    <div class="input-wrap has-prefix">
                        <span class="form-prefix">До</span>
                        <input class="form-light form-small filter___number_input"  name="<?=$arItem['VALUES']['MAX']['CONTROL_NAME']?>" id="<?=$arItem['VALUES']['MAX']['CONTROL_ID']?>" value="<?=$arItem['VALUES']['MAX']['HTML_VALUE']?floor($arItem['VALUES']['MAX']['HTML_VALUE']):($arResult['PARAMS']['VALUE_TO']?$arResult['PARAMS']['VALUE_TO']:'')?>" type="text" placeholder="<?=ceil($arItem['VALUES']['MAX']['VALUE'])?>" data-filter-default="" data-filter-tag="До" data-only-number>
                    </div>
                </div>
            </li>
        </ul>

        <!--<div class="p-filters-drop-footer"> <button class="btn-reset btn-reset-js" disabled><span>Очистить фильтр</span></button> </div>-->

    </div>

</div>
