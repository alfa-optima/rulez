<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

$arItem = $arResult;
$arCur = current($arItem["VALUES"]);

$values = [];
foreach( $arItem["VALUES"] as $val => $ar ){   $values[$val] = $ar['VALUE'];   }
array_multisort($values, SORT_ASC, SORT_STRING, $arItem["VALUES"]);

$hasChecked = false;
foreach( $arItem["VALUES"] as $val => $ar ){
    $checked = false;
    if( $ar["CHECKED"] ){
        $checked = true;
    } else if( in_array($ar['FACET_VALUE'], $arResult['PARAMS']['VALUES']) ){
        $checked = true;
    } else if( in_array($ar['VALUE'], $arResult['PARAMS']['VALUES']) ){
        $checked = true;
    } else if( $ar["VALUE"] == $arResult['PARAMS']['VALUES'] ){
        $checked = true;
    }
    if( $checked ){   $hasChecked = true;   }
}

$opened = $arItem['DISPLAY_EXPANDED'] == 'Y' || $hasChecked; ?>


<div class="p-filters-item p-filters-item-js filter_list_block <? if( $opened ){ ?>p-filters-is-open<? } ?>">

    <div class="p-filters-select p-filters-select-js">
        <div class="p-filters-title"><span><?=html_entity_decode($arItem['NAME'])?></span> <span class="p-filters-selected p-filters-selected-js">0</span></div>
        <div class="p-filters-angle">
            <svg height="6" width="10" fill="none">
                <use xlink:href="#icon-arr-filter"></use>
            </svg>
        </div>
    </div>

    <div class="p-filters-drop p-filters-drop-js">

        <ul class="p-filters-drop-list p-filters-group-js item-float" data-filter-group="<?=$arItem["CODE"]?>" data-show-items="8">

            <? foreach( $arItem["VALUES"] as $val => $ar ){

                $checked = false;
                if( $ar["CHECKED"] ){
                    $checked = true;
                } else if( in_array($ar['FACET_VALUE'], $arResult['PARAMS']['VALUES']) ){
                    $checked = true;
                } else if( in_array($ar['VALUE'], $arResult['PARAMS']['VALUES']) ){
                    $checked = true;
                } else if( $ar["VALUE"] == $arResult['PARAMS']['VALUES'] ){
                    $checked = true;
                }

                $disabled = false;
                if( $ar["ELEMENT_COUNT"] == 0 && !$checked ){   $disabled = true;   }

                if( $ar["VALUE"] != '-' ){ ?>

                    <li>
                        <label class="check-label checkbox_value">
                            <input
                                class="filter___number_checkbox"
                                type="checkbox"
                                value="<? echo $ar["HTML_VALUE"] ?>"
                                name="<? echo $ar["CONTROL_NAME"] ?>"
                                id="<? echo $ar["CONTROL_ID"] ?>"
                                <?=$checked?'checked':''; ?>
                                <?=$disabled?'disabled':''?>
                                data-filter-name="<?=md5($ar["VALUE"])?>"
                            />
                            <span class="p-filters-label-text-js"><?=$ar["VALUE"]?></span>
                        </label>
                    </li>

                <? }
            } ?>

        </ul>

        <div class="p-filters-drop-footer">
            <!--<button class="btn-reset btn-reset-js" disabled><span>Очистить фильтр</span></button>-->
            <a href="#" class="btn-round_small p-filters-btn-more p-filters-btn-more-js" style="display: none;"><span>Показать все</span></a>
            <a href="#" class="btn-round_small p-filters-btn-more p-filters-btn-less-js" style="display: none;"><span>Скрыть</span></a>
        </div>

    </div>
</div>
