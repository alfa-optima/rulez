<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

$arItem = $arResult;
$arCur = current($arItem["VALUES"]);

if( is_array($arItem["VALUES"]) && count($arItem["VALUES"]) > 0 ){

    $values = [];
    foreach( $arItem["VALUES"] as $val => $ar ){   $values[$val] = $ar['VALUE'];   }
    array_multisort($values, SORT_ASC, SORT_STRING, $arItem["VALUES"]);

    foreach( $arItem["VALUES"] as $val => $ar ){
        $checked = false;
        if( $ar["CHECKED"] ){
            $checked = true;
        } else if( in_array($ar['FACET_VALUE'], $arResult['PARAMS']['VALUES']) ){
            $checked = true;
        } else if( in_array($ar['VALUE'], $arResult['PARAMS']['VALUES']) ){
            $checked = true;
        } else if( $ar["VALUE"] == $arResult['PARAMS']['VALUES'] ){
            $checked = true;
        }
    } ?>


    <div class="p-filters-item p-filters-item-js filter_list_block p-filters-is-open" style="border: none; padding: 1.9rem 0 0 0;">

        <div class="p-filters-drop p-filters-drop-js">

            <ul class="p-filters-drop-list p-filters-group-js" data-filter-group="<?=$arItem["CODE"]?>" data-show-items="8">

                <? foreach( $arItem["VALUES"] as $val => $ar ){

                    $checked = false;
                    if( $ar["CHECKED"] ){
                        $checked = true;
                    } else if( in_array($ar['FACET_VALUE'], $arParams['VALUES']) ){
                        $checked = true;
                    } else if( in_array($ar['VALUE'], $arParams['VALUES']) ){
                        $checked = true;
                    } else if( $ar["VALUE"] == $arParams['VALUES'] ){
                        $checked = true;
                    }

                    $disabled = false;
                    if( $ar["ELEMENT_COUNT"] == 0 && !$checked ){   $disabled = true;   }

                    if( $ar["VALUE"] != '-' ){ ?>

                        <li style="margin: 0 0 0 0.6rem;">
                            <label class="check-label">
                                <input
                                    class="filter___number_checkbox"
                                    type="checkbox"
                                    value="<? echo $ar["HTML_VALUE"] ?>"
                                    name="<? echo $ar["CONTROL_NAME"] ?>"
                                    id="<? echo $ar["CONTROL_ID"] ?>"
                                    <?=$checked?'checked':''?>
                                    <?=$disabled?'disabled':''?>
                                    data-filter-name="<?=md5($ar["VALUE"])?>"
                                />
                                <span class="p-filters-label-text-js"><?=strlen($ar['LABEL_TITLE'])?$ar['LABEL_TITLE']:$ar["VALUE"]?></span>
                            </label>
                        </li>

                    <? }
                } ?>

            </ul>

        </div>
    </div>

<?php } ?>