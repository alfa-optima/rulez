<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

$arItem = $arResult; ?>

<div class="p-filters-item p-filters-item-js <? if( $arItem['DISPLAY_EXPANDED'] == 'Y' ){ ?>p-filters-is-open<? } ?> filter_number_block" <? if( $arParams['SHOW_TOP_BORDER'] == 'Y' ){ ?>style="border-width: 1px 0 1px; margin-top: 20px;"<?php } ?>>

    <div class="p-filters-select p-filters-select-js">
        <div class="p-filters-title"><span><?=html_entity_decode(str_replace(' (базовая)', '', $arItem['NAME']))?></span> <span class="p-filters-selected p-filters-selected-js">0</span></div>
        <div class="p-filters-angle">
            <svg height="6" width="10" fill="none">
                <use xlink:href="#icon-arr-filter"></use>
            </svg>
        </div>
    </div>

    <div class="p-filters-drop p-filters-drop-js">
        <ul class="p-filters-drop-list p-filters-group-js" data-filter-group="<?=$arItem["CODE"]?>">
            <li>
                <div class="form-row">
                    <div class="input-wrap has-prefix">
                        <span class="form-prefix">От</span>
                        <input class="form-light form-small filter___number_input" name="<?=$arItem['VALUES']['MIN']['CONTROL_NAME']?>" id="<?=$arItem['VALUES']['MIN']['CONTROL_ID']?>" value="<?=$arItem['VALUES']['MIN']['HTML_VALUE']?floor($arItem['VALUES']['MIN']['HTML_VALUE']):($arResult['PARAMS']['PRICE_FROM']?$arResult['PARAMS']['PRICE_FROM']:'')?>" type="text" placeholder="<?=floor($arItem['VALUES']['MIN']['VALUE'])?>" data-filter-name="<?=$arItem['VALUES']['MIN']['CONTROL_NAME']?>" data-filter-from data-filter-default="" data-filter-tag-prefix="от" data-filter-unit="р." data-only-number>
                    </div>
                    <div class="input-wrap has-prefix">
                        <span class="form-prefix">До</span>
                        <input class="form-light form-small filter___number_input"  name="<?=$arItem['VALUES']['MAX']['CONTROL_NAME']?>" id="<?=$arItem['VALUES']['MAX']['CONTROL_ID']?>" value="<?=$arItem['VALUES']['MAX']['HTML_VALUE']?floor($arItem['VALUES']['MAX']['HTML_VALUE']):($arResult['PARAMS']['PRICE_TO']?$arResult['PARAMS']['PRICE_TO']:'')?>" type="text" placeholder="<?=ceil($arItem['VALUES']['MAX']['VALUE'])?>"
                         data-filter-name="<?=$arItem['VALUES']['MAX']['CONTROL_NAME']?>" data-filter-to data-filter-default="" data-filter-tag-prefix="до" data-filter-unit="р." data-only-number>
                    </div>
                </div>
            </li>
        </ul>

        <div class="p-filters-drop-footer">
            <a href="#" class="btn-round_small p-filters-btn-more p-filters-btn-more-js" style="display: none;"><span>Показать все</span></a>
            <a href="#" class="btn-round_small p-filters-btn-more p-filters-btn-less-js" style="display: none;"><span>Скрыть</span></a>
            <!--<button class="btn-reset-js btn-filters-clear" disabled><span>Очистить фильтр <em class="p-filters-selected-js"></em></span></button>-->
        </div>

        <!--<div class="p-filters-drop-footer"> <button class="btn-reset btn-reset-js" disabled><span>Очистить фильтр</span></button> </div>-->

    </div>

</div>
