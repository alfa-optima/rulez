<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$arResult = $arParams['arItem'];
$arFilter = $arParams['arFilter'];

// Наличие
if( $arResult['CODE'] == project\catalog::STATUS_PROP_CODE ){
    $newValues = [];
    foreach ( $arResult['VALUES'] as $key => $value ){
        if( trim(strtolower($value['VALUE'])) == 'в наличии' ){   $newValues[] = $value;   }
    }
    $arResult['VALUES'] = $newValues;
}

// Акция
if( $arResult['CODE'] == project\catalog::ACTION_PROP_CODE ){
    $newValues = [];
    foreach ( $arResult['VALUES'] as $key => $value ){
        if( trim(strtolower($value['VALUE'])) == 'да' ){
            $value['LABEL_TITLE'] = 'Акция';
            $newValues[] = $value;
        }
    }
    $arResult['VALUES'] = $newValues;
}

// Рулик
if( $arResult['CODE'] == project\catalog::IS_RULIK_REC_PROP_CODE ){
    $newValues = [];
    foreach ( $arResult['VALUES'] as $key => $value ){
        if( trim(strtolower($value['VALUE'])) == 'да' ){
            $value['LABEL_TITLE'] = '<b>Рулик рекомендует!</b>';
            $newValues[] = $value;
        }
    }
    $arResult['VALUES'] = $newValues;
}

$arResult['PARAMS'] = array( 'arItem' => $arResult );
if( isset($arFilter['PROP_VALUES'][$arResult['CODE']]) ){
    $arResult['PARAMS']['VALUES'] = $arFilter['PROP_VALUES'][$arResult['CODE']];

}

if( isset($arFilter['PRICE_FROM']) ){
    $arResult['PARAMS']['PRICE_FROM'] = $arFilter['PRICE_FROM'];
}
if( isset($arFilter['PRICE_TO']) ){
    $arResult['PARAMS']['PRICE_TO'] = $arFilter['PRICE_TO'];
}

if( isset($arFilter['VALUE_FROM']) ){
    $arResult['PARAMS']['VALUE_FROM'] = $arFilter['VALUE_FROM'];
}
if( isset($arFilter['VALUE_TO']) ){
    $arResult['PARAMS']['VALUE_TO'] = $arFilter['VALUE_TO'];
}

$this->IncludeComponentTemplate();