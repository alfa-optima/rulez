<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>


<!--CONTENT-->
<div class="content">

    <? if( $arResult['IS_AUTH'] == 'Y' ){ ?>

        <div class="content-align">

            <div class="content-inner article-content">

                <h1><?$APPLICATION->ShowTitle()?></h1>

                <p>В личном кабинете Вы можете проверить текущее состояние корзины, ход выполнения Ваших заказов, просмотреть или изменить личную информацию, а также подписаться на новости и другие информационные рассылки.</p>

                <h2>Личная информация</h2>
                <ul>
                    <li><a href="/personal/data/">Редактирование личных данных</a></li>
                </ul>

                <h2>Заказы</h2>
                <ul>
                    <li><a href="/basket/">Моя корзина</a></li>
                    <li><a href="/personal/orders/">Мои заказы</a></li>
                </ul>

            </div>

        </div>


        <aside class="sidebar">
            <? // personal
            $APPLICATION->IncludeComponent(
                "bitrix:menu",  "personal", // Шаблон меню
                Array(
                    "ROOT_MENU_TYPE" => "personal", // Тип меню
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "CACHE_SELECTED_ITEMS" => "N",
                    "MENU_CACHE_GET_VARS" => array(""),
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "left",
                    "USE_EXT" => "N",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N",
                    "REQUEST_URI" => $_SERVER['REQUEST_URI']
                )
            ); ?>
        </aside>



    <? } else {

        include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/auth_form.php';

    } ?>

</div>
<!--CONTENT end-->