<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;


$arResult['section'] = $arParams['section'];

$arResult['iblock'] = $arParams['iblock'];

$arResult['component'] = $arParams['component'];


$arResult['fullURL'] = $_SERVER['REQUEST_URI'];
$arResult['pureURL'] = tools\funcs::pureURL();
$arResult['arURI'] = tools\funcs::arURI( $arResult['pureURL'] );

$arResult['smart_filter_path'] = $arParams['smart_filter_path'];


$this->IncludeComponentTemplate();