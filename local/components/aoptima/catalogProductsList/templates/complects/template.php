<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$sections = [];
$catalogIblocks = project\catalog::iblocks(); 
foreach ( $catalogIblocks as $iblock ){
    $firstLevelSection = project\catalog::firstLevelCategory( $iblock['ID'] );
    $sections[$firstLevelSection['ID']] = $firstLevelSection;
} ?>


<div class="m-container p-filters-js">

    <div class="m-aside shutter--filters-js stop-remove-class">

        <div class="m-aside__header">
            Все товары
            <a href="#" class="btn-filters-close js-btn-shutter-close" title="Закрыть фильтр">
                <svg class="svg-ico-close" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 57.2 57.2">
                    <path d="M34.3 28.6L56 6.9c1.6-1.6 1.6-4.1 0-5.7 -1.6-1.6-4.1-1.6-5.7 0L28.6 22.9 6.9 1.3c-1.6-1.6-4.1-1.6-5.7 0 -1.6 1.6-1.6 4.1 0 5.7l21.7 21.6L1.3 50.3c-1.6 1.5-1.6 4.1 0 5.6 0.8 0.8 1.8 1.2 2.8 1.2s2-0.4 2.8-1.2l21.7-21.6L50.3 56c0.8 0.8 1.8 1.2 2.8 1.2s2-0.4 2.8-1.2c1.6-1.6 1.6-4.1 0-5.7L34.3 28.6z"></path>
                </svg>
            </a>
        </div>

        <div class="m-aside__align">
            <div class="m-aside__holder">

                <?php if( count($sections) > 0 ){ ?>

                    <ul class="articles-nav">

                        <? foreach( $sections as $section ){ ?>

                            <li>
                                <a href="<?=$section['SECTION_PAGE_URL']?>"><?=$section['NAME']?></a>
                                <?php if( is_array($section['SUBSECTIONS']) ){ ?>
                                    <ul>
                                        <? foreach( $section['SUBSECTIONS'] as $sub_section ){ ?>
                                            <li>
                                                <a href="<?=$sub_section['SECTION_PAGE_URL']?>"><?=$sub_section['NAME']?></a>
                                            </li>
                                        <? } ?>
                                    </ul>
                                <?php } ?>
                            </li>

                        <? } ?>

                    </ul>

                <?php } ?>

            </div>
        </div>

    </div>

    <div class="m-content">

        <div class="m-heading">
            <h1><?=$arResult['section']['NAME']?></h1>
        </div>

        <div class="m-options">
            <a href="#" class="filters-switcher btn-filters-js" title="Показать фильтры">
                <div class="p-filters-activated p-filters-activated-js hide">0</div>
                <span>Список</span>
                <em><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M3 13h2v-2H3v2zm0 4h2v-2H3v2zm0-8h2V7H3v2zm4 4h14v-2H7v2zm0 4h14v-2H7v2zM7 7v2h14V7H7zm-4 6h2v-2H3v2zm0 4h2v-2H3v2zm0-8h2V7H3v2zm4 4h14v-2H7v2zm0 4h14v-2H7v2zM7 7v2h14V7H7z" /></svg></em>
            </a>
        </div>


        <? // adv_banner
        $APPLICATION->IncludeComponent(
            "aoptima:advBanner", "", array('type' => 'catalog_top')
        ); ?>


        <? $sortField = project\catalog::getSortField();
        $sortOrder = project\catalog::getSortOrder();

        $GLOBALS[project\catalog::FILTER_NAME]['SECTION_ID'] = $arResult['section']['ID'];
        $GLOBALS[project\catalog::FILTER_NAME]['INCLUDE_SUBSECTIONS'] = 'Y';
        $GLOBALS[project\catalog::FILTER_NAME]['PROPERTY_STATUS_TOVARA'] = '_%';

        $pageNumber = intval($_GET['PAGEN_1'])>0?intval($_GET['PAGEN_1']):1;

        $APPLICATION->IncludeComponent(
            "bitrix:catalog.section", "catalog_complects",
            Array(
                "IS_ADMIN" => $USER->IsAdmin()?"Y":"N",
                "PAGE_NUMBER" => $pageNumber,
                "HAS_NEXT" => project\catalog::hasNextProducts(
                    $GLOBALS[project\catalog::FILTER_NAME],
                    project\catalog::COMPLECTS_CNT
                ),
                "IBLOCK_TYPE" => project\catalog::IBLOCK_TYPE,
                "IBLOCK_ID" => $arResult['section']['IBLOCK_ID'],
                "SECTION_USER_FIELDS" => array(),
                "ELEMENT_SORT_FIELD" => 'PROPERTY_STATUS_TOVARA',
                "ELEMENT_SORT_ORDER" => 'ASC',
                "ELEMENT_SORT_FIELD2" => project\catalog::getCatSortFieldCode($sortField),
                "ELEMENT_SORT_ORDER2" => $sortOrder,
                "FILTER_NAME" => project\catalog::FILTER_NAME,
                "HIDE_NOT_AVAILABLE" => "N",
                "PAGE_ELEMENT_COUNT" => project\catalog::COMPLECTS_CNT,
                "LINE_ELEMENT_COUNT" => "3",
                "PROPERTY_CODE" => array('STATUS_TOVARA'),
                "OFFERS_LIMIT" => 0,
                "TEMPLATE_THEME" => "",
                "PRODUCT_SUBSCRIPTION" => "N",
                "SHOW_DISCOUNT_PERCENT" => "N",
                "SHOW_OLD_PRICE" => "N",
                "MESS_BTN_BUY" => "Купить",
                "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                "MESS_BTN_SUBSCRIBE" => "Подписаться",
                "MESS_BTN_DETAIL" => "Подробнее",
                "MESS_NOT_AVAILABLE" => "Нет в наличии",
                "SECTION_URL" => "",
                "DETAIL_URL" => "",
                "SECTION_ID_VARIABLE" => "SECTION_ID",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => project\catalog::COMPLECTS_CACHE_TYPE,
                "CACHE_TIME" => project\catalog::COMPLECTS_CACHE_TIME,
                "CACHE_GROUPS" => "Y",
                "SET_META_KEYWORDS" => "N",
                "META_KEYWORDS" => "",
                "SET_META_DESCRIPTION" => "N",
                "META_DESCRIPTION" => "",
                "BROWSER_TITLE" => "-",
                "ADD_SECTIONS_CHAIN" => "N",
                "DISPLAY_COMPARE" => "N",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "CACHE_FILTER" => "Y",
                "PRICE_CODE" => array(
                    project\catalog::RASSROCHKA_PRICE_CODE,
                    project\catalog::BASE_PRICE_CODE
                ),
                "USE_PRICE_COUNT" => "N",
                "SHOW_PRICE_COUNT" => "1",
                "PRICE_VAT_INCLUDE" => "Y",
                "CONVERT_CURRENCY" => "N",
                "BASKET_URL" => "/personal/basket.php",
                "ACTION_VARIABLE" => "action",
                "PRODUCT_ID_VARIABLE" => "id",
                "USE_PRODUCT_QUANTITY" => "N",
                "ADD_PROPERTIES_TO_BASKET" => "Y",
                "PRODUCT_PROPS_VARIABLE" => "prop",
                "PARTIAL_PRODUCT_PROPERTIES" => "N",
                "PRODUCT_PROPERTIES" => "",
                "PAGER_TEMPLATE" => "",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Товары",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "Y",
                "ADD_PICT_PROP" => "-",
                "LABEL_PROP" => "-",
                'SHOW_ALL_WO_SECTION' => "Y"
            )
        ); ?>


        <? // Описание
        if( strlen($arResult['section']['DESCRIPTION']) > 0 ){ ?>
            <div class="m-description"><?=$arResult['section']['~DESCRIPTION']?></div>
        <? } ?>


    </div>
</div>

