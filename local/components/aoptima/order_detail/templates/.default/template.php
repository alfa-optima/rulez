<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>

<div class="content">

    <? if( $arResult['IS_AUTH'] == 'Y' ){ ?>

        <div class="content-align">

            <div class="content-inner article-content">

                <div class="order-details">

                    <? if( $arResult['ORDER'] ){ ?>

                        <div class="od-header order-details__header">
                            <h1 class="h1 od-header__title">Заказ №<?=$arResult['ORDER']['ID']?> от <?=ConvertDateTime($arResult['ORDER']['order']->getDateInsert(), "DD.MM.YYYY", "ru")?></h1>
                            <div class="od-header__status <?=$arResult['ORDER']['status']['CLASS']?>"><?=$arResult['ORDER']['status']['NAME']?></div>
                        </div>

                        <div class="order-details__body">

                            <div class="order-details__content">
                                <div class="od-address i-list">

                                    <div class="i-list__item">
                                        <i class="i-list__icon icon-i-list-user-color"></i>
                                        <div class="i-list__content">
                                            <h3>Покупатель:</h3>
                                            <p><?=$arResult['ORDER']['DATA']['NAME']?></p>
                                            <p><?=$arResult['ORDER']['DATA']['EMAIL']?></p>
                                            <p><?=$arResult['ORDER']['DATA']['PHONE']?></p>
                                        </div>
                                    </div>

<!--                                    <div class="i-list__item">-->
<!--                                        <i class="i-list__icon icon-i-list-check-color"></i>-->
<!--                                        <div class="i-list__content">-->
<!--                                            <h3>Дата доставки</h3>-->
<!--                                            <p>20 мая 2019 г.</p>-->
<!--                                        </div>-->
<!--                                    </div>-->

                                    <div class="i-list__item">
                                        <div class="i-list__content">
                                            <h3>Вариант доставки:</h3>
                                            <p><?=$arResult['ORDER']['DS']['NAME']?></p>
                                        </div>
                                    </div>

                                    <? if( strlen($arResult['ORDER']['DATA']['ADDRESS']) > 0 ){ ?>
                                        <div class="i-list__item">
                                            <i class="i-list__icon icon-i-list-adr-color"></i>
                                            <div class="i-list__content">
                                                <h3>Адрес для доставки:</h3>
                                                <p><?=$arResult['ORDER']['DATA']['ADDRESS']?></p>
                                                <? if( strlen($arResult['ORDER']['DATA']['KV']) > 0 ){ ?>
                                                    <p>Квартира: <?=$arResult['ORDER']['DATA']['KV']?></p>
                                                <? } ?>
                                                <? if( strlen($arResult['ORDER']['DATA']['FLOOR']) > 0 ){ ?>
                                                    <p>Этаж: <?=$arResult['ORDER']['DATA']['FLOOR']?></p>
                                                <? } ?>
                                            </div>
                                        </div>
                                    <? } ?>


                                    <div class="i-list__item">
                                        <div class="i-list__content">
                                            <h3>Вариант оплаты:</h3>
                                            <p><?=$arResult['ORDER']['PS']['NAME']?></p>
                                        </div>
                                    </div>


<!--                                    <div class="i-list__item">-->
<!--                                        <i class="i-list__icon icon-i-list-receipt-color"></i>-->
<!--                                        <div class="i-list__content">-->
<!--                                            <h3>Электронный чек</h3>-->
<!--                                            <p><a href="#">Посмотреть</a></p>-->
<!--                                        </div>-->
<!--                                    </div>-->

                                </div>
                            </div>

                            <div class="order-details__aside">
                                <div class="summary order-details__summary">
                                    <div class="summary__item">
                                        <div>Товары (<?=round($arResult['ORDER']['BASKET_CNT'], 0)?>)</div>
                                        <div><?=$arResult['ORDER']['SUM_FORMAT']?> р.</div>
                                    </div>
                                    <div class="summary__item">
                                        <div>Доставка</div>
                                        <div><mark><?=$arResult['ORDER']['DELIVERY_PRICE']>0?$arResult['ORDER']['DELIVERY_PRICE_FORMAT']:'бесплатно'?></mark></div>
                                    </div>
                                    <div class="summary__total">
                                        <div>Итого</div>
                                        <div><?=$arResult['ORDER']['TOTAL_SUM_FORMAT']?> р.</div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    <? } else { ?>

                        <div class="od-header order-details__header">
                            <h1 class="h1 od-header__title">Заказ не найден</h1>
                            <div class="od-header__add">У Вас нет заказа с таким номером</div>
                        </div>

                    <? } ?>

                </div>

            </div>

        </div>

        <aside class="sidebar">
            <? // personal
            $APPLICATION->IncludeComponent(
                "bitrix:menu",  "personal", // Шаблон меню
                Array(
                    "ROOT_MENU_TYPE" => "personal", // Тип меню
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "CACHE_SELECTED_ITEMS" => "N",
                    "MENU_CACHE_GET_VARS" => array(""),
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "left",
                    "USE_EXT" => "N",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N",
                    "REQUEST_URI" => $_SERVER['REQUEST_URI']
                )
            ); ?>
        </aside>


    <? } else {

        include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/auth_form.php';

    } ?>

</div>

