<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('sale');
use Bitrix\Sale;
use Bitrix\Sale\Internals;

\Bitrix\Main\Loader::includeModule('aoptima.tools');  use AOptima\Tools as tools;
\Bitrix\Main\Loader::includeModule('aoptima.project');  use AOptima\Project as project;

function getPropValue( $props, $prop_code ){
    foreach ( $props as $prop ){
        if( $prop['CODE'] == $prop_code ){
            return $prop['VALUE'][array_keys($prop['VALUE'])[0]];
        }
    }
    return false;
}

$arResult['IS_AUTH'] = $USER->IsAuthorized()?'Y':'N';

if( $arResult['IS_AUTH'] == 'Y' ){
    
    $arResult['ORDER_ID'] = tools\funcs::arURI(tools\funcs::pureURL())[3];
    
    // Получаем заказ
    $filter_array = array(
        'USER_ID' => $USER->GetID(),
        'ID' => $arResult['ORDER_ID']
    );
    $orders = \Bitrix\Sale\OrderTable::getList(array(
        'filter' => $filter_array,
        'order' => array('ID' => 'DESC'),
        'limit' => 1
    ));
    if ( $order = $orders->fetch() ){

        $arResult['ORDER'] = $order;
        $arResult['ORDER']['order'] = Sale\Order::load($order['ID']);

        $APPLICATION->AddChainItem( 'Заказ №'.$arResult['ORDER']['ID'], '/personal/orders/'.$arResult['ORDER']['ID'].'/' );
        $APPLICATION->SetPageProperty("title", 'Заказ №'.$arResult['ORDER']['ID']);

        $status_code = 'A';
        if( $arResult['ORDER']['order']->isPaid() ){            $status_code = 'P';     }
        if( $arResult['ORDER']['ALLOW_DELIVERY'] == 'Y' ){      $status_code = 'D';     }
        if( $arResult['ORDER']['STATUS_ID'] == 'F' ){           $status_code = 'Z';     }
        if( $arResult['ORDER']['CANCELED'] == 'Y' ){            $status_code = 'C';     }

        $arResult['ORDER']['status'] = project\order::$statuses[$status_code];

        $propertyCollection = $arResult['ORDER']['order']->getPropertyCollection();
        $arProps = $propertyCollection->getArray();
        $arResult['ORDER']['DATA']['NAME'] = getPropValue($arProps['properties'],  'NAME');
        $arResult['ORDER']['DATA']['EMAIL'] = getPropValue($arProps['properties'],  'EMAIL');
        $arResult['ORDER']['DATA']['PHONE'] = getPropValue($arProps['properties'],  'PHONE');
        $arResult['ORDER']['DATA']['ADDRESS'] = getPropValue($arProps['properties'],  'ADDRESS');
        $arResult['ORDER']['DATA']['KV'] = getPropValue($arProps['properties'],  'KV');
        $arResult['ORDER']['DATA']['FLOOR'] = getPropValue($arProps['properties'],  'FLOOR');


        $paymentCollection = $arResult['ORDER']['order']->getPaymentCollection();
        foreach ($paymentCollection as $payment) {
            $arResult['ORDER']['PS_ID'] = $payment->getPaymentSystemId();
            $arResult['ORDER']['PS'] = project\ps::getByID($arResult['ORDER']['PS_ID']);
        }


        $shipmentCollection = $arResult['ORDER']['order']->getShipmentCollection();
        foreach ($shipmentCollection as $shipment) {
            $arResult['ORDER']['DS_ID'] = $shipment->getDeliveryId();
            $arResult['ORDER']['DS'] = project\ds::getByID($arResult['ORDER']['DS_ID']);
        }


        $arResult['ORDER']['DELIVERY_PRICE'] = $arResult['ORDER']['order']->getDeliveryPrice();
        $arResult['ORDER']['DELIVERY_PRICE_FORMAT'] = number_format($arResult['ORDER']['DELIVERY_PRICE'], project\catalog::PRICE_ROUND, ",", " ");

        $arResult['ORDER']['TOTAL_SUM'] = $arResult['ORDER']['order']->getPrice();
        $arResult['ORDER']['TOTAL_SUM_FORMAT'] = number_format($arResult['ORDER']['TOTAL_SUM'], project\catalog::PRICE_ROUND, ",", " ");

        $arResult['ORDER']['SUM'] = $arResult['ORDER']['TOTAL_SUM'] - $arResult['ORDER']['DELIVERY_PRICE'];
        $arResult['ORDER']['SUM_FORMAT'] = number_format($arResult['ORDER']['SUM'], project\catalog::PRICE_ROUND, ",", " ");


        $arResult['ORDER']['BASKET_CNT'] = 0;
        $basket = $arResult['ORDER']['order']->getBasket();
        foreach ( $basket as $basketItem ){
            $arResult['ORDER']['BASKET_CNT'] += $basketItem->getQuantity();
        }



    }


        
        
}

$this->IncludeComponentTemplate();