<?php

use Bitrix\Main\EventManager;




$eventManager = EventManager::getInstance();

if(
    \Bitrix\Main\Loader::includeModule('aoptima.project')
    &&
    \Bitrix\Main\Loader::includeModule('catalog')
){

    EventManager::getInstance()->addEventHandler(
        "catalog",
        "\Bitrix\Catalog\Price::OnAfterAdd",
        array(
            "AOptima\\Project\\handlers",
            "OnPriceAdd"
        )
    );
    EventManager::getInstance()->addEventHandler(
        "catalog",
        "\Bitrix\Catalog\Price::OnAfterUpdate",
        array(
            "AOptima\\Project\\handlers",
            "OnPriceUpdate"
        )
    );
    EventManager::getInstance()->addEventHandler(
        "catalog",
        "\Bitrix\Catalog\Price::OnBeforeDelete",
        array(
            "AOptima\\Project\\handlers",
            "OnBeforePriceDelete"
        )
    );


}




AddEventHandler("catalog", "OnProductAdd", "OnProductUpdateHandler");
AddEventHandler("catalog", "OnProductUpdate", "OnProductUpdateHandler");
function OnProductUpdateHandler($ID, $arFields){
    \Bitrix\Main\Loader::includeModule('aoptima.tools');
    \Bitrix\Main\Loader::includeModule('aoptima.project');
    // Комплекты
    $el = \AOptima\Tools\el::info($ID);
    if( $el['IBLOCK_ID'] == \AOptima\Project\complect::IBLOCK_ID ){
        $items = \AOptima\Project\complect::get_items($el['ID']);
        $res = \AOptima\Project\complect::save( $el['ID'], $items );
    }
}


if (!CModule::IncludeModule("sale")) return;

use Bitrix\Main;
use Bitrix\Sale;
use Bitrix\Main\Context,
    Bitrix\Currency\CurrencyManager,
    Bitrix\Sale\Order,
    Bitrix\Sale\SaleBasket,
    Bitrix\Sale\Delivery,
    Bitrix\Sale\PaySystem;

\Bitrix\Main\EventManager::getInstance()->addEventHandler('sale',
    'OnSaleOrderSaved',
    ['Handler', 'OnSaleOrderSaved']
//    'OnSaleStatusOrderChange',
//    ['Handler', 'OnSaleStatusOrderChange']
);



class Handler {

    const SHIPPING_LIST = [
        6 => 220,
        3 => 222,
        4 => 224
    ];

    const PAYMENT_LIST = [
        3 => 444,
        5 => 232,
        7 => 450,
        8 => 234,
        9 => 438,
        11 => 440,
        12 => 446,
        13 => 448,
        14 => 452,
        15 => 236,
        16 => 238,
        17 => 240,
        18 => 434,
        19 => 436,
        20 => 442,
    ];

    const UF_CATEGORY = 'UF_CRM_1577693018';

    const CATEGORY_NOTE = '550';
    const CATEGORY_XIAOMI = '552';
    const CATEGORY_GARDEN = '554';
    const CATEGORY_OTHER = '902';

    function OnSaleOrderSaved($event)
    {

        $order = $event->getParameter("ENTITY");
        $isNew = $event->getParameter("IS_NEW");

        if ($isNew) {
            if (in_array($order->getField('STATUS_ID'), ['N'])) {

                $propertyCollection = $order->getPropertyCollection();

//            var_dump($propertyCollection);
//            exit();

                $city = $propertyCollection->getItemByOrderPropertyId(9)->getField('VALUE');
                $street = $propertyCollection->getItemByOrderPropertyId(4)->getField('VALUE');
                $flat = $propertyCollection->getItemByOrderPropertyId(5)->getField('VALUE');
                $floor = $propertyCollection->getItemByOrderPropertyId(6)->getField('VALUE');
                $comment = $propertyCollection->getItemByOrderPropertyId(7)->getField('VALUE');
                $phone = $propertyCollection->getItemByOrderPropertyId(1)->getField('VALUE');
                $name = $propertyCollection->getItemByOrderPropertyId(2)->getField('VALUE');
                $email = $propertyCollection->getItemByOrderPropertyId(3)->getField('VALUE');

                $curl = curl_init();
                $products = [];

                foreach ($order->getPaymentCollection() as $payment) {
                    $customPayment = $payment->getPaymentSystemId();
                    break;
                }

                foreach ($order->getShipmentCollection() as $delivery) {
                    $customShipping = $delivery->getDeliveryId();
                    break;
                }

                $productsCategories = array(
                    '550' => array(1573, 1363, 1623, 1568, 1574, 1536),
                    '552' => array(1632, 1634, 1637, 1640, 1642, 1643, 1457, 1463, 1465, 1466,
                        1471, 1473, 1475, 1478, 1606, 1301, 1361)
                );
                $category = '554';
                $dbBasketItems = CSaleBasket::GetList(array(), array("ORDER_ID" => $order->getId()));

                CModule::IncludeModule("iblock");
                $analogProducts = [];

                $sections = [];
                while ($arItems = $dbBasketItems->Fetch()) {
                    $sections[] = self::getProduct($arItems['PRODUCT_ID']);
                    $element = CIBlockElement::GetByID($arItems['PRODUCT_ID'])->fetch();
                    if ($category === '554' || $category === '552') {
                        if (in_array($element['IBLOCK_SECTION_ID'], $productsCategories['550'])) {
                            $category = '550';
                        } elseif (in_array($element['IBLOCK_SECTION_ID'], $productsCategories['552'])) {
                            $category = '552';
                        }
                    }

                    $products[] = [
                        "PRODUCT_ID" => $arItems['ID'],
                        "PRODUCT_NAME" => $arItems['NAME'],
                        "PRICE" => $arItems['PRICE'],
                        "QUANTITY" => $arItems['QUANTITY'],
                    ];
                    $analogProducts[] = $arItems['NAME'];
                }

                $queryData = [
                    'fields' => [
                        self::UF_CATEGORY => self::getCategoryByArray($sections),
                        "TITLE" => "Заказ в интернет-магазине rulezzz # " . $order->getId(),// . " [ТЕСТОВЫЙ]",
                        "TYPE_ID" => "SALE",
                        "STAGE_ID" => "NEW",
                        "NAME" => $name,
                        "EMAIL" => [
                            ["VALUE" => $email, "VALUE_TYPE" => "WORK"]
                        ],
                        "CATEGORY_ID" => 0,
                        "IS_NEW" => "Y",
                        "OPENED" => "Y",
                        "PROBABILITY" => 20,
                        "CURRENCY_ID" => $order->getCurrency(),
                        "OPPORTUNITY" => $order->getPrice(),
                        "SOURCE_ID" => 1, //источник сайт Rulezzz
                        "PHONE" => [
                            ["VALUE" => $phone, "VALUE_TYPE" => "WORK"]
                        ],
                        "COMMENTS" => $comment . ". \n" . $floor . "этаж",
                        "UF_CRM_1554301260" => self::PAYMENT_LIST[$customPayment],
                        "UF_CRM_1554298233" => self::SHIPPING_LIST[$customShipping],
                        "UF_CRM_1577693018" => $category, //Категория товаров
                        "ADDRESS" => $street,
                        "ADDRESS_2" => $flat,
                        "ADDRESS_CITY" => $city,
                        "UF_CRM_1580205419" => $analogProducts,
                        "UF_CRM_1584708066" => $order->getId()
//                    "ADDRESS_POSTAL_CODE" => ,
//                    "ADDRESS_REGION" => ,
//                    "ADDRESS_PROVINCE" => ,
//                    "ADDRESS_COUNTRY" => ,
//                    "UF_CRM_1568705434491" => $city . ' ' . $street . ' ' . $flat . ' ' . $floor
                    ],
                    'params' => []
                ];


                $queryUrl = 'https://ctm.bitrix24.by/rest/70/bloltlt2cppcpei2/crm.lead.add';


                curl_setopt_array($curl, array(
                    CURLOPT_SSL_VERIFYPEER => 0,
                    CURLOPT_POST => 1,
                    CURLOPT_HEADER => 0,
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => $queryUrl,
                    CURLOPT_POSTFIELDS => http_build_query($queryData),
                ));

                $result = json_decode(curl_exec($curl), 1);
                $dealId = $result['result'];


                $queryUrl = 'https://ctm.bitrix24.by/rest/70/bloltlt2cppcpei2/crm.lead.productrows.set';

                $queryData = [
                    "id" => $dealId,
                    "rows" => $products,
                ];

                curl_setopt_array($curl, array(
                    CURLOPT_SSL_VERIFYPEER => 0,
                    CURLOPT_POST => 1,
                    CURLOPT_HEADER => 0,
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => $queryUrl,
                    CURLOPT_POSTFIELDS => http_build_query($queryData),
                ));
                $result = curl_exec($curl);
                $result = json_decode($result, 1);

//            var_dump($result);
//            exit();
            }
        }
        return new \Bitrix\Main\EventResult(
            \Bitrix\Main\EventResult::SUCCESS
        );
    }

    public static function getProduct($id)
    {

        $curl = curl_init();

        $queryUrl = 'https://ctm.bitrix24.by/rest/70/bloltlt2cppcpei2/crm.product.get';

        $queryData = [
            'id' => $id
        ];

        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $queryUrl,
            CURLOPT_POSTFIELDS => http_build_query($queryData),
        ));

        $result = curl_exec($curl);
        $result = json_decode($result, 1);

        return $result['result']['SECTION_ID'];
    }

    /**
     * @param $array - массив section_id всех товаров
     * @return mixed|string
     */
    public static function getCategoryByArray($array)
    {
        $returnedeCategory = self::CATEGORY_OTHER;
        foreach ($array as $sectionId){
            $categoryId = self::getCategoryId($sectionId);
            if($categoryId < $returnedeCategory){
                $returnedeCategory = $categoryId;
            }
        }

        return $categoryId;
    }

    public static function getCategoryId($sectionId)
    {
        $sections = [
            824 => self::CATEGORY_NOTE,  // Ноутбуки
            778 => self::CATEGORY_NOTE,  // Принтеры
            820 => self::CATEGORY_NOTE,  // Моники
            838 => self::CATEGORY_NOTE,  // Планшеты
            1072 => self::CATEGORY_NOTE, // Телеки

            1238 => self::CATEGORY_XIAOMI, // Стулья
            852  => self::CATEGORY_XIAOMI, // Колонки
            1262 => self::CATEGORY_XIAOMI, // Сумки

            1050 => self::CATEGORY_GARDEN, // Триммеры
            1030 => self::CATEGORY_GARDEN, // Газонокосилки
            1040 => self::CATEGORY_GARDEN, // Мойки высокого давления
            880  => self::CATEGORY_GARDEN, // Эхолоты
            936  => self::CATEGORY_GARDEN, // Бетономешалки
            938  => self::CATEGORY_GARDEN, // Виброплиты
        ];

        if(isset($sections[$sectionId])){
            return $sections[$sectionId];
        } else{
            return self::CATEGORY_OTHER;
        }
    }
}

