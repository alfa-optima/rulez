<?php

$_SERVER["DOCUMENT_ROOT"] = "/home/bitrix/www";
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
set_time_limit(0);


\Bitrix\Main\Loader::includeModule('aoptima.project');
\Bitrix\Main\Loader::includeModule('aoptima.tools');


$catalog_iblocks = array_keys( \AOptima\Project\catalog::iblocks() );
\AOptima\Tools\facet_index::start_reindex( $catalog_iblocks );



require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");