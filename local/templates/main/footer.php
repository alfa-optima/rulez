<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Page\Asset;   $asset = Asset::getInstance();
\Bitrix\Main\Loader::includeModule( 'aoptima.tools' ); use AOptima\Tools as tools;
\Bitrix\Main\Loader::includeModule('aoptima.project'); use AOptima\Project as project; ?>

        <? if( !tools\funcs::isMain() ){ ?>

                <? if( project\funcs::isContentPage() ){ ?>

                                <? if( tools\funcs::arURI()[1] == 'vozvrat-tovara' ){

                                    // vozvrat_form
                                    $APPLICATION->IncludeComponent(
                                        "aoptima:vozvrat_form", ""
                                    );

                                } ?>

                            </section>
                        </div>

                        <aside class="sidebar">
                            <div class="sidebar__holder">

                                <? // content_left_menu
                                $APPLICATION->IncludeComponent(
                                    "bitrix:menu",  "content_left_menu", // Шаблон меню
                                    Array(
                                        "ROOT_MENU_TYPE" => "content_left", // Тип меню
                                        "MENU_CACHE_TYPE" => "A",
                                        "MENU_CACHE_TIME" => "3600",
                                        "MENU_CACHE_USE_GROUPS" => "N",
                                        "CACHE_SELECTED_ITEMS" => "N",
                                        "MENU_CACHE_GET_VARS" => array(""),
                                        "MAX_LEVEL" => "1",
                                        "CHILD_MENU_TYPE" => "left",
                                        "USE_EXT" => "N",
                                        "DELAY" => "N",
                                        "ALLOW_MULTI_SELECT" => "N"
                                    )
                                ); ?>

                            </div>
                        </aside>

                        <div class="article-share-layout">

                            <div class="article-share share42init"></div>

                            <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/share42/share42.js"></script>

                        </div>

                    </div>


                <? } ?>

            </div>

        <? } ?>

    </div>


    <? // _footer.php
    include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH."/include/_footer.php"; ?>


    <? // _site_modals
    include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH."/include/_site_modals.php"; ?>


 
    <? // JS
    $asset->addJs(SITE_TEMPLATE_PATH."/js/jquery.cookie.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/json.min.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/noty/packaged/jquery.noty.packaged.min.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/my_f.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/libs.min.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/common.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/my.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/shop.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/personal.js"); ?>

    <? if( $_SESSION['success_register'] == 'Y' ){
        $_SESSION['success_register'] = 'N'; ?>
        <script>
        $(document).ready(function(){
            show_message('Вы успешно зарегистрированы!', 'success');
        })
        </script>
    <? } ?>

    <script>
    (function(w,d,u){
        var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
        var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
    })(window,document,'https://cdn.bitrix24.by/b9123853/crm/tag/call.tracker.js');
    </script>

</body>
</html>