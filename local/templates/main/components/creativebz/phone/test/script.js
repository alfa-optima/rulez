function creativebzPhoneLoad(flag){
    if(!window.creativePhones)
        window.creativePhones = {counterLoaded: false, phonesLoaded: false};
    if(flag)
        window.creativePhones[flag] = true;

    if(window.creativePhones.counterLoaded && window.creativePhones.phonesLoaded && document.readyState == 'complete'){
        var yaCounter = 'yaCounter'+window.creativePhones.yaCounter;
        yaCounter = typeof window[yaCounter] == 'object' ? window[yaCounter] : false;
        for(var goal in window.creativePhones.goals){
            if(window.creativePhones.goals.hasOwnProperty(goal) && !window.creativePhones.goals[goal]){
                if(yaCounter) {
                    yaCounter.reachGoal(goal);
                }
                if(typeof ga == "function"){
                    ga('send', 'event', 'creativebz', goal);
                }
                window.creativePhones.goals[goal] = true;
            }
        }
        delete window.creativePhones;
    }
}

if(window.yandex_metrika_callbacks === undefined)
    window.yandex_metrika_callbacks = [];
window.yandex_metrika_callbacks.push(function(){
    setTimeout(function(){
        creativebzPhoneLoad('counterLoaded');
    }, 100); // wait while yaCounter created on next callback item call
});

if (window.frameCacheVars !== undefined){
    BX.addCustomEvent("onFrameDataReceived", function(){ creativebzPhoneLoad('phonesLoaded'); });
} else {
    BX.ready(function(){ creativebzPhoneLoad('phonesLoaded'); });
}

// wait for ga load
window.addEventListener('load', function(){
    creativebzPhoneLoad();
});
