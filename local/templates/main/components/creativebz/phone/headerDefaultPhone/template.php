<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponentTemplate $this */
$frame = $this->createFrame()->begin('');

//echo "<pre>"; print_r( $arParams ); echo "</pre>";

$phone = $arResult['PHONES'][$arResult['PHONE_ID']];
$clickCode = '';
if(!empty($phone['PROPERTY_GOAL_NAME_VALUE'])){
    $clickCode = "onclick=\"".
        (empty($arResult['YA_COUNTER_ID']) ? '' :
            "yaCounter{$arResult['YA_COUNTER_ID']}.reachGoal('{$phone['PROPERTY_GOAL_NAME_VALUE']}_click');").
        " if(typeof ga == 'function')ga('send', 'event', 'creativebz', '{$phone['PROPERTY_GOAL_NAME_VALUE']}_click');\"";
}

//if ($arResult['CLASS']) {
//    $clickCode .= ' class="'.$arResult['CLASS'].'"';
//}

if ($arResult['ID']) {
    $clickCode .= ' id="'.$arResult['ID'].'"';
}

$phone = strip_tags($phone['NAME']);
$phone = preg_replace( '/^\s*\+{0,1}\s*375\s*\([0-9]+\)\s *(.+)/', '$1', $phone );

if( !empty($phone['PROPERTY_LINK_VALUE']) ){?>

    <a href="<?=strpos($phone['PROPERTY_LINK_VALUE'], '@') ? 'mailto' : 'tel'?>:<?=$phone['PROPERTY_LINK_VALUE']?>" <?=
    $clickCode?>>

        <? foreach($arParams["ICONS"] as $css_class){ ?>
            <i class="<?=$css_class?>">&nbsp;</i>
        <? } ?>

        <span><strong><?=$phone?></strong></span>
    </a>

<? } else { ?>

    <a href="tel:+<?=preg_replace('/[^0-9]/', '', $phone['NAME'])?>" <?=
    $clickCode?>>

        <? foreach($arParams["ICONS"] as $css_class){ ?>
            <i class="<?=$css_class?>">&nbsp;</i>
        <? } ?>

        <span><strong><?=$phone?></strong></span>
    </a>

<? }

if($useCounter) { ?>

    <script>
        <?if(!isset($GLOBALS['CREATIVEBZ_PHONE'])):?>
            window.creativePhones = {
                yaCounter: <?=$arResult['YA_COUNTER_ID']?>,
                goals: {},
                counterLoaded: window.creativePhones ? window.creativePhones.counterLoaded : false,
                phonesLoaded: window.creativePhones ? window.creativePhones.phonesLoaded : false
            };
        <?endif?>
        window.creativePhones.goals['<?=$phone['PROPERTY_GOAL_NAME_VALUE']?>'] = false;
    </script>

    <? $GLOBALS['CREATIVEBZ_PHONE'] = true;

}

$frame->end();