<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$arFilter = $arParams['arFilter'];
$this->setFrameMode(true);

$statusProp = false;
$actionProp = false;
$rulikProp = false;
foreach( $arResult['ITEMS'] as $key => $arItem ){
    if( $arItem['CODE'] == project\catalog::STATUS_PROP_CODE ){
        $statusProp = $arItem;
    }
    if( $arItem['CODE'] == project\catalog::ACTION_PROP_CODE ){
        $actionProp = $arItem;
    }
    if( $arItem['CODE'] == project\catalog::IS_RULIK_REC_PROP_CODE ){
        $rulikProp = $arItem;
    }
}

$sectionFilterProps = $arParams['~SECTION_FILTER_PROPS'];
if( count($sectionFilterProps) > 0 ){
    $iblockProps = tools\prop::getList($arParams['IBLOCK_ID']);
    foreach( $arResult['ITEMS'] as $key => $arItem ){
        if( !isset($arItem["PRICE"]) ){
            $propXMLID = false;
            foreach ( $iblockProps as $propID => $iblockProp ){
                if( $arItem['ID'] == $propID ){    $propXMLID = $iblockProp['XML_ID'];     }
            }
            if(  strlen($propXMLID) > 0  &&  !in_array($propXMLID, $sectionFilterProps)  ){
                unset( $arResult['ITEMS'][$key] );
            }
        }
    }
} ?>


<?php if( $USER->IsAdmin() ){ ?>
    <?php //echo "<pre>"; print_r( $arResult['ITEMS'] ); echo "</pre>"; ?>
<?php } ?>

<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smartfilter">

    <? foreach($arResult["HIDDEN"] as $arItem){ ?>
        <input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" />
    <? } ?>

    <!--filters-->
    <!--Имя тега берется с аттрибута data-filter-tag-->
    <!--Если есть элемент с классом .p-filters-label-text-js, то именем тега будет контент этого элемента-->
    <!--Если есть data-filter-tag, то тег берется с этого аттрибута-->

    <!--arrow filter icon svg-->
    <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
        <symbol id="icon-arr-filter" viewBox="0 0 10 6">
            <path d="M1 1L5 5L9 1" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
        </symbol>
    </svg>
    <!--arrow filter svg end-->

    <div class="p-filters-list">

        <?php // Галочка Рулик
        if( $rulikProp ){
            $APPLICATION->IncludeComponent(
                "aoptima:filterItem", "single_checkbox",
                [ 'arFilter' => $arFilter, 'arItem' => $rulikProp ]
            );
        } ?>

        <?php // Галочка в наличии
        if( $statusProp ){
            $APPLICATION->IncludeComponent(
                "aoptima:filterItem", "single_checkbox",
                [ 'arFilter' => $arFilter, 'arItem' => $statusProp ]
            );
        } ?>

        <?php // Галочка Акция
        if( $actionProp ){
            $APPLICATION->IncludeComponent(
                "aoptima:filterItem", "single_checkbox",
                [ 'arFilter' => $arFilter, 'arItem' => $actionProp ]
            );
        } ?>

        <? // Цена
        foreach( $arResult['ITEMS'] as $key => $arItem ){
            if( isset($arItem["PRICE"]) ){

                if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0)
                    continue;

                $params = array( 'arItem' => $arItem );
                if( isset($arFilter['PRICE_FROM']) ){
                    $params['PRICE_FROM'] = $arFilter['PRICE_FROM'];
                }
                if( isset($arFilter['PRICE_TO']) ){
                    $params['PRICE_TO'] = $arFilter['PRICE_TO'];
                }

                // filterItem
                $APPLICATION->IncludeComponent(
                    "aoptima:filterItem", "price",
                    [ 'arFilter' => $arFilter, 'arItem' => $arItem, 'SHOW_TOP_BORDER' => ( $rulikProp || $statusProp || $actionProp )?'Y':'N' ]
                );

            }
        }

        // Не цены
        foreach( $arResult['ITEMS'] as $key => $arItem ){
            
            if( empty($arItem["VALUES"]) || isset($arItem["PRICE"]) ){   continue;   }
            if ( in_array($arItem["DISPLAY_TYPE"], ['A', 'B']) && ( $arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0 ) ){   continue;   }

            switch ($arItem["DISPLAY_TYPE"]){
                case "A"://NUMBERS_WITH_SLIDER

                    // filterItem
                    $params = array( 'arItem' => $arItem );
                    if( isset($arFilter['PROP_VALUES'][$arItem['CODE']]['MIN']) ){
                        $params['VALUE_FROM'] = $arFilter['PROP_VALUES'][$arItem['CODE']]['MIN'];
                    }
                    if( isset($arFilter['PROP_VALUES'][$arItem['CODE']]['MAX']) ){
                        $params['VALUE_TO'] = $arFilter['PROP_VALUES'][$arItem['CODE']]['MAX'];
                    }

                    $APPLICATION->IncludeComponent(
                        "aoptima:filterItem", "number",
                        [ 'arFilter' => $arFilter, 'arItem' => $arItem ]
                    );

                break;
                case "B"://NUMBERS

                    // filterItem
                    $params = array( 'arItem' => $arItem );
                    if( isset($arFilter['PROP_VALUES'][$arItem['CODE']]['MIN']) ){
                        $params['VALUE_FROM'] = $arFilter['PROP_VALUES'][$arItem['CODE']]['MIN'];
                    }
                    if( isset($arFilter['PROP_VALUES'][$arItem['CODE']]['MAX']) ){
                        $params['VALUE_TO'] = $arFilter['PROP_VALUES'][$arItem['CODE']]['MAX'];
                    }

                    $APPLICATION->IncludeComponent(
                        "aoptima:filterItem", "number",
                        [ 'arFilter' => $arFilter, 'arItem' => $arItem ]
                    );

                break;
                default://CHECKBOXES

                    // filterItem
                    $params = array( 'arItem' => $arItem );
                    if( isset($arFilter['PROP_VALUES'][$arItem['CODE']]) ){
                        $params['VALUES'] = $arFilter['PROP_VALUES'][$arItem['CODE']];
                    }

                    if( $arItem['DISPLAY_TYPE'] == 'F' ){

                        $APPLICATION->IncludeComponent(
                            "aoptima:filterItem", "list_f",
                            [ 'arFilter' => $arFilter, 'arItem' => $arItem ]
                        );

                    } else if(
                        $arItem['DISPLAY_TYPE'] == 'K'
                        ||
                        $arItem['DISPLAY_TYPE'] == 'P'
                    ){

                        $APPLICATION->IncludeComponent(
                            "aoptima:filterItem", "list_b",
                            [ 'arFilter' => $arFilter, 'arItem' => $arItem ]
                        );

                    }

            }
        } ?>

    </div>

    <script type="text/javascript">
        filterInfo = <?=\CUtil::PhpToJSObject($arResult)?>;
        var smartFilter = new JCSmartFilter('<?echo \CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=\CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=\CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
    </script>

</form>





