<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$arFilter = $arParams['arFilter'];
$this->setFrameMode(true);

$sectionFilterProps = $arParams['~SECTION_FILTER_PROPS'];
if( count($sectionFilterProps) > 0 ){
    $iblockProps = tools\prop::getList($arParams['IBLOCK_ID']);
    foreach( $arResult['ITEMS'] as $key => $arItem ){
        if( !isset($arItem["PRICE"]) ){
            $propXMLID = false;
            foreach ( $iblockProps as $propID => $iblockProp ){
                if( $arItem['ID'] == $propID ){    $propXMLID = $iblockProp['XML_ID'];     }
            }
            if(  strlen($propXMLID) > 0  &&  !in_array($propXMLID, $sectionFilterProps)  ){
                unset( $arResult['ITEMS'][$key] );
            }
        }
    }
} 

echo json_encode($arResult);