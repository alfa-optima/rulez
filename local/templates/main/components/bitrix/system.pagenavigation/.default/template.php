<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if(!$arResult["NavShowAlways"]){
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
    return;
}

$countPagesDisplay = 3; // kolichestvo otobrajaemih stranic v paginacii


$i = 1;
$left_border = $arResult["NavPageNomer"];
$right_border = $arResult["NavPageNomer"];
$max = $arResult["NavPageCount"];
while($i < $countPagesDisplay){
    if($i % 2 == 0){
        $lb = $left_border;
        $left_border = tools\funcs::addLeftBorder($left_border);
        if ($left_border == $lb){
            $rb = $right_border;
            $right_border = tools\funcs::addRightBorder($right_border, $max);
            if ($right_border == $rb){   $i = $countPagesDisplay;   }
        }
    } else {
        $rb = $right_border;
        $right_border = tools\funcs::addRightBorder($right_border, $max);
        if ($right_border == $rb){
            $lb = $left_border;
            $left_border = tools\funcs::addLeftBorder($left_border);
            if ($left_border == $lb){   $i = $countPagesDisplay;   }
        }
    }
    $i++;
}
$arResult["nStartPage"] =$left_border;
$arResult["nEndPage"] = $right_border;

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : ""); ?>



<div class="products-pagination paginationBlock">
    <div class="pagination layout-text">

        <div class="pg-left">

            <? if ($arResult["NavPageNomer"] > 1): ?>

                <?if($arResult["bSavePage"]):?>

                    <a class="pg-prev" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>" style="cursor: pointer" data-page-number="<?=($arResult["NavPageNomer"]-1)?>">
                        <span>Назад</span>
                        <i class="pg-prev-arr">&nbsp;</i>
                    </a>

                <?else:?>

                    <?if ($arResult["NavPageNomer"] > 2):?>

                        <a class="pg-prev" style="cursor: pointer" data-page-number="<?=($arResult["NavPageNomer"]-1)?>" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>">
                            <span>Назад</span>
                            <i class="pg-prev-arr">&nbsp;</i>
                        </a>

                    <?else:?>

                        <a class="pg-prev" style="cursor: pointer" data-page-number="<?=$strNavQueryStringFull?>" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">
                            <span>Назад</span>
                            <i class="pg-prev-arr">&nbsp;</i>
                        </a>

                    <?endif?>

                <?endif?>

            <?else:?>

                <a class="pg-prev disabled">
                    <span>Назад</span>
                    <i class="pg-prev-arr">&nbsp;</i>
                </a>

            <?endif?>

        </div>


        <ul class="pg-list">

            <? if ($arResult["NavPageNomer"] > 1): ?>

                <?if($arResult["bSavePage"]):?>

                    <?if ($arResult["NavPageNomer"] > 2){?>

                        <li>
                            <a style="cursor: pointer" data-page-number="1" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1">1</a>
                        </li>

                    <? } ?>

                <?else:?>

                    <?if ($arResult["nStartPage"] != 1):?>

                        <li>
                            <a style="cursor: pointer" data-page-number="1" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">1</a>
                        </li>

                    <?endif?>

                <?endif?>

            <?endif?>

            <?if ($arResult["nStartPage"] > 2):?>

                <li>
                    <span class="dots">...</span>
                </li>

            <?endif?>

            <?while($arResult["nStartPage"] <= $arResult["nEndPage"]):?>

                <?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>

                    <li class="active"><span><?=$arResult["nStartPage"]?></span></li>

                <?elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):?>

                    <li>
                        <a style="cursor: pointer" data-page-number="<?=$arResult["nStartPage"]?>" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$arResult["nStartPage"]?></a>
                    </li>

                <?else:?>

                    <li>
                        <a style="cursor: pointer" data-page-number="<?=$arResult["nStartPage"]?>" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><?=$arResult["nStartPage"]?></a>
                    </li>

                <?endif?>

                <?$arResult["nStartPage"]++?>

            <?endwhile?>

            <?if ($arResult["nEndPage"] < ($arResult["NavPageCount"] - 1)):?>

                <li>
                    <span class="dots">...</span>
                </li>

            <?endif?>

            <?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
                <?if($arResult["nEndPage"] < $arResult["NavPageCount"]):?>

                    <li>
                        <a style="cursor: pointer" data-page-number="<?=$arResult["NavPageCount"]?>" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>"><?=$arResult["NavPageCount"]?></a>
                    </li>

                <?endif?>

            <?endif?>

        </ul>


        <div class="pg-right">

            <?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
                <?if($arResult["nEndPage"] < $arResult["NavPageCount"]):?>

                    <a class="pg-next" style="cursor: pointer" data-page-number="<?=($arResult["NavPageNomer"]+1)?>" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>">
                        <span>Вперед</span>
                        <i class="pg-next-arr">&nbsp;</i>
                    </a>

                <?else:?>

                    <a class="pg-next" style="cursor: pointer" data-page-number="<?=($arResult["NavPageNomer"]+1)?>" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>">
                        <span>Вперед</span>
                        <i class="pg-next-arr">&nbsp;</i>
                    </a>

                <?endif?>

            <?else:?>

                <a class="pg-next disabled">
                    <span>Вперед</span>
                    <i class="pg-next-arr">&nbsp;</i>
                </a>

            <?endif?>

        </div>

    </div>
</div>


















