<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$arResult['SHORT_PROPS_LIST'] = [];
if(
    is_array( $arResult["PROPERTIES"][project\catalog::SHORT_LIST_PROP_CODE]['VALUE'] )
    &&
    count($arResult["PROPERTIES"][project\catalog::SHORT_LIST_PROP_CODE]['VALUE']) > 0
){
    foreach ( $arResult["PROPERTIES"][project\catalog::SHORT_LIST_PROP_CODE]['VALUE'] as $prop_xml_id ){
        foreach ( $arResult["PROPERTIES"] as $prop_code => $prop ){
            if(
                $prop_xml_id == $prop['XML_ID']
                &&
                strlen($prop['VALUE']) > 0
            ){
                $arResult['SHORT_PROPS_LIST'][$prop['NAME']] = $prop['VALUE'];
            }
        }
    }
}


$arResult['prop_groups'] = $arParams['~prop_groups'];
$arResult['stopProps'] = $arParams['~stopProps'];

$arResult['grouped_props'] = [];  $arResult['not_grouped_props'] = [];
$arResult['grouped_props_ids'] = [];

if(
    is_array($arResult['prop_groups'])
    &&
    count($arResult['prop_groups']) > 0
){
    foreach ( $arResult['prop_groups'] as $prop_xml_id => $group_name ){
        foreach ( $arResult['PROPERTIES'] as $key => $prop ){
            if(
                strlen($prop['VALUE']) > 0
                &&
                !in_array( $prop['CODE'], $arResult['stopProps'] )
                &&
                $prop['XML_ID'] == $prop_xml_id
            ){
                $arResult['grouped_props'][$group_name][$prop['CODE']] = $prop;
                $arResult['grouped_props_ids'][] = $prop['ID'];
            }
        }
    }
}

foreach ( $arResult['PROPERTIES'] as $key => $prop ){
    if(
        strlen($prop['VALUE']) > 0
        &&
        !in_array( $prop['CODE'], $arResult['stopProps'] )
        &&
        !in_array($prop['ID'], $arResult['grouped_props_ids'])
    ){
        $arResult['not_grouped_props'][$prop['CODE']] = $prop;
    }
}


$arResult['courierDaysMinsk'] = false;
$arResult['courierDaysRB'] = false;
$daysDiff = isset($arParams['DELIVERY_RB_PLUS_DAYS'])?$arParams['DELIVERY_RB_PLUS_DAYS']:1;

if ( strlen( $arResult[ "PROPERTIES" ][ 'SROK_POSTAVKI' ][ 'VALUE' ] ) > 0 ){
    if ( $arResult[ "PROPERTIES" ][ 'SROK_POSTAVKI' ][ 'VALUE' ] == 0 ){
        $arResult['courierDaysMinsk'] = 'до 1 дня';
        $arResult['courierDaysRB'] = 'до 2 дней';
    } else {
        if ( substr_count( $arResult[ "PROPERTIES" ][ 'SROK_POSTAVKI' ][ 'VALUE' ], '-' ) ){
            $days = explode( '-', $arResult[ "PROPERTIES" ][ 'SROK_POSTAVKI' ][ 'VALUE' ] );
            $arResult['courierDaysMinsk'] = 'до&nbsp;'.$days[ 0 ] . '-' . $days[ 1 ] . '&nbsp;' . tools\funcs::pfCnt( $days[ 1 ], "дня", "дней", "дней" );
            $arResult['courierDaysRB'] = 'до&nbsp;'. ($days[0]+$daysDiff) . '-' . ($days[1]+$daysDiff) . '&nbsp;' . tools\funcs::pfCnt( ($days[1]+$daysDiff), "дня", "дней", "дней" );
        } else {
            $arResult['courierDaysMinsk'] = 'до&nbsp;'.intval($arResult[ "PROPERTIES" ][ 'SROK_POSTAVKI' ][ 'VALUE' ]) . '&nbsp;' . tools\funcs::pfCnt( intval($arResult[ "PROPERTIES" ][ 'SROK_POSTAVKI' ][ 'VALUE' ]), "дня", "дней", "дней" );
            $arResult['courierDaysRB'] = 'до&nbsp;'. (intval($arResult[ "PROPERTIES" ][ 'SROK_POSTAVKI' ][ 'VALUE' ])+$daysDiff) . '&nbsp;' . tools\funcs::pfCnt( intval($arResult[ "PROPERTIES" ][ 'SROK_POSTAVKI' ][ 'VALUE' ])+$daysDiff, "дня", "дней", "дней" );
        }
    }
}
