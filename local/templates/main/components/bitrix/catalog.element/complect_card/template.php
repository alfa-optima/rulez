<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$complect = $arParams['~COMPLECT']; ?>


<section itemscope itemtype="http://schema.org/Product" class="section p-card complectCardBlock" item_id="<?=$arResult['ID']?>">

    <!--product card caption-->
    <div class="p-card__caption">
        <h1 itemprop="name" class="p-card-title p-card__caption-title"><?=$arResult['NAME']?></h1>
<!--        <div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating" class="p-rate_lg p-card__caption-rate">-->
<!--            <span itemprop="ratingValue" content="4.5" style="width: 90%">4.5</span>-->
<!--            <meta itemprop="ratingCount" content="1">-->
<!--        </div>-->
    </div>
    <!--product card caption end-->

    <!--product card main-->
    <div class="p-card__main">

        <? // product_labels
        $APPLICATION->IncludeComponent(
            "aoptima:product_labels", "product_card",
            array( 'element' => $arResult )
        ); ?>

        <!--product card gallery-->
        <div class="p-card-set lazy-images-js">
            <div class="set equal-height-js">

                <? $cnt = 0;
                foreach( $complect['PRODUCTS'] as $product ){ $cnt++; ?>

                    <?php if( $cnt != 1 ){ ?>
                        <div class="set__sign set__sign_plus">+</div>
                    <?php } ?>

                    <div class="set__item">

                        <div itemscope itemtype="http://schema.org/product-short" class="product-short product-short-js">

                            <div class="product-short__main">

                                <? // product_labels
                                $APPLICATION->IncludeComponent(
                                    "aoptima:product_labels", "catalog_item",
                                    array( 'element' => $product )
                                ); ?>

                                <a itemprop="url" href="<?=$product['DETAIL_PAGE_URL']?>" >
                                    <div class="product-short__figure">
                                        <? if( intval($product['DETAIL_PICTURE']) > 0 ){ ?>
                                            <img itemprop="image" src="<?=SITE_TEMPLATE_PATH?>/img/preloader.svg" data-src="<?=tools\funcs::rIMGG($product['DETAIL_PICTURE'], 4, 125, 90)?>">
                                        <? } else { ?>
                                            <img itemprop="image" src="<?=SITE_TEMPLATE_PATH?>/img/preloader.svg" data-src="<?=SITE_TEMPLATE_PATH?>/img/no-photo.png" style="max-width: 125px">
                                        <? } ?>
                                    </div>
<!--                                    <div class="product-short__category">Сумки</div>-->
                                    <h3 itemprop="name" class="product-short__title"><span><?=$product['NAME']?></span></h3>
                                </a>
                            </div>

                            <div class="product-short__footer">
                                <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="product-short__offer">
                                    <div class="p-short-price">
<!--                                        <div class="p-short-price__old"><strong class="p-short-price__old-val">1457</strong>&nbsp<em class="p-short-price__old-unit">р.</em></div>-->
                                        <div class="p-short-price__new"><strong class="p-short-price__new-val" itemprop="price" content="<?=$product['CATALOG_PRICE_'.project\catalog::BASE_PRICE_ID]?>"><?=number_format($product['CATALOG_PRICE_'.project\catalog::BASE_PRICE_ID], project\catalog::PRICE_ROUND, ".", " ")?></strong>&nbsp;<em class="p-short-price__new-unit" itemprop="priceCurrency" content="BYN">р.</em></div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                <? } ?>

            </div>
        </div>
        <!--product card gallery end-->
    </div>
    <!--product card main end-->

    <!--product card aside-->
    <div class="p-card__aside">

        <div class="p-card-action">

            <a style="cursor: pointer;" class="action-btn action-btn_like p-card-action__btn fav___button" item_id="<?=$arResult['ID']?>" data-active-text="Мне нравится" data-inactive-text="В избранное"><span>В избранное</span> <svg class="svg-ico-like" xmlns="http://www.w3.org/2000/svg" width="19" height="17" viewBox="0 0 19 17">
                    <path d="M18 5.67611C18 3.09352 15.9446 1 13.4091 1C11.7535 1 10.3077 1.89561 9.49995 3.23407C8.69232 1.89561 7.24584 1 5.59027 1C3.05483 1 1 3.09352 1 5.67611C1 7.08219 1.61169 8.34013 2.57576 9.19676L9.08102 15.8233C9.19212 15.9364 9.34279 16 9.49995 16C9.65711 16 9.80778 15.9364 9.91888 15.8233L16.4241 9.19676C17.3882 8.34013 18 7.08219 18 5.67611Z"></path>
                </svg>
            </a>

<!--            <a style="cursor: pointer;" item_id="--><?//=$complect['ID']?><!--" class="action-btn action-btn_compare p-card-action__btn compare___button" data-active-text="Добавлен к сравнению" data-inactive-text="Сравнить"><em class="action-btn__counter compare_cnt_block"></em><span>Сравнить</span> <svg class="svg-ico-compare" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">-->
<!--                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0 5.89474V16H3.36842V5.89474H0ZM0.842105 6.73684V15.1579H2.52632V6.73684H0.842105ZM4.21053 16V0H7.57895V16H4.21053ZM5.05263 15.1579V0.842105H6.73684V15.1579H5.05263ZM8.42105 2.52632V16H11.7895V2.52632H8.42105ZM9.26316 3.36842V15.1579H10.9474V3.36842H9.26316ZM12.6316 16V7.57895H16V16H12.6316ZM13.4737 15.1579V8.42105H15.1579V15.1579H13.4737Z"></path>-->
<!--                </svg>-->
<!--            </a>-->

        </div>

        <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="p-card-offer">

            <div class="p-card-offer__meta">
                <div itemprop="availability" content="http://schema.org/InStock" class="p-card-stock on"><i>&nbsp;</i>В наличии</div>
                <? if( strlen($arResult["PROPERTIES"]['CML2_ARTICLE']['VALUE']) > 0 ){ ?>
                    <div class="p-card-code"><span class="p-card-code__label">Код :</span> <?=$arResult["PROPERTIES"]['CML2_ARTICLE']['VALUE']?>-<span class="b242ga_track_id"></span></div>
                <? } ?>
            </div>

            <div class="p-card-offer__buy">

                <div class="p-card-price">

                    <? if( $arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['VALUE'] > 0 ){ ?>

                        <?php if(
                            $arResult['COMPLECT_PRODUCTS_SUM'] > 0
                            &&
                            $arResult['COMPLECT_PRODUCTS_SUM'] > $arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['DISCOUNT_VALUE_VAT']
                        ){ ?>
                            <div class="p-card-price__old">
                                <strong><?=number_format($arResult['COMPLECT_PRODUCTS_SUM'], project\catalog::PRICE_ROUND, ".", " ")?></strong>&nbsp;<em>р.</em>
                            </div>
                        <? } ?>

                        <div class="p-card-price__new">
                            <strong itemprop="price" content="<?=$arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['DISCOUNT_VALUE_VAT']?>"><?=number_format($arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['DISCOUNT_VALUE_VAT'], project\catalog::PRICE_ROUND, ".", "")?></strong>&nbsp;<em itemprop="priceCurrency" content="BYN">р.</em>
                        </div>

                        <? if(
                            $arResult['COMPLECT_PRODUCTS_SUM'] > 0
                            &&
                            $arResult['COMPLECT_PRODUCTS_SUM'] > $arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['DISCOUNT_VALUE_VAT']
                        ){ ?>
                            <div class="p-card-price__note">Спецпредложение</div>
                        <? } ?>

                    <? } ?>

                </div>

                <div class="p-card-options">

                    <? if( $arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['VALUE'] > 0 ){ ?>

                        <div class="p-card-options__item">
                            <a style="cursor:pointer;" class="btn-buy p-card-options__btn add_to_basket_button to___process" item_id="<?=$arResult['ID']?>">
                                <span class="btn-buy__buy">В корзину</span>
                                <span class="btn-buy__in-cart">В&nbsp;корзине</span>
                                <i><svg class="svg-ico-cart" xmlns="http://www.w3.org/2000/svg" width="23" height="21" viewBox="0 0 23 21">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M23 0.902583C23 1.40102 22.6057 1.80517 22.1192 1.80517H20.3482L14.4767 15.6447H2.34872L0 6.01718H11.4035C11.89 6.01718 12.2842 6.42117 12.2842 6.91976C12.2842 7.4182 11.89 7.82235 11.4035 7.82235H2.25645L3.72437 13.8396H13.3215L19.1931 0H22.1192C22.6059 0 23 0.403934 23 0.902583ZM7.84736 18.5028C7.84736 19.8797 6.75405 21 5.41057 21C4.06724 21 2.97399 19.8797 2.97399 18.5028C2.97399 17.1258 4.06724 16.0055 5.41057 16.0055C6.75405 16.0055 7.84736 17.1258 7.84736 18.5028ZM5.41057 17.2089C6.10667 17.2089 6.6731 17.7895 6.6731 18.5028C6.6731 19.2163 6.10667 19.7966 5.41057 19.7966C4.71467 19.7966 4.14824 19.2163 4.14824 18.5028C4.14824 17.7895 4.71467 17.2089 5.41057 17.2089ZM14.3235 18.5028C14.3235 19.8797 13.23 21 11.8866 21C10.5433 21 9.45009 19.8797 9.45009 18.5028C9.45009 17.1258 10.5433 16.0055 11.8866 16.0055C13.23 16.0055 14.3235 17.1258 14.3235 18.5028ZM11.8866 17.2089C12.5828 17.2089 13.1493 17.7895 13.1493 18.5028C13.1493 19.2163 12.5827 19.7966 11.8866 19.7966C11.1906 19.7966 10.6243 19.2163 10.6243 18.5028C10.6243 17.7895 11.1906 17.2089 11.8866 17.2089Z"></path>
                                    </svg>
                                </i>
                            </a>
                        </div>

                        <div class="p-card-options__item">
                            <a style="cursor:pointer;" class="btn-outline p-card-options__btn open-p-add-form-popup-js">Купить в один клик</a>
                        </div>

                    <? } ?>

                </div>

            </div>

            <?php if( strlen( $arResult["PROPERTIES"][project\catalog::IS_RULIK_REC_PAYMENT_DESCR_PROP_CODE]['VALUE'] ) > 0 ){ ?>

                <div class="p-card-offer__payment">

                    <div class="p-card-offer__payment-title">

                        <?php if( strlen( $arResult["PROPERTIES"][project\catalog::IS_RULIK_REC_PAYMENT_TITLE_PROP_CODE]['VALUE'] ) > 0 ){

                            echo $arResult["PROPERTIES"][project\catalog::IS_RULIK_REC_PAYMENT_TITLE_PROP_CODE]['VALUE'];

                        } else {

                            echo 'Рекомендуемый способ оплаты';
                        } ?>

                    </div>

                    <div class="p-card-offer__payment-text"><?=$arResult["PROPERTIES"][project\catalog::IS_RULIK_REC_PAYMENT_DESCR_PROP_CODE]['VALUE']?></div>

                </div>

            <? } ?>

            <div class="p-add-form-popup p-add-form-popup-js stop-remove-class">
                <div class="p-add-form p-add-form-js">
                    <div class="p-add-form__figure">
                        <img class="p-add-form__figure-img" src="<?=tools\funcs::rIMGG($arResult['DETAIL_PICTURE'], 4, 700, 400)?>"/>
                    </div>
                    <div class="p-add-form__content">
                        <div class="p-add-form__title"><?=$arResult['NAME']?></div>
                        <div class="p-add-form__meta">

                            <div class="p-add-form__rate p-rate">
                                <span content="<?=$arParams['product_rating']?>" style="width: <?=round($arParams['product_rating']/5*100, 0)?>%;"><?=$arParams['product_rating']?></span>
                                <meta content="1">
                            </div>

                        </div>
                        <div class="p-add-form__options">
                            <div class="p-add-form-price p-add-form__price">
<? if(
    $arResult['PRICES'][project\catalog::RASSROCHKA_PRICE_CODE]['DISCOUNT_VALUE_VAT'] > 0
    &&
    $arResult['PROPERTIES'][project\catalog::RASSROCHKA_PROP_CODE]['VALUE'] == 'Да'
    &&
    $arResult['PRICES'][project\catalog::RASSROCHKA_PRICE_CODE]['DISCOUNT_VALUE_VAT']
    >
    $arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['DISCOUNT_VALUE_VAT']
){ ?>
    <div class="p-add-form-price__old"><strong><?=number_format($arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['VALUE_VAT'], project\catalog::PRICE_ROUND, ".", " ")?></strong>&nbsp;<em>р.</em></div>
<? } ?>
                                <div class="p-add-form-price__new"><strong class="p-add-form-price-js" data-price="<?=$arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['DISCOUNT_VALUE_VAT']?>"><?=number_format($arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['DISCOUNT_VALUE_VAT'], project\catalog::PRICE_ROUND, ".", " ")?></strong>&nbsp;<em>р.</em></div>
                            </div>
                            <div class="p-add-form__counter p-add-form__counter-js">
                                <input class="spinner spinner-js" max="100" min="1" type="number" name="quick_quantity" value="1" data-only-number>
                            </div>
                        </div>
                    </div>
                    <div class="p-add-form__footer">
                        <div class="p-add-form__total">
                            <div class="p-add-form__total-label">Итого</div>
                            <div class="p-add-form__total-sum"><strong class="p-add-form-total-js"><?=number_format($arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['DISCOUNT_VALUE_VAT'], project\catalog::PRICE_ROUND, ".", " ")?></strong> <em>р.</em></div>
                        </div>
                    </div>
                </div>
                <div class="quick-order p-add-form__order">
                    <form class="validate-js" onsubmit="return false;">
                        <div class="quick-order__form">
                            <label for="f-wrap-validation-js" class="quick-order__label">Быстрый заказ: </label>
                            <input class="quick-order__input field-js is___phone" id="f-wrap-validation-js" name="phone" placeholder="+375" type="text"">
                            <div class="error-note quick-order__error-note">Введите корректный номер телефона в формате +375 и 9 цифр</div>
                            <!--<div class="success-note quick-order__success-note">Success text</div>-->
                            <button class="quick-order__send-btn buy_1_click_button to___process"  item_id="<?=$arResult['ID']?>">
                                <svg width="19" height="13" viewBox="0 0 19 13" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12.8021 0.19552C12.5342 -0.0651732 12.1126 -0.0651732 11.8447 0.19552C11.5859 0.447405 11.5859 0.866628 11.8447 1.11793L16.6993 5.84269H0.679892C0.306465 5.84269 0 6.13157 0 6.49501C0 6.85845 0.306465 7.15672 0.679892 7.15672H16.6993L11.8447 11.8727C11.5859 12.1334 11.5859 12.5532 11.8447 12.8045C12.1126 13.0652 12.5342 13.0652 12.8021 12.8045L18.8059 6.9612C19.0647 6.70932 19.0647 6.2901 18.8059 6.0388L12.8021 0.19552Z"></path>
                                </svg>
                                <span class="hide-text">Отправить</span>
                            </button>
                        </div>
                    </form>
                </div>
                <a href="#" class="close-p-add-form-popup p-add-form-popup-close-js">
                    <svg width="8" height="8" viewBox="0 0 8 8" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0.646447 1.35355L6.64645 7.35355L7.35355 6.64645L1.35355 0.646447L0.646447 1.35355ZM6.64645 0.646447L0.646447 6.64645L1.35355 7.35355L7.35355 1.35355L6.64645 0.646447Z"></path>
                    </svg>
                    <span>Закрыть</span>
                </a>
            </div>

        </div>

        <?php if( strlen( $arResult["PROPERTIES"][project\catalog::IS_RULIK_REC_DESCR_PROP_CODE]['VALUE'] ) > 0 ){ ?>
            <div class="p-card__advantages">
                <div class="p-card__advantages-title">Рулеззз фишки!</div>
                <div class="p-card__advantages-text">
                    <?=$arResult["PROPERTIES"][project\catalog::IS_RULIK_REC_DESCR_PROP_CODE]['VALUE']?>
                </div>
            </div>
        <?php } ?>

        <? if(
            count( $arParams['credit_sections'] ) > 0
            &&
            $arResult['PRICES'][project\catalog::RASSROCHKA_PRICE_CODE]['VALUE']
            &&
            $arResult['PROPERTIES'][project\catalog::RASSROCHKA_PROP_CODE]['VALUE'] == 'Да'
        ){ ?>

            <div class="credit tabs-js">

                <div class="credit-nav">
                    <div class="credit-select tabs__select-js"></div>
                    <div class="credit-thumbs tabs__thumbs-js tabs__select-drop-js">
                        <? $cnt = 0;
                        foreach( $arParams['credit_sections'] as $key => $section ){ $cnt++; ?>
                            <a href="#<?=$section['CODE']?>" <? if( $cnt==1 ){ ?>class="tabs-active"<? } ?>><span><?=$section['NAME']?></span><em>▼</em></a>
                        <? } ?>
                    </div>
                </div>

                <!-- Кредиты и рассрочки -->
                <div class="tabs__panels-js">

                    <? foreach( $arParams['credit_sections'] as $key => $section ){ ?>

                        <div id="<?=$section['CODE']?>" class="credit-panel cards-js">

                            <div class="swiper-container cards-slider cards-slider-js">
                                <div class="swiper-wrapper">

                                    <? foreach ( $section['items'] as $key => $item ){

                                        $month_price = number_format($arResult['PRICES'][project\catalog::RASSROCHKA_PRICE_CODE]['DISCOUNT_VALUE_VAT']/$item['PROPERTY_SROK_VALUE'], 0, ",", " "); ?>

                                        <div class="swiper-slide cards-slider__slide" data-card-title="<?=$item['NAME']?>" <? if( $month_price > 0 ){ ?>data-card-text="<strong><mark><?=$month_price?> руб в месяц</mark></strong> - <?=$item['PROPERTY_SROK_VALUE']?> <?=tools\funcs::pfCnt($item['PROPERTY_SROK_VALUE'], 'месяц', 'месяца', "месяцев")?>"<? } ?> data-descr="<?=base64_encode($item['PREVIEW_TEXT'])?>">
                                            <? if( intval($item['PREVIEW_PICTURE']) > 0 ){ ?>
                                                <div class="cards-slider-figure">
                                                    <div class="cards-slider-img" style="background-image: url(<?=\CFile::GetPath($item['PREVIEW_PICTURE'])?>)"><?=$item['NAME']?></div>
                                                </div>
                                            <? } ?>
                                        </div>

                                    <? } ?>

                                </div>
                            </div>

                            <div class="cards-slider-nav">
                                <div class="cards-slider__prev cards-slider__prev-js">
                                    <svg width="19" height="13" viewBox="0 0 19 13" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M6.1979 12.8045C6.46575 13.0652 6.88744 13.0652 7.1553 12.8045C7.4141 12.5526 7.4141 12.1334 7.1553 11.8821L2.30074 7.15731L18.3201 7.15731C18.6935 7.15731 19 6.86843 19 6.50499C19 6.14155 18.6935 5.84328 18.3201 5.84328L2.30074 5.84328L7.1553 1.12732C7.4141 0.866628 7.4141 0.446818 7.1553 0.195519C6.88744 -0.0651731 6.46575 -0.0651731 6.1979 0.195519L0.194104 6.0388C-0.0647013 6.29068 -0.0647013 6.7099 0.194104 6.9612L6.1979 12.8045Z"></path>
                                    </svg>
                                    <span>Prev</span>
                                </div>
                                <div class="cards-slider__next cards-slider__next-js">
                                    <svg width="19" height="13" viewBox="0 0 19 13" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12.8021 0.19552C12.5342 -0.0651732 12.1126 -0.0651732 11.8447 0.19552C11.5859 0.447405 11.5859 0.866628 11.8447 1.11793L16.6993 5.84269H0.679892C0.306465 5.84269 0 6.13157 0 6.49501C0 6.85845 0.306465 7.15672 0.679892 7.15672H16.6993L11.8447 11.8727C11.5859 12.1334 11.5859 12.5532 11.8447 12.8045C12.1126 13.0652 12.5342 13.0652 12.8021 12.8045L18.8059 6.9612C19.0647 6.70932 19.0647 6.2901 18.8059 6.0388L12.8021 0.19552Z"></path>
                                    </svg>
                                    <span>Next</span>
                                </div>
                            </div>

                            <div class="credit-panel__content">

                                <div class="credit-info">
                                    <div class="credit-info__col">
                                        <div class="card-info">
                                            <strong class="card-info__title card-info__title-js"></strong>
                                            <div class="card-info__content card-info__text-js"></div>
                                        </div>
                                    </div>
                                    <div class="credit-info__col card-info__descr-js"></div>
                                </div>

                                <? if(
                                    0
                                    &&
                                    $arResult['PROPERTIES'][ project\catalog::RASSROCHKA_PROP_CODE]['VALUE'] == 'Да'
                                    &&
                                    $arResult['PRICES'][ project\catalog::RASSROCHKA_PRICE_CODE]['VALUE'] > 0
                                ){ ?>

                                    <div class="credit-note">

                                        <div class="credit-note__col">При покупке в рассрочку скидка на товар не действует</div>

                                        <div class="credit-note__col">
                                            <a style="cursor: pointer" class="btn-alt btn-alt_full-width add_to_basket_button to___process" item_id="<?=$arResult['ID']?>">Купить в рассрочку</a>
                                        </div>

                                    </div>

                                <? } ?>

                            </div>

                        </div>

                    <? } ?>

                </div>

            </div>

        <? } ?>

    </div>
    <!--product card aside end-->

    <!--product card services-->
    <div class="p-card-services p-card__services">
        <div class="p-card-services__list">
            <div class="p-card-services__item">
                <strong>
                    <i class="icon-helper-delivery-color">&nbsp;</i>
                    <span>Доставка</span>
                </strong>
                <p>По Минску <mark>курьером &mdash; сегодня, завтра, бесплатно</mark>.</p>
                <?php if( strlen($arResult["PROPERTIES"]['SROK_POSTAVKI']['VALUE']) > 0 ){ ?>
                    <p>По всей Беларуси курьером в течение: <span class="nobr"><?=$arResult["PROPERTIES"]['SROK_POSTAVKI']['VALUE']?></span>.</p>
                <?php } ?>
            </div>
            <div class="p-card-services__item p-card-services__item_gift toGiftsLinkBlock" style="display: none;">
                <a href="#productCardDopBlock" class="js-scroll-to-anchor">
                    <strong>
                        <i class="icon-gift">&nbsp;</i>
                        <span>Выбери подарки!</span>
                    </strong>
                    <p>С этим товаром идут крутые бесплатные подарочки, <mark class="mark_gift">листай и выбирай!</mark></p>
                </a>
            </div>
            <div class="p-card-services__item productPaymentBlock">
                <strong>
                    <i class="icon-helper-credit-color">&nbsp;</i>
                    <span>Оплата</span>
                </strong>
                <p>Наличными, банковской картой, картами рассрочек, безналичный платежом.</p>
            </div>
            <div class="p-card-services__item productExpertBlock">
                <strong>
                    <i class="icon-helper-call-color">&nbsp;</i>
                    <span>Помощь экспертов</span>
                </strong>
                <p>Консультации специалистов по телефону и в удобных мессенджерах, пишите нам!</p>
            </div>
        </div>
    </div>
    <!--product card services end-->


    <? if(
        $arResult['PRICES'][project\catalog::CREDIT_PRICE_CODE]['VALUE_VAT']
        &&
        $arResult['PROPERTIES'][project\catalog::RASSROCHKA_PROP_CODE]['VALUE'] == 'Да'
        &&
        count($arParams['rassrochkaMonths']) > 0
    ){ ?>

        <!--product card installment-->
        <div class="p-card__installment">
            <h2>Оплата частями</h2>
            <div class="installment installment_product-card installment-js">
                <form onsubmit="return false">
                    <div class="installment__main installment__main_product-card">

                        <!--<h3>Цена:</h3>-->

                        <input class="installment-price-js" type="hidden" value="<?=$arResult['PRICES'][project\catalog::CREDIT_PRICE_CODE]['VALUE_VAT']?>" style="width: auto;">

                        <!--<h3>Размер рассрочки, %</h3>-->

                        <input class="installment-rate-js" type="hidden" value="<?=\Bitrix\Main\Config\Option::get('aoptima.project', 'RASSROCHKA_PROCENT')?>" style="width: auto;">

                        <h3>Выберите срок:</h3>
                        <div class="ch-group">

                            <? $cnt = 0;
                            foreach ( $arParams['rassrochkaMonths'] as $key => $monthItem ){ $cnt++; ?>

                                <div class="ch-group__item">
                                    <label class="radio-label checkbox_value">

                                        <? // data-installment-enable
                                        // теоретически может быть ситуация, что, допустим, рассрочку на 24 мес могут дать только,
                                        // если человек сделает первый взнос в размере 50%
                                        //  сейчас везде по умолчанию - 0 ?>

                                        <input
                                            class="installment-term-check-js"
                                            type="radio"
                                            name="i-term"
                                            value="<?=$monthItem['months_cnt']?>"
                                            data-installment-enable="0"
                                            data-installment-rate="<?=$monthItem['koeff']?>"
                                            <? if( $cnt == 1 ){ ?>checked<? } ?>
                                        >

                                        <span><?=$monthItem['months_cnt']?> мес.</span>
                                    </label>
                                </div>

                            <? } ?>

                        </div>

                        <h3>Выберите первоначальный взнос:</h3>

                        <div class="ch-group">

                            <div class="ch-group__item">
                                <label class="radio-label checkbox_value">
                                    <input class="installment-fee-check-js" type="radio" name="i-initial-fee" value="0" checked>
                                    <span>Нет</span>
                                </label>
                            </div>

                            <? for(
                                $v = \Bitrix\Main\Config\Option::get('aoptima.project', 'RASSROCHKA_MIN_VZNOS');
                                $v <= \Bitrix\Main\Config\Option::get('aoptima.project', 'RASSROCHKA_MAX_VZNOS');
                                $v++
                            ){
                                if(
                                    $v == \Bitrix\Main\Config\Option::get('aoptima.project', 'RASSROCHKA_MIN_VZNOS')
                                    ||
                                    $v == \Bitrix\Main\Config\Option::get('aoptima.project', 'RASSROCHKA_MAX_VZNOS')
                                    ||
                                    $v%10 == 0
                                ){ ?>

                                    <div class="ch-group__item">
                                        <label class="radio-label checkbox_value">
                                            <input class="installment-fee-check-js" type="radio" name="i-initial-fee" value="<?=$v?>">
                                            <span><?=$v?>%</span>
                                        </label>
                                    </div>

                                <? }
                            } ?>

                        </div>

                    </div>

                    <div class="installment__aside installment__aside_product-card">

                        <div class="installment-prod">
                            <? if( intval($arResult['DETAIL_PICTURE']['ID']) > 0 ){ ?>
                                <div class="installment-prod__figure">
                                    <img src="<?=tools\funcs::rIMGG($arResult['DETAIL_PICTURE']['ID'], 4, 95, 54)?>" alt="image description">
                                </div>
                            <? } ?>
                            <h3 class="installment-prod__title"><?=$arResult['~NAME']?></h3>
                        </div>

                        <div class="installment-result">
                            <div class="installment-result__item"><span class="installment-result-fee-js">--</span> - первоначальный взнос</div>
                            <div class="installment-result__item"><span class="installment-result-val-js">--</span> - в месяц</div>
                            <div class="installment-result__item"><span class="installment-result-term-js">--</span></div>
                        </div>

                        <div class="installment-footer">

                            <!--                            <button class="btn-def add_to_basket_button to___process" item_id="--><?//=$arResult['ID']?><!--" type="button">Купить в рассрочку за <span class="installment-result-val-js">--</span> в месяц</button>-->

                            <input class="btn-outline installment-reset-js" type="reset" value="Сбросить">

                            <!--                            <button class="btn-def">Купить в кредит</button>-->

                        </div>

                    </div>
                </form>
            </div>
        </div>
        <!--product card installment end-->

    <? } ?>


</section>








<?php if( 1 ){ ?>


    <!--product infobar-->
    <section class="section section_p-infobar tabs-js">

        <h2 class="section__title hide">Вся информации о товаре</h2>

        <div class="divisions-tabs__nav p-infobar-tabs__nav">
            <div class="divisions-tabs__select tabs__select-js"></div>
            <div class="divisions-tabs-thumbs tabs__thumbs-js tabs__select-drop-js">
                <a href="#pInfobarDesc" class="tabs-active">Описание</a>
            </div>
        </div>

        <div class="tabs__panels-js">

            <div id="pInfobarDesc">
                <h3 class="hide">Описание</h3>
                <ul class="p-infobar-list">
                    <li>
                        <?php if( strlen($arResult['DETAIL_TEXT']) > 0 ){ ?>
                            <p><?=$arResult['DETAIL_TEXT']?></p>
                        <? } ?>
                    </li>
                </ul>
            </div>

        </div>

    </section>
    <!--product infobar end-->

<?php } ?>
