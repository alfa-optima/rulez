<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$arResult['SHORT_PROPS_LIST'] = [];
if(
    is_array( $arResult["PROPERTIES"][project\catalog::SHORT_LIST_PROP_CODE]['VALUE'] )
    &&
    count($arResult["PROPERTIES"][project\catalog::SHORT_LIST_PROP_CODE]['VALUE']) > 0
){
    foreach ( $arResult["PROPERTIES"][project\catalog::SHORT_LIST_PROP_CODE]['VALUE'] as $prop_xml_id ){
        foreach ( $arResult["PROPERTIES"] as $prop_code => $prop ){
            if(
                $prop_xml_id == $prop['XML_ID']
                &&
                strlen($prop['VALUE']) > 0
            ){
                $arResult['SHORT_PROPS_LIST'][$prop['NAME']] = $prop['VALUE'];
            }
        }
    }
}


$arResult['prop_groups'] = $arParams['~prop_groups'];
$arResult['stopProps'] = $arParams['~stopProps'];

$arResult['grouped_props'] = [];  $arResult['not_grouped_props'] = [];
$arResult['grouped_props_ids'] = [];

if(
    is_array($arResult['prop_groups'])
    &&
    count($arResult['prop_groups']) > 0
){
    foreach ( $arResult['prop_groups'] as $prop_xml_id => $group_name ){
        foreach ( $arResult['PROPERTIES'] as $key => $prop ){
            if(
                strlen($prop['VALUE']) > 0
                &&
                !in_array( $prop['CODE'], $arResult['stopProps'] )
                &&
                $prop['XML_ID'] == $prop_xml_id
            ){
                $arResult['grouped_props'][$group_name][$prop['CODE']] = $prop;
                $arResult['grouped_props_ids'][] = $prop['ID'];
            }
        }
    }
}


foreach ( $arResult['PROPERTIES'] as $key => $prop ){
    if(
        strlen($prop['VALUE']) > 0
        &&
        !in_array( $prop['CODE'], $arResult['stopProps'] )
        &&
        !in_array($prop['ID'], $arResult['grouped_props_ids'])
    ){
        $arResult['not_grouped_props'][$prop['CODE']] = $prop;
    }
}


$arResult['COMPLECT_PRODUCTS_IDS'] = project\complect::getProducts($arResult['ID']);

$arResult['COMPLECT_PRODUCTS_SUM'] = 0;
foreach ( $arResult['COMPLECT_PRODUCTS_IDS'] as $id ){
    $el = tools\el::info( $id );
    $arResult['COMPLECT_PRODUCTS'][$id] = $el;
    $arResult['COMPLECT_PRODUCTS_SUM'] += $el['CATALOG_PRICE_'.project\catalog::BASE_PRICE_ID];
}

