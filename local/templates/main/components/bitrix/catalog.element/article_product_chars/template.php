<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$prop_groups = $arParams['~prop_groups'];
$stopProps = $arParams['~stopProps'];

$grouped_props = [];  $not_grouped_props = [];
$grouped_props_ids = [];

if(
    is_array($prop_groups)
    &&
    count($prop_groups) > 0
){
    foreach ( $prop_groups as $prop_xml_id => $group_name ){
        foreach ( $arResult['PROPERTIES'] as $key => $prop ){
            if(
                strlen($prop['VALUE']) > 0
                &&
                !in_array( $prop['CODE'], $stopProps )
                &&
                $prop['XML_ID'] == $prop_xml_id
            ){
                $group_name = $prop_groups[$prop['XML_ID']];
                $grouped_props[$group_name][$prop['CODE']] = $prop;
                $grouped_props_ids[] = $prop['ID'];
            }
        }
    }
}

foreach ( $arResult['PROPERTIES'] as $key => $prop ){
    if(
        strlen($prop['VALUE']) > 0
        &&
        !in_array( $prop['CODE'], $stopProps )
        &&
        !in_array($prop['ID'], $grouped_props_ids)
    ){
        $not_grouped_props[$prop['CODE']] = $prop;
    }
} ?>


<? if( count($grouped_props) > 0 ){
    foreach( $grouped_props as $group_name => $props ){ ?>
        <h4><?=$group_name?></h4>
        <ul class="dl-list">
            <? foreach( $props as $prop_code => $prop ){ ?>
                <li>
                    <div><?=$prop['NAME']?></div>
                    <div><?=$prop['VALUE']?></div>
                </li>

            <? } ?>
        </ul>
    <? }
} ?>

<? if( count($not_grouped_props) > 0 ){
    if( count($grouped_props) > 0 ){ ?>
        <h4>Другие параметры</h4>
    <? } ?>
    <ul class="dl-list">
        <? foreach( $not_grouped_props as $prop_code => $prop ){ ?>
            <li>
                <div><?=$prop['NAME']?></div>
                <div><?=$prop['VALUE']?></div>
            </li>

        <? } ?>
    </ul>
<? } ?>
