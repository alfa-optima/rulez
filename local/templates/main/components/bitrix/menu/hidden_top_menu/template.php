<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult)){ ?>

    <nav class="nav-mob">
        <ul class="nav-mob__list nav-mob-rolls-js">

            <? foreach($arResult as $arItem){
                if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
                    continue; ?>

                <li>
                    <a href="<?=$arItem['LINK']?>">
                        <span><?=$arItem['TEXT']?></span>
                    </a>
                </li>

            <? } ?>

        </ul>
    </nav>
	
<? } ?>