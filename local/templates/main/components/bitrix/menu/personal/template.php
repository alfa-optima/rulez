<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult)){ ?>

    <div class="sidebar__holder">
        <ul class="articles-nav">

            <? foreach($arResult as $arItem){
                if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
                    continue;

                if($arItem["SELECTED"]){ ?>
                    <li class="current" style="cursor: pointer" onclick="window.location.href = '<?=$arItem["LINK"]?>';"><span><?=$arItem["TEXT"]?></span></li>
                <? } else { ?>
                    <li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
                <? }

            } ?>

            <li><a href="/out.php?to=<?=$arParams['REQUEST_URI']?>">Выйти</a></li>

        </ul>
    </div>
	
<? } ?>