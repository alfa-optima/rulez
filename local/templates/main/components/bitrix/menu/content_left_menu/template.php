<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult)){ ?>

    <ul class="articles-nav" style="    margin-top: 30px;">
        <li class="current">
            <ul>
                <? foreach($arResult as $arItem){ ?>

                    <li <? if( $arItem['SELECTED'] ){ ?>class="current"<? } ?>>
                        <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
                    </li>

                <? } ?>
            </ul>
        </li>
    </ul>

<? } ?>