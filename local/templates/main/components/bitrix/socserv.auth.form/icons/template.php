<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
die(); 

$svg = array(
	'VKontakte' => '<svg class="svg-ico-vkontakte" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 107.585 61.139"><path fill="#4b73a4" d="M104.8 51.2c-2.6-2.6-13.2-13.7-13.2-13.7 -1.6-2.1-2.1-4.7-0.5-6.8 3.2-4.2 9-11.6 11.1-14.8 3.2-4.2 9-13.2 1.1-13.2H85.9c-1.6 0-2.6 1.1-3.7 2.1 0 0-6.8 12.6-9 16.8 -5.8 11.6-10 7.9-10 2.6V5.9c0-3.2-2.6-5.8-5.8-5.8H44.3c-3.7-0.5-6.8 1.6-9.5 4.2 0 0 6.8-1.1 6.8 7.9v13.7c0 2.1-1.6 3.7-3.7 3.7 -1.1 0-2.1-0.5-3.2-1.1 -5.3-7.4-10-15.3-13.2-23.7 -0.5-1.1-2.1-2.1-3.2-2.1H2.6C1.1 2.7 0 3.7 0 5.3c0 0.5 0 0.5 0 1.1 4.7 13.2 25.3 54.3 49 54.3h10c2.1 0 3.7-1.6 3.7-3.7v-5.8c0-2.1 1.6-3.7 3.7-3.7 1.1 0 2.1 0.5 2.6 1.1L80.6 59.6c1.1 1.1 2.6 1.6 3.7 1.6h15.8C109.1 60.7 109.1 55.4 104.8 51.2z"></path></svg>',
	'Facebook' => '<svg class="svg-ico-facebook" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 123 263.5"><path fill="#3a589e" d="M80.9 85.2V57.9c0-10.4 6.6-12.6 11.5-12.6s29.5 0 29.5 0V-0.1H81.5c-44.8 0-55.2 33.9-55.2 55.2v30.1H0.1v46.5h26.2c0 59.6 0 131.7 0 131.7H81c0 0 0-72.7 0-131.7h37.2l4.9-46.5L80.9 85.2 80.9 85.2z"></path></svg>',
	'Odnoklassniki' => '<svg class="svg-ico-odnoklassniki" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 87.1 146.5"><path fill="#f2822e" d="M44 68.4c19.1 0 34.2-15.2 34.2-34.2C78.1 15.2 63 0 44 0 24.9 0 9.8 15.2 9.8 34.2 9.8 53.2 24.9 68.4 44 68.4zM44 17.1c9.3 0 17.1 7.8 17.1 17.1 0 9.3-7.8 17.1-17.1 17.1 -9.3 0-17.1-7.8-17.1-17.1C26.9 24.9 34.7 17.1 44 17.1zM78.2 73.3c-4.9 0-14.7 9.8-34.2 9.8 -19.5 0-29.3-9.8-34.2-9.8C4.4 73.3 0 77.7 0 83c0 4.9 2.9 7.3 4.9 8.3 5.9 3.4 24.4 11.2 24.4 11.2L8.3 128.9c0 0-3.9 4.4-3.9 7.8 0 5.4 4.4 9.8 9.8 9.8 4.9 0 7.3-3.4 7.3-3.4s22-26.4 22-25.9l22 25.9c0 0 2.5 3.4 7.3 3.4 5.4 0 9.8-4.4 9.8-9.8 0-2.9-3.9-7.8-3.9-7.8l-21-26.4c0 0 18.6-7.8 24.4-11.2 2-1.5 4.9-3.4 4.9-8.3C87.9 77.7 83.5 73.3 78.2 73.3z"></path></svg>',
);

foreach($arParams["~AUTH_SERVICES"] as $service_code => $service){
	if( !$svg[$service_code] ){
		unset($arParams["~AUTH_SERVICES"][$service_code]);
	}
} ?>



<div class="popup-def__sub-title">Войти через соцсети</div>
<div class="popup-def__soc">

    <? foreach($arParams["~AUTH_SERVICES"] as $service_code => $service){ ?>

        <a href="javascript:void(0)" style="cursor: pointer" onclick="<?=$service['ONCLICK']?>" class="popup-def__soc-item" target="_blank">
            <?=$svg[$service_code]?>
            <span><?=$service['NAME']?></span>
        </a>

    <? } ?>

</div>
