<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

// Инфо об элементе
$el = tools\el::info_by_code( $arResult['VARIABLES']['ELEMENT_CODE'], $arParams['IBLOCK_ID'] );

if( intval($el['ID']) > 0 ){

    // Все категории акций
    $actionSections = project\action::sections();

    $APPLICATION->SetPageProperty("description", $el["PROPERTY_DESCRIPTION_VALUE"]?$el["PROPERTY_DESCRIPTION_VALUE"]:$el["NAME"]);
    $APPLICATION->SetPageProperty("keywords", $el["PROPERTY_KEYWORDS_VALUE"]?$el["PROPERTY_KEYWORDS_VALUE"]:$el["NAME"]);
    $APPLICATION->SetPageProperty("title", $el["PROPERTY_TITLE_VALUE"]?$el["PROPERTY_TITLE_VALUE"]:$el["NAME"]);

    $sect_id = tools\el::sections($el['ID'])[0]['ID'];
    $sect = tools\section::info($sect_id);
    $APPLICATION->AddChainItem($sect["NAME"], $sect["SECTION_PAGE_URL"]);
    $APPLICATION->AddChainItem($el["NAME"], $el["DETAIL_PAGE_URL"]);

    $goods_pos_enum = tools\prop_enum::getByID($el['PROPERTY_GOODS_POSITION_ENUM_ID']); ?>

    <div class="content">

        <div class="content-align">
            <section class="content-inner article-content">

                <h1><?=$el['NAME']?></h1>

                <? if( $goods_pos_enum['XML_ID'] == 'POS_1' ){
                    $APPLICATION->IncludeComponent(
                        "aoptima:articleGoodsBlock", "",
                        array('el' => $el, 'pos' => $goods_block_pos_xml_id)
                    );
                } ?>

                <?=$el['DETAIL_TEXT']?>

                <? if( $goods_pos_enum['XML_ID'] == 'POS_2' ){
                    $APPLICATION->IncludeComponent(
                        "aoptima:articleGoodsBlock", "",
                        array('el' => $el, 'pos' => $goods_block_pos_xml_id)
                    );
                } ?>

                <? if(
                    is_array($el["PROPERTY_OBZ_PHOTOS_VALUE"])
                    &&
                    count($el["PROPERTY_OBZ_PHOTOS_VALUE"]) > 0
                ){ ?>

                    <div class="swiper-container images-gallery images-gallery-js">
                        <div class="swiper-wrapper">

                            <? foreach( $el["PROPERTY_OBZ_PHOTOS_VALUE"] as $key => $photo_id ){ ?>
                                <div class="swiper-slide">
                                    <img src="<?=tools\funcs::rIMGG($photo_id, 5, 1364, 936)?>" />
                                </div>
                            <? } ?>

                        </div>
                        <div class="swiper-pagination"></div>
                    </div>

                <? } ?>

                <? if( $goods_pos_enum['XML_ID'] == 'POS_3' ){
                    $APPLICATION->IncludeComponent(
                        "aoptima:articleGoodsBlock", "",
                        array('el' => $el, 'pos' => $goods_block_pos_xml_id)
                    );
                } ?>

                <?=html_entity_decode($el["PROPERTY_DETAIL_TEXT_2_VALUE"]['TEXT'])?>

                <? if(
                    $goods_pos_enum['XML_ID'] == 'POS_4'
                    ||
                    !$goods_pos_enum
                ){
                    $APPLICATION->IncludeComponent(
                        "aoptima:articleGoodsBlock", "",
                        array('el' => $el, 'pos' => $goods_block_pos_xml_id)
                    );
                } ?>

            </section>
        </div>


        <div class="article-share-layout">
            <div class="article-share share42init"></div>
            <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/share42/share42.js"></script>
        </div>


        <aside class="sidebar">
            <div class="sidebar__holder">
                <ul class="articles-nav">
                    <li class="current">
                        <a href="/actions/">Акции</a>
                        <ul>
                            <? foreach( $actionSections as $key => $section ){ ?>
                                <? if( $section['ID'] == $sect['ID'] ){ ?>
                                    <li class="current"><a href="<?=$section['SECTION_PAGE_URL']?>"><?=$section['NAME']?></a></li>
                                <? } else { ?>
                                    <li><a href="<?=$section['SECTION_PAGE_URL']?>"><?=$section['NAME']?></a></li>
                                <? } ?>
                            <? } ?>
                        </ul>
                    </li>
                </ul>
            </div>
        </aside>

    </div>

<? } else {

    include($_SERVER["DOCUMENT_ROOT"]."/local/templates/main/include/404include.php");

} ?>