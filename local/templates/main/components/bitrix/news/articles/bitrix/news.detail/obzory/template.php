<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools; ?>

<article class="review">

    <div class="review-caption">

        <div class="review-caption__meta">
            <div class="review-caption__title"><?=$arResult['~NAME']?></div>

<!--            <div class="review-caption__rate">-->
<!--                <div class="p-rate_lg">-->
<!--                    <span itemprop="ratingValue" content="4.5" style="width: 90%">4.5</span>-->
<!--                    <meta itemprop="ratingCount" content="1">-->
<!--                </div>-->
<!--            </div>-->

        </div>

        <div class="review-caption__author">
            <? if( intval($arResult["PROPERTIES"]['EXPERT_PHOTO']['VALUE']) > 0 ){ ?>
                <div class="review-caption__author-photo">
                    <img src="<?=tools\funcs::rIMGG($arResult["PROPERTIES"]['EXPERT_PHOTO']['VALUE'], 5, 168, 168)?>"/>
                </div>
            <? } ?>
            <? if( strlen($arResult["PROPERTIES"]['EXPERT_NAME']['VALUE']) > 0 ){ ?>
                <div class="review-caption__author-name"><?=$arResult["PROPERTIES"]['EXPERT_NAME']['VALUE']?></div>
            <? } ?>
        </div>

    </div>

    <div class="review-content article-content">

        <? if(
            is_array($arResult["PROPERTIES"]['OBZ_PHOTOS']['VALUE'])
            &&
            count($arResult["PROPERTIES"]['OBZ_PHOTOS']['VALUE']) > 0
        ){ ?>
            <div class="review-article-gallery-wrap">
                <div class="swiper-container review-article-gallery review-article-gallery-js">
                    <div class="swiper-wrapper">
                        <? foreach( $arResult["PROPERTIES"]['OBZ_PHOTOS']['VALUE'] as $key => $photo_id ){ ?>
                            <div class="swiper-slide">
                                <img src="<?=tools\funcs::rIMGG($photo_id, 5, 1364, 936)?>" />
                            </div>
                        <? } ?>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        <? } ?>


        <div class="review-features tabs-js layout-text">

            <div class="review-features__nav">

                <div class="review-features__select tabs__select-js"></div>

                <div class="review-features__thumbs tabs__thumbs-js tabs__select-drop-js">

                    <a href="#review-description" class="tabs-active"><span>Описание</span></a>

                    <? if( strlen($arParams['~product_chars']) > 0 ){ ?>
                        <a href="#review-features"><span>Харрактеристики</span></a>
                    <? } ?>

                </div>

            </div>

            <div class="tabs__panels-js">

                <div id="review-description">

                    <?=$arResult['~DETAIL_TEXT']?>

                    <div class="read-more">
                        <!--Add class active after content has showed-->
                        <a href="#?" class="btn-read-more">
                            <svg width="10" height="6" viewBox="0 0 10 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 1L5 5L9 1" stroke="#6E52DD" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                            </svg>
                            <span>Далее</span>
                            <span>Скрыть</span>
                        </a>
                    </div>

                </div>

                <? if( strlen($arParams['~product_chars']) > 0 ){ ?>

                    <div id="review-features"><?=$arParams['~product_chars']?></div>

                <? } ?>

            </div>

        </div>


        <? if( intval($arResult['DETAIL_PICTURE']['ID']) > 0 ){ ?>

            <figure>
                <img src="<?=tools\funcs::rIMGG($arResult['DETAIL_PICTURE']['ID'], 4, 1600, 1600)?>">
                <figcaption><?=$arResult['~NAME']?></figcaption>
            </figure>

        <? } ?>

        <? if(
            is_array($arResult['PROPERTIES']['CHARS']['VALUE'])
            &&
            count($arResult['PROPERTIES']['CHARS']['VALUE']) > 0
        ){ ?>

            <div class="review-conclusion layout-flood equal-height-js">

                <? $cnt = 0;
                foreach( $arResult['PROPERTIES']['CHARS']['VALUE'] as $key => $char ){ $cnt++; ?>

                    <div class="review-conclusion__item">

                        <div class="review-conclusion__item-caption">
                            <i class="icon-power" style="background-image: url(<?=\CFile::GetPath($char['SUB_VALUES']['CHARS_PHOTOS']['VALUE'])?>);"></i>
                            <h3><?=$char['SUB_VALUES']['CHARS_TITLES']['VALUE']?></h3>
                        </div>

                        <? if( strlen($char['SUB_VALUES']['CHARS_DESCRS']['VALUE']['TEXT']) > 0 ){ ?>
                            <p><?=$char['SUB_VALUES']['CHARS_DESCRS']['~VALUE']['TEXT']?></p>
                        <? } ?>

                    </div>

                    <? if($cnt%3 == 0 && $cnt < count($arResult['PROPERTIES']['CHARS']['VALUE'])){
                        echo '</div><div class="review-conclusion layout-flood equal-height-js">';
                    }

                } ?>

            </div>

        <? } ?>

    </div>

</article>