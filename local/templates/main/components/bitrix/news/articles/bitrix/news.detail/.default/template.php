<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project; ?>


<?=$arResult['DETAIL_TEXT']?>

<? if(
    is_array($arResult["PROPERTIES"]['OBZ_PHOTOS']['VALUE'])
    &&
    count($arResult["PROPERTIES"]['OBZ_PHOTOS']['VALUE']) > 0
){ ?>

    <div class="swiper-container images-gallery images-gallery-js">
        <div class="swiper-wrapper">

            <? foreach( $arResult["PROPERTIES"]['OBZ_PHOTOS']['VALUE'] as $key => $photo_id ){ ?>
                <div class="swiper-slide">
                    <img src="<?=tools\funcs::rIMGG($photo_id, 5, 1364, 936)?>" />
                </div>
            <? } ?>

        </div>
        <div class="swiper-pagination"></div>
    </div>

<? } ?>

<?=$arResult["PROPERTIES"]['DETAIL_TEXT_2']['~VALUE']['TEXT']?>
