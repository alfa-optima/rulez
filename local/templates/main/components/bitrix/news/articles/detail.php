<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

// Инфо об элементе
$el = tools\el::info_by_code( $arResult['VARIABLES']['ELEMENT_CODE'], $arParams['IBLOCK_ID'] );

if( intval($el['ID']) > 0 ){

    // Все категории статей
    $actionSections = project\article::sections();

    $APPLICATION->SetPageProperty("description", $el["PROPERTY_DESCRIPTION_VALUE"]?$el["PROPERTY_DESCRIPTION_VALUE"]:$el["NAME"]);
    $APPLICATION->SetPageProperty("keywords", $el["PROPERTY_KEYWORDS_VALUE"]?$el["PROPERTY_KEYWORDS_VALUE"]:$el["NAME"]);
    $APPLICATION->SetPageProperty("title", $el["PROPERTY_TITLE_VALUE"]?$el["PROPERTY_TITLE_VALUE"]:$el["NAME"]);

    $sect_id = tools\el::sections($el['ID'])[0]['ID'];
    $sect = tools\section::info($sect_id);
    $APPLICATION->AddChainItem($sect["NAME"], $sect["SECTION_PAGE_URL"]);
    $APPLICATION->AddChainItem($el["NAME"], $el["DETAIL_PAGE_URL"]);

    // Обзоры
    if( $sect['UF_IS_OBZOR'] ){

        $product_chars = '';
        if( intval($el["PROPERTY_TOVAR_VALUE"]) > 0 ){
            $iblock_id = tools\el::getIblock($el["PROPERTY_TOVAR_VALUE"]);
            $catalogIblocks = project\catalog::iblocks();
            if( in_array($iblock_id, array_keys($catalogIblocks)) ){
                $tovar = tools\el::info($el["PROPERTY_TOVAR_VALUE"]);
                ob_start();
                    $componentElementParams = Array(
                        'prop_groups' => project\catalog::getPropGroups($tovar),
                        'stopProps' => project\catalog::detailCardStopProps(),
                        'IS_AUTH' => $USER->IsAuthorized()?'Y':'N',
                        'IBLOCK_TYPE' => project\catalog::IBLOCK_TYPE,
                        'IBLOCK_ID' => $iblock_id,
                        "ACTION_VARIABLE" => "action",
                        "ADD_DETAIL_TO_SLIDER" => "N",
                        "ADD_ELEMENT_CHAIN" => "N",
                        "ADD_PICT_PROP" => "-",
                        "ADD_PROPERTIES_TO_BASKET" => "Y",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "ADD_TO_BASKET_ACTION" => array("BUY"),
                        "BACKGROUND_IMAGE" => "-",
                        "BASKET_URL" => "/personal/basket.php",
                        "BRAND_USE" => "N",
                        "BROWSER_TITLE" => "-",
                        "CACHE_GROUPS" => "Y",
                        'CACHE_TYPE' => project\catalog::CACHE_TYPE,
                        'CACHE_TIME' => project\catalog::CACHE_TIME,
                        "CHECK_SECTION_ID_VARIABLE" => "N",
                        "CONVERT_CURRENCY" => "N",
                        "DETAIL_PICTURE_MODE" => "IMG",
                        "DETAIL_URL" => "",
                        "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                        "DISPLAY_COMPARE" => "N",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PREVIEW_TEXT_MODE" => "E",
                        "ELEMENT_CODE" => "",
                        "ELEMENT_ID" => $el["PROPERTY_TOVAR_VALUE"],
                        "GIFTS_DETAIL_BLOCK_TITLE" => "Выберите один из подарков",
                        "GIFTS_DETAIL_HIDE_BLOCK_TITLE" => "N",
                        "GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => "3",
                        "GIFTS_DETAIL_TEXT_LABEL_GIFT" => "Подарок",
                        "GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => "Выберите один из товаров, чтобы получить подарок",
                        "GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE" => "N",
                        "GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => "3",
                        "GIFTS_MESS_BTN_BUY" => "Выбрать",
                        "GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
                        "GIFTS_SHOW_IMAGE" => "Y",
                        "GIFTS_SHOW_NAME" => "Y",
                        "GIFTS_SHOW_OLD_PRICE" => "Y",
                        "HIDE_NOT_AVAILABLE" => "N",
                        "LABEL_PROP" => "-",
                        "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
                        "LINK_IBLOCK_ID" => "",
                        "LINK_IBLOCK_TYPE" => "",
                        "LINK_PROPERTY_SID" => "",
                        "MESSAGE_404" => "",
                        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                        "MESS_BTN_BUY" => "Купить",
                        "MESS_BTN_COMPARE" => "Сравнить",
                        "MESS_BTN_SUBSCRIBE" => "Подписаться",
                        "MESS_NOT_AVAILABLE" => "Нет в наличии",
                        "META_DESCRIPTION" => "-",
                        "META_KEYWORDS" => "-",
                        "OFFERS_LIMIT" => "0",
                        "PARTIAL_PRODUCT_PROPERTIES" => "N",
                        "PRICE_CODE" => array( project\catalog::RASSROCHKA_PRICE_CODE, project\catalog::BASE_PRICE_CODE ),
                        "PRICE_VAT_INCLUDE" => "Y",
                        "PRICE_VAT_SHOW_VALUE" => "N",
                        "PRODUCT_ID_VARIABLE" => "id",
                        "PRODUCT_PROPERTIES" => array(),
                        "PRODUCT_PROPS_VARIABLE" => "prop",
                        "PRODUCT_QUANTITY_VARIABLE" => "",
                        "PRODUCT_SUBSCRIPTION" => "N",
                        "PROPERTY_CODE" => [],
                        "SECTION_ID_VARIABLE" => "SECTION_ID",
                        "SECTION_URL" => "",
                        "SEF_MODE" => "N",
                        "SET_BROWSER_TITLE" => "N",
                        "SET_CANONICAL_URL" => "N",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_META_DESCRIPTION" => "N",
                        "SET_META_KEYWORDS" => "N",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "N",
                        "SET_VIEWED_IN_COMPONENT" => "N",
                        "SHOW_404" => "N",
                        "SHOW_CLOSE_POPUP" => "N",
                        "SHOW_DEACTIVATED" => "N",
                        "SHOW_DISCOUNT_PERCENT" => "N",
                        "SHOW_MAX_QUANTITY" => "N",
                        "SHOW_OLD_PRICE" => "N",
                        "SHOW_PRICE_COUNT" => "1",
                        "TEMPLATE_THEME" => "blue",
                        "USE_COMMENTS" => "N",
                        "USE_ELEMENT_COUNTER" => "Y",
                        "USE_GIFTS_DETAIL" => "Y",
                        "USE_GIFTS_MAIN_PR_SECTION_LIST" => "Y",
                        "USE_MAIN_ELEMENT_SECTION" => "N",
                        "USE_PRICE_COUNT" => "N",
                        "USE_PRODUCT_QUANTITY" => "N",
                        "USE_VOTE_RATING" => "N"
                    );
                    $APPLICATION->IncludeComponent(
                        'bitrix:catalog.element', 'article_product_chars',
                        $componentElementParams
                    );
                    $product_chars = ob_get_contents();
                ob_end_clean();
            }
        } ?>

        <? $ElementID = $APPLICATION->IncludeComponent(
            "bitrix:news.detail", "obzory",
            Array(
                'product_chars' => $product_chars,
                "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
                "DISPLAY_NAME" => $arParams["DISPLAY_NAME"],
                "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
                "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "FIELD_CODE" => $arParams["DETAIL_FIELD_CODE"],
                "PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
                "DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
                "SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
                "META_KEYWORDS" => $arParams["META_KEYWORDS"],
                "META_DESCRIPTION" => $arParams["META_DESCRIPTION"],
                "BROWSER_TITLE" => $arParams["BROWSER_TITLE"],
                "SET_CANONICAL_URL" => $arParams["DETAIL_SET_CANONICAL_URL"],
                "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
                "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
                "SET_TITLE" => $arParams["SET_TITLE"],
                "MESSAGE_404" => $arParams["MESSAGE_404"],
                "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                "SHOW_404" => $arParams["SHOW_404"],
                "FILE_404" => $arParams["FILE_404"],
                "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
                "ADD_SECTIONS_CHAIN" => $arParams["ADD_SECTIONS_CHAIN"],
                "ACTIVE_DATE_FORMAT" => $arParams["DETAIL_ACTIVE_DATE_FORMAT"],
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                "USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
                "GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
                "DISPLAY_TOP_PAGER" => $arParams["DETAIL_DISPLAY_TOP_PAGER"],
                "DISPLAY_BOTTOM_PAGER" => $arParams["DETAIL_DISPLAY_BOTTOM_PAGER"],
                "PAGER_TITLE" => $arParams["DETAIL_PAGER_TITLE"],
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => $arParams["DETAIL_PAGER_TEMPLATE"],
                "PAGER_SHOW_ALL" => $arParams["DETAIL_PAGER_SHOW_ALL"],
                "CHECK_DATES" => $arParams["CHECK_DATES"],
                "ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
                "ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
                "SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
                "SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
                "IBLOCK_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
                "USE_SHARE" => $arParams["USE_SHARE"],
                "SHARE_HIDE" => $arParams["SHARE_HIDE"],
                "SHARE_TEMPLATE" => $arParams["SHARE_TEMPLATE"],
                "SHARE_HANDLERS" => $arParams["SHARE_HANDLERS"],
                "SHARE_SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
                "SHARE_SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
                "ADD_ELEMENT_CHAIN" => (isset($arParams["ADD_ELEMENT_CHAIN"]) ? $arParams["ADD_ELEMENT_CHAIN"] : ''),
                'STRICT_SECTION_CHECK' => (isset($arParams['STRICT_SECTION_CHECK']) ? $arParams['STRICT_SECTION_CHECK'] : ''),
            ),
            $component
        ); ?>

        
    <? } else {

        $goods_pos_enum = tools\prop_enum::getByID($el['PROPERTY_GOODS_POSITION_ENUM_ID']); ?>
        
        <div class="content">

            <div class="content-align">
                <section class="content-inner article-content">

                    <h1><?=$el['NAME']?></h1>
                    
                    <? if( $goods_pos_enum['XML_ID'] == 'POS_1' ){
                        $APPLICATION->IncludeComponent(
                            "aoptima:articleGoodsBlock", "",
                            array('el' => $el, 'pos' => $goods_block_pos_xml_id)
                        );
                    } ?>

                    <?=$el['DETAIL_TEXT']?>

                    <? if( $goods_pos_enum['XML_ID'] == 'POS_2' ){
                        $APPLICATION->IncludeComponent(
                            "aoptima:articleGoodsBlock", "",
                            array('el' => $el, 'pos' => $goods_block_pos_xml_id)
                        );
                    } ?>

                    <? // articleMediaSlider
                    $APPLICATION->IncludeComponent(
                        "aoptima:articleMediaSlider", "",
                        [ 'el' => $el ]
                    ); ?>

                    <? if( $goods_pos_enum['XML_ID'] == 'POS_3' ){
                        $APPLICATION->IncludeComponent(
                            "aoptima:articleGoodsBlock", "",
                            array('el' => $el, 'pos' => $goods_block_pos_xml_id)
                        );
                    } ?>

                    <?=html_entity_decode($el["PROPERTY_DETAIL_TEXT_2_VALUE"]['TEXT'])?>

                    <? if(
                        $goods_pos_enum['XML_ID'] == 'POS_4'
                        ||
                        !$goods_pos_enum
                    ){
                        $APPLICATION->IncludeComponent(
                            "aoptima:articleGoodsBlock", "",
                            array('el' => $el, 'pos' => $goods_block_pos_xml_id)
                        );
                    } ?>

                </section>
            </div>


            <div class="article-share-layout">
                <div class="article-share share42init"></div>
                <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/share42/share42.js"></script>
            </div>


            <aside class="sidebar">
                <div class="sidebar__holder">
                    <ul class="articles-nav">
                        <li class="current">
                            <a href="/articles/">Статьи</a>
                            <ul>
                                <? foreach( $actionSections as $key => $section ){ ?>
                                    <? if( $section['ID'] == $sect['ID'] ){ ?>
                                        <li class="current"><a href="<?=$section['SECTION_PAGE_URL']?>"><?=$section['NAME']?></a></li>
                                    <? } else { ?>
                                        <li><a href="<?=$section['SECTION_PAGE_URL']?>"><?=$section['NAME']?></a></li>
                                    <? } ?>
                                <? } ?>
                            </ul>
                        </li>
                    </ul>
                </div>
            </aside>

        </div>

    <? }

} else {

    include($_SERVER["DOCUMENT_ROOT"]."/local/templates/main/include/404include.php");

} ?>