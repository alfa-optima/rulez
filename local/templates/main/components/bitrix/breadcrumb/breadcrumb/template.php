<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
include "lang/ru/template.php";

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$arURI = tools\funcs::arURI();

if(empty($arResult))
	return "";

$strReturn = '<div class="breadcrumbs"><ul itemscope itemtype="http://schema.org/BreadcrumbList" class="breadcrumbs__list"><li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="/" title="Главная"><span itemprop="name">Главная</span><meta itemprop="position" content="0" /></a></li>';
$page_title = $GLOBALS['APPLICATION']->GetTitle();

$itemSize = count($arResult)-1;

$cnt = 0;
for ($index = 0; $index <= $itemSize; $index++) { $cnt++;
	$title = /*htmlspecialcharsex(*/ $arResult[$index]["TITLE"] /*)*/;
	if ($arResult[($index+1)]["TITLE"]!=$arResult[($index)]["TITLE"]){
		if(($arResult[$index]["LINK"] <> "") && $arResult[$index+1]["TITLE"]){
			$strReturn .= '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="'.$arResult[$index]["LINK"].'" title="'.$title.'"><span itemprop="name">'.$title.'</span><meta itemprop="position" content="'.$cnt.'" /></a></li>';
		} else if( $arURI[1] != 'product' ){
			$strReturn .= '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name">'.$title.'</span></li>';
		}
	}
}

$strReturn .= '</ul></div>';

return $strReturn; ?>
