<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$this->setFrameMode(true);


if ( count($arResult['ITEMS']) > 0 ){

    foreach( $arResult['ITEMS'] as $arItem ){

        $tov_name = $arItem['~NAME'];

        if( strlen($arParams['SEARCH_Q']) > 0 ){
            $tov_name = project\funcs::searchUpdateName( $tov_name, $arParams['~SEARCH_Q'] );
        } ?>
        
        <div class="s-result__list-item">

            <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="s-result__list-inner">

                <div class="s-result__list-figure">
                    <? if( intval($arItem['DETAIL_PICTURE']['ID']) > 0 ){ ?>
                        <img src="<?=tools\funcs::rIMGG($arItem['DETAIL_PICTURE']['ID'], 4, 261, 178)?>" alt="<?=$arItem['NAME']?>">
                    <? } else { ?>
                        <img itemprop="image" src="<?=SITE_TEMPLATE_PATH?>/img/no-photo.png">
                    <? } ?>

                </div>

                <div class="s-result__list-content">

                    <strong class="s-result__list-title"><?=$tov_name?></strong>

                    <div class="s-result__list-meta">

                        <? if( $arItem['PRICES'][project\catalog::BASE_PRICE_CODE]['VALUE'] > 0 ){ ?>

                            <div class="s-result__list-price"><?=number_format($arItem['PRICES'][project\catalog::BASE_PRICE_CODE]['DISCOUNT_VALUE_VAT'], project\catalog::PRICE_ROUND, ".", " ")?> р.</div>

                        <? } ?>

                    </div>

                </div>

            </a>

        </div>

    <? }

} ?>





