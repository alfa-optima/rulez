<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$stopProps = $arParams['~stopProps'];

if( count($arResult['ITEMS']) > 0 ){

    $props = [];

    foreach ( $arResult['ITEMS'] as $item_key => $arItem ){
        foreach ( $arItem['PROPERTIES'] as $prop_key => $arProp ){
            if( !in_array( $arProp['CODE'], $stopProps ) ){
                $props[$arProp['NAME']] = [
                    'ID' => $arProp['ID'],
                    'CODE' => $arProp['CODE'],
                    'NAME' => $arProp['NAME'],
                ];
            }
        }
        ksort($props);
    }

    foreach( $props as $prop_key => $prop ){
        $nal = false;
        foreach ( $arResult['ITEMS'] as $item_key => $arItem ){
            if( strlen( $arItem['PROPERTIES'][$prop['CODE']]['VALUE'] ) > 0 ){   $nal = true;   }
        }
        if( !$nal ){    unset( $props[$prop_key] );    }
    }

    $prop_groups = project\catalog::getPropGroups($arResult['ITEMS'][0]);

    $grouped_props = [];  $not_grouped_props = [];
    $grouped_props_ids = [];

    if(
        is_array($prop_groups)
        &&
        count($prop_groups) > 0
    ){
        foreach ( $prop_groups as $prop_xml_id => $group_name ){
            foreach ( $arResult['ITEMS'] as $item_key => $arItem ){
                foreach ( $arItem['PROPERTIES'] as $key => $prop ){
                    if(
                        strlen($prop['VALUE']) > 0
                        &&
                        !in_array( $prop['CODE'], $stopProps )
                        &&
                        $prop['XML_ID'] == $prop_xml_id
                    ){
                        $grouped_props[$group_name][$prop['CODE']] = $prop;
                        $grouped_props_ids[] = $prop['ID'];
                    }
                }
            }
        }
    }

    foreach ( $arResult['ITEMS'] as $item_key => $arItem ){
        foreach ( $arItem['PROPERTIES'] as $key => $prop ){
            if(
                strlen($prop['VALUE']) > 0
                &&
                !in_array( $prop['CODE'], $stopProps )
                &&
                !in_array($prop['ID'], $grouped_props_ids)
            ){
                $not_grouped_props[$prop['CODE']] = $prop;
            }
        }
    }

} ?>


<div class="content compareBlock">
    <div class="article-content">

        <h1 class="full-width">Сравнение товаров</h1>

        <? if( count($arResult['ITEMS']) > 0 ){ ?>

            <div class="products-compare products-compare-js">

                <!--show if cart is empty-->
                <div class="products-compare__empty products-compare__empty-js" style="display: none;"><a href="catalog.html">Выберите товары для сравнения</a></div>

                <div class="table-auto products-compare__table-js">
                    <table>

                        <thead class="products-compare__caption">
                        <tr>
                            <td>&nbsp;</td>

                            <? foreach( $arResult['ITEMS'] as $key => $arItem ){

                                $arItem['PICTURE'] = false;

                                $el_pics = [];
                                if( intval($arItem['DETAIL_PICTURE']['ID']) > 0 ){
                                    $el_pics[] = $arItem['DETAIL_PICTURE']['ID'];
                                }
                                if( is_array( $arItem['PROPERTIES']['MORE_PHOTO']['VALUE'] ) ){
                                    foreach ( $arItem['PROPERTIES']['MORE_PHOTO']['VALUE'] as $photo_id ){
                                        if( intval($photo_id) > 0 ){
                                            $el_pics[] = $photo_id;
                                        }
                                    }
                                }
                                if( count($el_pics) > 0 ){
                                    $pic_id = $el_pics[0];
                                    $arItem['PICTURE'] = tools\funcs::rIMGG($pic_id, 4, 175, 120);
                                } ?>

                                <td>
                                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>">

                                        <figure class="products-compare__prod-figure">
                                            <? if( $arItem['PICTURE'] ){ ?>
                                                <img src="<?=$arItem['PICTURE']?>" alt="<?=$arItem['NAME']?>">
                                            <? } else { ?>
                                                <img src="<?=SITE_TEMPLATE_PATH?>/img/no-photo.png">
                                            <? } ?>

                                        </figure>

                                        <div class="products-compare__prod-title"><?=$arItem['NAME']?></div>

                                        <strong class="products-compare__prod-price"><?=number_format($arItem['PRICES'][project\catalog::BASE_PRICE_CODE]['DISCOUNT_VALUE_VAT'], project\catalog::PRICE_ROUND, ".", " ")?> р.</strong>

                                        <div class="products-compare__remove compare_delete_button to___process" item_id="<?=$arItem['ID']?>"><em>x</em></div>

                                    </a>
                                </td>

                            <? } ?>

                        </tr>
                        </thead>

                        <? if( count($grouped_props) > 0 ){
                            foreach( $grouped_props as $group_name => $props ){ ?>

                                <tbody class="products-compare__group">

                                    <tr class="products-compare__title">
                                        <td colspan="100">
                                            <div><?=$group_name?></div>
                                        </td>
                                    </tr>

                                    <tr class="products-compare__row-intend">
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>

                                    <? foreach( $props as $key => $prop ){ ?>

                                        <tr>

                                            <td>
                                                <div><?=$prop['NAME']?></div>
                                            </td>

                                            <? foreach( $arResult['ITEMS'] as $key => $arItem ){ ?>

                                                <td>
                                                    <div><?=strlen($arItem["PROPERTIES"][$prop['CODE']]['VALUE'])>0?$arItem["PROPERTIES"][$prop['CODE']]['VALUE']:'-'?></div>
                                                </td>

                                            <? } ?>

                                        </tr>

                                    <? } ?>

                                    <tr class="products-compare__row-intend">
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>

                                </tbody>

                            <? }
                        } ?>

                        <? if( count($not_grouped_props) > 0 ){ ?>

                            <tbody class="products-compare__group">

                                <? if( count($grouped_props) > 0 ){ ?>

                                    <tr class="products-compare__title">
                                        <td colspan="100"><div>Другие параметры</div></td>
                                    </tr>

                                <? } ?>

                                <tr class="products-compare__row-intend">
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>

                                <? foreach( $not_grouped_props as $key => $prop ){ ?>

                                    <tr>

                                        <td>
                                            <div><?=$prop['NAME']?></div>
                                        </td>

                                        <? foreach( $arResult['ITEMS'] as $key => $arItem ){ ?>

                                            <td>
                                                <div><?=strlen($arItem["PROPERTIES"][$prop['CODE']]['VALUE'])>0?$arItem["PROPERTIES"][$prop['CODE']]['VALUE']:'-'?></div>
                                            </td>

                                        <? } ?>

                                    </tr>

                                <? } ?>

                                <tr class="products-compare__row-intend">
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>

                            </tbody>

                        <? } ?>

                    </table>

                </div>

            </div>

        <? } ?>

    </div>

</div>