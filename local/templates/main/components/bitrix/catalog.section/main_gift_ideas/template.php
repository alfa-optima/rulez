<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

if ( count($arResult['ITEMS']) > 0 ){ ?>

    <div id="gift_idea_<?=$arParams['CATEGORY_ID']?>">


        <div class="topics__list equal-height-js">

            <? foreach ($arResult['ITEMS'] as $key => $arItem){ ?>

                <article class="topics__item">

                    <? // catalog_item
                    $APPLICATION->IncludeComponent(
                        "aoptima:catalog_item", "slider",
                        array( 'arItem' => $arItem, 'hideChars' => 'Y' )
                    ); ?>

                </article>

            <? } ?>

            <? // adv_banner
            $APPLICATION->IncludeComponent(
                "aoptima:advBanner", "main_page_gift_ideas",
                array( 'type' => "main_page_gift_ideas" )
            ); ?>

        </div>

<!--        <a href="catalog.html#1" class="link-all">-->
<!--            <span>Все</span>-->
<!--            <svg height="6" width="10" fill="none">-->
<!--                <use xlink:href="#icon-angle"></use>-->
<!--            </svg>-->
<!--        </a>-->

    </div>

<? } ?>





