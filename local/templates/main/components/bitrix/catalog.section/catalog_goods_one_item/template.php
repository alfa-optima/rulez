<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

if ( count($arResult['ITEMS']) > 0 ){

    foreach ($arResult['ITEMS'] as $key => $arItem){
        // catalog_item
        $params = [
            'arItem' => $arItem,
            'component' => $this,
            'SEARCH_Q' => $arParams['SEARCH_Q']
        ];
        $APPLICATION->IncludeComponent( "aoptima:catalog_item", "", $params );
    }

}



