<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);
    
if ( count($arResult['ITEMS']) > 0 ){

    $cnt = 0;
    foreach ( $arResult['ITEMS'] as $key => $arItem ){

        // catalog_item
        $APPLICATION->IncludeComponent(
            "aoptima:catalog_item", "slider_small",
            array(
                'arItem' => $arItem,
                'category' => $arParams['~category']
            )
        );

    }

} ?>


