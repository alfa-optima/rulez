<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

if ( count($arResult['ITEMS']) > 0 ){ ?>

    <div id="basket_soput_<?=$arParams['category']['ID']?>">
        <div class="swiper-container shot-slider cart-similar-slider-js">

            <div class="swiper-wrapper">

                <? foreach ($arResult['ITEMS'] as $key => $arItem){

                    // catalog_item
                    $APPLICATION->IncludeComponent(
                        "aoptima:catalog_item", "slider_small",
                        array(
                            'arItem' => $arItem,
                            'category' => $arParams['~category'],
                            'isInBasket' => 'Y'
                        )
                    );

                } ?>

            </div>

            <div class="shot-slider__prev cart-similar-slider__prev-js">
                <svg width="19" height="13" viewBox="0 0 19 13" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6.1979 12.8045C6.46575 13.0652 6.88744 13.0652 7.1553 12.8045C7.4141 12.5526 7.4141 12.1334 7.1553 11.8821L2.30074 7.15731L18.3201 7.15731C18.6935 7.15731 19 6.86843 19 6.50499C19 6.14155 18.6935 5.84328 18.3201 5.84328L2.30074 5.84328L7.1553 1.12732C7.4141 0.866628 7.4141 0.446818 7.1553 0.195519C6.88744 -0.0651731 6.46575 -0.0651731 6.1979 0.195519L0.194104 6.0388C-0.0647013 6.29068 -0.0647013 6.7099 0.194104 6.9612L6.1979 12.8045Z"></path>
                </svg>
                <span>Prev</span>
            </div>

            <div class="shot-slider__next cart-similar-slider__next-js">
                <svg width="19" height="13" viewBox="0 0 19 13" xmlns="http://www.w3.org/2000/svg">
                    <path d="M12.8021 0.19552C12.5342 -0.0651732 12.1126 -0.0651732 11.8447 0.19552C11.5859 0.447405 11.5859 0.866628 11.8447 1.11793L16.6993 5.84269H0.679892C0.306465 5.84269 0 6.13157 0 6.49501C0 6.85845 0.306465 7.15672 0.679892 7.15672H16.6993L11.8447 11.8727C11.5859 12.1334 11.5859 12.5532 11.8447 12.8045C12.1126 13.0652 12.5342 13.0652 12.8021 12.8045L18.8059 6.9612C19.0647 6.70932 19.0647 6.2901 18.8059 6.0388L12.8021 0.19552Z"></path>
                </svg>
                <span>Next</span>
            </div>

        </div>
    </div>

<? } ?>





