<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

$class = '';
if( $arParams['IS_SEARCH'] == 'Y' ){  $class = 'is___search';  }
if( $arParams['IS_FAVORITES'] == 'Y' ){  $class = 'is___favorites';  }
if( $arParams['IS_BEST_PRICES'] == 'Y' ){  $class = 'is___best_prices';  }
if( $arParams['IS_GIFT_IDEAS'] == 'Y' ){  $class = 'is___gift_ideas';  }
if( $arParams['IS_BRAND'] == 'Y' ){  $class = 'is___brand';  }

$maxCNT = $arParams['PAGE_ELEMENT_COUNT'] - 1;

if( is_array($arParams['IDS']) ){
    $newItems = [];
    foreach ( $arResult['IDS'] as $id ){
        foreach ($arResult['ITEMS'] as $arItem){
            if( $arItem['ID'] == $id ){
                $newItems[] = $arItem;
            }
        }
    }
    $arResult['ITEMS'] = $newItems;
}



if( $arParams['IS_AJAX'] != 'Y' ){ ?>
    
    <div class="products" data-toggle-view-id="products">
        <section class="products__list equal-height-js catalogLoadArea">

            <? if ( count($arResult['ITEMS']) > 0 ){ ?>

                <? foreach ($arResult['ITEMS'] as $key => $arItem){ $cnt++;
                    // catalog_item
                    $params = [
                        'arItem' => $arItem,
                        'component' => $this,
                        'SEARCH_Q' => $arParams['SEARCH_Q']
                    ];
                    $APPLICATION->IncludeComponent( "aoptima:catalog_item", "", $params );
                } ?>

            <? } else { ?>

                <div class="load-more no___count_block">
                    <p>В данном разделе / по данному фильтру товаров нет</p>
                </div>

            <? } ?>


        </section>
    </div>


    <? $APPLICATION->IncludeComponent(
        "aoptima:pagination", "", [
            "CUR_URL" => $arParams['CUR_URL'],
            'productsCNT' => $arResult['NAV_RESULT']->NavRecordCount,
            'pagesCNT' => $arResult['NAV_RESULT']->NavPageCount,
            'pageNumber' => $arParams['PAGE_NUMBER'],
            'class' => $class
        ]
    ); ?>



    <div class="load-more" <?php if( $arParams['PAGE_NUMBER'] >= 2 || $arResult['NAV_RESULT']->NavPageCount <= 1){ ?>style="display:none;"<?php } ?>>
        <a
            style="cursor: pointer; <? if( $arParams['HAS_NEXT'] == 'N' ){ echo 'display:none;'; } ?>"
            class="btn-def load-more__btn <?=$class?> catalogMoreButton to___process"
        >Показать еще</a>
    </div>




<? } else {


    if ( count($arResult['ITEMS']) > 0 ){

        foreach ($arResult['ITEMS'] as $key => $arItem){
            // catalog_item
            $params = [
                'arItem' => $arItem,
                'component' => $this,
                'SEARCH_Q' => $arParams['SEARCH_Q']
            ];
            $APPLICATION->IncludeComponent( "aoptima:catalog_item", "", $params );
        }


        if( $arParams['IS_RELOAD'] == 'Y' ){

            $APPLICATION->IncludeComponent(
                "aoptima:pagination", "", [
                    "CUR_URL" => $arParams['CUR_URL'],
                    'productsCNT' => $arResult['NAV_RESULT']->NavRecordCount,
                    'pagesCNT' => $arResult['NAV_RESULT']->NavPageCount,
                    'pageNumber' => $arParams['PAGE_NUMBER'],
                    'hidden' => true,
                    'class' => $class
                ]
            );

        }

    }



}



