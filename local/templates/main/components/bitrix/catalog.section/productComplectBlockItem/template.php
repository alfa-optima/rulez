<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if( count($arResult['ITEMS']) > 0 ){

    foreach ($arResult['ITEMS'] as $key => $arItem){

        // catalog_item
        $APPLICATION->IncludeComponent(
            "aoptima:catalog_item", "complect",
            array('arItem' => $arItem)
        );

    }

}