<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

$class = 'is___complects';

$maxCNT = $arParams['PAGE_ELEMENT_COUNT'] - 1;

if( is_array($arParams['IDS']) ){
    $newItems = [];
    foreach ( $arResult['IDS'] as $id ){
        foreach ($arResult['ITEMS'] as $arItem){
            if( $arItem['ID'] == $id ){
                $newItems[] = $arItem;
            }
        }
    }
    $arResult['ITEMS'] = $newItems;
}



if( $arParams['IS_AJAX'] != 'Y' ){ ?>

    
    <div class="sets">
        <div class="sets__list lazy-images-js catalogLoadArea">
            
            <? if ( count($arResult['ITEMS']) > 0 ){

                $cnt = 0;
                foreach ($arResult['ITEMS'] as $key => $arItem){ $cnt++;

                    // complect_item
                    $params = [
                        'arItem' => $arItem,
                        'component' => $this
                    ];
                    $APPLICATION->IncludeComponent( "aoptima:complect_item", "", $params );

                }
            
            } ?>
            
        </div>
    </div>


    <? $APPLICATION->IncludeComponent(
        "aoptima:pagination", "", [
            'productsCNT' => $arResult['NAV_RESULT']->NavRecordCount,
            'pagesCNT' => $arResult['NAV_RESULT']->NavPageCount,
            'pageNumber' => $arParams['PAGE_NUMBER'],
            'class' => $class
        ]
    ); ?>


    <div class="load-more" <?php if( $arParams['PAGE_NUMBER'] >= 2 ){ ?>style="display:none;"<?php } ?>>
        <a
            style="cursor: pointer;<? if( $arParams['HAS_NEXT'] == 'N' ){ echo 'display:none;'; } ?>"
            class="btn-def load-more__btn <?=$class?> catalogMoreButton to___process"
        >Показать еще</a>
    </div>
    </div>



<? } else {


    if ( count($arResult['ITEMS']) > 0 ){

        $cnt = 0;
        foreach ($arResult['ITEMS'] as $key => $arItem){ $cnt++;

            // complect_item
            $params = [
                'arItem' => $arItem,
                'component' => $this
            ];
            $APPLICATION->IncludeComponent( "aoptima:complect_item", "", $params );

        }

        if( $arParams['IS_RELOAD'] == 'Y' ){
            $APPLICATION->IncludeComponent(
                "aoptima:pagination", "", [
                    'productsCNT' => $arResult['NAV_RESULT']->NavRecordCount,
                    'pagesCNT' => $arResult['NAV_RESULT']->NavPageCount,
                    'pageNumber' => $arParams['PAGE_NUMBER'],
                    'hidden' => true,
                    'class' => $class
                ]
            );
        }

    } else { ?>

        <div class="load-more no___count_block">
            <p>Больше ничего не найдено</p>
        </div>

    <? }


}





