<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams
@var array $arResult */
$this->setFrameMode(true);

if (count($arResult["ITEMS"]) > 0){ ?>

    <!--services-->
    <section class="helper-alt-section">
        <h2 class="hide">Сервисы</h2>
        <div class="helper-alt">
            <div class="helper-alt__list equal-height-js">

                <? foreach($arResult["ITEMS"] as $arItem){ ?>

                    <div class="helper-alt__item">
                        <strong><i class="<?=$arItem["PROPERTIES"]['ICON_CLASS']['VALUE']?>">&nbsp;</i> <span><?=$arItem['NAME']?></span></strong>
                        <p><?=$arItem['PREVIEW_TEXT']?></p>
                    </div>

                <? } ?>

            </div>
        </div>
    </section>
    <!--services end-->

<? } ?>