<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if ( count($arResult["ITEMS"]) > 0 ){ ?>
    <section class="main-section main-section_promo">

        <h2 class="hide">Промо слайдер</h2>

        <div class="promo-slider swiper-container promo-slider-js">
            <div class="swiper-wrapper">

                <? foreach($arResult["ITEMS"] as $arItem){
                    $arFile = \CFile::GetFileArray($arItem['PREVIEW_PICTURE']['ID']);
                    // Вариант 1
                    if ( $arItem['PROPERTIES']['VID']['VALUE_XML_ID'] == 'variant_1' ){ ?>

                        <div class="promo-slider__item swiper-slide" style="background: #<?=$arItem['PROPERTIES']['COLOR_TOP']['VALUE']?>; background: linear-gradient(to bottom, #<?=$arItem['PROPERTIES']['COLOR_TOP']['VALUE']?> 0%, #<?=$arItem['PROPERTIES']['COLOR_BOTTOM']['VALUE']?> 100%);">
                            <a class="w-promo-slide" <? if( strlen($arItem['PROPERTIES']['BUTTON_LINK']['VALUE']) > 0 ){ echo 'href="'.$arItem['PROPERTIES']['BUTTON_LINK']['VALUE'].'"'; } ?>>
                                <div class="w-promo-slide__layout layout-narrow max-wrap">

                                    <div class="w-promo-slide__figure">
                                        <img src="<?=tools\funcs::rIMGG($arItem['PREVIEW_PICTURE']['ID'], 5, $arFile['WIDTH'], 436)?>" class="word-img-parallax-js" alt="<?=$arItem["NAME"]?>">
                                    </div>

                                    <div class="w-promo-slide__content" data-swiper-parallax="-400">
                                        <div class="w-promo-slide__title"><?=$arItem['NAME']?></div>
                                        <? if(
                                            strlen($arItem['PROPERTIES']['BUTTON_LINK']['VALUE']) > 0
                                            &&
                                            strlen($arItem['PROPERTIES']['BUTTON_TITLE']['VALUE']) > 0
                                        ){ ?>
                                            <div class="w-promo-slide__footer">
                                                <span class="btn-round btn-round_small w-promo-slide__btn"><?=$arItem['PROPERTIES']['BUTTON_TITLE']['VALUE']?></span>
                                            </div>
                                        <? } ?>
                                    </div>

                                </div>
                            </a>
                        </div>

                    <? // Вариант 2
                    } else if ( $arItem['PROPERTIES']['VID']['VALUE_XML_ID'] == 'variant_2' ){ ?>

                        <div class="promo-slider__item swiper-slide" style="background-color: #<?=$arItem['PROPERTIES']['COLOR_TOP']['VALUE']?>; background: linear-gradient(to bottom, #<?=$arItem['PROPERTIES']['COLOR_TOP']['VALUE']?> 0%, #<?=$arItem['PROPERTIES']['COLOR_BOTTOM']['VALUE']?> 100%);">

                            <a class="i-promo-slide" <? if( strlen($arItem['PROPERTIES']['BUTTON_LINK']['VALUE']) > 0 ){ echo 'href="'.$arItem['PROPERTIES']['BUTTON_LINK']['VALUE'].'"'; } ?>>

                                <div class="i-promo-slide__figure" data-swiper-parallax="200" style="background-image: url(<?=tools\funcs::rIMGG($arItem['PREVIEW_PICTURE']['ID'], 5, $arFile['WIDTH'], 436)?>);">&nbsp;</div>

                                <div class="i-promo-slide__layout layout-narrow max-wrap">
                                    <div class="i-promo-slide__content">
                                        <div class="i-promo-slide__title" data-swiper-parallax="-700"><?=$arItem['NAME']?></div>
                                        <div class="i-promo-slide__footer" data-swiper-parallax="-400">
                                            <? if(
                                                strlen($arItem['PROPERTIES']['BUTTON_LINK']['VALUE']) > 0
                                                &&
                                                strlen($arItem['PROPERTIES']['BUTTON_TITLE']['VALUE']) > 0
                                            ){ ?>
                                                <span class="btn-round i-promo-slide__btn"><?=$arItem['PROPERTIES']['BUTTON_TITLE']['VALUE']?></span>
                                            <? } ?>
                                        </div>
                                    </div>
                                </div>

                            </a>
                        </div>

                     <? // Вариант 3
                    } else if ( $arItem['PROPERTIES']['VID']['VALUE_XML_ID'] == 'variant_3' ){ ?>


                        <div class="promo-slider__item swiper-slide" style="background-color: #<?=$arItem['PROPERTIES']['COLOR_TOP']['VALUE']?>; background: linear-gradient(to bottom, #<?=$arItem['PROPERTIES']['COLOR_TOP']['VALUE']?> 0%, #<?=$arItem['PROPERTIES']['COLOR_BOTTOM']['VALUE']?> 100%);">

                            <a <? if( strlen($arItem['PROPERTIES']['BUTTON_LINK']['VALUE']) > 0 ){ echo 'href="'.$arItem['PROPERTIES']['BUTTON_LINK']['VALUE'].'"'; } ?> class="c-promo-slide">
                                <div class="c-promo-slide__layout layout-narrow max-wrap">

                                    <div class="c-promo-slide__figure">

                                        <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" class="c-promo-slide__img c-promo-slide__img_desk" />

                                        <?php if( intval($arItem['DETAIL_PICTURE']['ID']) > 0  ){ ?>
                                            <img src="<?=$arItem['DETAIL_PICTURE']['SRC']?>" class="c-promo-slide__img c-promo-slide__img_mob"/>
                                        <? } ?>

                                    </div>

                                    <div class="c-promo-slide__content" data-swiper-parallax="-400">

<!--                                    <div class="c-promo-slide__title">Суперпредложение!</div>-->

                                        <? if(
                                            strlen($arItem['PROPERTIES']['BUTTON_LINK']['VALUE']) > 0
                                            &&
                                            strlen($arItem['PROPERTIES']['BUTTON_TITLE']['VALUE']) > 0
                                        ){ ?>
                                            <div class="c-promo-slide__footer">
                                                <span class="btn-round btn-round_small w-promo-slide__btn"><?=$arItem['PROPERTIES']['BUTTON_TITLE']['VALUE']?></span>
                                            </div>
                                        <? } ?>

                                    </div>

                                </div>
                            </a>

                        </div>

                    <? }
                } ?>

            </div>

            <div class="swiper-pagination"></div>
            <div class="slider-arrow slider-arrow_prev slider-arrow_prev-js"></div>
            <div class="slider-arrow slider-arrow_next slider-arrow_next-js"></div>

        </div>
    </section>
<? } ?>