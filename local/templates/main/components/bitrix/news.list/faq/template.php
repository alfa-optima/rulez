<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams
@var array $arResult */
$this->setFrameMode(true);

if (count($arResult["ITEMS"]) > 0){ ?>

    <div class="content">
        <div class="article-content">

            <h1>Часто задаваемые вопросы</h1>

<!--            <h2>Ответы на часто задаваемые вопросы</h2>-->

            <div class="rolls layout-text payment__methods rolls-js">

                <? foreach($arResult["ITEMS"] as $arItem){ ?>

                    <div class="rolls__item rolls__item-js">
                        <div class="rolls__header rolls__header-js">
                            <span><?=$arItem['NAME']?></span>
                            <em class="rolls__angle rolls__hand-js">&#9660;</em>
                        </div>
                        <div class="rolls__panel typography rolls__panel-js">
                            <p><?=$arItem['PREVIEW_TEXT']?></p>
                        </div>
                    </div>

                <? } ?>

            </div>
        </div>
    </div>

<? } ?>