<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

if (count($arResult["ITEMS"]) > 0){ ?>

    <ul class="h-contacts__phones">

        <? foreach($arResult["ITEMS"] as $arItem){ ?>

            <li>
                <a href="tel:+<?=preg_replace('/[^0-9]/', '', strip_tags($arItem["PROPERTIES"]['PHONE']['VALUE']))?>"
                   class="<?=$arItem["PROPERTIES"]['CSS_CLASS_COLOR']['VALUE']?>">
                    <span><?=html_entity_decode($arItem["PROPERTIES"]['PHONE']['VALUE'])?></span>
                </a>
            </li>

        <? } ?>

    </ul>

<? } ?>