<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

//////////////////////////////////////////////
// $arItem["PROPERTIES"]["PROP_CODE"]["VALUE"]
//////////////////////////////////////////////

if (count($arResult["ITEMS"]) > 0){ ?>

    <div class="soc-footer__list">

        <? foreach($arResult["ITEMS"] as $arItem){
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], \CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], \CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));?>

            <div class="soc-footer__item" id='<?=$this->GetEditAreaId($arItem["ID"]);?>'>
                <a target="_blank" href="<?=$arItem["PROPERTIES"]['LINK']['VALUE']?>" title="<?=$arItem['NAME']?>" class="<?=$arItem["PROPERTIES"]['CSS_CLASS']['VALUE']?>">
                    <span><?=$arItem['NAME']?></span>
                    <?=html_entity_decode($arItem["PROPERTIES"]['SVG']['VALUE']['TEXT'])?>
                </a>
            </div>

        <? } ?>

    </div>

<? } ?>