<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

if (count($arResult["ITEMS"]) > 0){ ?>

    <a href="tel:+<?=preg_replace('/[^0-9]/', '', strip_tags($arResult["ITEMS"][0]["PROPERTIES"]['PHONE']['VALUE']))?>">
        <? foreach($arResult["ITEMS"] as $arItem){ ?>
            <i class="<?=$arItem["PROPERTIES"]['CSS_CLASS']['VALUE']?>">&nbsp;</i>
        <? } ?>
        <span><strong><?=strip_tags($arResult["ITEMS"][0]["PROPERTIES"]['PHONE_SHORT']['VALUE'])?></strong></span>
    </a>

<? } ?>