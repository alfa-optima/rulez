<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams
@var array $arResult */
$this->setFrameMode(true);
\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if (count($arResult["ITEMS"]) > 0){ ?>
    <section class="main-section main-section_opinions layout-narrow max-wrap">
        <h2 class="hide">Статьи</h2>
        <div class="opinions">
            <div class="opinions__list equal-height-js">
                <? $cnt = 0;
                foreach( $arResult["ITEMS"] as $key => $arItem ){ $cnt++;
                    if( $cnt == 1 ){ ?>
                        <div class="opinions__item">
                            <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="opinion opinion_alt">
                                <div class="opinion__layout">
                                    <? if( intval($arItem['PROPERTIES']['EXPERT_PHOTO']['VALUE']) > 0 ){ ?>
                                        <div class="opinion__figure">
                                            <img src="<?=tools\funcs::rIMGG($arItem['PROPERTIES']['EXPERT_PHOTO']['VALUE'], 5, 62, 62)?>" alt="Полезные советы">
                                        </div>
                                    <? } ?>
                                    <div class="opinion__label opinion__label_alt">Полезные советы</div>
                                    <div class="opinion__text opinion__text_alt"><?=$arItem['NAME']?></div>
                                </div>
                            </a>
                        </div>
                    <? } else if( $cnt == 2 ){ ?>
                        <div class="opinions__item">
                            <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="opinion">
                                <div class="opinion__layout">
                                    <? if( intval($arItem['PROPERTIES']['EXPERT_PHOTO']['VALUE']) > 0 ){ ?>
                                        <div class="opinion__figure">
                                            <img src="<?=tools\funcs::rIMGG($arItem['PROPERTIES']['EXPERT_PHOTO']['VALUE'], 5, 62, 62)?>" alt="Полезные советы">
                                        </div>
                                    <? } ?>
                                    <div class="opinion__label opinion__label_alt">Полезные советы</div>
                                    <div class="opinion__text opinion__text_alt"><?=$arItem['NAME']?></div>
                                </div>
                            </a>
                        </div>
                    <? }
                } ?>
            </div>
        </div>
    </section>
<? } ?>