<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$cnt = 0;
$maxCNT = $arParams["NEWS_COUNT"]-1;

$allCNT = count($arResult['ITEMS']);

// Подгрузка
if ( $arParams['IS_LOAD'] == 'Y' ){

    if( count($arResult['ITEMS']) > 0 ){
        foreach ( $arResult['ITEMS'] as $key => $arItem ){ $cnt++;
            if( $cnt <= $maxCNT ){ ?>

                <div class="news__list-item article___item" item_id="<?=$arItem['ID']?>">
                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="news-elem">
                        <div class="news-elem__content">
                            <div class="news-elem__content-align">
                                <div class="news-elem__category"><?=$arParams['articleSections'][$arItem['IBLOCK_SECTION_ID']]['NAME']?></div>
                                <div class="news-elem__title"><span><?=$arItem['NAME']?></span></div>
                                <time class="news-elem__date" datetime="<?=ConvertDateTime($arItem["PROPERTIES"]['SORT_DATE']['VALUE'], "YYYY-MM-DD", "ru")?>"><?=ConvertDateTime($arItem["PROPERTIES"]['SORT_DATE']['VALUE'], "DD.MM.YYYY", "ru")?></time>
                                <div class="news-elem__text"><?=$arItem['PREVIEW_TEXT']?></div>
                            </div>
                        </div>
                        <div class="news-elem__figure lozad" data-background-image="<?=tools\funcs::rIMGG($arItem["PREVIEW_PICTURE"]['ID'], 5, 287, 223)?>" style="background-color: #8f77ed; background-image: url(<?=SITE_TEMPLATE_PATH?>/img/preloader.svg);">&nbsp;</div>
                    </a>
                </div>

            <? } else {
                echo '<ost></ost>';
            }
        }

    }

// Первый вывод
} else { ?>

    <articles>
    
        <!--CONTENT-->
        <section class="section-news section-news-entry first___articles_block">
            <div class="caption-box">

                <h1 class="caption-box__title">Статьи</h1>

                <div class="caption-box__options">

                    <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
                        <symbol id="select-arrow" viewBox="0 0 10 6">
                            <path d="M0.292893 0.292893C0.683417 -0.0976311 1.31658 -0.0976311 1.70711 0.292893L5 3.58579L8.29289 0.292893C8.68342 -0.0976311 9.31658 -0.0976311 9.70711 0.292893C10.0976 0.683417 10.0976 1.31658 9.70711 1.70711L5.70711 5.70711C5.31658 6.09763 4.68342 6.09763 4.29289 5.70711L0.292893 1.70711C-0.0976311 1.31658 -0.0976311 0.683417 0.292893 0.292893Z"></path>
                        </symbol>
                    </svg>

                    <div class="topics-links__nav">

                        <div class="topics-links__select select-in-selector-js">
                            <span><?=$arParams['section']["NAME"]?></span>
                            <i><svg height="10" width="10">
                                <use xlink:href="#select-arrow" />
                            </svg></i>
                        </div>

                        <? if(
                            is_array($arParams['articleSections'])
                            &&
                            count($arParams['articleSections']) > 0
                        ){ ?>

                            <div class="topics-links select-in-drop-js">
                                <? foreach( $arParams['articleSections'] as $sect_id => $sect ){ ?>
                                    <div class="topics-links__item <? if( $sect_id == $arParams['section']['ID'] ){  echo 'current';  } ?>">
                                        <a style="cursor: pointer" class="article_section_link to___process" section_id="<?=$sect['ID']?>"><?=$sect['NAME']?></a>
                                    </div>
                                <? } ?>
                            </div>

                        <? } ?>

                    </div>

                </div>

            </div>

            <div class="news-entry">
                <div class="news-entry__list equal-height-js">

                    <? if( count($arResult['ITEMS']) > 0 ){

                        foreach ( $arResult['ITEMS'] as $key => $arItem ){ $cnt++;

                            if( $cnt == 1 ){ ?>

                                <div class="news-entry__list-item news-entry__list-item_large article___item" item_id="<?=$arItem['ID']?>">
                                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="news-elem news-elem_alt">
                                        <div class="news-elem__content">
                                            <div class="news-elem__content-align">
                                                <div class="news-elem__title"><span><?=$arItem['NAME']?></span></div>
                                                <time class="news-elem__date" datetime="<?=ConvertDateTime($arItem["PROPERTIES"]['SORT_DATE']['VALUE'], "YYYY-MM-DD", "ru")?>"><?=ConvertDateTime($arItem["PROPERTIES"]['SORT_DATE']['VALUE'], "DD.MM.YYYY", "ru")?></time>
                                            </div>
                                        </div>
                                        <div class="news-elem__figure lozad" data-background-image="<?=\CFile::GetPath($arItem["PREVIEW_PICTURE"]['ID'])?>" style="background-color: #8f77ed; background-image: url(<?=SITE_TEMPLATE_PATH?>/img/preloader.svg);">&nbsp;</div>
                                    </a>
                                </div>

                                <? unset($arResult['ITEMS'][$key]);

                            } else if(
                                $cnt == 2
                                &&
                                $cnt <= $maxCNT
                            ){ ?>

                                <div class="news-entry__list-item article___item" item_id="<?=$arItem['ID']?>">
                                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="news-elem">
                                        <div class="news-elem__content">
                                            <div class="news-elem__content-align">
                                                <div class="news-elem__category"><?=$arParams['articleSections'][$arItem['IBLOCK_SECTION_ID']]['NAME']?></div>
                                                <div class="news-elem__title"><span><?=$arItem['NAME']?></span></div>
                                                <time class="news-elem__date" datetime="<?=ConvertDateTime($arItem["PROPERTIES"]['SORT_DATE']['VALUE'], "YYYY-MM-DD", "ru")?>"><?=ConvertDateTime($arItem["PROPERTIES"]['SORT_DATE']['VALUE'], "DD.MM.YYYY", "ru")?></time>
                                                <div class="news-elem__text"><?=$arItem['PREVIEW_TEXT']?></div>
                                            </div>
                                        </div>
                                        <div class="news-elem__figure lozad" data-background-image="<?=tools\funcs::rIMGG($arItem["PREVIEW_PICTURE"]['ID'], 5, 287, 223)?>" style="background-color: #8f77ed; background-image: url(<?=SITE_TEMPLATE_PATH?>/img/preloader.svg);">&nbsp;</div>
                                    </a>
                                </div>

                                <?  unset($arResult['ITEMS'][$key]);

                            }
                        }

                    } else { ?>

                        <p class="no___count" style="margin: 5rem 0 7rem 1.8rem">В данном разделе статей нет</p>

                    <? } ?>

                </div>
            </div>

        </section>

        <? if( count($arResult['ITEMS']) > 0 ){ ?>

            <section class="section-news second___articles_block">
                <h2 class="section__title hide-text">Статьи</h2>
                <div class="news">
                    <div class="news__list equal-height-js articlesLoadArea">

                        <? $cnt = 2;
                        foreach ( $arResult['ITEMS'] as $key => $arItem ){ $cnt++;
                            if( $cnt <= $maxCNT ){ ?>

                                <div class="news__list-item article___item" item_id="<?=$arItem['ID']?>">
                                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="news-elem">
                                        <div class="news-elem__content">
                                            <div class="news-elem__content-align">
                                                <div class="news-elem__category"><?=$arParams['articleSections'][$arItem['IBLOCK_SECTION_ID']]['NAME']?></div>
                                                <div class="news-elem__title"><span><?=$arItem['NAME']?></span></div>
                                                <time class="news-elem__date" datetime="<?=ConvertDateTime($arItem["PROPERTIES"]['SORT_DATE']['VALUE'], "YYYY-MM-DD", "ru")?>"><?=ConvertDateTime($arItem["PROPERTIES"]['SORT_DATE']['VALUE'], "DD.MM.YYYY", "ru")?></time>
                                                <div class="news-elem__text"><?=$arItem['PREVIEW_TEXT']?></div>
                                            </div>
                                        </div>
                                        <div class="news-elem__figure lozad" data-background-image="<?=tools\funcs::rIMGG($arItem["PREVIEW_PICTURE"]['ID'], 5, 287, 223)?>" style="background-color: #8f77ed; background-image: url(<?=SITE_TEMPLATE_PATH?>/img/preloader.svg);">&nbsp;</div>
                                    </a>
                                </div>

                            <? }
                        } ?>

                    </div>
                </div>
            </section>

            <div class="load-more">
                <a style="cursor: pointer; <? if( $cnt <= $maxCNT ){ ?>display:none;<? } ?>" class="btn-def load-more__btn articlesMoreButton to___process">Показать еще</a>
            </div>

        <? } ?>
        
        <script>
        <? if(
            $arParams['IS_RELOAD'] != 'Y'
            &&
            $arParams['adv_banner']
            &&
            $allCNT > 0
        ){ ?>
            $(document).ready(function(){
                setArticlesAdvBanner('<?=$arParams['~adv_banner']?>');
            })
        <? } ?>
        </script>

    </articles>

<? } ?>




