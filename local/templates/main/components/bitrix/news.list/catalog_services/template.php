<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

if (count($arResult["ITEMS"]) > 0){ ?>

    <section class="section section_helper">
        <h2 class="hide">Сервисы</h2>
        <div class="helper-alt">
            <div class="helper-alt__list equal-height-js">

                <? foreach($arResult["ITEMS"] as $arItem){
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], \CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], \CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));?>

                    <div class="helper-alt__item" id='<?=$this->GetEditAreaId($arItem["ID"]);?>'>
                        <a>
                            <strong>
                                <i class="icon-helper-call-color">&nbsp;</i> <span><?=$arItem["NAME"]?></span>
                            </strong>
                            <p><?=$arItem["PREVIEW_TEXT"]?></p>
                        </a>
                    </div>

                <? } ?>

            </div>
        </div>
    </section>

<? } ?>