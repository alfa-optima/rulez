<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams
@var array $arResult */
$this->setFrameMode(true);

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if (count($arResult["ITEMS"]) > 0){ ?>

    <section class="main-section main-section_brands layout max-wrap">
        <h2 class="hide">Бренды</h2>
        <div class="brands">
            <div class="brands__list">

                <? foreach($arResult["ITEMS"] as $arItem){ ?>

                    <a href="/brand/<?=$arItem['CODE']?>/" class="brands__item">
                        <img src="<?=tools\funcs::rIMGG($arItem['PREVIEW_PICTURE']['ID'], 4, 140, 80)?>" alt="<?=$arItem['NAME']?>" />
                    </a>

                <? } ?>

            </div>
        </div>
    </section>

<? } ?>