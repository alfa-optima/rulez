<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global \CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if ( count($arResult["SECTIONS"]) > 0 ){
    $arSection = $arParams['~arSection'];
    foreach ( $arResult["SECTIONS"] as $sect ){
        if( $arSection['ID'] == $sect['ID'] ){
            $arSection['ELEMENT_CNT'] = $sect['ELEMENT_CNT'];
        }
    }
    $sect_1 = false;
    $subsections = [];
    foreach ( $arResult["SECTIONS"] as $key => $sect ){
        if(
            $sect['IBLOCK_SECTION_ID'] == $arSection['ID']
            &&
            $sect['DEPTH_LEVEL'] == ($arSection['DEPTH_LEVEL']+1)
            //&&
            //$sect['UF_MAIN_PAGE_MENU']
        ){    $subsections[] = $sect;    }
    }
    if( is_array($arSection['seoPages']) ){
        $subsections = array_merge($subsections, $arSection['seoPages']);
    }
    if( $arSection ){ ?>
        <div class="quick-links__item has-shadow grid-item-js">
            <div class="category-links">
                <div class="category-links__title">
                    <?=html_entity_decode($arSection['UF_MAIN_MENU_SVG'])?>
                    <a href="<?=$arSection['SECTION_PAGE_URL']?>">
                        <span><strong><?=$arSection['NAME']?></strong></span>
                    </a>&nbsp;
                    <em><?=$arSection['ELEMENT_CNT']?></em>
                </div>
                <? if( count($subsections) > 0 ){ ?>
                    <ul class="category-links__list">
                        <? foreach( $subsections as $subsection ){ ?>
                            <li>
                                <a href="<?=$subsection['SECTION_PAGE_URL']?>"><?=$subsection['NAME']?></a>
                            </li>
                        <? } ?>
                    </ul>
                <? } ?>
            </div>
        </div>
    <? }
} ?>