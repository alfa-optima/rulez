<? include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("404 Страница не найдена"); ?>



<div class="content">

    <div class="content-align">

        <section class="content-inner article-content">

            <h1>ОШИБКА 404</h1>
            <p>Извините, страница не найдена.<br> Возможно вы ошиблись при вводе адреса, либо использовали неверную ссылку.</p>

            <p>
                <a href="/"><strong>Вернуться на главную</strong></a>
            </p>

        </section>

    </div>

    <aside class="sidebar">

        <div class="sidebar__holder">

            <? // menu_catalog
            $APPLICATION->IncludeComponent(
                "aoptima:menu_catalog", "left_menu"
            ); ?>

        </div>

    </aside>

</div>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>