<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<!--FOOTER-->
<footer class="footer">
    <div class="footer__holder max-wrap layout">
        <!--footer-common-->
        <div class="footer-common">
            <div class="footer-grid">

                <? // footer_catalog_menu
                $APPLICATION->IncludeComponent(
                    "aoptima:footer_catalog_menu", "", array()
                ); ?>

                <div class="footer-grid__item">
                    <strong class="footer-grid__title"><a href="/"><span>Rulez.by</span></a></strong>
                    <? // bottom_menu
                    $APPLICATION->IncludeComponent(
                        "bitrix:menu",  "bottom_menu", // Шаблон меню
                        Array(
                            "ROOT_MENU_TYPE" => "bottom", // Тип меню
                            "MENU_CACHE_TYPE" => "A",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "N",
                            "CACHE_SELECTED_ITEMS" => "N",
                            "MENU_CACHE_GET_VARS" => array(""),
                            "MAX_LEVEL" => "1",
                            "CHILD_MENU_TYPE" => "left",
                            "USE_EXT" => "N",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "N"
                        )
                    ); ?>
                </div>

                <div class="footer-grid__item">

                    <strong class="footer-grid__title">
                        <a href="/contacts/"><span>Контакты</span></a>
                    </strong>

                    <div class="footer-grid__content">

                        <? // phonesBlock
                        $APPLICATION->IncludeComponent(
                            "aoptima:phonesBlock", "footerList", array()
                        ); ?>

                        <div class="soc-footer">

                            <? // footer_soc_links
                            $APPLICATION->IncludeComponent(
                                "bitrix:news.list", "footer_soc_links",
                                array(
                                    "COMPONENT_TEMPLATE" => "footer_soc_links",
                                    "IBLOCK_TYPE" => "content",
                                    "IBLOCK_ID" => "6",
                                    "NEWS_COUNT" => "20",
                                    "SORT_BY1" => "SORT",
                                    "SORT_ORDER1" => "ASC",
                                    "SORT_BY2" => "NAME",
                                    "SORT_ORDER2" => "ASC",
                                    "FILTER_NAME" => "",
                                    "FIELD_CODE" => array(),
                                    "PROPERTY_CODE" => array('LINK', 'SVG', 'CSS_CLASS'),
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "AJAX_MODE" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_FILTER" => "N",
                                    "CACHE_GROUPS" => "Y",
                                    "PREVIEW_TRUNCATE_LEN" => "",
                                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                    "SET_TITLE" => "N",
                                    "SET_BROWSER_TITLE" => "N",
                                    "SET_META_KEYWORDS" => "N",
                                    "SET_META_DESCRIPTION" => "N",
                                    "SET_STATUS_404" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "PARENT_SECTION" => "",
                                    "PARENT_SECTION_CODE" => "",
                                    "INCLUDE_SUBSECTIONS" => "Y",
                                    "DISPLAY_DATE" => "Y",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "Y",
                                    "DISPLAY_PREVIEW_TEXT" => "Y",
                                    "PAGER_TEMPLATE" => ".default",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "PAGER_TITLE" => "Новости",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "AJAX_OPTION_ADDITIONAL" => "undefined",
                                    "SET_LAST_MODIFIED" => "N",
                                    "PAGER_BASE_LINK_ENABLE" => "N",
                                    "SHOW_404" => "N",
                                    "MESSAGE_404" => ""
                                ),
                                false
                            ); ?>

                        </div>

                        <div class="subs  subs-js">
                            <a style="cursor: pointer" class="btn-subs btn-subs-js jq-switch-class_initialized">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <circle cx="6.18" cy="17.82" r="2.18"></circle>
                                    <path d="M4 4.44v2.83c7.03 0 12.73 5.7 12.73 12.73h2.83c0-8.59-6.97-15.56-15.56-15.56zm0 5.66v2.83c3.9 0 7.07 3.17 7.07 7.07h2.83c0-5.47-4.43-9.9-9.9-9.9z"></path>
                                </svg>
                                <span>Подписать на рассылку</span>
                            </a>
                            <div class="subs-feedback"><span>Подписка оформлена</span></div>
                            <form onsubmit="return false;" class="subs-form subs__form stop-remove-class">
                                <div class="subs__title">Подписка на рассылку</div>
                                <label class="input-wrap subs-form__input-wrap">
                                    <input class="input-def subs-form__input focus-field-js" name="email" type="email">
                                    <span class="label-float">Введите Ваш e-mail</span>
                                    <span class="error-note error___p"></span>
                                    <span class="success-note">Ок</span>
                                </label>
                                <button type="button" class="subs-form__send subscribe___button to___process">Ok</button>
                            </form>
                        </div>

                        <a class="pay-cards">
                            <? $APPLICATION->IncludeFile( SITE_TEMPLATE_PATH."/inc/footer_bank_cards.inc.php", Array(), Array("MODE"=>"html") ); ?>
                        </a>

                    </div>
                </div>
            </div>
        </div>
        <!--footer-common end-->



        <!--footer bottom-->
        <div class="footer-bottom">
            <div class="footer-bottom__holder">
                <div class="footer-bottom__col">
                    <span class="copyright">&copy; <? $APPLICATION->IncludeFile( SITE_TEMPLATE_PATH."/inc/footer_link.inc.php", Array(), Array("MODE"=>"html") ); ?> <?if( date('Y')==2019){echo(date('Y')); } else { echo("2019 - ".date('Y')); }?></span>
                </div>
                <div class="footer-bottom__col">
                    <div class="license"><? $APPLICATION->IncludeFile( SITE_TEMPLATE_PATH."/inc/footer_license.inc.php", Array(), Array("MODE"=>"html") ); ?></div>
                </div>
                <div class="footer-bottom__col">
                    <!--developer-->
                    <div class="developer">
                        <div class="developer__holder">
                            <a href="http://astronim.by/" class="developer__logo" target="_blank">Astronim*Support</a>
                            <div class="developer__label">
                                <a href="http://astronim.by/" target="_blank"><span>Разработка сайта</span></a>
                            </div>
                        </div>
                    </div>
                    <!--developer end-->
                </div>
            </div>
        </div>
        <!--footer bottom end-->


    </div>
</footer>
<!--FOOTER end-->
<!--</div>-->


