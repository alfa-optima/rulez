<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<?php if( 0 ){ ?>

    <div class="p-add-form-popup p-add-form-popup-js stop-remove-class">
        <div class="p-add-form p-add-form-js">
            <div class="p-add-form__figure">
                <img class="p-add-form__figure-img" src="<?=tools\funcs::rIMGG($arResult['DETAIL_PICTURE'], 4, 700, 400)?>"/>
            </div>
            <div class="p-add-form__content">
                <div class="p-add-form__title"><?=$arResult['NAME']?></div>
                <div class="p-add-form__meta">

                    <div class="p-add-form__rate p-rate">
                        <span content="<?=$arParams['product_rating']?>" style="width: <?=round($arParams['product_rating']/5*100, 0)?>/*%;">*/<?=$arParams['product_rating']?></span>
                        <meta content="1">
                    </div>

                    <? if( in_array($arResult['PROPERTIES']['STATUS_TOVARA']['VALUE'], project\catalog::$nal_statuses)  ){ ?>
                        <div class="p-add-form__stock p-stock on"><i>&nbsp;</i>В наличии</div>
                    <? } else { ?>
                        <div class="p-add-form__stock p-stock on">Нет в наличии</div>
                    <? } ?>

                </div>
                <div class="p-add-form__options">
                    <div class="p-add-form-price p-add-form__price">
                        <? if(
                            $arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['VALUE_VAT']
                            !=
                            $arResult['PRICES'][ project\catalog::BASE_PRICE_CODE][ 'DISCOUNT_VALUE_VAT']
                        ){ ?>
                            <div class="p-add-form-price__old"><strong><?=number_format($arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['VALUE_VAT'], project\catalog::PRICE_ROUND, ".", " ")?></strong>&nbsp;<em>р.</em></div>
                        <? } ?>
                        <div class="p-add-form-price__new"><strong class="p-add-form-price-js" data-price="<?=$arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['DISCOUNT_VALUE_VAT']?>"><?=number_format($arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['DISCOUNT_VALUE_VAT'], project\catalog::PRICE_ROUND, ".", " ")?></strong>&nbsp;<em>р.</em></div>
                    </div>
                    <div class="p-add-form__counter p-add-form__counter-js">
                        <input class="spinner spinner-js" max="100" min="1" type="number" name="quick_quantity" value="1" data-only-number>
                    </div>
                </div>
            </div>
            <div class="p-add-form__footer">
                <div class="p-add-form__total">
                    <div class="p-add-form__total-label">Итого</div>
                    <div class="p-add-form__total-sum"><strong class="p-add-form-total-js"><?=number_format($arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['DISCOUNT_VALUE_VAT'], project\catalog::PRICE_ROUND, ".", " ")?></strong> <em>р.</em></div>
                </div>
            </div>
        </div>
        <div class="quick-order p-add-form__order">
            <form class="validate-js" onsubmit="return false;">
                <div class="quick-order__form">
                    <label for="f-wrap-validation-js" class="quick-order__label">Быстрый заказ: </label>
                    <input class="quick-order__input field-js is___phone" id="f-wrap-validation-js" name="phone" placeholder="+375" type="text"">
                    <div class="error-note quick-order__error-note">Введите корректный номер телефона в формате +375 и 9 цифр</div>
                    <!--<div class="success-note quick-order__success-note">Success text</div>-->
                    <button class="quick-order__send-btn buy_1_click_button to___process"  item_id="<?=$arResult['ID']?>">
                        <svg width="19" height="13" viewBox="0 0 19 13" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12.8021 0.19552C12.5342 -0.0651732 12.1126 -0.0651732 11.8447 0.19552C11.5859 0.447405 11.5859 0.866628 11.8447 1.11793L16.6993 5.84269H0.679892C0.306465 5.84269 0 6.13157 0 6.49501C0 6.85845 0.306465 7.15672 0.679892 7.15672H16.6993L11.8447 11.8727C11.5859 12.1334 11.5859 12.5532 11.8447 12.8045C12.1126 13.0652 12.5342 13.0652 12.8021 12.8045L18.8059 6.9612C19.0647 6.70932 19.0647 6.2901 18.8059 6.0388L12.8021 0.19552Z"></path>
                        </svg>
                        <span class="hide-text">Отправить</span>
                    </button>
                </div>
            </form>
        </div>
        <a href="#" class="close-p-add-form-popup p-add-form-popup-close-js">
            <svg width="8" height="8" viewBox="0 0 8 8" xmlns="http://www.w3.org/2000/svg">
                <path d="M0.646447 1.35355L6.64645 7.35355L7.35355 6.64645L1.35355 0.646447L0.646447 1.35355ZM6.64645 0.646447L0.646447 6.64645L1.35355 7.35355L7.35355 1.35355L6.64645 0.646447Z"></path>
            </svg>
            <span>Закрыть</span>
        </a>
    </div>

<?php } ?>






<!--Login-->
<div class="popup-def" id="popup-login">
    <div class="popup-def__container" data-selectable="true">

        <div class="popup-def__heading">
            <strong class="popup-def__title">Войти</strong>
            <a href="#popup-reg" class="popup-def__link open-popup-def-js" title="Зарегистрироваться"><span>Зарегистрироваться</span></a>
        </div>

        <form onsubmit="return false;" method="post" class="popup-def__form validate-js auth___form">
            <label class="input-wrap popup-def__form-input">
                <input class="input-def" name="login" type="text">
                <span class="label-float">Логин</span>
                <span class="error-note">Текст ошибки</span>
                <span class="success-note">Текст успешной отправки</span>
            </label>
            <label class="input-wrap popup-def__form-input">
                <input class="input-def" name="password" type="password">
                <span class="label-float">Пароль</span>
                <span class="error-note">Текст ошибки</span>
                <span class="success-note">Текст успешной отправки</span>
            </label>

            <p class="error___p"></p>

            <div class="input-wrap input-wrap_btn popup-def__form-input popup-def__form-input_btn">
                <input class="btn-alt popup-def__btn auth___button to___process" type="button" value="Войти">
            </div>
        </form>

        <div class="popup-def__meta">
            <a href="#popup-password" class="popup-def__link open-popup-def-js" title="Востановление пароля"><span>Восстановить пароль?</span></a>
        </div>

        <? $APPLICATION->IncludeComponent(
            "bitrix:system.auth.form", "soc_auth",
            Array(
                "FORGOT_PASSWORD_URL" => "",
                "PROFILE_URL" => "",
                "REGISTER_URL" => "",
                "SHOW_ERRORS" => "N"
            )
        ); ?>

    </div>
</div>



<!--Registration-->
<div class="popup-def" id="popup-reg">
    <div class="popup-def__container" data-selectable="true">
        <div class="popup-def__heading">
            <strong class="popup-def__title">Зарегистрироваться</strong>
            <a href="#popup-login" class="popup-def__link open-popup-def-js"
               title="Вход в личный кабинет"><span>Войти</span></a>
        </div>
        <form onsubmit="return false" method="post" class="popup-def__form validate-js register___form">
            <label class="input-wrap popup-def__form-input">
                <input class="input-def" name="EMAIL" type="text" readonly onfocus="$(this).prop('readonly', false);" autocomplete="off">
                <span class="label-float">Email</span>
                <span class="error-note">Текст ошибки</span>
                <span class="success-note">Текст успешной отправки</span>
            </label>
            <label class="input-wrap popup-def__form-input">
                <input class="input-def" name="PASSWORD" type="password" readonly onfocus="$(this).prop('readonly', false);" autocomplete="off">
                <span class="label-float">Пароль</span>
                <span class="error-note">Текст ошибки</span>
                <span class="success-note">Текст успешной отправки</span>
            </label>
            <label class="input-wrap popup-def__form-input">
                <input class="input-def" name="CONFIRM_PASSWORD" type="password" readonly onfocus="$(this).prop('readonly', false);" autocomplete="off">
                <span class="label-float">Повтор пароля</span>
                <span class="error-note">Текст ошибки</span>
                <span class="success-note">Текст успешной отправки</span>
            </label>

            <p class="error___p"></p>

            <div class="input-wrap input-wrap_btn popup-def__form-input popup-def__form-input_btn">
                <input class="btn-alt popup-def__btn register___button to___process" type="button" value="Зарегистрироваться">
            </div>
        </form>
    </div>
</div>


<!--Restore password-->
<div class="popup-def" id="popup-password">
    <div class="popup-def__container" data-selectable="true">
        <div class="popup-def__heading">
            <strong class="popup-def__title">Восстановление пароля</strong>
            <a href="#popup-login" class="popup-def__link open-popup-def-js"
               title="Вход в личный кабинет"><span>Назад</span></a>
        </div>
        <form onsubmit="return false;" method="post" class="popup-def__form validate-js">
            <label class="input-wrap popup-def__form-input">
                <input class="input-def" name="email" type="text">
                <span class="label-float">Ваш Email</span>
                <span class="error-note">Текст ошибки</span>
                <span class="success-note">Текст успешной отправки</span>
            </label>

            <p class="error___p"></p>

            <div class="input-wrap input-wrap_btn popup-def__form-input popup-def__form-input_btn password___recovery_button to___process">
                <input class="btn-alt popup-def__btn" type="button" value="Восстановить">
            </div>
        </form>

        <div class="popup-def__meta">
            <a href="#popup-reg" class="popup-def__link open-popup-def-js" title="Зарегистрироваться"><span>Зарегистрироваться</span></a>
        </div>

    </div>
</div>



<div id="messageModal" class="p-5" style="display: none;max-width:600px;">
    <div class="modal-title h2">
        <em class="icon___block"></em>
        <div class="modal-title__inner text___block"></div>
    </div>
</div>



<div id="orderModal" class="p-5" style="display: none;max-width:600px;">
    <div class="modal-title h2"><em class="icon-error"></em><div class="modal-title__inner">Предупреждение</div></div>
    <p>Нажми ОК, чтобы закрыть</p>
    <button data-fancybox-close class="btn-def">ОК</button>
</div>




<div id="exampleModal" class="p-5" style="display: none;max-width:600px;">
    <div class="modal-title h2">
        <em class="icon-error"></em>
        <div class="modal-title__inner">Это модальное окно с ошибкой</div>
    </div>
    <div class="modal-title h2">
        <em class="icon-success"></em>
        <div class="modal-title__inner">Это модальное окно успешное</div>
    </div>
    <p>Нажми ОК, чтобы закрыть</p>
    <button data-fancybox-close class="btn-def">ОК</button>
</div>