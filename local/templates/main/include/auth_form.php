<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>


<div class="content">
    <div class="article-content">

        <h1>Авторизация</h1>

        <div class="layout-text">
            <div class="cabinet-profile-form">
                <form class="simple-form validate-js auth___form" onsubmit="return false;">

                    <div class="simple-form__group">
                        <label for="auth-login">Email/логин:</label>
                        <input id="auth-login" class="input-def" name="login" placeholder="Email/логин" type="text">
                        <label for="auth-password">Пароль:</label>
                        <input id="auth-password" class="input-def" name="password" type="password" placeholder="Пароль">
                    </div>

                    <p class="error___p"></p>

                    <div class="input-wrap input-wrap_btn simple-form__input simple-form__input_btn">
                        <input class="btn-alt simple-form__btn auth___button to___process" type="button" value="Войти" style="min-width: 100px">
                    </div>

                </form>

                <br>

                <? $APPLICATION->IncludeComponent(
                    "bitrix:system.auth.form", "soc_auth",
                    Array(
                        "FORGOT_PASSWORD_URL" => "",
                        "PROFILE_URL" => "",
                        "REGISTER_URL" => "",
                        "SHOW_ERRORS" => "N"
                    )
                ); ?>

            </div>



        </div>



    </div>
</div>



