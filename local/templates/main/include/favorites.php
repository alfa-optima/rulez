<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;
\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

global $APPLICATION;
$favorites = $APPLICATION->get_cookie('favorites');
if( strlen($favorites) > 0 ){
    $favorites = explode('|', $favorites);
} else {
    $favorites = [];
} ?>


<div class="content">
    <div class="article-content">

        <h1>Избранное</h1>

        <? if( count($favorites) > 0 ){ ?>

            <p>В этом разделе размещены отмеченные Вами товары.</p>

            <? $sortField = project\catalog::getSortField();
            $sortOrder = project\catalog::getSortOrder();

            $GLOBALS[project\catalog::FILTER_NAME]['ID'] = $favorites;
            $GLOBALS[project\catalog::FILTER_NAME]['INCLUDE_SUBSECTIONS'] = 'Y';

            $APPLICATION->IncludeComponent(
                "bitrix:catalog.section", "favorites",
                Array(
                    "IBLOCK_TYPE" => project\catalog::IBLOCK_TYPE,
                    "IBLOCK_ID" => $section['IBLOCK_ID'],
                    "SECTION_USER_FIELDS" => array(),
                    "ELEMENT_SORT_FIELD" => project\catalog::getCatSortFieldCode($sortField),
                    "ELEMENT_SORT_ORDER" => $sortOrder,
                    "ELEMENT_SORT_FIELD2" => "NAME",
                    "ELEMENT_SORT_ORDER2" => $sortOrder,
                    "FILTER_NAME" => project\catalog::FILTER_NAME,
                    "HIDE_NOT_AVAILABLE" => "N",
                    "PAGE_ELEMENT_COUNT" => project\catalog::GOODS_CNT + 1,
                    "LINE_ELEMENT_COUNT" => "3",
                    "PROPERTY_CODE" => array('STATUS_TOVARA'),
                    "OFFERS_LIMIT" => 0,
                    "TEMPLATE_THEME" => "",
                    "PRODUCT_SUBSCRIPTION" => "N",
                    "SHOW_DISCOUNT_PERCENT" => "N",
                    "SHOW_OLD_PRICE" => "N",
                    "MESS_BTN_BUY" => "Купить",
                    "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                    "MESS_BTN_SUBSCRIBE" => "Подписаться",
                    "MESS_BTN_DETAIL" => "Подробнее",
                    "MESS_NOT_AVAILABLE" => "Нет в наличии",
                    "SECTION_URL" => "",
                    "DETAIL_URL" => "",
                    "SECTION_ID_VARIABLE" => "SECTION_ID",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "CACHE_TYPE" => project\catalog::CACHE_TYPE,
                    "CACHE_TIME" => project\catalog::CACHE_TIME,
                    "CACHE_GROUPS" => "Y",
                    "SET_META_KEYWORDS" => "N",
                    "META_KEYWORDS" => "",
                    "SET_META_DESCRIPTION" => "N",
                    "META_DESCRIPTION" => "",
                    "BROWSER_TITLE" => "-",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "DISPLAY_COMPARE" => "N",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "CACHE_FILTER" => "Y",
                    "PRICE_CODE" => array(
                        project\catalog::RASSROCHKA_PRICE_CODE,
                        project\catalog::BASE_PRICE_CODE
                    ),
                    "USE_PRICE_COUNT" => "N",
                    "SHOW_PRICE_COUNT" => "1",
                    "PRICE_VAT_INCLUDE" => "Y",
                    "CONVERT_CURRENCY" => "N",
                    "BASKET_URL" => "/personal/basket.php",
                    "ACTION_VARIABLE" => "action",
                    "PRODUCT_ID_VARIABLE" => "id",
                    "USE_PRODUCT_QUANTITY" => "N",
                    "ADD_PROPERTIES_TO_BASKET" => "Y",
                    "PRODUCT_PROPS_VARIABLE" => "prop",
                    "PARTIAL_PRODUCT_PROPERTIES" => "N",
                    "PRODUCT_PROPERTIES" => "",
                    "PAGER_TEMPLATE" => "",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Товары",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "Y",
                    "ADD_PICT_PROP" => "-",
                    "LABEL_PROP" => "-",
                    'SHOW_ALL_WO_SECTION' => "Y"
                )
            ); ?>

        <? } else { ?>
            <p>Список избранного пуст.</p>
        <? } ?>

    </div>
</div>
