<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$arSections = [];
$catalogIblocks = project\catalog::iblocks();
foreach ( $catalogIblocks as $iblock ){
    if( $iblock['ID'] != project\complect::IBLOCK_ID ){
        
        $iblock_sections = project\catalog::allIblockCategories($iblock['ID']);

        if( count($iblock_sections) > 0 ){
            foreach ( $iblock_sections as $section ){
                if( $section['UF_MAIN_PAGE_MENU'] ){
                    $section['seoPages'] = project\SeopageTable::getBySectionXMLID($section['XML_ID']);
                    foreach ( $section['seoPages'] as $key => $seoPage ){
                        if( $seoPage['SHOW_ON_MAIN_PAGE'] != 'Y' ){
                            unset($section['seoPages'][$key]);
                        }
                    }
                    $arSections[$section['ID']] = $section;
                }
            }
        }

//        $sections = [];   $levels = [];
//        foreach ( $iblock_sections as $section ){
//            if( $section['UF_MAIN_PAGE_MENU'] ){
//                $sections[$section['DEPTH_LEVEL']][] = $section;
//                $levels[] = $section['DEPTH_LEVEL'];
//            }
//        }

//        if( count($sections) > 0 ){
//            $min_level = min($levels);
//            foreach ( $sections[$min_level] as $section ){
//                $section['seoPages'] = project\SeopageTable::getBySectionXMLID($section['XML_ID']);
//                foreach ( $section['seoPages'] as $key => $seoPage ){
//                    if( $seoPage['SHOW_ON_MAIN_PAGE'] != 'Y' ){
//                        unset($section['seoPages'][$key]);
//                    }
//                }
//                $arSections[$section['ID']] = $section;
//            }
//        }

    }
}

if( count($arSections) > 0 ){ ?>
    <section class="main-section main-section_quick-links layout-narrow max-wrap">
        <h2 class="hide">Быстрые ссылки</h2>
        <div class="quick-links">
            <?/*quick links icons svg*/?>
            <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
                <symbol id="icon-catalog-computer" viewBox="0 0 35 21">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M27 8.5C27 8.22386 27.2239 8 27.5 8H33.5C33.7761 8 34 8.22386 34 8.5C34 8.77614 33.7761 9 33.5 9H27.5C27.2239 9 27 8.77614 27 8.5Z"></path>
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M1.1 0H20.9C21.5075 0 22 0.503723 22 1.125V15.375C22 15.9963 21.5075 16.5 20.9 16.5H12.4667V18.75H17.6C17.8025 18.75 17.9667 18.9179 17.9667 19.125V20.625C17.9667 20.8321 17.8025 21 17.6 21H4.4C4.19751 21 4.03333 20.8321 4.03333 20.625V19.125C4.03333 18.9179 4.19751 18.75 4.4 18.75H9.53333V16.5H1.1C0.492529 16.5 0 15.9963 0 15.375V1.125C0 0.503723 0.492529 0 1.1 0ZM4.76667 20.25H17.2333V19.5H4.76667V20.25ZM11.7333 18.75H10.2667V16.5H11.7333V18.75ZM20.9 15.75C21.1025 15.75 21.2667 15.5821 21.2667 15.375V1.125C21.2667 0.917908 21.1025 0.75 20.9 0.75H1.1C0.89751 0.75 0.733333 0.917908 0.733333 1.125V15.375C0.733333 15.5821 0.89751 15.75 1.1 15.75H20.9Z"></path>
                    <path d="M25.5 5C25.5 4.72386 25.7239 4.5 26 4.5H34C34.2761 4.5 34.5 4.72386 34.5 5V19C34.5 19.2761 34.2761 19.5 34 19.5H26C25.7239 19.5 25.5 19.2761 25.5 19V5Z" stroke="#000" fill="none"></path>
                    <rect x="2" y="2" width="18" height="12" rx="1"></rect>
                </symbol>
                <symbol id="icon-catalog-notebook" viewBox="0 0 28 17">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M2.67544 15.4288H25.3246C25.7932 15.4288 26.1739 15.0673 26.1739 14.6228V0.805982C26.1739 0.36154 25.7932 0 25.3246 0H2.67544C2.20708 0 1.82609 0.36154 1.82609 0.805982V14.6228C1.82609 15.0673 2.20733 15.4288 2.67544 15.4288ZM2.5541 0.805982C2.5541 0.742424 2.60846 0.690842 2.67544 0.690842H25.3246C25.3915 0.690842 25.4459 0.742424 25.4459 0.805982V14.6228C25.4459 14.6864 25.3915 14.738 25.3246 14.738H2.67544C2.60846 14.738 2.5541 14.6864 2.5541 14.6228V0.805982ZM28 16.6905V16.0455H16.5008C16.4438 16.2256 16.2701 16.3573 16.0623 16.3573H11.9379C11.7299 16.3573 11.5562 16.2253 11.4994 16.0455H0V16.6905C0 16.8614 0.146087 17 0.326149 17H27.6739C27.8539 17 28 16.8614 28 16.6905Z"></path>
                </symbol>
                <symbol id="icon-catalog-monitor" viewBox="0 0 23 22">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M17 22H6L11.5002 13L17 22ZM11.5002 14.7984L7.68199 21.047H15.3185L11.5002 14.7984Z"></path>
                    <path d="M0 0V15H8.43333C8.64532 15 8.81667 14.8324 8.81667 14.625C8.81667 14.4176 8.64532 14.25 8.43333 14.25H0.766667V0.75H22.2333V14.25H14.5667C14.3547 14.25 14.1833 14.4176 14.1833 14.625C14.1833 14.8324 14.3547 15 14.5667 15H23V0H0Z"></path>
                    <rect x="2" y="2" width="19" height="11" rx="1"></rect>
                </symbol>
                <symbol id="icon-catalog-monoblock" viewBox="0 0 25 23">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M1.25 0H23.75C24.4403 0 25 0.551697 25 1.23214V16.8393C25 17.5197 24.4403 18.0714 23.75 18.0714H14.1667V20.5357H20C20.2301 20.5357 20.4167 20.7196 20.4167 20.9464V22.5893C20.4167 22.8161 20.2301 23 20 23H5C4.7699 23 4.58333 22.8161 4.58333 22.5893V20.9464C4.58333 20.7196 4.7699 20.5357 5 20.5357H10.8333V18.0714H1.25C0.559692 18.0714 0 17.5197 0 16.8393V1.23214C0 0.551697 0.559692 0 1.25 0ZM5.41667 22.1786H19.5833V21.3571H5.41667V22.1786ZM13.3333 20.5357H11.6667V18.0714H13.3333V20.5357ZM23.75 17.25C23.9801 17.25 24.1667 17.0661 24.1667 16.8393V1.23214C24.1667 1.00533 23.9801 0.821429 23.75 0.821429H1.25C1.0199 0.821429 0.833333 1.00533 0.833333 1.23214V16.8393C0.833333 17.0661 1.0199 17.25 1.25 17.25H23.75Z"></path>
                    <rect x="2" y="2" width="21" height="14" rx="1"></rect>
                </symbol>
            </svg>
            <?/*quick links icons svg end*/?>
            <div class="quick-links__list grid-js">
                <div class="quick-links__item quick-links__item_advise has-shadow grid-item-js">
                    <div class="advise advise_small">
                        <? // manager
                        $APPLICATION->IncludeComponent(
                            "aoptima:manager", ""
                        ); ?>
                        <? // callback_form
                        $APPLICATION->IncludeComponent(
                            "aoptima:callback_form", "left",
                            array()
                        ); ?>
                    </div>
                </div>
                <? foreach ( $arSections as $arSection ){
                    $APPLICATION->IncludeComponent(
                        "bitrix:catalog.section.list",   "main_sections",
                        Array(
                            "arSection" => $arSection,
                            "IBLOCK_TYPE" => "catalog",
                            "IBLOCK_ID" => $arSection['IBLOCK_ID'],
                            "COUNT_ELEMENTS" => "Y",
                            "TOP_DEPTH" => "2",
                            "SECTION_FIELDS" => array(),
                            "SECTION_USER_FIELDS" => array('UF_MAIN_PAGE_MENU', 'UF_MAIN_MENU_SVG'),
                            "VIEW_MODE" => "LIST",
                            "SHOW_PARENT_NAME" => "Y",
                            "SECTION_URL" => "",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "36000000",
                            "CACHE_GROUPS" => "Y",
                            "ADD_SECTIONS_CHAIN" => "N"
                        )
                    );
                } ?>
                <div class="quick-links__item grid-item-js">
                    <a href="#gameZone" class="game-zone-anchor lozad" data-background-image="<?=SITE_TEMPLATE_PATH?>/img/img-game-zone.jpg"><span>Game Zone</span></a>
                </div>
            </div>
        </div>
    </section>
<? } ?>
<section class="main-section main-section_categories layout max-wrap">
    <h2 class="hide">Каталог</h2>
    <div class="categories">
        <ul class="categories__list">
            <? foreach ( $catalogIblocks as $iblock ){
                if( $iblock['ID'] != project\complect::IBLOCK_ID ){
                    $firstLevelSection = project\catalog::firstLevelCategory($iblock['ID']); ?>
                    <li class="categories__item">
                        <a class="categories__inner" href="<?=$firstLevelSection['SECTION_PAGE_URL']?>">
                            <svg width='28' height='17'>
                                <use xlink:href='#icon-catalog-notebook-alt'></use>
                            </svg>
                            <span><?=$firstLevelSection['NAME']?></span>
                        </a>
                    </li>
                <? }
            } ?>
        </ul>
    </div>
</section>