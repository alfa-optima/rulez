<? if ( !defined( "B_PROLOG_INCLUDED" ) || B_PROLOG_INCLUDED !== true ) die();
use Bitrix\Main\Page\Asset;   $asset = Asset::getInstance();
\Bitrix\Main\Loader::includeModule( 'aoptima.tools' ); use AOptima\Tools as tools;
\Bitrix\Main\Loader::includeModule('aoptima.project'); use AOptima\Project as project;

project\funcs::redirects();
project\compare::checkCookieIDS();
project\favorites::checkCookieIDS();

unset( $_SESSION['ACTIVE_PS_XML_ID'] );

$pureURL = tools\funcs::pureURL();
$arURI = tools\funcs::arURI( $pureURL );
?>
<!DOCTYPE html>
<!--[if lt IE 9]>
<html class="no-js old-ie" lang="ru"> <![endif]-->
<!--[if IE 9]>
<html class="no-js ie9" lang="ru"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="ru">
<!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <title><?$APPLICATION->ShowTitle()?> &mdash; интернет-магазин RULEZ.BY <?
    $pageNumber = $_GET['PAGEN_1'];
    if( intval($_GET['PAGEN_1']) > 1 ){
        echo ' - страница '.strip_tags(intval(trim($_GET['PAGEN_1'])));
    } ?></title>

    <link rel="canonical" href="<?=tools\funcs::pureURL()?><?
    if( intval($_GET['PAGEN_1']) > 1 ){
        echo '?PAGEN_1='.strip_tags(intval(trim($_GET['PAGEN_1'])));
    } ?>"/>

    <? if(
        intval($_GET['PAGEN_1']) >= 2
        &&
        count($_GET) == 1
    ){} else if( count($_GET) > 0 ){ ?>
        <meta name="robots" content="noindex, follow" />
    <? } ?>
    <?/*Mobile optimized*/?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0">
    <?/*Theme color*/?>
    <meta name="theme-color" content="#6e52dd">
	<?$APPLICATION->ShowHead()?>
    <?/*SEO*/?>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-TLWSFDV');</script>
    <!-- End Google Tag Manager -->

    <link rel="shortcut icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico">
    <link rel="icon" sizes="16x16 32x32 64x64" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico">
    <link rel="apple-touch-icon" href="<?=SITE_TEMPLATE_PATH?>/apple-touch-icon.png">
    <?/*manifest*/?>
    <link rel="manifest" href="<?=SITE_TEMPLATE_PATH?>/manifest.webmanifest">

    <? // CSS
    $asset->addCss(SITE_TEMPLATE_PATH."/css/libs.min.css");
    $asset->addCss(SITE_TEMPLATE_PATH."/css/main.css");
    $asset->addCss(SITE_TEMPLATE_PATH."/css/my.css");
    $asset->addCss(SITE_TEMPLATE_PATH."/css/buttons.min.css"); ?>

    <script>
    var quickSearchSession;
    var vozvratFormFields;
    var catalogSortField = "<?=project\catalog::getSortField()?>";
    var catalogSortOrder = "<?=project\catalog::getSortOrder()?>";
    var searchSortField = "<?=project\catalog::getSortField()?>";
    var searchSortOrder = "<?=project\catalog::getSortOrder()?>";
    var curURL = "<?=$_SERVER['REQUEST_URI']?>";
    var baseDomain = "<?=\Bitrix\Main\Config\Option::get('main', 'server_name')?>";
    var filterInfo = false;
    var orderFormFields;
    </script>

    <? // JS
    $asset->addJs(SITE_TEMPLATE_PATH."/js/modernizr.min.js");
    $asset->addJs(SITE_TEMPLATE_PATH."/js/jquery.min.js"); ?>
</head>

<body class="<? if( tools\funcs::isMain() ){ ?>home-page<? } ?>">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TLWSFDV" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <?/*B242GA start*/?>
    <script src="https://ga2.b24.macte.pro/b242ga.js"></script>
    <script>
    B242GAInit({
        portal:'https://ctm.bitrix24.by/',
        showPopup: false
    })
    </script>
    <?/*B242GA end*/?>

    <div id="panel"><?$APPLICATION->ShowPanel();?></div>

    <? // _header.php
    include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH."/include/_header.php"; ?>

    <div class="main">
        <? if( tools\funcs::isMain() ){
            // _main_page.php
            include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH."/include/_main_page.php";
        } else { ?>
            <div class="max-wrap layout">
                <? // Хлебные крошки
                $APPLICATION->IncludeComponent(
                    "bitrix:breadcrumb", "breadcrumb",
                    Array( "PATH" => "", "SITE_ID" => "s1" )
                ); ?>

                <? if( project\funcs::isContentPage() ){ ?>

                    <div class="content">

                        <div class="content-align">
                            <section class="content-inner article-content">
                                <h1><?$APPLICATION->ShowTitle()?></h1>
                <? } ?>
        <? } ?>