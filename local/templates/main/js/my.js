var searchInterval;


$(document).ready(function(){

    var observer = lozad();
    observer.observe();

    datepickerInit();

    $(document).on('keyup', '.is___phone', function(e){
        checkPhone( $(this) );
    })



    // контроль формата телефона
    var phone_input_class = 'phone___input';
    prepare_phone_inputs( phone_input_class );
    $(document).on('keyup', '.'+phone_input_class, function(e){
        check_phone_inputs( $(this) );
    })



    // Быстрый поиск
    $(document).on('keyup', '.headerSearchForm input[name=q]', function(e){
        quickSearchSession = false;
        var input = $(this);
        var q = $(input).val();
        $('.quickSearchBlock').remove();
        if( q.length >= 3 ){
            clearInterval(searchInterval);
            searchInterval = setInterval(function(){
                clearInterval(searchInterval);
                quickSearch(input);
            }, 700);
        }
    })
    $(document).on('focus', '.headerSearchForm input[name=q]', function(e){
        quickSearchSession = false;
        var input = $(this);
        var q = $(input).val();
        $('.quickSearchBlock').remove();
        if( q.length >= 3 ){
            clearInterval(searchInterval);
            quickSearch(input);
        }
    })
    $(document).click(function (e){
        quickSearchSession = false;
        if (
            $(e.target).parents('.quickSearchBlock').length==0
            &&
            $(e.target).attr('name') != 'q'
        ){
            $('.quickSearchBlock').remove();
        }
    });


    // Выбор категории статей
    $(document).on('click', '.article_section_link', function(e){
        var button = $(this);
        if( !is_process(button) ){
            var section_id = !$(button).parent().hasClass('current')?$(button).attr('section_id'):false;
            var postParams = { section_reload: 'Y' };
            if( section_id ){
                postParams['section_id'] = section_id;
            }
            process(true);
            $.post("/ajax/articlesLoad.php", postParams, function(data){
                if (data.status == 'ok'){

                    $('articles').replaceWith( data.html );

                    $('.article_section_link').parent().removeClass('current');

                    if( section_id ){

                        $('.article_section_link[section_id='+section_id+']').parent().addClass('current');

                        history.replaceState({}, null, data.section.SECTION_PAGE_URL);

                        if( $('input[name=section_id]').length == 0 ){
                            $('body').append('<input type="hidden" name="section_id">')
                        }
                        $('input[name=section_id]').val(section_id);

                        var new_title = data.section.UF_TITLE?data.section.UF_TITLE:data.section.NAME;
                        $('title').text( new_title );

                        var breadcrumbs = '<div class="breadcrumbs"><ul itemscope="" itemtype="http://schema.org/BreadcrumbList" class="breadcrumbs__list"><li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a itemprop="item" href="/" title="Главная"><span itemprop="name">Главная</span><meta itemprop="position" content="0"></a></li><li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a itemprop="item" href="/articles/" title="Статьи"><span itemprop="name">Статьи</span><meta itemprop="position" content="1"></a></li><li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><span itemprop="name">'+data.section.NAME+'</span></li></ul></div>';
                        $('.breadcrumbs').replaceWith(breadcrumbs);

                    } else {

                        history.replaceState({}, null, '/articles/');

                        $('input[name=section_id]').remove();

                        var new_title = 'Статьи';

                        var breadcrumbs = '<div class="breadcrumbs"><ul itemscope="" itemtype="http://schema.org/BreadcrumbList" class="breadcrumbs__list"><li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a itemprop="item" href="/" title="Главная"><span itemprop="name">Главная</span><meta itemprop="position" content="0"></a></li><li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><span itemprop="name">Статьи</span></li></ul></div>';
                        $('.breadcrumbs').replaceWith(breadcrumbs);

                    }

                    $('title').text( new_title );

                    setArticlesAdvBanner(data.adv_banner);

                    var observer = lozad();
                    observer.observe();

                    equalHeight();

                } else if (data.status == 'error'){
                    show_message(data.text, 'warning')
                }
            }, 'json')
            .fail(function(data, status, xhr){
                console.log('Ошибка запроса'); //show_message('Ошибка запроса', 'warning');
            })
            .always(function(data, status, xhr){
                process(false);
            })
        }
    })



    // Подгрузка статей
    $(document).on('click', '.articlesMoreButton', function(e){
        if( !is_process(this) ){
            var stop_ids = [];
            if( $('.article___item').length > 0 ){
                $('.article___item').each(function(){
                    stop_ids.push( $(this).attr('item_id') );
                })
            }
            var postParams = {
                stop_ids:stop_ids
            };
            if( $('input[name=section_id]').length > 0 ){
                postParams['section_id'] = $('input[name=section_id]').val();
            }
            process(true);
            $.post("/ajax/articlesLoad.php", postParams, function(data){
                if (data.status == 'ok'){

                    $('.articlesLoadArea').last().append( data.html );
                    if( $('ost').length > 0 ){
                        $('.articlesMoreButton').show();
                    } else {
                        $('.articlesMoreButton').hide();
                    }
                    $('ost').remove();

                    var observer = lozad();
                    observer.observe();

                    //equalHeight();

                } else if (data.status == 'error'){
                    show_message( data.text, 'warning', 'hover' );
                }
            }, 'json')
            .fail(function(data, status, xhr){
                show_message( 'Ошибка запроса', 'warning', 'hover' );
            })
            .always(function(data, status, xhr){
                process(false);
            })
        }
    })



    // Выбор категории акций
    $(document).on('click', '.action_section_link', function(e){
        var button = $(this);
        if( !is_process(button) ){
            var section_id = !$(button).parent().hasClass('current')?$(button).attr('section_id'):false;
            var postParams = { section_reload: 'Y' };
            if( section_id ){
                postParams['section_id'] = section_id;
            }
            process(true);
            $.post("/ajax/actionsLoad.php", postParams, function(data){
                if (data.status == 'ok'){

                    $('actions').replaceWith( data.html );

                    $('.action_section_link').parent().removeClass('current');

                    if( section_id ){

                        $('.action_section_link[section_id='+section_id+']').parent().addClass('current');

                        history.replaceState({}, null, data.section.SECTION_PAGE_URL);

                        if( $('input[name=section_id]').length == 0 ){
                            $('body').append('<input type="hidden" name="section_id">')
                        }
                        $('input[name=section_id]').val(section_id);

                        var new_title = data.section.UF_TITLE?data.section.UF_TITLE:data.section.NAME;
                        $('title').text( new_title );

                        var breadcrumbs = '<div class="breadcrumbs"><ul itemscope="" itemtype="http://schema.org/BreadcrumbList" class="breadcrumbs__list"><li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a itemprop="item" href="/" title="Главная"><span itemprop="name">Главная</span><meta itemprop="position" content="0"></a></li><li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a itemprop="item" href="/actions/" title="Статьи"><span itemprop="name">Акции</span><meta itemprop="position" content="1"></a></li><li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><span itemprop="name">'+data.section.NAME+'</span></li></ul></div>';
                        $('.breadcrumbs').replaceWith(breadcrumbs);

                    } else {

                        history.replaceState({}, null, '/actions/');

                        $('input[name=section_id]').remove();

                        var new_title = 'Статьи';

                        var breadcrumbs = '<div class="breadcrumbs"><ul itemscope="" itemtype="http://schema.org/BreadcrumbList" class="breadcrumbs__list"><li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a itemprop="item" href="/" title="Главная"><span itemprop="name">Главная</span><meta itemprop="position" content="0"></a></li><li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><span itemprop="name">Акции</span></li></ul></div>';
                        $('.breadcrumbs').replaceWith(breadcrumbs);

                    }

                    $('title').text( new_title );

                    setActionsAdvBanner(data.adv_banner);

                    var observer = lozad();
                    observer.observe();

                    equalHeight();

                } else if (data.status == 'error'){
                    show_message(data.text, 'warning')
                }
            }, 'json')
                .fail(function(data, status, xhr){
                    console.log('Ошибка запроса'); //show_message('Ошибка запроса', 'warning');
                })
                .always(function(data, status, xhr){
                    process(false);
                })
        }
    })



    // Подгрузка акций
    $(document).on('click', '.actionsMoreButton', function(e){
        if( !is_process(this) ){
            var stop_ids = [];
            if( $('.action___item').length > 0 ){
                $('.action___item').each(function(){
                    stop_ids.push( $(this).attr('item_id') );
                })
            }
            var postParams = {
                stop_ids:stop_ids
            };
            if( $('input[name=section_id]').length > 0 ){
                postParams['section_id'] = $('input[name=section_id]').val();
            }
            process(true);
            $.post("/ajax/actionsLoad.php", postParams, function(data){
                if (data.status == 'ok'){

                    $('.actionsLoadArea').last().append( data.html );
                    if( $('ost').length > 0 ){
                        $('.actionsMoreButton').show();
                    } else {
                        $('.actionsMoreButton').hide();
                    }
                    $('ost').remove();

                    var observer = lozad();
                    observer.observe();

                    //equalHeight();

                } else if (data.status == 'error'){
                    show_message( data.text, 'warning', 'hover' );
                }
            }, 'json')
            .fail(function(data, status, xhr){
                show_message( 'Ошибка запроса', 'warning', 'hover' );
            })
            .always(function(data, status, xhr){
                process(false);
            })
        }
    })




    $(document).on('change', 'input[type=file]', '.input-file-js', function(e){
        var $curInput = $(this);
        var val = $curInput.val().replace(/C:\\fakepath\\/i, '');
        if( val.length > 0 ){
            $curInput.closest('.input-file-js').find('.input-file-label-js').html(val);
        } else {
            $curInput.closest('.input-file-js').find('.input-file-label-js').html('Выберите файл');
        }
    })





    // Запрос обратного звонка
    $(document).on('click', '.callback___button', function(e){
        if ( !is_process(this) ){
            // Форма
            var this_form = $(this).parents('form');
            // Настройка полей для проверки
            var fields = {
                'name': {  tag: 'input',  name_ru: 'Имя',
                    required: true,  required_error: 'Введите, пожалуйста, Ваше имя'  },
                'phone': {  tag: 'input',  name_ru: 'Телефон',  is_phone: true,
                    required: true,  required_error: 'Введите, пожалуйста, Ваш телефон'  }
            }
            // Проверка полей
            var checkResult = checkFields( this_form, fields );
            // Если нет ошибок
            if ( checkResult.errors.length == 0 ){
                var form_data = $(this_form).serialize();
                process(true);
                $.post( "/ajax/ajax.php", {  action:"callback",  form_data: form_data  }, function( response ){
                    if( response.status == 'ok' ){
                        $('.advise-callback-close').trigger('click');
                        show_message(
                            'Ваш запрос успешно отправлен<br>Мы свяжемся с&nbsp;вами в&nbsp;ближайшее время!',
                            'success', 'hover'
                        );
                        $(this_form).find("input[type=text], input[type=password]").val("");

                        dataLayer.push({'event': 'event-callback-zakazat'});

                    } else if ( response.status == 'error' ){
                        show_error( this_form, response.text );
                    }
                }, 'json')
                .fail(function(response, status, xhr){ show_error( this_form, "Ошибка запроса" ); })
                .always(function(response, status, xhr){  process(false);  })
                // Если есть ошибки
            } else {    show_error( this_form, [checkResult.errors[0]] );    }
        }
    })









    $(document).on('click', '.subscribe___button', function(e){
        if ( !is_process(this) ){
            // Форма
            var this_form = $(this).parents('form');
            $(this_form).find('.error___p').hide();
            // Настройка полей для проверки
            var fields = {
                'email': {
                    tag: 'input',  name_ru: 'Email', is_email: true,
                    required: true,  required_error: 'Введите Ваш Email'
                }
            }
            // Проверка полей
            var checkResult = checkFields( this_form, fields );
            // Если нет ошибок
            if ( checkResult.errors.length == 0 ){
                var postParams = {
                    action: "subscribe",
                    email: $(this_form).find('input[name=email]').val()
                };
                process(true);
                $.post("/ajax/ajax.php", postParams, function(data){
                    if (data.status == 'ok'){
                        show_message( 'Успешное сохранение подписки!', 'success', 'hover' );
                        $(this_form).find("input[type=email]").val("");
                        $('body').trigger('click');
                    } else if (data.status == 'error'){
                        show_error( this_form, data.text );
                        $(this_form).find('.error___p').show();
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    show_error( this_form, 'Ошибка запроса' );
                    $(this_form).find('.error___p').show();
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            } else {
                show_error( this_form, [checkResult.errors[0]] );
                $(this_form).find('.error___p').show();
            }
        }
    })














});