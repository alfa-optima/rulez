var cityInputInterval;


function process(attr){
	if (attr == true){
		$('.to___process').addClass('is___process');
	} else if(attr == false){
		$('.to___process').removeClass('is___process');
	}
}
function is_process(el){
	if (el){
		return $(el).hasClass('is___process');
	} else {
		return false;
	}
}


// Функция проверки полей формы
function checkFields ( this_form, fields ){
	var result = { errors: [], values: {} };
	if( this_form && fields ){
		$(this_form).find("input, textarea").removeClass("is___error");
		$(this_form).find('p.error___p').html('');
		for ( field_name in fields ){
			var field = fields[field_name];
			if( 'tag' in  field ){
				var link = $(this_form).find(field.tag+'[name='+field_name+']');
				if( $(link).length > 0 ){
					var value = $(link).val();
					result.values[field_name] = value;
					var field_title = 'name_ru' in field?field.name_ru:field_name;
					var is_required = 'required' in field && field.required;
					var type = 'type' in field && field.type;
					if( type != undefined && type == 'checkbox' ){
						if(  is_required  &&  !$(link).prop('checked')  ){
							if( 'required_error' in field ){
								var error_text = field.required_error;
							} else {
								var error_text = 'Отметьте, пожалуйста, галочку "'+field_title+'"';
							}
							result['errors'].push({ text: error_text,  link: link });
						}
					} else {
						// Проверка длины
						if(  is_required  &&  value.length == 0  ){
							if( 'required_error' in field ){
								var error_text = field.required_error;
							} else {
								var error_text = 'Не указано поле "'+field_title+'"';
							}
							result['errors'].push({ text: error_text,  link: link });
						} else if( value.length > 0 ){
							// Проверка на длину
							if(
								is_required
								&&
								'min_length' in field  &&  field.min_length > value.length
							){
								var error_text = 'Мин. длина поля "'+field_title+'" - '+field.min_length+' '+pfCnt(field.min_length, "символ", "символа", "символов");
								result['errors'].push({ text: error_text,  link: link });
								// Проверка на телефон
							} else if(
								'is_phone' in field  &&  field.is_phone == true
								&&
								!(/^ *\+? *?\d+([-\ 0-9]*|(\(\d+\)))*\d+ *$/.test(value))
							){
								var error_text = 'В номере телефона допускаются только цифры, а также символы "+", "-", пробел и круглые скобки!';
								result['errors'].push({ text: error_text,  link: link });
								// Проверка на Email
							} else if(
								'is_email' in field  &&  field.is_email == true
								&&
								!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(value))
							){
								var error_text = 'Email содержит ошибки!';
								result['errors'].push({ text: error_text,  link: link });
							}
						}
					}
				}
			}
		}
	}
	return result;
}


// Функция вывода ошибки в форме
function show_error( this_form, errors, show_noty ){
	if( this_form && errors ){
		var value_type = typeof errors;
		if( value_type == 'object' && errors.length > 0 ){
			var final_error_text = [];
			for( key in errors ){
				var error = errors[key];
				var error_text = ('text' in error)?error.text:false;
				var link = ('link' in error)?error.link:false;
				if( link && error_text ){
					final_error_text.push( error_text );
					var title = error_text.replace(/<[^>]+>/g, '');
					var field_type = $(link)[0].tagName;
					$(link).addClass('is___error').attr( (field_type=='select'?'onchange':'oninput'),'$(this).removeClass("is___error"); $(this).attr("title", ""); $(this).parents("form").find(".error___p").html("")').attr('title', title);
				}
			}
			final_error_text = final_error_text.join('<br>');
			$(this_form).find(".error___p").html( final_error_text );
			if( show_noty ){    show_message( final_error_text );    }
		} else if( value_type == 'string' && errors.length > 0 ){
			$(this_form).find(".error___p").html( errors );
			if( show_noty ){    show_message( errors );    }
		}
		$(this_form).find('.success___p').html('');
	}
}
function show_success(this_form, message, show_noty){
	if (this_form){
		$(this_form).find(".error___p").html('');
		$(this_form).find('.success___p').html(message);
	}
	if (show_noty){    show_message( message );    }
}

function show_message( message, type, closeWith ) {
	if( !type ){    type = 'warning';    }
	if( !closeWith ){    closeWith = 'hover';    }
	if( type == 'success' ){
	    $('#messageModal .icon___block').addClass('icon-success');
	    $('#messageModal .icon___block').removeClass('icon-error');
	} else {
		$('#messageModal .icon___block').removeClass('icon-success');
		$('#messageModal .icon___block').addClass('icon-error');
	}
	$('#messageModal .text___block').html(message);
	$('.fancybox-close-small').trigger('click');
	$.fancybox.open({ src  : '#messageModal', type : 'inline' });
}


function create_form(form_class, method, action, target_blank){
	var methodAttr = '';   
	var actionAttr = '';   
	var targetAttr = '';
	if ( method && method.length > 0 ){   methodAttr = ' method="'+method+'"';   }
	if ( action && action.length > 0 ){   actionAttr = ' action="'+action+'"';   }
	if( target_blank ){   targetAttr = ' target="_blank"';   }
	if ( $(form_class).length > 0 ){
		$('.'+form_class).html('');
	} else {
		$('body').append('<form class="'+form_class+'" '+methodAttr+' '+actionAttr+' '+targetAttr+'></form>');
	}
	return $('.'+form_class);
}

function get_pureURL(URL){
	if ( !URL ){   URL = document.URL;   }
	var arURI = URL.split("?");
	var pureURI = arURI[0];
	return pureURI;
}


function addToRequestURI(URL, pm, val){
	var arURI = URL.split("?");
	var pureURI = arURI[0];
	if (URL.indexOf("?") != -1){
		var arRequests = arURI[1].split("&");
		var arNewRequests = [];
		var cnt = -1; 
		$.each(arRequests, function(key, request){
			arTMP = request.split("=");
			if (arTMP[0] != pm && request.length > 0){  cnt++;
				arNewRequests[cnt] = request;
			}
		})
		cnt++;
		arNewRequests[cnt] = pm+"="+val;
		return pureURI+"?"+arNewRequests.join("&");
	} else {
		return pureURI+"?"+pm+"="+val;
	}
}


function removeFromRequestURI( URL, key ){
	var arURI = URL.split("?");
	var pureURI = arURI[0];
	if (URL.indexOf("?") != -1){

		var arRequests = arURI[1].split("&");
		
		var arNewRequests = [];
		var cnt = -1;
		for( k in arRequests){
			var request = arRequests[k];
			arTMP = request.split("=");
			if ( arTMP[0] != key ){  cnt++;
				arNewRequests[cnt] = request;
			}
		}
		var newURL = pureURI+"?"+arNewRequests.join("&");
		if( arNewRequests.length == 0 ){
			newURL = newURL.replace("?", "");
		}
		return newURL;
	} else {
		return pureURI;
	}
}


function GET(){
	var URL = document.URL;
	var pureURL = get_pureURL();
	var GET_string = URL.replace(pureURL, '');
	var GET_string = GET_string.replace('?', '');
	var get = {};
	if( GET_string.length > 0 ){
		var params = GET_string.split('&');
		$.each(params, function(k, param){
			var ar = param.split('=');
			var key = ar[0];
			var value = ar[1];
			get[key] = value;
		})
	}
	var arCnt = Object.keys( get ).length;
	return arCnt>0?get:false;
}


function rawurldecode( str ) {  
    var histogram = {};  
    var ret = str.toString();   
    var replacer = function(search, replace, str) {  
        var tmp_arr = [];  
        tmp_arr = str.split(search);  
        return tmp_arr.join(replace);  
    };  
    histogram["'"]   = '%27';  
    histogram['(']   = '%28';  
    histogram[')']   = '%29';  
    histogram['*']   = '%2A';  
    histogram['~']   = '%7E';  
    histogram['!']   = '%21';  
    for (replace in histogram) {  
        search = histogram[replace];
        ret = replacer(search, replace, ret)
    }  
    ret = decodeURIComponent(ret);  
    return ret;  
}



function checkPhone( value ){
	var type = typeof value;
	var reg = /[^\d+() -]/g;
	if( type == 'string' ){
		return n = value.replace( reg , '' );
	} else if( type == 'object' ){
	    var val = $(value).val();
		value.val( val.replace( reg , '' ) );
	}
}


// Функция number_format для форматирования чисел
// Формат: number_format(1234.56, 2, ',', ' ');
function number_format(number, decimals, dec_point, thousands_sep) {
  number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + (Math.round(n * k) / k)
        .toFixed(prec);
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
    .split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '')
    .length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1)
      .join('0');
  }
  return s.join(dec);
}



function pfCnt(n, form1, form2, form5){ // pfCnt(productCount, "товар", "товара", "товаров")
    var n = n % 100;
    var n1 = n % 10;
    if (n > 10 && n < 20) return form5;
    if (n1 > 1 && n1 < 5) return form2;
    if (n1 == 1) return form1;
    return form5;
}





function minus_zero(this_input, not_empty_value) {
	var input_value = $(this_input).val();
	input_value = input_value.toString().replace(/[^\d;]/g, '');
	$(this_input).val(input_value);
	var dlina = input_value.length;
	if (dlina > 0){
		stop = false;
		for (var n = 1; n <= dlina; n++) {
			if (stop == false){
				if (input_value.substring((n-1),n) == 0){
					var new_str = input_value.substring(n,dlina);
					if (not_empty_value){
						$(this_input).val( (new_str.length == 0)?1:new_str );
					} else {
						$(this_input).val( (new_str.length == 0)?'':new_str );
					}
				} else {
					stop = true;
				}
			}
		}
	} else if (not_empty_value){
		$(this_input).val(1);
	}
}





var Base64 = {
   _keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
   //метод для кодировки в base64 на javascript
  encode : function (input) {
    var output = "";
    var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
    var i = 0
    input = Base64._utf8_encode(input);
       while (i < input.length) {
       chr1 = input.charCodeAt(i++);
      chr2 = input.charCodeAt(i++);
      chr3 = input.charCodeAt(i++);
       enc1 = chr1 >> 2;
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
      enc4 = chr3 & 63;
       if( isNaN(chr2) ) {
         enc3 = enc4 = 64;
      }else if( isNaN(chr3) ){
        enc4 = 64;
      }
       output = output +
      this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
      this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
     }
    return output;
  },
   //метод для раскодировки из base64
  decode : function (input) {
    var output = "";
    var chr1, chr2, chr3;
    var enc1, enc2, enc3, enc4;
    var i = 0;
     input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
     while (i < input.length) {
       enc1 = this._keyStr.indexOf(input.charAt(i++));
      enc2 = this._keyStr.indexOf(input.charAt(i++));
      enc3 = this._keyStr.indexOf(input.charAt(i++));
      enc4 = this._keyStr.indexOf(input.charAt(i++));
       chr1 = (enc1 << 2) | (enc2 >> 4);
      chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
      chr3 = ((enc3 & 3) << 6) | enc4;
       output = output + String.fromCharCode(chr1);
       if( enc3 != 64 ){
        output = output + String.fromCharCode(chr2);
      }
      if( enc4 != 64 ) {
        output = output + String.fromCharCode(chr3);
      }
   }
   output = Base64._utf8_decode(output);
     return output;
   },
   // метод для кодировки в utf8
  _utf8_encode : function (string) {
    string = string.replace(/\r\n/g,"\n");
    var utftext = "";
    for (var n = 0; n < string.length; n++) {
      var c = string.charCodeAt(n);
       if( c < 128 ){
        utftext += String.fromCharCode(c);
      }else if( (c > 127) && (c < 2048) ){
        utftext += String.fromCharCode((c >> 6) | 192);
        utftext += String.fromCharCode((c & 63) | 128);
      }else {
        utftext += String.fromCharCode((c >> 12) | 224);
        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
        utftext += String.fromCharCode((c & 63) | 128);
      }
     }
    return utftext;
 
  },
  //метод для раскодировки из urf8
  _utf8_decode : function (utftext) {
    var string = "";
    var i = 0;
    var c = c1 = c2 = 0;
    while( i < utftext.length ){
      c = utftext.charCodeAt(i);
       if (c < 128) {
        string += String.fromCharCode(c);
        i++;
      }else if( (c > 191) && (c < 224) ) {
        c2 = utftext.charCodeAt(i+1);
        string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
        i += 2;
      }else {
        c2 = utftext.charCodeAt(i+1);
        c3 = utftext.charCodeAt(i+2);
        string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
        i += 3;
      }
     }
     return string;
  }
}






function panel_regulation(wind_top){
	var panel_html = $('#panel').html();
	if (panel_html.length > 0){
		$('#bx-panel-expander').attr('onmouseout', 'panel_regulation();');
		$('#bx-panel-hider').attr('onmouseout', 'panel_regulation();');
		var panel_height = $('#panel').outerHeight(true);
		if (panel_height > 0){
			if (!wind_top || wind_top < 10){
				$('header').css('margin-top', panel_height+'px');
			} else {
				$('header').css('margin-top', '0');
			}
		}
	}	
}


function htmlspecialchars_decode(str){
	var map = {
		'&amp;': '&',
		'&lt;': '<',
		'&gt;': '>',
		'&quot;': '"',
		'&#039;': "'"
	};
	return str.replace(/&amp;|&lt;|&gt;|&quot;|&#039;/g, function(m) {return map[m];});
}


function in_array(value, array){
	for ( key in array ){
		var val = array[key];
		if( val == value ){    return true;    }
	}
    return false;
}


function getPaymentButton(order_id){
	$.post("/ajax/getPaymentButton.php", {order_id:order_id}, function(data){
		if( data ){
			$('.pay___block').html(data);
			$('.pay___block form input[type=submit]').addClass('btn-buy');
		}
	})
}


// function getPaymentButton(order_id){
// 	$.post("/ajax/getPaymentButton.php", {order_id:order_id}, function(data){
// 		if( data ){
// 			if ( $('.hidden_pay_block').length == 0 ){
// 				$('body').append('<div class="hidden_pay_block" style="display:none"></div>');
// 			}
// 			$('.hidden_pay_block').html(data);
// 			if ( $('.hidden_pay_block form').length > 0 ){
// 				var form = $('.hidden_pay_block form');
// 				$('.pay___block').html( $(form).get(0).outerHTML );
// 				$('.pay___block form').html('');
// 				$('.pay___block form').attr('target', 'blank')
// 				$(form).find('input').each(function(){
// 					var input_html = $(this).get(0).outerHTML;
// 					$('.pay___block form').append(input_html);
// 				})
// 				$('.pay___block input[type=submit]').addClass('pay___button').addClass('best__pay');
// 				$('.pay___block input[type=submit]').val('Оплатить ('+number_format($(form).find('input[name=Sum]').val(), 0, ',', ' ')+' руб.)')
// 				$('.hidden_pay_block').remove();
// 			}
// 		}
// 	})
// }





function vozvratFormSend( this_form ){

	var fields = vozvratFormFields;

	// Подготовка
	$(this_form).find("input, textarea").removeClass("is___error");
	$(this_form).find('p.error___p').html('');

	// Задаём переменные
	var gRecaptchaResponse = $(this_form).find("textarea[name=g-recaptcha-response]").val();

	//var agree_input = $(this_form).find("input[name=agree]:visible");
	//var agree = $(agree_input).prop('checked');
	var errors = [];

	// Проверки
	for ( key in fields ) {
		var field = fields[key];
		var tag = false;
		if( $(this_form).find('input[name='+field['CODE']+']').length > 0 ){
			tag = 'input';
		}
		if( $(this_form).find('textarea[name='+field['CODE']+']').length > 0 ){
			tag = 'textarea';
		}
		if( $(this_form).find('select[name='+field['CODE']+']').length > 0 ){
			tag = 'select';
		}
		if( tag ) {
			var postfix = '';
			var selection = $(this_form).find(tag+'[name='+field['CODE']+']'+postfix);
			var value = $(selection).val();
			if (field['required']) {
				if ( 'variants' in field ) {
					if (
						value == undefined
						||
						!value
						||
						value == 'empty'
					) {
						errors.push([key, 'Заполните поле "' + field.NAME + '"']);
					}
				} else if (value.length == 0) {
					errors.push([key, 'Заполните поле "'+field.NAME+'"']);
				}
			}
			if(
				value != undefined
				&&
				value.length > 0
				&&
				'check_reg' in field
			){
				var reg = new RegExp( field.check_reg, 'g' );
				if( !reg.test(value) ){
					if( 'check_error' in field ){
						errors.push([key, 'Поле "' + field.NAME + '": '+field['check_error']]);
					} else {
						errors.push([key, 'Недопустимый формат поля "' + field.NAME + '"']);
					}
				}
			}
			if(
				value != undefined
				&&
				value.length > 0
				&&
				'max_length' in field
				&&
				value.length > field.max_length
			){
				errors.push([key, 'Поле "' + field.NAME + '": максимальная длина '+field.max_length+' символов']);
			}
		}
	}

	// if (!agree) {
	//     errors.push(['agree', 'Отметьте, пожалуйста, согласие на обработку персональных данных!']);
	// }

	if (gRecaptchaResponse.length == 0) {
		errors.push([false, 'Пройдите, пожалуйста, проверку пользователя!']);
	}

	// Если есть ошибки
	if ( errors.length > 0 ){

		var ers = [];
		for(key in errors){
			var item_name = errors[key][0];
			var tag = false;
			if( item_name ) {
				if( $(this_form).find('input[name=' + item_name + ']').length > 0 ){
					tag = $(this_form).find('input[name=' + item_name + ']');
				} else if( $(this_form).find('textarea[name=' + item_name + ']').length > 0 ){
					tag = $(this_form).find('textarea[name=' + item_name + ']');
				} else if( $(this_form).find('select[name=' + item_name + ']').length > 0 ){
					tag = $(this_form).find('select[name=' + item_name + ']');
				}
			}
			if( tag ){
				$(tag).addClass('is___error');
				$(tag).attr('oninput','$(this).removeClass(\'is___error\'); $(this).attr("title", "");');
			}
			ers.push(errors[key][1]);
		}
		show_error( this_form, ers.join("<br>") );

		return false;

	} else {

		$('#vozvratFormFrame').remove();
		$('body').append('<iframe id="vozvratFormFrame" name="vozvratFormFrame" style="display: none"></iframe>');
		$(this_form).attr('onsubmit', 'return false;');
		$(this_form).find('p.error___p').html('');
		$(this_form).find('.vozvratFormButton').html('Ожидание...');
		$(this_form).find('.vozvratFormButton').css('opacity', '0.8');

		return true;
	}
}
function vozvratFormResponse( data ){
	var this_form = $('.vozvratForm');
	if (data.status == 'ok'){
		show_message("Ваше сообщение успешно отправлено!", 'success');
		$('.vozvratForm').replaceWith( htmlspecialchars_decode( data.html ) );
	} else if (data.status == 'error'){
		$('.vozvratForm').find('p.error___p').html( data.text );
	}
	$('#vozvratFormFrame').remove();
	$(this_form).attr('onsubmit', 'return vozvratFormSend($(this));');
	$(this_form).find('.vozvratFormButton').html('Отправить');
	$(this_form).find('.vozvratFormButton').css('opacity', '1');
}





function productReviewSend( this_form ){
    $(this_form).attr('onsubmit', 'return false;');
    $(this_form).find('.error___p').html('');
    $(this_form).find('input[type=submit]').val('Ожидание...');
    $(this_form).find('input[type=submit]').css('opacity', '0.8');
}
function productReviewAnswerSend( this_form ){
    $(this_form).attr('onsubmit', 'return false;');
    $(this_form).find('.error___p').html('');
    $(this_form).find('input[type=submit]').val('Ожидание...');
    $(this_form).find('input[type=submit]').css('opacity', '0.8');
}


function productReviewResponse( data ){
	var this_form = $('.productReviewForm');
	if (data.status == 'ok'){
		show_message("Отзыв успешно отправлен!<br>Он появится на сайте после&nbsp;модерации", 'success');
		$('.productReviewFormBlock').replaceWith( htmlspecialchars_decode( data.html ) );
	} else if (data.status == 'error'){
		$(this_form).find('.error___p').html(data.text);
	}
    $(this_form).attr('onsubmit', 'productReviewSend( $(this) );');
    $(this_form).find('input[type=submit]').val('Отправить');
    $(this_form).find('input[type=submit]').css('opacity', '1');
}

function productReviewAnswerResponse( data ){
	var this_form = $('.productReviewAnswerForm');
	if (data.status == 'ok'){
		show_message("Ответ успешно отправлен!<br>Он появится на сайте после&nbsp;модерации", 'success');
		$('.productReviewAnswerFormBlock').replaceWith( htmlspecialchars_decode( data.html ) );
	} else if (data.status == 'error'){
		$(this_form).find('.error___p').html(data.text);
	}
	$(this_form).attr('onsubmit', 'productReviewAnswerSend( $(this) );');
	$(this_form).find('input[type=submit]').val('Отправить');
	$(this_form).find('input[type=submit]').css('opacity', '1');
}



function setArticlesAdvBanner( banner_html ){
	banner_html = '<section class="section-news">'+banner_html+'</section>';
	if( $('.article___item').length > 0 ){
		var after_block = $('.first___articles_block');
		if( $('.second___articles_block').length > 0 ){
			after_block = $('.second___articles_block');
		}
		var items_cnt = $(after_block).find('.article___item').length;
		var after_cnt = 4;
		if( items_cnt > after_cnt ){
			$(after_block).after('<section class="section-news third___articles_block"><h2 class="section__title hide-text">Статьи</h2><div class="news"><div class="news__list equal-height-js articlesLoadArea"></div></div></section>');
			var cnt = 0;
			$(after_block).find('.article___item').each(function(){
				cnt++;
				if( cnt > after_cnt ){
					var item_clone = $(this).clone();
					$('.third___articles_block .news__list').append( $(item_clone).get(0).outerHTML );
					$(this).remove();
				}
			});
			$(after_block).after(banner_html);
		} else if( items_cnt <= after_cnt ){
			$(after_block).after(banner_html);
		}
	}
}



function setActionsAdvBanner( banner_html ){
	banner_html = '<section class="section-news">'+banner_html+'</section>';
	if( $('.article___item').length > 0 ){
		var after_block = $('.first___actions_block');
		if( $('.second___actions_block').length > 0 ){
			after_block = $('.second___actions_block');
		}
		var items_cnt = $(after_block).find('.action___item').length;
		var after_cnt = 4;
		if( items_cnt > after_cnt ){
			$(after_block).after('<section class="section-news third___actions_block"><h2 class="section__title hide-text">Акции</h2><div class="news"><div class="news__list equal-height-js actionsLoadArea"></div></div></section>');
			var cnt = 0;
			$(after_block).find('.action___item').each(function(){
				cnt++;
				if( cnt > after_cnt ){
					var item_clone = $(this).clone();
					$('.third___actions_block .news__list').append( $(item_clone).get(0).outerHTML );
					$(this).remove();
				}
			});
			$(after_block).after(banner_html);
		} else if( items_cnt <= after_cnt ){
			$(after_block).after(banner_html);
		}
	}
}



function dop_tabs() {
	var $tabs = $('.dop-tabs-js');

	function setLinkAll(tabs) {
		var $linkAll = $('.link-all', tabs.$activePanel),
			$captionBox = $('.caption-box', $(tabs.$tabs));
		$('.link-all', $captionBox).remove();
		if ($linkAll.length) {
			$linkAll.clone().attr('data-id', tabs.activeId).appendTo($captionBox);
		}
	}

	function lazyLoadImages($_image) {
		$.each($_image, function (index, element) {
			var observer = lozad(element);
			observer.observe();
		});
	}
	if ($tabs.length) {
		var $tabsPanels = $('.tabs__panels-js');
		$tabs.on('msTabs.afterInit', function (e, el, tabs) {
			setLinkAll(tabs);
			lazyLoadImages($('img', tabs.$activePanel));
		}).msTabs({
			anchor: $('.tabs__thumbs-js').find('a'),
			panels: $tabsPanels,
			panel: $tabsPanels.children(),
			compactView: {
				elem: '.tabs__select-js',
				drop: '.tabs__select-drop-js',
				arrowTpl: '<i><svg width="10" height="6" viewBox="0 0 10 6" xmlns="http://www.w3.org/2000/svg"><path d="M0.292893 0.292893C0.683417 -0.0976311 1.31658 -0.0976311 1.70711 0.292893L5 3.58579L8.29289 0.292893C8.68342 -0.0976311 9.31658 -0.0976311 9.70711 0.292893C10.0976 0.683417 10.0976 1.31658 9.70711 1.70711L5.70711 5.70711C5.31658 6.09763 4.68342 6.09763 4.29289 5.70711L0.292893 1.70711C-0.0976311 1.31658 -0.0976311 0.683417 0.292893 0.292893Z"></path></svg></i>',
				openClass: 'tabs-select-open'
			},
			afterOpen: function (e, el, tabs) {
				setLinkAll(tabs);
				lazyLoadImages($('img', tabs.$activePanel));
			}
		});
	}
	var $tabsNested = $('.tabs-nested-js');
	if ($tabsNested.length) {
		var $tabsNestedPanels = $('.tabs-nested__panels-js');
		$tabsNested.msTabs({
			anchor: $('.tabs-nested__thumbs-js').find('a'),
			panels: $tabsNestedPanels,
			panel: $tabsNestedPanels.children(),
			compactView: {
				elem: '.tabs-nested__select-js',
				drop: '.tabs-nested__select-drop-js',
				arrowTpl: '<i><svg width="10" height="6" viewBox="0 0 10 6" xmlns="http://www.w3.org/2000/svg"><path d="M0.292893 0.292893C0.683417 -0.0976311 1.31658 -0.0976311 1.70711 0.292893L5 3.58579L8.29289 0.292893C8.68342 -0.0976311 9.31658 -0.0976311 9.70711 0.292893C10.0976 0.683417 10.0976 1.31658 9.70711 1.70711L5.70711 5.70711C5.31658 6.09763 4.68342 6.09763 4.29289 5.70711L0.292893 1.70711C-0.0976311 1.31658 -0.0976311 0.683417 0.292893 0.292893Z"></path></svg></i>',
				openClass: 'tabs-select-open'
			}
		});
	}
}







function cityFocus( input ){
	var cityName = $(input).val();
	var locIDinput = $(input).parent('label').find('input[type=hidden]');
	var id = $(input).attr('id');
	var locID = $(locIDinput).val();
	if( locID.length > 0 ){
		$(input).attr('cityName', cityName);
	} else {
		$(input).attr('cityName', '');
	}
	$(input).val('');
}
function cityKeyUp( input ){
	$(input).removeClass('is___error');
	var locIDinput = $(input).parent('label').find('input[type=hidden]');
	$(input).attr('cityName', '');
	$(locIDinput).val('');
}
function cityFocusOut( input, lastLocID ){
	if( !$(input)[0].hasAttribute('process') ){   $(input).attr('process', 'N');   }
	if( $(input).attr('process') == 'N' ) {
		var val = $(input).val();
		var locIDinput = $(input).parent('label').find('input[type=hidden]');
		var locID = $(locIDinput).val();
		var cityName = $(input).attr('cityName');
		if (val.length == 0 && cityName.length > 0 && locID.length > 0) {
			$(input).val(cityName);
		}
		$(input).attr('cityName', '');
		if ( lastLocID != locID ) {
			///////////////////////////////////////////////
			if (
				$(input)[0].hasAttribute('name')
				&&
				in_array($(input).attr('name'), ['np_name'])
			) {
				orderReload();
			}
			///////////////////////////////////////////////
		}
	}
}
function citySelect( event, ui ){
	var input = event.target;
	if( !$(input)[0].hasAttribute('process') ){   $(input).attr('process', 'N');   }
	if( $(input).attr('process') == 'N' ) {
		$(input).attr('process', 'Y');
		var locIDinput = $(input).parent('label').find('input[type=hidden]');
		if ($(locIDinput).length > 0) {
			clearInterval(cityInputInterval);
			cityInputInterval = setInterval(function () {
				if ($(input).val() == ui.item.value) {
					clearInterval(cityInputInterval);
					$(input).val(ui.item.label);
					var loc_id = ui.item.value;
					$(locIDinput).val(loc_id);
					$(input).blur();
					$(input).attr('process', 'N');
					///////////////////////////////////////////////
					var id = $(input).attr('id');
					if (
						$(input)[0].hasAttribute('name')
						&&
						in_array($(input).attr('name'), ['np_name'])
					) {     orderReload();     }
					///////////////////////////////////////////////
				}
			}, 50);
		}
	}
}





function subscribe( form ){
	var postParams = {
	    action: "subscribe",
		email: $(form).find('input[name=email]')
	};
	process(true);
	$.post("/ajax/ajax.php", postParams, function(data){
	    if (data.status == 'ok'){
			show_message( 'Успешное сохранение подписки', 'warning', 'hover' );
	    } else if (data.status == 'error'){
			show_message( data.text, 'warning', 'hover' );
	    }
	}, 'json')
	.fail(function(data, status, xhr){
		show_message( 'Ошибка запроса', 'warning', 'hover' );
	})
	.always(function(data, status, xhr){
	    process(false);
	})
	return false;
}




function is_wrong_number( number ){
	return (
		(/^0[0-9]+$/.test(number))
		||
		(/,+/.test(number))
		||
		(/^[^0-9]/.test(number))
		||
		(/[^0-9.]/.test(number))
		||
		(/\..*\./.test(number))
	);
}
function prepare_number_inputs( input_class ){
	if( input_class ){
		$( '.' + input_class ).each(function(){
			var value = $(this).val();
			$(this).attr('prev_value', value);
		})
	}
}
function check_number_inputs( input ){
	if( input ){
		var value = $(input).val();
		var new_value = value.replace(new RegExp(",",'g'), ".");
		if( new_value != value ){
			value = new_value;
			if( !is_wrong_number( value ) ){
				$(input).val( value );
				$(input).attr( 'prev_value', value );
			}
		}
		var prev_value = $(input).attr('prev_value');
		if( value.length > 0 ){
			if( is_wrong_number( value ) ){
				if( value.startsWith(prev_value) ){
					$(input).val( prev_value );
				} else {
					$(input).val('');
				}
			} else {    $(input).attr( 'prev_value', value );    }
		} else {    $(input).attr( 'prev_value', value );    }
	}
}






function is_wrong_phone( phone ){
	return (
		// дублирование +
		(/\++.*\++/.test(phone))
		||
		// плюс посреди номера
		(/^[^+ ]+\+$/.test(phone))
		||
		// ) в начале номера
		(/^ *[)]$/.test(phone))
		||
		// недопустимые символы
		(/[^0-9+() -]/.test(phone))
	);
}
function prepare_phone_inputs( input_class ){
	if( input_class ){
		$( '.' + input_class ).each(function(){
			var value = $(this).val();
			$(this).attr('prev_value', value);
		})
	}
}
function check_phone_inputs( input ){
	if( input ){
		var value = $(input).val();
		var new_value = value.replace(new RegExp(",",'g'), ".");
		if( new_value != value ){
			value = new_value;
			if( !is_wrong_phone( value ) ){
				$(input).val( value );
				$(input).attr( 'prev_value', value );
			}
		}
		var prev_value = $(input).attr('prev_value');
		if( value.length > 0 ){
			if( is_wrong_phone( value ) ){
				if( value.startsWith(prev_value) ){
					$(input).val( prev_value );
				} else {    $(input).val('');    }
			} else {    $(input).attr( 'prev_value', value );    }
		} else {    $(input).attr( 'prev_value', value );    }
	}
}



function quickSearch(input) {
	var q = $(input).val();
	quickSearchSession = Math.round(Math.random()*1000000000);
	var postParams = { q: q, session:quickSearchSession };
	process(true);
	$('#searchPanel').addClass('loading');
	$.post("/ajax/quickSearch.php", postParams, function(data){
	    if (data.status == 'ok'){
	    	if( quickSearchSession == data.session ){
				if( $('.quickSearchBlock').length > 0 ){
					$('.quickSearchBlock').replaceWith(data.html);
				} else {
					$('header').after(data.html);
				}
	    	} else {
				$('.quickSearchBlock').remove();
			}
	    }
	}, 'json')
	.fail(function(data, status, xhr){
	    //show_message( 'Ошибка запроса' );
	})
	.always(function(data, status, xhr){
	    process(false);
		$('#searchPanel').removeClass('loading');
	})
}


