$(document).ready(function(){



    // Авторизация
    $(document).on('click', '.auth___button', function(e){
        if ( !is_process(this) ){
            // Форма
            var this_form = $(this).parents('form');
            // Настройка полей для проверки
            var fields = {
                'login': {  tag: 'input',  name_ru: 'Логин',
                    required: true,  required_error: 'Введите Ваш Email/логин'  },
                'password': {  tag: 'input',  name_ru: 'Пароль',
                    required: true,  required_error: 'Введите Ваш пароль'  }
            }
            // Проверка полей
            var checkResult = checkFields( this_form, fields );
            // Если нет ошибок
            if ( checkResult.errors.length == 0 ){
                var form_data = $(this_form).serialize();
                process(true);
                $.post( "/ajax/ajax.php", {  action:"auth",  form_data: form_data  }, function( response ){
                    if( response.status == 'ok' ){
                        $('.fancybox-close-small').trigger('click');
                        var url = document.URL;
                        var ar = url.split('#');
                        window.location.href = ar[0];
                    } else if ( response.status == 'error' ){
                        show_error( this_form, response.text );
                    }
                }, 'json')
                    .fail(function(response, status, xhr){ show_error( this_form, "Ошибка запроса" ); })
                    .always(function(response, status, xhr){  process(false);  })
                // Если есть ошибки
            } else {    show_error( this_form, [checkResult.errors[0]] );    }
        }
    })



    // Регистрация
    $(document).on('click', '.register___button', function(e){
        if ( !is_process(this) ){
            // Форма
            var this_form = $(this).parents('form');
            // Настройка полей для проверки
            var fields = {
                'EMAIL': {  tag: 'input',  name_ru: 'Email',  is_email: true,
                    required: true,  required_error: 'Введите, пожалуйста, Ваш Email'  },
                'PASSWORD': {  tag: 'input',  name_ru: 'Пароль',  min_length: 8,
                    required: true,  required_error: 'Придумайте пароль'  },
                'CONFIRM_PASSWORD': {  tag: 'input',  name_ru: 'Повтор пароля',
                    required: true,  required_error: 'Повторите ввод пароля'  },
            }
            // Проверка полей
            var checkResult = checkFields( this_form, fields );
            // Если нет ошибок
            if ( checkResult.errors.length == 0 ){
                var form_data = $(this_form).serialize();
                process(true);
                $.post( "/ajax/ajax.php", {  action:"register",  form_data: form_data  }, function( response ){
                    if( response.status == 'ok' ){
                        $('.fancybox-close-small').trigger('click');
                        /*show_message(
                            response.text,
                            'success', 'button'
                        );*/
                        $(this_form).find("input[type=text], input[type=password]").val("");
                        window.location.href = document.URL;
                    } else if ( response.status == 'auth' ){
                        window.location.href = document.URL;
                    } else if ( response.status == 'error' ){
                        show_error( this_form, response.text );
                    }
                }, 'json')
                .fail(function(response, status, xhr){ show_error( this_form, "Ошибка запроса" ); })
                .always(function(response, status, xhr){  process(false);  })
            // Если есть ошибки
            } else {    show_error( this_form, [checkResult.errors[0]] );    }
        }
    })



    // Восстановление пароля (ЗАПРОС ССЫЛКИ)
    $(document).on('click', '.password___recovery_button', function(e){
        if ( !is_process(this) ){
            // Форма
            var this_form = $(this).parents('form');
            // Настройка полей для проверки
            var fields = {
                'email': {  tag: 'input',  name_ru: 'Email',  is_email: true,
                    required: true,  required_error: 'Введите Ваш Email'  },
            }
            // Проверка полей
            var checkResult = checkFields( this_form, fields );
            // Если нет ошибок
            if ( checkResult.errors.length == 0 ){
                var form_data = $(this_form).serialize();
                process(true);
                $.post( "/ajax/ajax.php", {  action:"password_recovery",  form_data: form_data  }, function( response ){
                    if( response.status == 'ok' ){
                        $('.fancybox-close-small').trigger('click');
                        show_message(
                            'На&nbsp;указанный&nbsp;Email отправлена ссылка для&nbsp;восстановления пароля',
                            'success', 'button'
                        );
                        $(this_form).find("input[type=text], input[type=password]").val("");
                    } else if ( response.status == 'auth' ){
                        window.location.href = document.URL;
                    } else if ( response.status == 'error' ){
                        show_error( this_form, response.text );
                    }
                }, 'json')
                    .fail(function(response, status, xhr){ show_error( this_form, "Ошибка запроса" ); })
                    .always(function(response, status, xhr){  process(false);  })
                // Если есть ошибки
            } else {    show_error( this_form, [checkResult.errors[0]] );    }
        }
    })



    // Восстановление пароля (СОХРАНЕНИЕ)
    $(document).on('click', '.password___recovery_save_button', function(){
        if ( !is_process(this) ){
            // Форма
            var this_form = $(this).parents('form');
            // Настройка полей для проверки
            var fields = {
                'PASSWORD': {
                    tag: 'input',  name_ru: 'Пароль', min_length: 8,
                    required: true,  required_error: 'Введите, пожалуйста, новый пароль'
                },
                'CONFIRM_PASSWORD': {
                    tag: 'input', name_ru: 'Подтверждение пароля',
                    required: true,  required_error: 'Введите, пожалуйста, повтор пароля'
                },
            }
            // Проверка полей
            var checkResult = checkFields( this_form, fields );
            // Если нет ошибок
            if ( checkResult.errors.length == 0 ){
                if( checkResult.values.PASSWORD != checkResult.values.CONFIRM_PASSWORD ){
                    show_error( this_form, 'Пароли отличаются' );
                } else {
                    var form_data = $(this_form).serialize();
                    process(true);
                    $.post( "/ajax/ajax.php", {  action:"password_recovery_save",  form_data: form_data  }, function( response ){
                        if( response.status == 'ok' ){

                            $('.password_update_block').html('<p class="success___p">Пароль успешно изменён</p><p class="success___p"><a href="#popup-login" onclick="$(\'.login___link\').trigger(\'click\');">Авторизоваться</a></p>');

                        } else if ( response.status == 'error' ){
                            show_message( response.text, 'warning', 'hover' );
                        }
                    }, 'json')
                        .fail(function(response, status, xhr){ show_error( this_form, "Ошибка запроса" ); })
                        .always(function(response, status, xhr){  process(false);  })
                }
                // Если есть ошибки
            } else {    show_error( this_form, [checkResult.errors[0]] );    }
        }
    });



    // Редактирование личных данных
    $(document).on('click', '.personal___data_button', function(e){
        if ( !is_process(this) ){
            // Форма
            var this_form = $(this).parents('form');
            // Настройка полей для проверки
            var fields = {
                'EMAIL': {  tag: 'input',  name_ru: 'Email',  is_email: true,
                    required: true,  required_error: 'Введите, пожалуйста, Ваш Email'
                },
                'PERSONAL_PHONE': {  tag: 'input',  name_ru: 'Телефон',  is_phone: true,
                    required: false,  required_error: 'Введите, пожалуйста, Ваш телефон'
                },
                'UF_DOP_PHONE': {  tag: 'input',  name_ru: 'Доп. номер телефона',  is_phone: true,
                    required: false,  required_error: 'Введите, пожалуйста, Ваш доп. номер телефона'
                },
            }
            // Проверка полей
            var checkResult = checkFields( this_form, fields );
            // Если нет ошибок
            if ( checkResult.errors.length == 0 ){
                console.log(checkResult)
                if(
                    'PERSONAL_PHONE' in checkResult.values
                    &&
                    'UF_DOP_PHONE' in checkResult.values
                    &&
                    checkResult.values.UF_DOP_PHONE.length > 0
                    &&
                    checkResult.values.PERSONAL_PHONE.length == 0
                ){
                    show_error( this_form, 'Вы ввели доп. номер телефона, не введя основной' );
                } else {
                    var form_data = $(this_form).serialize();
                    process(true);
                    $.post( "/ajax/ajax.php", {  action:"personal_data",  form_data: form_data  }, function( response ){
                        if( response.status == 'ok' ){
                            $('.fancybox-close-small').trigger('click');
                            show_message(
                                'Успешное сохранение</u>!',
                                'success', 'hover'
                            );
                        } else if ( response.status == 'error' ){
                            show_error( this_form, response.text );
                        }
                    }, 'json')
                    .fail(function(response, status, xhr){ show_error( this_form, "Ошибка запроса" ); })
                    .always(function(response, status, xhr){  process(false);  })
                }

            // Если есть ошибки
            } else {    show_error( this_form, [checkResult.errors[0]] );    }
        }
    })





    // Редактирование пароля
    $(document).on('click', '.personal___password_button', function(e){
        if ( !is_process(this) ){
            // Форма
            var this_form = $(this).parents('form');
            // Настройка полей для проверки
            var fields = {
                'PASSWORD': {  tag: 'input',  name_ru: 'Пароль', min_length:8,
                    required: true,  required_error: 'Введите, пожалуйста, новый пароль'  },
                'CONFIRM_PASSWORD': {  tag: 'input',  name_ru: 'Повтор пароля',
                    required: true,  required_error: 'Введите, пожалуйста, пароль повторно'  },
            }
            // Проверка полей
            var checkResult = checkFields( this_form, fields );
            // Если нет ошибок
            if ( checkResult.errors.length == 0 ){
                var form_data = $(this_form).serialize();
                process(true);
                $.post( "/ajax/ajax.php", {  action:"personal_password_edit",  form_data: form_data  }, function( response ){
                    if( response.status == 'ok' ){
                        show_message(
                            'Успешное сохранение</u>!',
                            'success', 'hover'
                        );
                        $(this_form).find('input[type=password]').val('');$(this_form).find('input[type=password]').trigger('change');
                    } else if ( response.status == 'error' ){
                        show_error( this_form, response.text );
                    }
                }, 'json')
                .fail(function(response, status, xhr){ show_error( this_form, "Ошибка запроса" ); })
                .always(function(response, status, xhr){  process(false);  })
            // Если есть ошибки
            } else {    show_error( this_form, [checkResult.errors[0]] );    }
        }
    })



    // Подгрузка заказов в ЛК
    $(document).on('click', '.ordersMoreButton', function(e){
        if( !is_process(this) ){
            var stop_ids = [];
            if( $('.order___item').length > 0 ){
                $('.order___item').each(function(){
                    stop_ids.push( $(this).attr('item_id') );
                })
            }
            var params = { stop_ids:stop_ids };
            process(true);
            $.post("/ajax/ordersLoad.php", params, function(data){
                if (data.status == 'ok'){

                    $('.orders_load_area').append( data.html );

                    if( $('ost').length > 0 ){
                        $('.ordersMoreButton').show();
                    } else {
                        $('.ordersMoreButton').hide();
                    }
                    $('ost').remove();

                    $('.rolls-js').msRolls('init');

                } else if (data.status == 'error'){
                    show_message(data.text, 'warning')
                }
            }, 'json')
            .fail(function(data, status, xhr){
                console.log('Ошибка запроса'); //show_message('Ошибка запроса', 'warning');
            })
            .always(function(data, status, xhr){
                process(false);
            })
        }
    })




});