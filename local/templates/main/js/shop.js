var old_basket_cnt, new_basket_cnt, basketRecalcProcess = false;
var basketCntInterval;
var filterInterval;
var basketLoadProcess = false;


function basketReInit(){
    ////////////////
    initSpinner($('.spinner-js'));
    //basketRecalc();
    ////////////////
    var observer = lozad();
    observer.observe();
    /////////////////
    $('.cart-keeper-js').switchClass({
        switchClassTo: $('.quick-cart-js'),
        remover: '.quick-cart-close-js',
        modifiers: {  activeClass: 'quick-cart_opened'  },
        cssScrollFixed: true
    });
    initCustomScrollBar();
    ////////////////
    tabs();
    var $cartSimilarSlider = $('.cart-similar-slider-js');
    if ($cartSimilarSlider.length) {
        $cartSimilarSlider.each(function () {
            var $thisSlider = $(this),
                $thisBtnNext = $('.cart-similar-slider__next-js', $thisSlider),
                $thisBtnPrev = $('.cart-similar-slider__prev-js', $thisSlider),
                cartSimilarSliderJs;
            cartSimilarSliderJs = new Swiper($thisSlider, {
                init: false,
                loop: false,
                keyboardControl: true,
                slidesPerView: 6,
                spaceBetween: 10,
                navigation: {
                    nextEl: $thisBtnNext,
                    prevEl: $thisBtnPrev,
                },
                breakpoints: {
                    1599: { slidesPerView: 5 },
                    1279: { slidesPerView: 4 },
                    1099: { slidesPerView: 3 },
                    991: { slidesPerView: 4 },
                    819: { slidesPerView: 3 },
                    649: { slidesPerView: 2 }
                }
            });
            cartSimilarSliderJs.on('init', function () {
                $(cartSimilarSliderJs.slides).matchHeight({
                    byRow: true,
                    property: 'height',
                    target: null,
                    remove: false
                });
                $(cartSimilarSliderJs.el).closest($thisSlider).addClass('is-loaded');
            });
            cartSimilarSliderJs.init();
        });
    }
    ////////////////
}

function basketSmallReInit( open ){
    activateBasketButtons();
    /////////////////
    $('.cart-keeper-js').switchClass({
        switchClassTo: $('.quick-cart-js'),
        remover: '.quick-cart-close-js',
        modifiers: {  activeClass: 'quick-cart_opened'  },
        cssScrollFixed: true
    });
    if( open && $('.quick-cart__item').length > 0 ){
        $('.cart-keeper-js').trigger('click');
    }
    initCustomScrollBar();
    ////////////////
}


function basketRecalc( input ) {
    var $curSpin = $(input),
        curLength = +$curSpin.val(),
        $item = $('.cart-item-js'),
        $curCart = $curSpin.closest('.cart-js'),
        $curItem = $curSpin.closest($item),
        $curSum = $curItem.find('.cart-item__sum-js'),
        totalLength = 0,
        totalPrice = 0;
    if (curLength > 1) {
        $curSum.parent().show();
    } else {
        $curSum.parent().hide();
    }
    var total = +$curItem.find('.cart-item__price-js').attr('data-price') * curLength;
    total = parseFloat(total).toFixed(2);
    $curSum.html(total).attr('data-total', +total);
    $.each($item, function (index, elem) {
        var val = +$(elem).find('.basketCntInput').val();
        totalLength += val;
        totalPrice += +$(elem).find('.cart-item__sum-js').attr('data-total');
    });
    $('.cart__total-length-js', $curCart).html(totalLength).attr('data-total-length', totalLength);
    var totalPriceFloat = parseFloat(totalPrice).toFixed(2);
    $('.cart__total-price-js', $curCart).html(totalPriceFloat).attr('data-total-price', totalPriceFloat);
    var resultPrice = totalPrice + +$('.cart__delivery-js', $curCart).attr('data-deliver');
    var resultPriceFloat = parseFloat(resultPrice).toFixed(2);
    $('.cart__result-price-js', $curCart).html(resultPriceFloat).attr('data-total-price', resultPriceFloat);
}

function basketReload( params, bSmallOpen ) {
    if( !params ){  var params = {};  }
    process(true);

    $('section.cart__list').addClass('js-loading');

    $.post("/ajax/basketReload.php", params, function(data){
        if (data.status == 'ok'){

            $('basket_small').replaceWith( data.top_basket_html );
            $('basketBlock').replaceWith( data.basket_html );

            basketReInit();
            basketSmallReInit( bSmallOpen );

        }
    }, 'json')
    .fail(function(data, status, xhr){
        console.log('Ошибка запроса'); //show_message('Ошибка запроса', 'warning');
    })
    .always(function(data, status, xhr){
        process(false);
    })
}


function orderReload( params ) {
    if( !params ){  var params = {};  }

    var form = $('.order___form');
    params['form_data'] = $(form).serialize()

    process(true);

    $('orderBlock').addClass('js-loading');

    $.post("/ajax/orderReload.php", params, function(data){
        if (data.status == 'ok'){

            $('orderBlock').replaceWith( data.html );

            inputHasValueClass();

            rollsInit();

        }
    }, 'json')
    .fail(function(data, status, xhr){
        //console.log('Ошибка запроса'); //show_message('Ошибка запроса', 'warning');
    })
    .always(function(data, status, xhr){
        process(false);
    })
}


function setFavorites(){
    var cur_favs_str = $.cookie('BITRIX_SM_favorites');
    cur_favs = [];
    if( cur_favs_str != undefined && cur_favs_str.length > 0 ){
        cur_favs = cur_favs_str.split('|');
    }
    if( $('.fav___button').length > 0 ){
        $('.fav___button').each(function(){
            var item_id = $(this).attr('item_id');
            if( in_array(item_id, cur_favs) ){
                $(this).addClass('added');
                $(this).find('span').eq(0).html('Мне нравится');
            } else {
                $(this).removeClass('added');
                $(this).find('span').eq(0).html('В избранное');
            }
        });
    }
    if( cur_favs.length > 0 ){
        $('.header_fav_cnt_block').html( cur_favs.length ).show();
        if( cur_favs.length >= 10 ){
            $('.header_fav_cnt_block').addClass('header_fav_cnt_block_big');
        } else {
            $('.header_fav_cnt_block').removeClass('header_fav_cnt_block_big');
        }
    } else {
        $('.header_fav_cnt_block').hide();
    }
}



function setCompares(){
    var cur_compares_str = $.cookie('BITRIX_SM_compares');
    cur_compares = [];
    if( cur_compares_str != undefined && cur_compares_str.length > 0 ){
        cur_compares = cur_compares_str.split('|');
    }
    if( $('.compare___button').length > 0 ){
        $('.compare___button').each(function(){
            var item_id = $(this).attr('item_id');
            if( in_array(item_id, cur_compares) ){
                $(this).addClass('added');
            } else {
                $(this).removeClass('added');
            }
        });
    }
    $('.compare_cnt_block').html(cur_compares.length);
    if( cur_compares.length > 0 ){
        $('.compare_cnt_block').show();
    } else {
        $('.compare_cnt_block').hide();
    }
}



function activateBasketButtons(){
    var basket_products = [];
    if( $('basket_small .quick-cart__item').length > 0 ){
        $('basket_small .quick-cart__item').each(function(){
            var product_id = $(this).attr('product_id');
            basket_products.push(product_id);
        })
    }
    if( $('.add_to_basket_button').length > 0 ){
        $('.add_to_basket_button').each(function(){
            var item_id = $(this).attr('item_id');
            if( in_array(item_id, basket_products) ){
                $(this).addClass('added');
            } else {
                $(this).removeClass('added');
            }
        })
    }
}



function order( this_form, isAdmin ){

    $(this_form).find('p.error___p').html('');

    var fields = orderFormFields;
    var errors_for_tags = [];
    var errors = [];

    // Проверки
    for ( key in fields ) {

        var field = fields[key];

        var tag = false;
        if( $(this_form).find('input[name='+field['CODE']+']').length > 0 ){
            tag = 'input';
        }
        if( $(this_form).find('textarea[name='+field['CODE']+']').length > 0 ){
            tag = 'textarea';
        }
        if( $(this_form).find('select[name='+field['CODE']+']').length > 0 ){
            tag = 'select';
        }

        if( tag ) {

            var postfix = '';
            if(
                tag == 'input'
                &&
                (
                    $(this_form).find(tag+'[name='+field['CODE']+']').attr('type') == 'checkbox'
                    ||
                    $(this_form).find(tag+'[name='+field['CODE']+']').attr('type') == 'radio'
                )
            ){     postfix = ':checked';     }

            var selection = $(this_form).find(tag+'[name='+field['CODE']+']'+postfix);
            var value = $(selection).val();

            if (
                'REQUIRED' in field
                &&
                field['REQUIRED']
            ){
                if ( 'variants' in field ) {
                    if (
                        value == undefined
                        ||
                        !value
                        ||
                        value == 'empty'
                    ) {
                        errors_for_tags.push([key, 'Это поле обязательно для заполнения']);
                        errors.push([key, 'Заполните поле "' + field.NAME + '"']);
                    }
                } else if (value.length == 0) {
                    errors_for_tags.push([key, 'Это поле обязательно для заполнения']);
                    errors.push([key, 'Заполните поле "'+field.NAME+'"']);
                }
            }

            if(
                value != undefined
                &&
                value.length > 0
                &&
                'check_reg' in field
            ){
                var reg = new RegExp( field.check_reg, 'g' );
                if( !reg.test(value) ){
                    if( 'check_error' in field ){
                        errors_for_tags.push([key, 'Поле "' + field.NAME + '": '+field['check_error']]);
                        errors.push([key, 'Поле "' + field.NAME + '": '+field['check_error']]);
                    } else {
                        errors_for_tags.push([key, 'Недопустимый формат поля "' + field.NAME + '"']);
                        errors.push([key, 'Недопустимый формат поля "' + field.NAME + '"']);
                    }
                }
            }

            if(
                value != undefined
                &&
                value.length > 0
                &&
                'max_length' in field
                &&
                value.length > field.max_length
            ){
                errors_for_tags.push([key, 'Поле "' + field.NAME + '": максимальная длина '+field.max_length+' символов']);
                errors.push([key, 'Поле "' + field.NAME + '": максимальная длина '+field.max_length+' символов']);
            }

        }
    }

    // Если есть ошибки
    if ( errors.length > 0 ){

        var ers = [];
        var first_tag = false;
        for(key in errors){
            var item_name = errors[key][0];
            var tag = false;
            if( item_name ) {
                if( $(this_form).find('input[name=' + item_name + ']').length > 0 ){
                    tag = $(this_form).find('input[name=' + item_name + ']');
                } else if( $(this_form).find('textarea[name=' + item_name + ']').length > 0 ){
                    tag = $(this_form).find('textarea[name=' + item_name + ']');
                } else if( $(this_form).find('select[name=' + item_name + ']').length > 0 ){
                    tag = $(this_form).find('select[name=' + item_name + ']');
                }
            }
            if( tag ){
                if( !first_tag ){
                    first_tag = tag;
                }
                $(tag).addClass('error');
                $(tag).attr('title', errors_for_tags[key][1]);
                $(tag).parents('label.input-wrap').find('span.error-note').remove();
                $(tag).parents('label.input-wrap').append('<span class="error-note">'+errors_for_tags[key][1]+'</span>');

                $(tag).attr('oninput','$(this).removeClass(\'error\'); $(this).attr("title", ""); $(this).parents("form").find("p.error___p").html("");');
            }
            ers.push(errors[key][1]);
        }

        show_error( this_form, ers.join("<br>") );
        var destination = $(first_tag).offset();
        $('body, html').animate({scrollTop: destination.top - $("header").outerHeight(true) - 15}, 400);

    } else {

        var error = false;
        var ps_id = $(this_form).find('input[name=ps_id]:checked').val();
        if( ps_id == undefined ){
            error = 'Выберите, пожалуйста, вариант оплаты';
        }
        if( error ){

            show_error(this_form, error);

        } else {

            $('.order___button').prop('disabled', true);
            var form_data = $(this_form).serialize();
            var postParams = {
                action: "order",
                form_data: form_data
            };
            process(true);
            $.post("/ajax/ajax.php", postParams, function(data){
                if (data.status == 'ok'){

                    dataLayer.push({
                        'event': 'event',
                        'eventCategory': 'форма_заказа',
                        'eventAction': 'отправить'
                    });

                    var form = create_form('quick_order_success', 'post', '/order_success/');
                    $(form).append('<input type="hidden" name="order_id" value="'+data.order_id+'">');
                    $(form).submit();

                } else if (data.status == 'no_rassrochka'){

                    var modalHTML = '<div class="modal-title h2"><em class="icon-error"></em><div class="modal-title__inner">Предупреждение</div></div>';
                    modalHTML += '<p>В&nbsp;корзине есть&nbsp;товары, для&nbsp;которых не&nbsp;доступно оформление&nbsp;в&nbsp;рассрочку:</p>';
                    for( key in data.products ){
                        var el = data.products[key]['el'];
                        modalHTML += '<p><strong>- '+el['NAME']+';</strong></p>';
                    }
                    modalHTML += '<p>Вы&nbsp;хотите оформить&nbsp;заказ без&nbsp;указанных&nbsp;товаров?</p>';

                    modalHTML += '<button class="btn-def rassrochkaConfirmButton yesRassrochkaOrderButton">Да</button>';
                    modalHTML += '<button class="btn-def rassrochkaConfirmButton noRassrochkaOrderButton">Нет</button>';

                    $('#orderModal').html(modalHTML);
                    $.fancybox.open({ src  : '#orderModal', type : 'inline' });

                    $('.order___button').prop('disabled', false);

                } else if (data.status == 'no_credit'){

                    var modalHTML = '<div class="modal-title h2"><em class="icon-error"></em><div class="modal-title__inner">Предупреждение</div></div>';
                    modalHTML += '<p>В&nbsp;корзине есть&nbsp;товары, для&nbsp;которых не&nbsp;доступно оформление&nbsp;в&nbsp;кредит:</p>';
                    for( key in data.products ){
                        var el = data.products[key]['el'];
                        modalHTML += '<p><strong>- '+el['NAME']+';</strong></p>';
                    }
                    modalHTML += '<p>Вы&nbsp;хотите оформить&nbsp;заказ без&nbsp;указанных&nbsp;товаров?</p>';

                    modalHTML += '<button class="btn-def creditConfirmButton yesCreditOrderButton">Да</button>';
                    modalHTML += '<button class="btn-def creditConfirmButton noCreditOrderButton">Нет</button>';

                    $('#orderModal').html(modalHTML);
                    $.fancybox.open({ src  : '#orderModal', type : 'inline' });

                    $('.order___button').prop('disabled', false);

                } else if (data.status == 'no_yur'){

                    var modalHTML = '<div class="modal-title h2"><em class="icon-error"></em><div class="modal-title__inner">Предупреждение</div></div>';
                    modalHTML += '<p>В&nbsp;корзине есть&nbsp;товары, для&nbsp;которых не&nbsp;доступно оформление&nbsp;юр.&nbsp;лицом:</p>';
                    for( key in data.products ){
                        var el = data.products[key]['el'];
                        modalHTML += '<p><strong>- '+el['NAME']+';</strong></p>';
                    }
                    modalHTML += '<p>Вы&nbsp;хотите оформить&nbsp;заказ без&nbsp;указанных&nbsp;товаров?</p>';

                    modalHTML += '<button class="btn-def yurConfirmButton yesYurOrderButton">Да</button>';
                    modalHTML += '<button class="btn-def yurConfirmButton noYurOrderButton">Нет</button>';

                    $('#orderModal').html(modalHTML);
                    $.fancybox.open({ src  : '#orderModal', type : 'inline' });

                    $('.order___button').prop('disabled', false);

                } else if (data.status == 'error'){
                    show_message(data.text, 'warning')
                    $('.order___button').prop('disabled', false);
                }
            }, 'json')
            .fail(function(data, status, xhr){
                console.log('Ошибка запроса'); //show_message('Ошибка запроса', 'warning');
                $('.order___button').prop('disabled', false);
            })
            .always(function(data, status, xhr){
                process(false);
            })

        }
    }
}



function catalogSortChange ( sort_field, sort_order, isSearch, isFavorites, isBrand, isBestPrices, isGiftIdeas ){
    catalogSortField = sort_field;
    catalogSortOrder = sort_order;
    var url = document.URL;
    url = addToRequestURI( url, 'sort_field', sort_field );
    url = addToRequestURI( url, 'sort_order', sort_order );
    history.replaceState({}, null, url);
    var params = {};
    if( isSearch == 'Y' ){   params['isSearch'] = 'Y';   }
    if( isFavorites == 'Y' ){   params['isFavorites'] = 'Y';   }
    if( isBrand == 'Y' ){   params['isBrand'] = 'Y';   }
    if( isBestPrices == 'Y' ){   params['isBestPrices'] = 'Y';   }
    if( isGiftIdeas == 'Y' ){   params['isGiftIdeas'] = 'Y';   }
    catalogLoad( params, true );
}

function searchSortChange ( sort_field, sort_order ){
    searchSortField = sort_field;
    searchSortOrder = sort_order;
    var url = document.URL;
    url = addToRequestURI( url, 'sort_field', sort_field );
    url = addToRequestURI( url, 'sort_order', sort_order );
    history.replaceState({}, null, url);
    var params = {
        isSearch: 'Y'
    };
    catalogLoad( params, true );
}


function catalogLoad ( params, is_reload, is_filter ){
    if( !params ){   var params = {};   }
    params['section_id'] = $('input[name=section_id]').val();
    if( $('.is___seo_page').length == 0 ){
        params['url'] = rawurldecode(document.URL);
    } else {
        params['url'] = $('input[name=seo_page_url]').val();
    }
    params['is_reload'] = is_reload?'Y':'N';
    process(true);
    var file = "/ajax/catalogLoad.php";
    if( params['isSearch'] == 'Y' ){
        file = "/ajax/searchCatalogLoad.php";
        params['sort_field'] = searchSortField;
        params['sort_order'] = searchSortOrder;
    } else {
        params['sort_field'] = catalogSortField;
        params['sort_order'] = catalogSortOrder;
    }
    if( params['isFavorites'] == 'Y' ){
        file = "/ajax/favoritesCatalogLoad.php";
    }
    if( params['isBrand'] == 'Y' ){
        file = "/ajax/brandCatalogLoad.php";
    }
    if( params['isBestPrices'] == 'Y' ){
        file = "/ajax/bestPricesCatalogLoad.php";
    }
    if( params['isGiftIdeas'] == 'Y' ){
        file = "/ajax/giftIdeasCatalogLoad.php";
    }
    if( $('.p-filters-js').length > 0 ){
        $('.p-filters-js').addClass('js-loading');
    }
    $.post(file, params, function(data){
        if (data.status == 'ok'){

            if( data.hasNext == 'Y' ){
                $('.catalogMoreButton').show();
            } else {
                $('.catalogMoreButton').hide();
            }

            if( is_filter ){
                updateFilter( data );
            }

            if( is_reload ){

                var html = data.html;

                if( html.length > 0 ){
                    $('.catalogLoadArea').html( html );
                    $('.catalogSortBlock').replaceWith( data.sort_html );
                    sortProducts();
                } else {
                    $('.catalogLoadArea').html( '<div class="load-more no___count_block"><p>В данном разделе / по данному фильтру товаров нет</p></div>' );
                }

                $('.paginationBlock:visible').remove();
                if( $('.hiddenPaginationBlock').length > 0 ){
                    var hiddenPaginationBlockClone = $('.hiddenPaginationBlock').clone();
                    $('.hiddenPaginationBlock').remove();
                    $('div.load-more').before( $(hiddenPaginationBlockClone) );
                    $('.hiddenPaginationBlock').removeClass('hiddenPaginationBlock');
                }

                if( params['pageNumber'] != undefined && params['pageNumber'] > 0 ){
                    $('.catalogMoreButton').hide();
                } else {
                    $('.catalogMoreButton').show();
                }

            } else {

                $('.paginationBlock:visible').remove();

                $('.catalogLoadArea').append( data.html );

            }


            preloadOtherImages();
            setFavorites();
            setCompares();
            activateBasketButtons();
            rulikBubble();

        } else if (data.status == 'error'){
            show_message(data.text, 'warning')
        }
    }, 'json')
    .fail(function(data, status, xhr){
        console.log('Ошибка запроса'); //show_message('Ошибка запроса', 'warning');
    })
    .always(function(data, status, xhr){
        process(false);
        $('.p-filters-js').removeClass('js-loading');
    })
}



function updateFilter( data ){
    filterInfo = JSON.parse( data.filterJSON );

    // Активация/деактивация чекбоксов
    $('.smartfilter input[type=checkbox]').prop('disabled', false);
    if( filterInfo && 'ITEMS' in filterInfo ){
        for( key in filterInfo.ITEMS ){
            var arItem = filterInfo.ITEMS[key];
            if( arItem.DISPLAY_TYPE == 'A' ){

            } else if( arItem.DISPLAY_TYPE == 'B' ){

            } else {
                if( 'VALUES' in arItem ){
                    for( val in arItem.VALUES ){
                        var ar = arItem.VALUES[val];
                        if( ar.ELEMENT_COUNT > 0 ){} else {
                            if(
                                $('.smartfilter input#'+ar.CONTROL_ID).attr('type') == 'checkbox'
                                &&
                                !$('.smartfilter input#'+ar.CONTROL_ID).prop('checked')
                            ){
                                $('.smartfilter input#'+ar.CONTROL_ID).prop('disabled', true);
                            }
                        }
                    }
                }
            }
        }
    }
}



// Смена категории в товарах брэнда
function brandChangeCategory ( button ){
    if(
        !is_process(button)
        &&
        !$(button).parents('li').hasClass('current')
    ){

        var section_id = $(button).attr('section_id');
        var iblock_id = $(button).attr('iblock_id');
        var postParams = {
            section_id: section_id,
            iblock_id: iblock_id,
        };
        postParams['sort_field'] = catalogSortField;
        postParams['sort_order'] = catalogSortOrder;
        postParams['is_reload'] = 'Y';

        process(true);
        $.post("/ajax/brandCatalogLoad.php", postParams, function(data){
            if (data.status == 'ok'){

                $('.catalogLoadArea').html( data.html );

                $('.managerBlock').replaceWith( data.manager_html );

                if( data.hasNext == 'Y' ){
                    $('.catalogMoreButton').show();
                } else {
                    $('.catalogMoreButton').hide();
                }

                $('ul.search___categories li').removeClass('current');
                $(button).parents('li').addClass('current');

                preloadOtherImages();
                setFavorites();
                setCompares();
                activateBasketButtons();
                rulikBubble();

                $('input[name=section_id]').val(section_id);
                $('input[name=iblock_id]').val(iblock_id);

                var url = removeFromRequestURI( document.URL, 'PAGEN_1' );
                history.replaceState({}, null, url);

                $('.paginationBlock:visible').remove();
                if( $('.hiddenPaginationBlock').length > 0 ){
                    var hiddenPaginationBlockClone = $('.hiddenPaginationBlock').clone();
                    $('.hiddenPaginationBlock').remove();
                    $('div.load-more').before( $(hiddenPaginationBlockClone) );
                    $('.hiddenPaginationBlock').removeClass('hiddenPaginationBlock');
                }


            }
        }, 'json')
        .fail(function(data, status, xhr){
            console.log('Ошибка запроса'); //show_message('Ошибка запроса', 'warning');
        })
        .always(function(data, status, xhr){
            process(false);
        })
    }
}



// Смена категории в поиске
function searchChangeCategory ( button ){
    if(
        !is_process(button)
        &&
        !$(button).parents('li').hasClass('current')
    ){

        var section_id = $(button).attr('section_id');
        var iblock_id = $(button).attr('iblock_id');
        var postParams = {
            section_id: section_id,
            iblock_id: iblock_id,
        };
        postParams['sort_field'] = searchSortField;
        postParams['sort_order'] = searchSortOrder;
        postParams['is_reload'] = 'Y';
        postParams['url'] = rawurldecode(document.URL);
        postParams['pageNumber'] = 1;
        process(true);
        $.post("/ajax/searchCatalogLoad.php", postParams, function(data){
            if (data.status == 'ok'){

                $('.catalogLoadArea').html( data.html );

                $('.managerBlock').replaceWith( data.manager_html );

                if( data.hasNext == 'Y' ){
                    $('.catalogMoreButton').show();
                } else {
                    $('.catalogMoreButton').hide();
                }

                $('ul.search___categories li').removeClass('current');
                $(button).parents('li').addClass('current');

                preloadOtherImages();
                setFavorites();
                setCompares();
                activateBasketButtons();
                rulikBubble();

                $('input[name=section_id]').val(section_id);
                $('input[name=iblock_id]').val(iblock_id);

                var url = removeFromRequestURI( document.URL, 'PAGEN_1' );
                history.replaceState({}, null, url);

                $('.paginationBlock:visible').remove();
                if( $('.hiddenPaginationBlock').length > 0 ){
                    var hiddenPaginationBlockClone = $('.hiddenPaginationBlock').clone();
                    $('.hiddenPaginationBlock').remove();
                    $('div.load-more').before( $(hiddenPaginationBlockClone) );
                    $('.hiddenPaginationBlock').removeClass('hiddenPaginationBlock');
                }
            }
        }, 'json')
        .fail(function(data, status, xhr){
            console.log('Ошибка запроса'); //show_message('Ошибка запроса', 'warning');
        })
        .always(function(data, status, xhr){
            process(false);
        })
    }
}


// Смена категории в избранном
function favoritesChangeCategory ( button ){
    if(
        !is_process(button)
        &&
        !$(button).parents('li').hasClass('current')
    ){
        var section_id = $(button).attr('section_id');
        var iblock_id = $(button).attr('iblock_id');
        var postParams = {
            section_id: section_id,
            iblock_id: iblock_id,
        };
        postParams['sort_field'] = catalogSortField;
        postParams['sort_order'] = catalogSortOrder;
        postParams['is_reload'] = 'Y';
        postParams['url'] = rawurldecode(document.URL);
        postParams['pageNumber'] = 1;
        process(true);
        $.post("/ajax/favoritesCatalogLoad.php", postParams, function(data){
            if (data.status == 'ok'){

                $('.catalogLoadArea').html( data.html );

                $('.managerBlock').replaceWith( data.manager_html );

                if( data.hasNext == 'Y' ){
                    $('.catalogMoreButton').show();
                } else {
                    $('.catalogMoreButton').hide();
                }

                $('ul.search___categories li').removeClass('current');
                $(button).parents('li').addClass('current');

                preloadOtherImages();
                setFavorites();
                setCompares();
                activateBasketButtons();
                rulikBubble();

                $('input[name=section_id]').val(section_id);
                $('input[name=iblock_id]').val(iblock_id);

                var url = removeFromRequestURI( document.URL, 'PAGEN_1' );
                history.replaceState({}, null, url);

                $('.paginationBlock:visible').remove();
                if( $('.hiddenPaginationBlock').length > 0 ){
                    var hiddenPaginationBlockClone = $('.hiddenPaginationBlock').clone();
                    $('.hiddenPaginationBlock').remove();
                    $('div.load-more').before( $(hiddenPaginationBlockClone) );
                    $('.hiddenPaginationBlock').removeClass('hiddenPaginationBlock');
                }
            }
        }, 'json')
        .fail(function(data, status, xhr){
            console.log('Ошибка запроса'); //show_message('Ошибка запроса', 'warning');
        })
        .always(function(data, status, xhr){
            process(false);
        })
    }
}


// Смена категории в Лучших ценах
function bestPricesChangeCategory ( button ){
    if(
        !is_process(button)
        &&
        !$(button).parents('li').hasClass('current')
    ){
        var section_id = $(button).attr('section_id');
        var section_code = $(button).attr('section_code');
        var section_name = $(button).attr('section_name');
        var iblock_id = $(button).attr('iblock_id');
        var postParams = {
            section_id: section_id,
            iblock_id: iblock_id,
        };
        postParams['sort_field'] = catalogSortField;
        postParams['sort_order'] = catalogSortOrder;
        postParams['is_reload'] = 'Y';
        postParams['url'] = rawurldecode(document.URL);
        postParams['pageNumber'] = 1;
        process(true);
        $.post("/ajax/bestPricesCatalogLoad.php", postParams, function(data){
            if (data.status == 'ok'){

                $('.catalogLoadArea').html( data.html );

                $('.managerBlock').replaceWith( data.manager_html );

                if( data.hasNext == 'Y' ){
                    $('.catalogMoreButton').show();
                } else {
                    $('.catalogMoreButton').hide();
                }

                $('ul.search___categories li').removeClass('current');
                $(button).parents('li').addClass('current');

                preloadOtherImages();
                setFavorites();
                setCompares();
                activateBasketButtons();
                rulikBubble();

                $('input[name=section_id]').val(section_id);
                $('input[name=iblock_id]').val(iblock_id);

                $('title').html('Лучшие цены в категории "'+section_name+'"');

                var url = '/best_prices/'+section_code+'/';
                url = addToRequestURI( url, 'sort_field', catalogSortField );
                url = addToRequestURI( url, 'sort_order', catalogSortOrder );
                url = removeFromRequestURI( url, 'PAGEN_1' );
                history.replaceState({}, null, url);

                $('div.breadcrumbs li span:last').html(section_name);


                $('.paginationBlock:visible').remove();
                if( $('.hiddenPaginationBlock').length > 0 ){
                    var hiddenPaginationBlockClone = $('.hiddenPaginationBlock').clone();
                    $('.hiddenPaginationBlock').remove();
                    $('div.load-more').before( $(hiddenPaginationBlockClone) );
                    $('.hiddenPaginationBlock').removeClass('hiddenPaginationBlock');
                }
            }
        }, 'json')
        .fail(function(data, status, xhr){
            console.log('Ошибка запроса'); //show_message('Ошибка запроса', 'warning');
        })
        .always(function(data, status, xhr){
            process(false);
        })
    }
}


// Смена категории в Идеях для подарков
function giftIdeasChangeCategory ( button ){
    if(
        !is_process(button)
        &&
        !$(button).parents('li').hasClass('current')
    ){
        var section_id = $(button).attr('section_id');
        var section_code = $(button).attr('section_code');
        var section_name = $(button).attr('section_name');

        var iblock_id = $(button).attr('iblock_id');
        var postParams = {
            section_id: section_id,
            iblock_id: iblock_id,
        };
        postParams['sort_field'] = catalogSortField;
        postParams['sort_order'] = catalogSortOrder;
        postParams['is_reload'] = 'Y';
        postParams['url'] = rawurldecode(document.URL);
        postParams['pageNumber'] = 1;
        process(true);
        $.post("/ajax/giftIdeasCatalogLoad.php", postParams, function(data){
            if (data.status == 'ok'){

                $('.catalogLoadArea').html( data.html );

                $('.managerBlock').replaceWith( data.manager_html );

                if( data.hasNext == 'Y' ){
                    $('.catalogMoreButton').show();
                } else {
                    $('.catalogMoreButton').hide();
                }

                $('ul.search___categories li').removeClass('current');
                $(button).parents('li').addClass('current');

                preloadOtherImages();
                setFavorites();
                setCompares();
                activateBasketButtons();
                rulikBubble();

                $('input[name=section_id]').val(section_id);
                $('input[name=iblock_id]').val(iblock_id);

                $('title').html('Идеи подарков в категории "'+section_name+'"');

                $('div.breadcrumbs li span:last').html(section_name);

                var url = '/gift_ideas/'+section_code+'/';
                url = addToRequestURI( url, 'sort_field', catalogSortField );
                url = addToRequestURI( url, 'sort_order', catalogSortOrder );
                url = removeFromRequestURI( url, 'PAGEN_1' );
                history.replaceState({}, null, url);

                $('.paginationBlock:visible').remove();
                if( $('.hiddenPaginationBlock').length > 0 ){
                    var hiddenPaginationBlockClone = $('.hiddenPaginationBlock').clone();
                    $('.hiddenPaginationBlock').remove();
                    $('div.load-more').before( $(hiddenPaginationBlockClone) );
                    $('.hiddenPaginationBlock').removeClass('hiddenPaginationBlock');
                }
            }
        }, 'json')
        .fail(function(data, status, xhr){
            console.log('Ошибка запроса'); //show_message('Ошибка запроса', 'warning');
        })
        .always(function(data, status, xhr){
            process(false);
        })
    }
}



function catalogInfo(){
    if( $('.catalog___item').length > 0 ){
        var ids = [];
        $('.catalog___item').each(function(){
            var item_id = $(this).attr('item_id');
            ids.push(item_id);
        })
        var postParams = { ids: ids };
        process(true);
        $.post("/ajax/catalogInfo.php", postParams, function(data){
            if (data.status == 'ok'){
                if( Object.keys( data.ratings ).length > 0 ){
                    for( id in data.ratings ){
                        var rating = data.ratings[id];
                        $('.catalog___item[item_id='+id+'] .product__rate span').attr('content', rating);
                        $('.catalog___item[item_id='+id+'] .product__rate span').html(rating);
                        $('.catalog___item[item_id='+id+'] .product__rate span').css('width', Math.round(rating/5*100));
                        $('.catalog___item[item_id='+id+'] .product__rate meta').attr('content', rating);
                    }
                }
            }
        }, 'json')
        .fail(function(data, status, xhr){})
        .always(function(data, status, xhr){
            process(false);
        })
    }
}



function productCardInfo(){
    if( $('.productCardBlock').length > 0 ){
        var product_id = $('.productCardBlock').attr('item_id');
        var postParams = { product_id: product_id };
        process(true);
        $.post("/ajax/productCardInfo.php", postParams, function(data){
            if (data.status == 'ok'){
                if( 'productCardDopBlock' in data ){
                    $('.productCardDopBlock').replaceWith( data.productCardDopBlock );

                    dop_tabs();

                    var $tapeSlider = $('.tape-slider-js');
                    if ($tapeSlider.length) {
                        $tapeSlider.each(function () {
                            var $thisSlider = $(this),
                            $thisBtnNext = $('.tape-slider__next-js', $thisSlider),
                            $thisBtnPrev = $('.tape-slider__prev-js', $thisSlider),
                            tapeSliderInited;
                            tapeSliderInited = new Swiper($thisSlider, {
                                init: false, loop: false, keyboardControl: true,
                                slidesPerView: 5,   spaceBetween: 12,
                                navigation: { nextEl: $thisBtnNext, prevEl: $thisBtnPrev },
                                breakpoints: { 1599: { slidesPerView: 4 }, 1199: { slidesPerView: 3 }, 879: { slidesPerView: 2 }, 639: { slidesPerView: 2, spaceBetween: 0 } }
                            });
                            tapeSliderInited.on('init', function () {
                                $(tapeSliderInited.slides).matchHeight({ byRow: true,   property: 'height', target: null,   remove: false });
                                $(tapeSliderInited.el).closest($thisSlider).addClass('is-loaded');
                            });
                            tapeSliderInited.init();
                        });
                    }

                    var $setsSlider = $('.sets-slider-js');
                    if ($setsSlider.length) {
                        $setsSlider.each(function () {
                            var $thisSlider = $(this),
                            $thisBtnNext = $thisSlider.find('.sets-slider__next-js'),
                            $thisBtnPrev = $thisSlider.find('.sets-slider__prev-js'),
                            setsSliderJs;
                            setsSliderJs = new Swiper($thisSlider, { init: false,   spaceBetween: 20, navigation: { nextEl: $thisBtnNext, prevEl: $thisBtnPrev } });
                            setsSliderJs.on('init', function () {
                                $(setsSliderJs.el).closest($thisSlider).addClass('is-loaded');
                            });
                            setsSliderJs.init();
                        });
                    }

                    if( $('#giftsBlock').length ){
                        $('.productPaymentBlock').hide();
                        $('.toGiftsLinkBlock').show();
                        if( $('.giftsTabLink.tabs-active').length == 0 ){
                            $('.giftsTabLink').trigger('click');
                        }
                    }

                }

                if( 'productCardPhotosBlock' in data ){

                    $('.productCardGalleryParentBlock').append( data.productCardPhotosBlock );

                    var $cardGallery = $('.p-card-gallery-js');
                    if ($cardGallery.length) {
                        var cardGalleryThumbsTpl = $('<div class="p-card-gallery-thumbs"><div class="p-card-gallery-thumbs__arrow-prev arrow-prev-js"></div><div class="swiper-container"><div class="swiper-wrapper"></div></div><div class="p-card-gallery-thumbs__arrow-next arrow-next-js"></div></div>');
                        $cardGallery.each(function () {
                            var $curSlider = $(this),
                                $imgList = $curSlider.find('.p-card-gallery-images-js'),
                                $imgListItem = $imgList.find('img').parent();
                            $imgList.after(cardGalleryThumbsTpl.clone());
                            var $galleryThumbs = $curSlider.find('.p-card-gallery-thumbs'),
                                $galleryThumbsArrPrev = $galleryThumbs.find('.arrow-prev-js'),
                                $galleryThumbsArrNext = $galleryThumbs.find('.arrow-next-js');
                            $.each($imgListItem, function () {
                                var $this = $(this);
                                $galleryThumbs.find('.swiper-wrapper').append($('<div class="swiper-slide p-card-gallery-thumbs__item"><img src="' + $this.find('img').attr('data-thumb') + '" alt="' + $this.find('img').attr('alt') + '"></div>'));
                            });
                            var cardImgSlider = new Swiper($imgList, {
                                init: false,
                                spaceBetween: 20,
                                preloadImages: false,
                                lazy: {
                                    loadPrevNext: true,
                                    loadOnTransitionStart: true
                                },
                                thumbs: {
                                    swiper: {
                                        el: $galleryThumbs.find('.swiper-container'),
                                        direction: 'vertical',
                                        slidesPerView: 'auto',
                                        spaceBetween: 14,
                                        freeMode: true,
                                        slideToClickedSlide: true,
                                        watchSlidesVisibility: true,
                                        watchSlidesProgress: true,
                                        navigation: {
                                            nextEl: $galleryThumbsArrNext,
                                            prevEl: $galleryThumbsArrPrev,
                                        },
                                        breakpoints: {
                                            767: {
                                                direction: 'horizontal'
                                            }
                                        }
                                    },
                                },
                                on: {
                                    lazyImageReady: function (slide, image) {
                                        objectFitImages($(image));
                                    }
                                }
                            });
                            cardImgSlider.on('init', function () {
                                $curSlider.addClass('is-loaded');
                                objectFitImages($('img', $(cardImgSlider.el)));
                            });
                            cardImgSlider.init();
                            $().fancybox({
                                selector: '.p-card-gallery-js a:visible',
                                infobar: true,
                                baseClass: 'white-spaces',
                                buttons: [
                                    "zoom",
                                    "close"
                                ],
                                beforeClose: function (instance, current) {
                                    cardImgSlider.slideTo(current.index);
                                }
                            });
                        });
                    }

                }

                if( 'recommendProductHTML' in data ){
                    var recommendProductHTML = data.recommendProductHTML;
                    if( recommendProductHTML.length > 0 ){
                        $('.productExpertBlock').replaceWith(recommendProductHTML);
                    }
                }

            }
        }, 'json')
        .fail(function(data, status, xhr){})
        .always(function(data, status, xhr){
            process(false);
        })
    }
}

function complectCardInfo(){
    if( $('.complectCardBlock').length > 0 ){
        var product_id = $('.complectCardBlock').attr('item_id');
        var postParams = { product_id: product_id };
        process(true);
        $.post("/ajax/complectCardInfo.php", postParams, function(data){
            if (data.status == 'ok'){

                if( 'recommendProductHTML' in data ){
                    var recommendProductHTML = data.recommendProductHTML;
                    if( recommendProductHTML.length > 0 ){
                        $('.productExpertBlock').replaceWith(recommendProductHTML);
                    }
                }

            }
        }, 'json')
        .fail(function(data, status, xhr){})
        .always(function(data, status, xhr){
            process(false);
        })
    }
}

function mainPageInfo(){
    if(
        $('mainPageBestPrices').length > 0
        ||
        $('mainPageGiftIdeas').length > 0
    ){
        process(true);
        $.post("/ajax/mainPageInfo.php", {}, function(data){
            if (data.status == 'ok'){
                if(
                    $('mainPageBestPrices').length > 0
                    &&
                    'mainPageBestPrices' in data
                ){
                    $('mainPageBestPrices').replaceWith( data.mainPageBestPrices );
                } else {
                    $('mainPageBestPrices').remove();
                }
                if(
                    $('mainPageGiftIdeas').length > 0
                    &&
                    'mainPageGiftIdeas' in data
                ){
                    $('mainPageGiftIdeas').replaceWith( data.mainPageGiftIdeas );
                } else {
                    $('mainPageGiftIdeas').remove();
                }

                dop_tabs();

            } 
        }, 'json')
        .fail(function(data, status, xhr){})
        .always(function(data, status, xhr){
            process(false);
        })
    }
}


$(document).ready(function(){

    setFavorites();
    setCompares();
    activateBasketButtons();

    //basketRecalc( $('.spinner-js') );

    catalogInfo();
    productCardInfo();
    complectCardInfo();
    mainPageInfo();

    $('.order___button').prop('disabled', false);


    $(document).on('click', '.yesRassrochkaOrderButton', function(e){
        $('.order___form input[name=rassrochka_accept]').remove();
        $('.order___form').append('<input type="hidden" name="rassrochka_accept" value="Y">');
        $('.fancybox-close-small').trigger('click');
        order( $('.order___form') );
    })
    $(document).on('click', '.noRassrochkaOrderButton', function(e){
        $('.order___form input[name=rassrochka_accept]').remove();
        $('.fancybox-close-small').trigger('click');
    })

    $(document).on('click', '.yesCreditOrderButton', function(e){
        $('.order___form input[name=credit_accept]').remove();
        $('.order___form').append('<input type="hidden" name="credit_accept" value="Y">');
        $('.fancybox-close-small').trigger('click');
        order( $('.order___form') );
    })
    $(document).on('click', '.noCreditOrderButton', function(e){
        $('.order___form input[name=credit_accept]').remove();
        $('.fancybox-close-small').trigger('click');
    })

    $(document).on('click', '.yesYurOrderButton', function(e){
        $('.order___form input[name=yur_accept]').remove();
        $('.order___form').append('<input type="hidden" name="yur_accept" value="Y">');
        $('.fancybox-close-small').trigger('click');
        order( $('.order___form') );
    })
    $(document).on('click', '.noYurOrderButton', function(e){
        $('.order___form input[name=yur_accept]').remove();
        $('.fancybox-close-small').trigger('click');
    })

    if( $('.is___seo_page').length > 0 ){
        /////////////////////////////
        var filter_form = $('.smartfilter');
        var form_data = $(filter_form).serialize();
        /////////////////////////////
        $.post(curURL+'?'+form_data+'&ajax=y', {}, function(data){
            if( data ){
                data = data.replace(new RegExp("'",'g'), "\"");
                data = data.replace(new RegExp("\t",'g'), "");
                var result = JSON.parse( data );
                var new_url = result.FILTER_URL;
                new_url = new_url.replace(new RegExp("/filter/clear/apply/",'g'), "/");
                new_url = new_url.replace(new RegExp("/filter/apply/",'g'), "/");
                new_url = rawurldecode( new_url );
                $('input[name=seo_page_url]').val(new_url);
            }
        })
        .fail(function(data, status, xhr){
        })
        .always(function(data, status, xhr){
        })
    }

    // ФИЛЬТР - числовой инпут
    $(document).on('keyup', '.filter___number_input', function(e){
        var input = $(this);
        minus_zero( $(input) );
        /////////////////////////////
        clearInterval(filterInterval);
        filterInterval = setInterval(function(){
            clearInterval(filterInterval);

            /////////////////////////////
            var filter_form = $(input).parents('form');
            var form_data = $(filter_form).serialize();
            /////////////////////////////

            if( $('.p-filters-js').length > 0 ){
                $('.p-filters-js').addClass('js-loading');
            }

            $.post(curURL+'?'+form_data+'&ajax=y', {}, function(data){
                if( data ){
                    data = data.replace(new RegExp("'",'g'), "\"");
                    data = data.replace(new RegExp("\t",'g'), "");
                    var result = JSON.parse( data );
                    var new_url = result.FILTER_URL;
                    new_url = new_url.replace(new RegExp("/filter/clear/apply/",'g'), "/");
                    new_url = new_url.replace(new RegExp("/filter/apply/",'g'), "/");
                    new_url = rawurldecode( new_url );
                    new_url = removeFromRequestURI( new_url, 'pageNumber' );
                    if( $('.is___seo_page').length == 0 ){
                        new_url = addToRequestURI( new_url, 'sort_field', catalogSortField );
                        new_url = addToRequestURI( new_url, 'sort_order', catalogSortOrder );
                        history.replaceState({}, null, new_url);
                    } else {
                        $('input[name=seo_page_url]').val(new_url);
                    }
                    catalogLoad( false, true, true );
                }
            })
            .fail(function(data, status, xhr){})
            .always(function(data, status, xhr){})

        }, 700);
    })


    // ФИЛЬТР - чекбокс
    $(document).on('change', '.filter___number_checkbox', function(e){
        var input = $(this);
        /////////////////////////////
        clearInterval(filterInterval);
        filterInterval = setInterval(function(){
            clearInterval(filterInterval);

            /////////////////////////////
            var filter_form = $(input).parents('form');
            var form_data = $(filter_form).serialize();
            /////////////////////////////

            if( $('.p-filters-js').length > 0 ){
                $('.p-filters-js').addClass('js-loading');
            }

            $.post(curURL+'?'+form_data+'&ajax=y', {}, function(data){
                if( data ){
                    data = data.replace(new RegExp("'",'g'), "\"");
                    data = data.replace(new RegExp("\t",'g'), "");
                    var result = JSON.parse( data );
                    var new_url = result.FILTER_URL;
                    new_url = new_url.replace(new RegExp("/filter/clear/apply/",'g'), "/");
                    new_url = new_url.replace(new RegExp("/filter/apply/",'g'), "/");
                    new_url = rawurldecode( new_url );
                    new_url = removeFromRequestURI( new_url, 'pageNumber' );
                    if( $('.is___seo_page').length == 0 ){
                        new_url = addToRequestURI( new_url, 'sort_field', catalogSortField );
                        new_url = addToRequestURI( new_url, 'sort_order', catalogSortOrder );
                        history.replaceState({}, null, new_url);
                    } else {
                        $('input[name=seo_page_url]').val(new_url);
                    }
                    catalogLoad( false, true, true );
                }
            })
            .fail(function(data, status, xhr){})
            .always(function(data, status, xhr){})
        }, 500);
    })


    // Смена сортировки
    $(document).on('click', '.sortLink', function(e){
        if( !is_process(this) ){
            var sort_field = $(this).attr('sort_field');
            var sort_order = $(this).attr('sort_order');
            if( sort_order == 'asc' ){
                sort_order = 'desc';
            } else {
                sort_order = 'asc';
            }
            var isSearch = $(this).hasClass('is___search')?'Y':'N';
            var isBrand = $(this).hasClass('is___brand')?'Y':'N';
            var isFavorites = $(this).hasClass('is___favorites')?'Y':'N';
            var isBestPrices = $(this).hasClass('is___best_prices')?'Y':'N';
            var isGiftIdeas = $(this).hasClass('is___gift_ideas')?'Y':'N';
            if( isSearch == 'Y' ){
                searchSortChange ( sort_field, sort_order );
            } else {
                catalogSortChange ( sort_field, sort_order, isSearch, isFavorites, isBrand, isBestPrices, isGiftIdeas );
            }

        }
    });


    // установка страницы в пагинации
    $(document).on('click', '.paginationBlock a', function(e){
        e.preventDefault();
        var link = $(this);
        if(
            !is_process(link)
            &&
            !$(link).hasClass('disabled')
            &&
            !$(link).hasClass('active')
        ){

            var title = $('title').html();
            title = title.replace(new RegExp(" - страница [2-9][0-9]*",'g'), "");

            var pageNumber = $(link).data('pagenumber');
            var url = document.URL;
            var new_url = rawurldecode( url );
            if( pageNumber > 1 ){
                new_url = addToRequestURI( new_url, 'PAGEN_1', pageNumber );
                title += ' - страница '+pageNumber;
            } else {
                new_url = removeFromRequestURI( new_url, 'PAGEN_1' );
            }
            history.replaceState({}, null, new_url);
            $('title').html(title);

            postParams = { pageNumber:pageNumber };
            if( $('.paginationBlock').hasClass('is___search') ){
                postParams.isSearch = 'Y';
            }
            if( $('.paginationBlock').hasClass('is___favorites') ){
                postParams.isFavorites = 'Y';
            }
            if( $('.paginationBlock').hasClass('is___brand') ){
                postParams.isBrand = 'Y';
            }
            if( $('.paginationBlock').hasClass('is___best_prices') ){
                postParams.isBestPrices = 'Y';
            }
            if( $('.paginationBlock').hasClass('is___gift_ideas') ){
                postParams.isGiftIdeas = 'Y';
            }
            if( $('.paginationBlock').hasClass('is___complects') ){
                postParams.isComplects = 'Y';
            }

            catalogLoad( postParams, true, false );
        }
    });


    // Подгрузка товаров в каталоге
    $(document).on('click', '.catalogMoreButton', function(e){
        if( !is_process(this) ){
            var stop_ids = [];
            if( $('.catalog___item').length > 0 ){
                $('.catalog___item').each(function(){
                    stop_ids.push( $(this).attr('item_id') );
                })
            }
            var postParams = {
                stop_ids:stop_ids,
            };
            if( $(this).hasClass('is___search') ){
                postParams.isSearch = 'Y';
            }
            if( $(this).hasClass('is___favorites') ){
                postParams.isFavorites = 'Y';
            }
            if( $(this).hasClass('is___brand') ){
                postParams.isBrand = 'Y';
            }
            if( $(this).hasClass('is___best_prices') ){
                postParams.isBestPrices = 'Y';
            }
            if( $(this).hasClass('is___gift_ideas') ){
                postParams.isGiftIdeas = 'Y';
            }
            if( $(this).hasClass('is___complects') ){
                postParams.isComplects = 'Y';
            }
            catalogLoad( postParams );
        }
    })





    // Кнопка Избранное
    $(document).on('click', '.fav___button', function(e){
        var button = $(this);
        var item_id = $(button).attr('item_id');
        var cur_favs = $.cookie('BITRIX_SM_favorites');
        var postParams = { item_id: item_id };
        if( cur_favs == undefined ){
            postParams['cur_favs'] = '';
        } else {
            postParams['cur_favs'] = cur_favs;
        }
        process(true);
        $.post("/ajax/checkFavorites.php", postParams, function(data){
            if (data.status == 'ok'){
                var new_favs = data.new_favs;
                var cookie_favs = [];
                for( key in new_favs ){
                    var val = new_favs[key];
                    cookie_favs.push(val);
                }
                $.cookie(
                    'BITRIX_SM_favorites',
                    cookie_favs.join('|'),
                    { expires: 90, path: '/' }
                );
                setFavorites();

                if( $('.favorites_left_sections_block').length > 0 ){
                    $('.favorites_left_sections_block').replaceWith( data.first_level_sections_html );
                }

            } else if (data.status == 'error'){
                show_message(data.text, 'warning')
            }
        }, 'json')
        .fail(function(data, status, xhr){
            console.log('Ошибка запроса'); //show_message('Ошибка запроса', 'warning');
        })
        .always(function(data, status, xhr){
            process(false);
        })
    })



    // Кнопка В сравнение
    $(document).on('click', '.compare___button', function(e){
        var button = $(this);
        var item_id = $(button).attr('item_id');
        var cur_compares = $.cookie('BITRIX_SM_compares');
        var postParams = { item_id: item_id };
        if( cur_compares == undefined ){
            postParams['cur_compares'] = '';
        } else {
            postParams['cur_compares'] = cur_compares;
        }
        process(true);
        $.post("/ajax/checkCompares.php", postParams, function(data){
            if (data.status == 'ok'){
                var new_compares = data.new_compares;
                var cookie_compares = [];
                for( key in new_compares ){
                    var val = new_compares[key];
                    cookie_compares.push(val);
                }
                $.cookie(
                    'BITRIX_SM_compares',
                    cookie_compares.join('|'),
                    { expires: 90, path: '/' }
                );
                setCompares();
            }
        }, 'json')
        .fail(function(data, status, xhr){
            console.log('Ошибка запроса'); //show_message('Ошибка запроса', 'warning');
        })
        .always(function(data, status, xhr){
            process(false);
        })
    })



    // Удаление из сравнения
    $(document).on('click', '.compare_delete_button', function(e){
        e.preventDefault();
        var button = $(this);
        if( !is_process(this) ){
            var item_id = $(button).attr('item_id');
            var cur_compares = $.cookie('BITRIX_SM_compares');
            var postParams = { item_id: item_id };
            if( cur_compares == undefined ){
                postParams['cur_compares'] = '';
            } else {
                postParams['cur_compares'] = cur_compares;
            }
            postParams['get_compares_html'] = 'Y';
            process(true);
            $.post("/ajax/checkCompares.php", postParams, function(data){
                if (data.status == 'ok'){
                    var new_compares = data.new_compares;
                    var cookie_compares = [];
                    for( key in new_compares ){
                        var val = new_compares[key];
                        cookie_compares.push(val);
                    }
                    $.cookie(
                        'BITRIX_SM_compares',
                        cookie_compares.join('|'),
                        { expires: 90, path: '/' }
                    );

                    $('.compareBlock').replaceWith( data.compares_html );


                }
            }, 'json')
            .fail(function(data, status, xhr){
                console.log('Ошибка запроса'); //show_message('Ошибка запроса', 'warning');
            })
            .always(function(data, status, xhr){
                process(false);
            })
        }
    })




    // Добавление товара в корзину
    $(document).on('click', '.add_to_basket_button', function(e){
        var button = $(this);
        if( !is_process(button) ){
            if( $(this).hasClass('added') ){
                window.location.href = "/basket/";
            } else {
                var item_id = $(button).attr('item_id');
                var postParams = {
                    action: "add_to_basket",
                    item_id: item_id
                };
                if( $(button).hasClass('isInBasket') ){
                    postParams['isInBasket'] = 'Y';
                    $('section.cart__list').addClass('js-loading');
                }
                process(true);
                $.post("/ajax/ajax.php", postParams, function(data){
                    if (data.status == 'ok'){

                        $('basket_small').replaceWith( data.top_basket_html );

                        /////////////////
                        $('.cart-keeper-js').switchClass({
                            switchClassTo: $('.quick-cart-js'),
                            remover: '.quick-cart-close-js',
                            modifiers: {  activeClass: 'quick-cart_opened'  },
                            cssScrollFixed: true
                        });
                        initCustomScrollBar();
                        ////////////////

                        if(
                            $(button).hasClass('isInBasket')
                            &&
                            'basket_html' in data
                            &&
                            $('basketBlock').length > 0
                        ){
                            $('basketBlock').replaceWith( data.basket_html );
                            basketReInit();
                            basketSmallReInit();
                        }

                        activateBasketButtons();

                    } else if (data.status == 'error'){
                        show_message(data.text, 'warning')
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    console.log('Ошибка запроса'); //show_message('Ошибка запроса', 'warning');
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            }
        }
    })



    // Изменение количества в корзине (spin)
    $(document).on('spin', '.basketCntInput', function(event, ui){
        var item_id = $(this).attr('item_id');
        var cur_quantity = $('.basketCntInput[item_id='+item_id+']').val();
        var new_quantity =  ui.value;
        if( new_quantity != cur_quantity ){
            clearInterval(basketCntInterval);
            basketCntInterval = setInterval(function(){
                clearInterval(basketCntInterval);
                var params = {
                    action: "recalc_qt",
                    item_id: item_id,
                    quantity: ui.value,
                };
                basketReload( params );
            }, 500);
        }
    })

    // Изменение количества в корзине (keyup)
    $(document).on('keyup', '.basketCntInput', function(e){
        var input = $(this);
        minus_zero(input);
        new_basket_cnt = $(input).val();
        if( new_basket_cnt.length == 0 ){  new_basket_cnt = 1;  }
        clearInterval(basketCntInterval);
        basketCntInterval = setInterval(function(){
            clearInterval(basketCntInterval);
            var params = {
                action: "recalc_qt",
                item_id: $(input).attr('item_id'),
                quantity: new_basket_cnt
            };
            basketReload( params );
        }, 700);
    })


    // Удаление товара из корзины (малая корзина)
    $(document).on('click', '.smallBasketRemoveButton', function(e){
        if( !is_process(this) ){
            var params = { action: "unset_item", item_id: $(this).attr('item_id') };
            basketReload( params, true );
        }
    })

    // Удаление товара из корзины
    $(document).on('click', '.basketRemoveButton', function(e){
        if( !is_process(this) ){
            var params = { action: "unset_item", item_id: $(this).attr('item_id') };
            basketReload( params );
        }
    })

    $(document).on('click', '.orderBasketItemRemove', function(e){
        if( !is_process(this) ){
            var params = { action: "unset_item", item_id: $(this).data('itemid') };
            orderReload( params );
        }
    })



    // Сохранение купона в корзине
    $(document).on('click', '.saveCouponButton', function(e){
        if( !is_process(this) ){
            var params = { action: "save_coupon", coupon: $('input[name=coupon]').val() };
            basketReload( params );
        }
    })





    // Быстрый заказ
    $(document).on('click', '.buy_1_click_button', function(e){
        var button = $(this);
        if( !is_process(button) ){
            // Форма
            var this_form = $(button).parents('form');
            // Настройка полей для проверки
            var fields = {
                'phone': {  tag: 'input',  name_ru: 'Телефон',  is_phone: true,
                    required: true,  required_error: 'Введите, пожалуйста, Ваш телефон'  }
            }
            // Проверка полей
            var checkResult = checkFields( this_form, fields );
            // Если нет ошибок
            if ( checkResult.errors.length == 0 ){
                var postParams = {
                    action: "quick_order",
                    item_id: $(button).attr('item_id'),
                    quantity: $('input[name=quick_quantity]').val(),
                    phone: checkResult.values.phone
                };
                process(true);
                $.post("/ajax/ajax.php", postParams, function(data){
                    if (data.status == 'ok'){
                        var form = create_form('quick_order_success', 'post', '/order_success/');
                        $(form).append('<input type="hidden" name="order_id" value="'+data.order_id+'">');
                        $(form).submit();
                    } else if (data.status == 'error'){
                        show_message(data.text, 'warning')
                    }
                }, 'json')
                    .fail(function(data, status, xhr){
                        console.log('Ошибка запроса'); //show_message('Ошибка запроса', 'warning');
                    })
                    .always(function(data, status, xhr){
                        process(false);
                    })

            } else {    show_error( this_form, [checkResult.errors[0]], true );    }
        }
    })




    // СТРАНИЦА ЗАКАЗА - Переключение вариантов доставки
    $(document).on('change', '.order___form input[name=ds_xml_id]', function(e){
        orderReload();
    })

    // СТРАНИЦА ЗАКАЗА - Переключение вариантов оплаты
    $(document).on('change', '.order___form input[name=ps_id]', function(e){
        var params = {
            'isPaymentSelect': 'Y'
        };
        orderReload( params );
    })





    // Голосование за отзыв
    $(document).on('click', '.productReviewVotePlus, .productReviewVoteMinus', function(e){
        if( !is_process(this) ){
            var link = $(this);
            var item_id = $(this).attr('item_id');

            if( $(this).hasClass('productReviewVotePlus') ){
                var vote = 'plus';
            } else if( $(this).hasClass('productReviewVoteMinus') ){
                var vote = 'minus';
            }
            var postParams = {
                item_id: item_id,
                vote: vote
            };
            process(true);
            $.post("/ajax/productReviewVote.php", postParams, function(data){
                if (data.status == 'ok'){

                    var vote_parent_block = $(link).parents('div.u-review__voting');
                    var cur_plus_cnt = Number($(vote_parent_block).find('.productReviewVotePlus span.u-review__voting-count').html());
                    var cur_minus_cnt = Number($(vote_parent_block).find('.productReviewVoteMinus span.u-review__voting-count').html());

                    if( data.action == 'minus' ){
                        if( $(link).hasClass('productReviewVotePlus') ){
                            $(link).find('span.u-review__voting-count').html( cur_plus_cnt-1 );
                            $(link).find('span.u-review__voting-count').data( 'count', cur_plus_cnt-1 );
                        } else if( $(link).hasClass('productReviewVoteMinus') ){
                            $(link).find('span.u-review__voting-count').html( cur_minus_cnt-1 );
                            $(link).find('span.u-review__voting-count').data( 'count', cur_minus_cnt-1 );
                        }
                    } else if( data.action == 'plus' ){
                        if( $(link).hasClass('productReviewVotePlus') ){
                            $(link).find('span.u-review__voting-count').html( cur_plus_cnt+1 );
                            $(link).find('span.u-review__voting-count').data( 'count', cur_plus_cnt+1 );
                        } else if( $(link).hasClass('productReviewVoteMinus') ){
                            $(link).find('span.u-review__voting-count').html( cur_minus_cnt+1 );
                            $(link).find('span.u-review__voting-count').data( 'count', cur_minus_cnt+1 );
                        }
                    } else if( data.action == 'plus_minus' ){
                        if( $(link).hasClass('productReviewVotePlus') ){
                            $(link).find('span.u-review__voting-count').html( cur_plus_cnt+1 );
                            $(link).find('span.u-review__voting-count').data( 'count', cur_plus_cnt+1 );
                            $(vote_parent_block).find('.productReviewVoteMinus span.u-review__voting-count').html( cur_minus_cnt-1 );
                            $(vote_parent_block).find('.productReviewVoteMinus span.u-review__voting-count').data( 'count', cur_minus_cnt-1 );
                        } else if( $(link).hasClass('productReviewVoteMinus') ){
                            $(link).find('span.u-review__voting-count').html( cur_minus_cnt+1 );
                            $(link).find('span.u-review__voting-count').data( 'count', cur_minus_cnt+1 );
                            $(vote_parent_block).find('.productReviewVotePlus span.u-review__voting-count').html( cur_plus_cnt-1 );
                            $(vote_parent_block).find('.productReviewVotePlus span.u-review__voting-count').data( 'count', cur_plus_cnt-1 );
                        }
                    }

                } else if (data.status == 'error'){
                    show_message(data.text, 'warning')
                }
            }, 'json')
            .fail(function(data, status, xhr){
                console.log('Ошибка запроса'); //show_message('Ошибка запроса', 'warning');
            })
            .always(function(data, status, xhr){
                process(false);
            })
        }
    })


    // Подгрузка отзывов к товару
    $(document).on('click', '.productReviewsMoreButton', function(e){
        if( !is_process(this) ){
            var postParams = {
                'product_id': $('input[name=product_id]').val(),
                stop_ids: []
            };
            $('.productReviewItem').each(function(){
                var item_id = $(this).attr('item_id');
                postParams['stop_ids'].push(item_id);
            })
            $('.productReviewSubItem').each(function(){
                var item_id = $(this).attr('item_id');
                postParams['stop_ids'].push(item_id);
            })
            process(true);
            $.post("/ajax/productReviewsLoad.php", postParams, function(data){
                if (data.status == 'ok'){

                    $('.productReviewsLoadArea').append(data.reviews_html);

                    var arResult = data.arResult;
                    if( 'SUB_ITEMS' in arResult ){
                        if( Object.keys( arResult['SUB_ITEMS'] ).length > 0 ){
                            for ( key in arResult['SUB_ITEMS'] ){
                                var sub_item = arResult['SUB_ITEMS'][key];
                                var parent_id = sub_item['PROPERTY_LINK_VALUE'];
                                $('.productReviewItem[item_id='+parent_id+'] .productReviewSubItemsArea').append(sub_item['HTML']);
                            }
                        }
                    }

                    if( arResult['SHOW_BUTTON'] == 'Y' ){
                        $('.productReviewsMoreBlock').show();
                    } else {
                        $('.productReviewsMoreBlock').hide();
                    }

                } else if (data.status == 'error'){
                    show_message(data.text, 'warning')
                }
            }, 'json')
            .fail(function(data, status, xhr){
                console.log('Ошибка запроса'); //show_message('Ошибка запроса', 'warning');
            })
            .always(function(data, status, xhr){
                process(false);
            })
        }
    })








    $(document).on('click', '.add-new-review-js', function(e){
        e.preventDefault();
        $('.productReviewAnswerFormBlock').hide();
        var $block = $('.productReviewFormBlock');
        $block.toggle();
        if ($block.is(':visible') && !$(this).is(':animated')) {
            $('html,body').stop().animate({
                scrollTop: $block.offset().top - $('.header').outerHeight() - 15
            }, 300);
        }
    })

    $(document).on('click', '.add-new-review-answer-js', function(e){
        $('.productReviewFormBlock').hide();
        var $block = $('.productReviewAnswerFormBlock');
        var item_id = $(this).attr('item_id');
        $block.find('input[name=review_id]').val(item_id);
        $block.show();
        if ($block.is(':visible') && !$(this).is(':animated')) {
            $('html,body').stop().animate({
                scrollTop: $block.offset().top - $('.header').outerHeight() - 15
            }, 300);
        }
    })












})