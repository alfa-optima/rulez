/* share42.com | 22.08.2016 | (c) Dimox */
(function($){$(function(){$('div.share42init').each(function(idx){var el=$(this),u=el.attr('data-url'),t=el.attr('data-title'),i=el.attr('data-image'),d=el.attr('data-description'),f=el.attr('data-path'),fn=el.attr('data-icons-file'),z=el.attr("data-zero-counter");if(!u)u=location.href;if(!fn)fn='icons.png';if(!z)z=0;if(!f){function path(name){var sc=document.getElementsByTagName('script'),sr=new RegExp('^(.*/|)('+name+')([#?]|$)');for(var p=0,scL=sc.length;p<scL;p++){var m=String(sc[p].src).match(sr);if(m){if(m[1].match(/^((https?|file)\:\/{2,}|\w:[\/\\])/))return m[1];if(m[1].indexOf("/")==0)return m[1];b=document.getElementsByTagName('base');if(b[0]&&b[0].href)return b[0].href+m[1];else return document.location.pathname.match(/(.*[\/\\])/)[0]+m[1];}}return null;}f=path('share42.js');}if(!t)t=document.title;if(!d){var meta=$('meta[name="description"]').attr('content');if(meta!==undefined)d=meta;else d='';}



u=encodeURIComponent(u);
t=encodeURIComponent(t);
t=t.replace(/\'/g,'%27');
i=encodeURIComponent(i);
d=encodeURIComponent(d);
d=d.replace(/\'/g,'%27');
var vkImage='';
if(i!='null'&&i!='')
    vkImage='&image='+i;

var s = [

    {
        href: '"#" data-count="vk" onclick="window.open(\'//vk.com/share.php?url='+u+'&title='+t+vkImage+'&description='+d+'\', \'_blank\', \'scrollbars=0, resizable=1, menubar=0, left=100, top=100, width=550, height=440, toolbar=0, status=0\');return false" title="Поделиться В Контакте"',
        name: 'Vkontakte',
        svg: '<svg class="svg-ico-vkontakte" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 107.585 61.139"><path fill="#4b73a4" d="M104.8 51.2c-2.6-2.6-13.2-13.7-13.2-13.7 -1.6-2.1-2.1-4.7-0.5-6.8 3.2-4.2 9-11.6 11.1-14.8 3.2-4.2 9-13.2 1.1-13.2H85.9c-1.6 0-2.6 1.1-3.7 2.1 0 0-6.8 12.6-9 16.8 -5.8 11.6-10 7.9-10 2.6V5.9c0-3.2-2.6-5.8-5.8-5.8H44.3c-3.7-0.5-6.8 1.6-9.5 4.2 0 0 6.8-1.1 6.8 7.9v13.7c0 2.1-1.6 3.7-3.7 3.7 -1.1 0-2.1-0.5-3.2-1.1 -5.3-7.4-10-15.3-13.2-23.7 -0.5-1.1-2.1-2.1-3.2-2.1H2.6C1.1 2.7 0 3.7 0 5.3c0 0.5 0 0.5 0 1.1 4.7 13.2 25.3 54.3 49 54.3h10c2.1 0 3.7-1.6 3.7-3.7v-5.8c0-2.1 1.6-3.7 3.7-3.7 1.1 0 2.1 0.5 2.6 1.1L80.6 59.6c1.1 1.1 2.6 1.6 3.7 1.6h15.8C109.1 60.7 109.1 55.4 104.8 51.2z"></path></svg>'
    },

    {
        href: '"#" data-count="fb" onclick="window.open(\'//www.facebook.com/sharer/sharer.php?u='+u+'\', \'_blank\', \'scrollbars=0, resizable=1, menubar=0, left=100, top=100, width=550, height=440, toolbar=0, status=0\');return false" title="Поделиться в Facebook"',
        name: 'Facebook',
        svg: '<svg class="svg-ico-facebook" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 123 263.5"><path fill="#3a589e" d="M80.9 85.2V57.9c0-10.4 6.6-12.6 11.5-12.6s29.5 0 29.5 0V-0.1H81.5c-44.8 0-55.2 33.9-55.2 55.2v30.1H0.1v46.5h26.2c0 59.6 0 131.7 0 131.7H81c0 0 0-72.7 0-131.7h37.2l4.9-46.5L80.9 85.2 80.9 85.2z"></path></svg>'

    },

    {
        href: '"#" data-count="twi" onclick="window.open(\'//twitter.com/intent/tweet?text='+t+'&url='+u+'\', \'_blank\', \'scrollbars=0, resizable=1, menubar=0, left=100, top=100, width=550, height=440, toolbar=0, status=0\');return false" title="Добавить в Twitter"',
        name: 'Twitter',
        svg: '<svg class="svg-ico-twitter" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="-368.9 301 240.3 195.3"><path fill="#1da0f2" d="M-128.6 324.1c-8.8 3.9-18.3 6.6-28.3 7.8 10.2-6.1 18-15.8 21.7-27.3 -9.5 5.7-20.1 9.8-31.3 12 -9-9.6-21.8-15.6-36-15.6 -27.2 0-49.3 22.1-49.3 49.3 0 3.9 0.4 7.6 1.3 11.2 -41-2.1-77.3-21.7-101.6-51.5 -4.2 7.3-6.7 15.7-6.7 24.8 0 17.1 8.7 32.2 21.9 41 -8.1-0.3-15.7-2.5-22.3-6.2 0 0.2 0 0.4 0 0.6 0 23.9 17 43.8 39.6 48.3 -4.1 1.1-8.5 1.7-13 1.7 -3.2 0-6.3-0.3-9.3-0.9 6.3 19.6 24.5 33.8 46.1 34.2 -16.9 13.2-38.1 21.1-61.2 21.1 -4 0-7.9-0.2-11.8-0.7 21.8 14 47.7 22.2 75.6 22.2 90.7 0 140.3-75.1 140.3-140.3 0-2.1 0-4.3-0.1-6.4C-143.6 342.7-135.2 334-128.6 324.1z"></path></svg>'

    },

    {
        href: '"#" data-count="odkl" onclick="window.open(\'//ok.ru/dk?st.cmd=addShare&st._surl='+u+'&title='+t+'\', \'_blank\', \'scrollbars=0, resizable=1, menubar=0, left=100, top=100, width=550, height=440, toolbar=0, status=0\');return false" title="Добавить в Одноклассники"',
        name: 'Odnoklassniki',
        svg: '<svg class="svg-ico-odnoklassniki" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 87.1 146.5"><path fill="#f2822e" d="M44 68.4c19.1 0 34.2-15.2 34.2-34.2C78.1 15.2 63 0 44 0 24.9 0 9.8 15.2 9.8 34.2 9.8 53.2 24.9 68.4 44 68.4zM44 17.1c9.3 0 17.1 7.8 17.1 17.1 0 9.3-7.8 17.1-17.1 17.1 -9.3 0-17.1-7.8-17.1-17.1C26.9 24.9 34.7 17.1 44 17.1zM78.2 73.3c-4.9 0-14.7 9.8-34.2 9.8 -19.5 0-29.3-9.8-34.2-9.8C4.4 73.3 0 77.7 0 83c0 4.9 2.9 7.3 4.9 8.3 5.9 3.4 24.4 11.2 24.4 11.2L8.3 128.9c0 0-3.9 4.4-3.9 7.8 0 5.4 4.4 9.8 9.8 9.8 4.9 0 7.3-3.4 7.3-3.4s22-26.4 22-25.9l22 25.9c0 0 2.5 3.4 7.3 3.4 5.4 0 9.8-4.4 9.8-9.8 0-2.9-3.9-7.8-3.9-7.8l-21-26.4c0 0 18.6-7.8 24.4-11.2 2-1.5 4.9-3.4 4.9-8.3C87.9 77.7 83.5 73.3 78.2 73.3z"></path></svg>'
    },

    {
        href: '"#" data-count="gplus" onclick="window.open(\'//plus.google.com/share?url='+u+'\', \'_blank\', \'scrollbars=0, resizable=1, menubar=0, left=100, top=100, width=550, height=440, toolbar=0, status=0\');return false" title="Поделиться в Google+"',
        name: 'Google+',
        svg: '<svg class="svg-ico-google-plus" width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M2.71535 8.4746C1.61769 7.76752 1.06105 6.5643 1.06105 4.8975C1.06105 0.191722 6.19915 0.00133915 6.25071 0H11.3828V0.11115C11.3828 0.658179 10.4503 0.783538 9.7531 0.877275L9.64836 0.891428C9.60677 0.897186 9.55773 0.903486 9.50441 0.910336C9.25388 0.942524 8.90881 0.986859 8.79711 1.04409C10.0963 1.73666 10.3072 2.82829 10.3072 4.45469C10.3072 6.30472 9.58274 7.28342 8.81385 7.97107L8.81211 7.97263C8.3382 8.39707 7.96371 8.73248 7.96371 9.17854C7.96371 9.61689 8.47772 10.0677 9.07275 10.5896L9.07458 10.5912C10.0493 11.4479 11.3828 12.6198 11.3828 14.5883C11.3828 16.6265 10.5061 18.0848 8.77858 18.9238C7.4468 19.5712 6.01257 19.6409 5.45771 19.6409C5.3771 19.6409 5.32195 19.6394 5.29628 19.6388H5.29621H5.29617L5.28117 19.6384C5.28117 19.6384 5.24166 19.6398 5.17403 19.6398C4.30961 19.6398 0 19.4405 0 15.5156C0 11.6582 4.69216 11.3578 6.13086 11.3573L6.16835 11.3578C5.33808 10.2496 5.51016 9.13011 5.51016 9.13011C5.43628 9.13502 5.33094 9.13993 5.19925 9.13993C4.65734 9.13993 3.6137 9.05355 2.71535 8.4746ZM6.0851 17.8299C8.10075 17.6833 9.44325 16.5251 9.34527 14.9554C9.25242 13.477 7.96907 12.3635 6.07439 12.3635C5.95967 12.3635 5.84473 12.3682 5.72867 12.3755C4.7464 12.4481 3.84359 12.8152 3.18584 13.4109C2.53747 13.9984 2.20625 14.7371 2.25379 15.4902C2.35177 17.0577 4.03754 17.9794 6.0851 17.8299ZM8.00522 3.8235C8.50874 5.5943 7.74855 7.44568 6.53707 7.78716C6.39802 7.82622 6.25361 7.84631 6.10809 7.84631C4.99816 7.84631 3.89849 6.72343 3.49117 5.17694C3.26351 4.30783 3.28181 3.54853 3.54295 2.81824C3.79962 2.09979 4.26051 1.61457 4.84059 1.45142C4.98008 1.41191 5.12471 1.39182 5.27001 1.39182C6.61027 1.3916 7.47179 1.94735 8.00522 3.8235ZM16.6521 4.33215V7.68003H20V9.80035H16.6521V13.1482H14.5318V9.80035H11.1839V7.68003H14.5318V4.33215H16.6521Z"></path></svg>'
    }

];

var l='';
for( key in s ){
    var item = s[key];
    l += '<a href="'+ item.href +'" target="_blank" class="article-share__item vkontakte" title="'+ item.name +'">'+ item.svg +'<span>'+ item.name +'</span></a>';
}

el.html(l);



})})})(jQuery);