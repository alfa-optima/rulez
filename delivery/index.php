<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Доставка");
?><p>
	 Мы&nbsp;доставим ваш заказ в&nbsp;любое место Беларуси.
</p>
<p>
 <strong>Доставка курьером до&nbsp;клиента по&nbsp;городу Минску</strong>
</p>
<p>
	 общая стоимость товаров&nbsp;от 100,00&nbsp;руб.&nbsp;и&nbsp;более&nbsp;— доставляется бесплатно, <br>
	 общая стоимость товаров&nbsp;менее 100,00&nbsp;р. составит 5,00&nbsp;руб. <br>
 <i>Цена доставки не&nbsp;зависит от&nbsp;количества заказанных товаров!</i>
</p>
<p>
 <strong>Доставка курьером до&nbsp;клиента по&nbsp;Беларуси</strong>
</p>
<p>
	 Точную стоимость доставки можно узнать на&nbsp;странице понравившегося вам товара.<br>
	 Если вы&nbsp;выбрали более одного товара, то&nbsp;добавьте товары в&nbsp;корзину магазина и&nbsp;выберите нужный способ доставки.
</p>
<p>
 <strong>Условия доставки и&nbsp;получения заказов</strong>
</p>
<p>
	 Доставка осуществляется только для товаров, входящих в&nbsp;заказ. Доставка нескольких товаров для выбора одного из&nbsp;них не&nbsp;производится.&nbsp;
</p>
<p>
	 Доставка по&nbsp;адресу обязывает Покупателя находиться на&nbsp;месте в&nbsp;согласованное время.
</p>
<p>
	 Доставка осуществляется до&nbsp;места, указанного Покупателем, при условии отсутствия предметов, загромождающих проход до&nbsp;места.
</p>
<p>
	 Если в&nbsp;указанном Покупателем месте есть препятствие, мешающие доставки товара или могут вызвать его повреждение, то&nbsp;товары доставляются курьером до&nbsp;ближайшего места с&nbsp;подходящими условиями.&nbsp;
</p>
<p>
	 Заводская упаковка товара не&nbsp;вскрывается до&nbsp;доставки покупателю, если только при заказе покупатель не&nbsp;просит проверить или настроить товар.
</p>
<p>
	 Если вам требуется установка и&nbsp;настройка товара, то&nbsp;все детали необходимо внести при заказе товара на&nbsp;сайте или проинформировать оператора колл-центра.&nbsp;
</p>
<p>
	 Курьер не&nbsp;даёт консультаций по&nbsp;эксплуатации и&nbsp;характеристикам товара, если необходимо, то&nbsp;всю информацию можно получить на&nbsp;сайте или у&nbsp;операторов колл-центра.&nbsp;
</p>
<p>
	 Рекомендуем Вам проверять модель, характеристики, комплектацию товара до&nbsp;оформления гарантийного талона и&nbsp;отъезда курьера. Претензии к&nbsp;внешнему виду и&nbsp;комплектации товара после заполнения гарантийного талона и&nbsp;отъезда курьера не&nbsp;принимаются.
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>