<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Способы оплаты");
?><h3>Вы&nbsp;можете оплатить заказ любым удобным для Вас способом:<br>
 </h3>
<p>
 <strong>1.</strong> Наличными в&nbsp;пунктах вывоза Rulez.by (см. ниже «Порядок оплаты»);
</p>
<p>
 <strong>2.</strong> Наличными экспедитору при доставке заказа;
</p>
<p>
 <strong>3. </strong>Путем банковского перевода (если&nbsp;Вы представитель юридического лица):
</p>
<p>
	 —&nbsp;Необходимо произвести резервирование товара, сформировав заказ через корзину магазина (выбрав способ оплаты «безналичный расчет для юр.лиц»), указать реквизиты вашей организации в&nbsp;соответствующих полях анкеты.
</p>
<p>
	 —&nbsp;Дождаться письма со&nbsp;счетом на&nbsp;ваш email для его оплаты.
</p>
<p>
</p>
<p>
 <strong><i>Резервирование товара!</i></strong>
</p>
<p>
</p>
<p>
 <i>Оплатить счет необходимо в&nbsp;течение срока его действия (срок действия счета указан в&nbsp;счете). При оплате счета после истечения срока его действия, мы&nbsp;не&nbsp;гарантируем наличие товара на&nbsp;складе.</i>
</p>
<p>
 <i>Отгрузка товара происходит после поступления денежных средства на&nbsp;наш расчетный счет.</i>
</p>
<p>
 <i>Чтобы ускорить процесс получения товара, рекомендуем присылать платёжное поручение на&nbsp;нашу электронную почту <a href="mailto:order@rulez.by">order@rulez.by</a></i>
</p>
<p>
</p>
<p>
 <strong>4.</strong> Пластиковыми картами через терминал при доставке заказа курьером;
</p>
<p>
 <i><strong>Важно!</strong> При оформлении заказа через оператора магазина сообщите об&nbsp;оплате банковской картой, если необходим этот вид расчета.</i>
</p>
<p>
</p>
<p>
 <strong>5.</strong> Банковской карточкой через интернет - сервис онлайн-платежей BePaid:
</p>
<p>
	 —&nbsp;Карточками VISA, Visa Electron, MasterCard, Maestro, БЕЛКАРТ, Халва;
</p>
<p>
</p>
<p>
	 —&nbsp;Через систему «Расчет» ЕРИП.
</p>
<p>
 <i><strong>Важно!</strong> При оплате через систему «Расчет» (ЕРИП) вам необходимо будет указать номер заказа. Этот номер будет направлен Вам обратным сообщением при формировании заказа через корзину магазина, если вы&nbsp;позвонили нам, то&nbsp;его сообщит Вам наш менеджер.</i> <br>
 <br>
</p>
<h3>Как оплатить покупку в&nbsp;системе «Расчет» (ЕРИП)?</h3>
<p>
	 Зайдите на&nbsp;сайт или приложение интернет-банка, системы электронных денег, либо используйте инфокиоск или терминал приема наличных денег (если оплата производится деньгами Webmoney, то&nbsp;на&nbsp;сайте wmtransfer.by необходимо нажать на&nbsp;иконку карты&nbsp;РБ с&nbsp;надписью «Любые платежи по&nbsp;Беларуси», найдите в&nbsp;меню пункт, содержащий название системы «Расчет» или ЕРИП, далее из&nbsp;списка выберите по&nbsp;очереди «Интернет-магазины и&nbsp;сервисы», «A-Z ЛАТИНСКИЕ ДОМЕНЫ», «Rulez.by», если платеж осуществляется в&nbsp;кассе банка, сообщите кассиру о&nbsp;необходимости проведения платежа через систему ЕРИП, затем введите в&nbsp;соответствующее поле или сообщите кассиру номер вашего заказа.<br>
 <br>
</p>
<h3>Как оплатить покупку карточкой через систему онлайн-платежей BePaid?</h3>
<p>
	 После выбора способа оплаты "Онлайн-оплата на сайте" и нажатия кнопки «Купить» вы перейдете на специальную защищенную платежную страницу процессинговой системы <a href="https://bepaid.by/">bePaid.</a> На платежной странице будет указан номер заказа и сумма платежа. Для оплаты вам необходимо ввести свои карточные данные и подтвердить платеж, нажав кнопку «Оплатить». Если ваша карта поддерживает технологию 3-D Secure, системой вам будет предложено пройти стандартную одноминутную процедуру проверки владельца карты на странице вашего банка (банка, который выдал вашу карту).
</p>
<p>
 <i>После оплаты наш менеджер свяжется с вами для уточнения деталей по доставке!</i>
</p>
 Обращаем ваше внимание, что после проведения платежа на указанный вами электронный адрес придет подтверждение оплаты.<br>
<img width="500" alt="Образец чека.jpg" src="/upload/medialibrary/c47/c47e735a5db3adcedbd1f6690218af78.jpg" height="500" title="Образец чека.jpg">&nbsp;<br>
 <br>
 Просим вас сохранять данные оплат. Мы принимаем платежи по следующим банковским картам: Visa, Visa Electron, MasterCard, Maestro, Белкарт. Платежи по банковским картам осуществляются через систему электронных платежей <a href="https://bepaid.by/">bePaid</a>. Платежная страница системы bePaid отвечает всем требованиям безопасности передачи данных (PCI DSS Level 1). Все конфиденциальные данные хранятся в зашифрованном виде и максимально устойчивы к взлому. Доступ к авторизационным страницам осуществляется с использованием протокола, обеспечивающего безопасную передачу данных в Интернетe (SSL/TLS). <br>
 <br>
 <i>Важно! </i><i>Возврат денежных средств осуществляется на карту, с которой ранее была произведена оплата. Срок поступления денежных средств на карту от 3 до 30 дней с момента осуществления возврата Продавцом. Условия возврата описаны здесь: <a href="https://rulez.by/vozvrat-tovara/">Условия и форма для возврата</a><a href="https://rulez.by/vozvrat-tovara/">.</a><a href="https://rulez.by/vozvrat-tovara/"></a></i><br>
 <br>
<p>
</p>
<h2>Порядок оплаты</h2>
<p>
	 Оплата наличными и&nbsp;банковскими картами для физических лиц производится как при самовывозе из&nbsp;ПВЗ Rulez, так и&nbsp;через экспедитора при доставке.
</p>
<p>
 <i>Важно! Бывают ситуации, когда в&nbsp;результате непредвиденных и&nbsp;непреднамеренных технических ошибок в&nbsp;каталоге магазина может быть указана заведомо неверная цена.</i>
</p>
<p>
</p>
<ul>
	<li>
	<p>
		 При обнаружении случая неверного указания цены заказанного Клиентом товара, Продавец сообщает об&nbsp;этом Клиенту для подтверждения заказа по&nbsp;исправленной цене. Клиент имеет право отказаться от&nbsp;такого заказа.
	</p>
 </li>
	<li>
	<p>
		 Если Клиент оплатил заказ до&nbsp;момента обнаружения ошибки и&nbsp;отказался от&nbsp;заказа, Продавец обязан вернуть Клиенту уплаченную за&nbsp;заказ сумму денег.
	</p>
 </li>
	<li>
	<p>
		 Если Клиент оплатил заказ до&nbsp;момента обнаружения ошибки и&nbsp;не&nbsp;отказался от&nbsp;заказа, а&nbsp;уплаченная за&nbsp;заказ сумма больше суммы после исправления цены товара, Продавец обязан вернуть Клиенту разницу между ошибочной и&nbsp;исправленной ценой товара.
	</p>
 </li>
	<li>
	<p>
		 Если Клиент оплатил заказ до&nbsp;момента обнаружения ошибки и&nbsp;не&nbsp;отказался от&nbsp;заказа, а&nbsp;уплаченная за&nbsp;заказ сумма меньше суммы после исправления цены товара, Клиент обязан доплатить Продавцу разницу между ошибочной и&nbsp;исправленной ценой товара.<br>
	</p>
 </li>
</ul>
<p>
 <br>
</p>
<h2>Возврат денежных средств при оплате картой или по&nbsp;системе ЕРИП:</h2>
<p>
	 Если оплата товара производилась с использованием пластиковой карты, то возврат денежных средств осуществляется на ту же карту, с которой совершался платеж.
</p>
<p>
	 Если оплата товара производилась по системе <i>«Расчет» (ЕРИП),</i> то денежных средств можно вернуть как наличными, так и на счет в банке.
</p>
<p>
	 Срок поступления денежных средств – от 3 до 30 календарных дней с момента осуществления возврата.
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>