<? require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use AOptima\Project as project;
use AOptima\Tools as tools;

if(
    \Bitrix\Main\Loader::includeModule('aoptima.project')
    &&
    \Bitrix\Main\Loader::includeModule('aoptima.tools')
){

    $arURI = tools\funcs::arURI( tools\funcs::pureURL() );

    // Инфоблоки каталога
    $catalogIblocks = project\catalog::iblocks();
    // Категории 1 уровня каталога
    $catalogFirstLevelCategories = [];
    foreach ( $catalogIblocks as $iblock_id => $iblock ){
        $category = project\catalog::firstLevelCategory($iblock_id);
        $catalogFirstLevelCategories[$category['CODE']] = $category;
    }

    // Проверка наличия SEO-страницы с символьным кодом $arURI[1]
    project\SeopagecategoryTable::checkTable();
    project\SeopageTable::checkTable();
    $seo_page = project\SeopageTable::getByCode($arURI[1]);

    // Каталог
    if( $catalogFirstLevelCategories[$arURI[1]] ){

        include( $_SERVER[ "DOCUMENT_ROOT" ] . "/catalog/catalog.php" );

    } else if( $arURI[1] == 'product' ){

        include( $_SERVER[ "DOCUMENT_ROOT" ] . "/catalog/product.php" );

    } else if( $seo_page ){

        include( $_SERVER[ "DOCUMENT_ROOT" ] . "/catalog/seo_page.php" );

    } else if( $arURI[1] == 'brand' ){

        if( strlen($arURI[2]) > 0 ){

            include( $_SERVER[ "DOCUMENT_ROOT" ] . SITE_TEMPLATE_PATH . "/include/brand.php" );

        } else {

            include( $_SERVER[ "DOCUMENT_ROOT" ] . SITE_TEMPLATE_PATH . "/include/404include.php" );
        }

    } else {

        include( $_SERVER[ "DOCUMENT_ROOT" ] . SITE_TEMPLATE_PATH . "/include/404include.php" );
    }

}